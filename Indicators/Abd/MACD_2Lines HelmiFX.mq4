//+------------------------------------------------------------------+
//|                                               MACD_2Lines.mq4 |
//|                                         Copyright © 2017, HelmiFX. |
//|                                      "www.helmifx.com" 				|
//+------------------------------------------------------------------+
#property copyright "Copyright © 2017, HelmiFX."
#property link "www.helmifx.com"

#property indicator_separate_window 
#property indicator_buffers 4
#property indicator_label1 "MACD_2Lines HelmiFX"

#property indicator_color1 Red
#property indicator_color2 Blue
#property indicator_color3 Red
#property indicator_color4 Green
               

string    indName="MACD_2Lines HelmiFX";

//---- buffers
double ExtMapBufRED[];
double ExtMapBufBlue[];
double ExtMapBufRed[];
double ExtMapBufLime[];

extern double FastEma = 12;
extern double SlowEma = 26;
extern double Signal  = 9;

int FixZeroD=0;

//+------------------------------------------------------------------+
//| Custom indicator initialization function                         |
//+------------------------------------------------------------------+
int init()
  {
   SetIndexStyle(0,DRAW_LINE,STYLE_SOLID,2);
   SetIndexStyle(1,DRAW_LINE,STYLE_SOLID,2);
   SetIndexStyle(2,DRAW_HISTOGRAM,STYLE_SOLID,2);
   SetIndexStyle(3,DRAW_HISTOGRAM,STYLE_SOLID,2);
   IndicatorDigits(Digits+1);

   SetIndexBuffer(0,ExtMapBufRED);
   SetIndexBuffer(1,ExtMapBufBlue);
   SetIndexBuffer(2,ExtMapBufRed);
   SetIndexBuffer(3,ExtMapBufLime);



//   IndicatorDigits(MarketInfo(Symbol(),MODE_DIGITS));	   
   
	
   //----
   return(0);
  } 

//+------------------------------------------------------------------+
//| Custom indicator deinitialization function                       |
//+------------------------------------------------------------------+
int deinit()
  {
//----

//----
   return(0);
  }
//+------------------------------------------------------------------+
//| Custom indicator iteration function                              |
//+------------------------------------------------------------------+
int start()                         
  {
  
   int limit;
   int counted_bars=IndicatorCounted();
   //---- last counted bar will be recounted
   if(counted_bars>0) counted_bars--;
   limit=Bars-counted_bars;
             
//  while(i>=0)                      
//    {
   for(int i=0; i<limit; i++)
    ExtMapBufRED[i] = iMA(NULL,0,FastEma,0,MODE_EMA,PRICE_CLOSE,i)-iMA(NULL,0,SlowEma,0,MODE_EMA,PRICE_CLOSE,i);
  
   for(i=0; i<limit; i++)
    ExtMapBufBlue[i]   = iMAOnArray(ExtMapBufRED,Bars,Signal,0,MODE_SMA,i);
    
   for(i=0; i<limit; i++)
      {
      if (NormalizeDouble(ExtMapBufRED[i] - ExtMapBufBlue[i],Digits) < 0)
         {
         ExtMapBufRed[i]= ExtMapBufRED[i] - ExtMapBufBlue[i];
         }
      else
         {
         ExtMapBufLime[i]= ExtMapBufRED[i] - ExtMapBufBlue[i];
         }
      }
//--------------------------------------------------------------------
   return;                          
  }
//--------------------------------------------------------------------