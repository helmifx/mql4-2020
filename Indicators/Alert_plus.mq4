//+------------------------------------------------------------------+
//|                                                   Alert plus.mq4 |
//|                                                    Naguisa Unada |
//|                                                             None |
//+------------------------------------------------------------------+
#property copyright "Naguisa Unada"
#property link      "None"
#property version   "1.00"
#property strict
#property indicator_chart_window
//--- input parameters
input bool   	alert_ON        	= true;							// Alert switch ON
input bool   	alert_Current		= false;							// Alert on Current bar
input bool   	alert_Message   	= true;							// Alert Message
input bool   	alert_Sound     	= true;							// Alert Sound
input bool   	alert_Email     	= false;							// Send E-mail
input bool		alert_Notify 		= false;          			// Send Push nortification
input string 	Soundfile       	= "alert2.wav";				// Alert sound file
input string	Ind_name 			= "";								// Indicator file name
input int		ArrUp_number		= 0;								// Up Arrow buffer number
input int		ArrDn_number		= 0;								// Down arrow buffer number
//+------------------------------------------------------------------+
//| Custom indicator initialization function                         |
//+------------------------------------------------------------------+
int OnInit()
{
	//--- indicator buffers mapping
	if (ArrDn_number >= 0 && ArrUp_number >= 0 && Ind_name != "")
		Alert("Alert plus starts monitoring arrows of " + Ind_name);
	else
	{
		Alert("Please input indicator name & buffer numbers");
		return(-1);
	}
	//---
	return(INIT_SUCCEEDED);
}
//+------------------------------------------------------------------+
//| Custom indicator iteration function                              |
//+------------------------------------------------------------------+
int OnCalculate(const int rates_total,
                const int prev_calculated,
                const datetime &time[],
                const double &open[],
                const double &high[],
                const double &low[],
                const double &close[],
                const long &tick_volume[],
                const long &volume[],
                const int &spread[])
{
	//---
	int 	 bar_number;
	double Up_arrow, Dn_arrow;
	
	if (alert_ON)
	{
		if (alert_Current)
			bar_number = 0;
		else     
			bar_number = 1;
		
		Up_arrow = iCustom(NULL, PERIOD_CURRENT, Ind_name, ArrUp_number, bar_number);
		Dn_arrow = iCustom(NULL, PERIOD_CURRENT, Ind_name, ArrDn_number, bar_number);

		if (Up_arrow > 0.0 && Up_arrow != EMPTY_VALUE)
			Do_Alert(" Up Trend");
		else if (Dn_arrow > 0.0 && Dn_arrow != EMPTY_VALUE)
			Do_Alert(" Down Trend");
	}
	//--- return value of prev_calculated for next call
	return(rates_total);
}
//+------------------------------------------------------------------+
void Do_Alert(string doWhat)
{
	static string   prev_alert = "nothing";
	static datetime prev_time;
	string message;

	if (prev_alert != doWhat || prev_time != Time[0]) 
	{
		prev_alert = doWhat;
		prev_time  = Time[0];
		message    = StringConcatenate(Symbol(), " at ", TimeToStr(TimeLocal(), TIME_SECONDS), " ", Ind_name, doWhat);
		
		if (alert_Message) 
			Alert(message);
			
		if (alert_Email)   
			SendMail(StringConcatenate(Symbol(), " ", Ind_name), message);
			
		if (alert_Notify)  
			SendNotification(message);
			
		if (alert_Sound)   
			PlaySound(Soundfile);
	}
}
//+------------------------------------------------------------------+
