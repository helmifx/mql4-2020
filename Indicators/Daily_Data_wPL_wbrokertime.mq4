//+------------------------------------------------------------------+
//|                                                    DailyData.mq4 |
//|                                                           mladen |
//+------------------------------------------------------------------+
#property copyright "mladen"
#property link      "mladenfx@gmail.com"

#import "kernel32.dll"
void GetLocalTime(int& localTimeArray[]);
void GetSystemTime(int& systemTimeArray[]);
int  GetTimeZoneInformation(int& localTZInfoArray[]);
bool SystemTimeToTzSpecificLocalTime(int& targetTZinfoArray[], int& systemTimeArray[], int& targetTimeArray[]);
#import

#property indicator_chart_window
#property indicator_buffers 4
#property indicator_color1  DimGray
#property indicator_color2  DimGray
#property indicator_color3  YellowGreen
#property indicator_color4  Tomato
#property indicator_width1  1
#property indicator_width2  1
#property indicator_width3  3
#property indicator_width4  3

//
//
//
//
//

extern color zoneColor      = WhiteSmoke;
extern int   CandleShift    = 8;
extern bool  ShowInfo       = True;
extern bool  ShowSwap       = True;
extern bool  ShowBackground = True;
extern bool  ShowBar        = True;
extern int     FontSize       = 8;
extern string  FontID         = "Arial";
extern color   FontColor      = DarkSlateGray;
extern bool     Display_Times_With_AMPM       = false;      // True=show 12 hour AM/PM time, false=show 24 hour time
extern bool     Broker_MMSS_Is_Gold_Standard  = true ;      // If true, make a correction up to a few seconds/minutes vs. your local CPU clock. FYI, we don't know Broker TZ/DST info but don't care.
extern bool 	 Display_Time_With_Seconds     = true;      // Turn on seconds.
extern bool     Weekend_Test_Mode             = false; // Normally false. True forces Broker_MMSS_Is_Gold_Standard=false as well. Clock updates are >=once-per-broker-second but on weekends the Broker clock freezes

//
//
//
//
//

double DayHigh[];
double DayLow[];
double DayOpen[];
double DayClose[];
double prevCurr =-1;
int    DataPeriod;
int    DataBar;
string indNames = "DailyData";
string objname = "p/l";

double spreadPips; 
string objname1 = "spread";
string objname2 = "brokertime"; 
string objname3 = "gmttime";
string objname4 = "localtime";

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+

int init()
{
   SetIndexBuffer(0, DayHigh);
   SetIndexBuffer(1, DayLow);
   SetIndexBuffer(2, DayClose);
   SetIndexBuffer(3, DayOpen);
      if (ShowBar)
         for (int i=0;i<4;i++)
            {
               SetIndexStyle(i,DRAW_HISTOGRAM);
               SetIndexShift(i,CandleShift);
               SetIndexLabel(i,"Daily data");
            }            
      else  for (i=0;i<4;i++) SetIndexStyle(i,DRAW_NONE);

   //
   //
   //
   //
   //
            
   switch(Period())
   {
      case PERIOD_MN1:
      case PERIOD_W1:  DataPeriod = PERIOD_MN1; break;
      case PERIOD_D1:  DataPeriod = PERIOD_W1;  break;
      default:         DataPeriod = PERIOD_D1;
   }
   prevCurr = -1;
   return(0);
}
int deinit()
{
   for (int counter=-1;counter<17;counter++) ObjectDelete(indNames+counter);
   return(0);
   
   if (ObjectFind(objname) >= 0)     ObjectDelete(objname);
  return(0);
}


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+

int start()
{

int    localTimeArray[4];
   int    systemTimeArray[4];
   int    newyorkTimeArray[4];
   int    londonTimeArray[4];
   int    tokyoTimeArray[4];
   int    sydneyTimeArray[4];
   int    aucklandTimeArray[4];
   int    moscowTimeArray[4];
   int    berlinTimeArray[4];
   int    seattleTimeArray[4];
   
   
   GetLocalTime(localTimeArray);
   datetime localTime = TimeArrayToTime(localTimeArray);

   datetime brokerTime = TimeCurrent();
   datetime brokerCorrection = 0;
   
   if (Broker_MMSS_Is_Gold_Standard && !Weekend_Test_Mode) 
   {
      brokerCorrection = TimeMinute(brokerTime)*60 + TimeSeconds(brokerTime) - TimeMinute(localTime)*60 - TimeSeconds(localTime);
      if (brokerCorrection > 1800) brokerCorrection = brokerCorrection - 3600;
      else if (brokerCorrection < -1800) brokerCorrection = brokerCorrection + 3600;
      //Alert("brokerCorrection seconds: ", brokerCorrection);
   }   
     
   GetSystemTime(systemTimeArray);
   datetime UTC = TimeArrayToTime(systemTimeArray)+brokerCorrection;


double pl = 0;
  for (int i=OrdersTotal()-1; i>=0; i--)    {
    OrderSelect(i, SELECT_BY_POS, MODE_TRADES);
    if (OrderSymbol() != Symbol())   continue;
    pl += pl + OrderProfit() + OrderCommission() + OrderSwap();
  }
  string objtext = "P/L: " + Symbol() + ":   " + DoubleToStr(pl,2);
  
  if (ObjectFind(objname) >= 0)     ObjectDelete(objname);
  ObjectCreate(objname,OBJ_LABEL,0,0,0,0);
  ObjectSet(objname,OBJPROP_CORNER,1);
  ObjectSet(objname,OBJPROP_XDISTANCE,22);
  ObjectSet(objname,OBJPROP_YDISTANCE,100);
  ObjectSetText(objname,objtext,FontSize,FontID,FontColor);
  WindowRedraw();
  
    
  double spread = MarketInfo(Symbol(), MODE_SPREAD); // this is in monetary units.

if ((Digits == 4) || (Digits == 2)) spreadPips = spread;
if ((Digits == 5) || (Digits == 3)) spreadPips = (spread * 0.1); // answer in traditional pips
  
  
  string objtext1 = "Spread: " + Symbol() + ":   " + DoubleToStr(spreadPips,2);
  
 if (ObjectFind(objname1) >= 0)     ObjectDelete(objname1);
  ObjectCreate(objname1,OBJ_LABEL,0,0,0,0);
  ObjectSet(objname1,OBJPROP_CORNER,1);
  ObjectSet(objname1,OBJPROP_XDISTANCE,22);
  ObjectSet(objname1,OBJPROP_YDISTANCE,112);
  ObjectSetText(objname1,objtext1,FontSize,FontID,FontColor);
  WindowRedraw();
  
   objectCreate("8",10,122,"-------------------------------------------",10,NULL); 
  
  string brokers = TimeToString1( TimeCurrent() );
  string objtext2 = "broker:" + brokers;
// Always display Broker Time

if (ObjectFind(objname2) >= 0)     ObjectDelete(objname2);
  ObjectCreate(objname2,OBJ_LABEL,0,0,0,0);
  ObjectSet(objname2,OBJPROP_CORNER,1);
  ObjectSet(objname2,OBJPROP_XDISTANCE,40);
  ObjectSet(objname2,OBJPROP_YDISTANCE,134);
  ObjectSetText(objname2,objtext2,FontSize,FontID,FontColor);
  WindowRedraw();
  
    string UTCs = TimeToString1( UTC );
  string objtext3 = "gmt:" + UTCs;
// Always display Broker Time

if (ObjectFind(objname3) >= 0)     ObjectDelete(objname3);
  ObjectCreate(objname3,OBJ_LABEL,0,0,0,0);
  ObjectSet(objname3,OBJPROP_CORNER,1);
  ObjectSet(objname3,OBJPROP_XDISTANCE,40);
  ObjectSet(objname3,OBJPROP_YDISTANCE,146);
  ObjectSetText(objname3,objtext3,FontSize,FontID,FontColor);
  WindowRedraw();
 
    string locals = TimeToString1( localTime  );
     string objtext4 = "local:" + locals;
// Always display Broker Time

if (ObjectFind(objname4) >= 0)     ObjectDelete(objname4);
  ObjectCreate(objname4,OBJ_LABEL,0,0,0,0);
  ObjectSet(objname4,OBJPROP_CORNER,1);
  ObjectSet(objname4,OBJPROP_XDISTANCE,40);
  ObjectSet(objname4,OBJPROP_YDISTANCE,158);
  ObjectSetText(objname4,objtext4,FontSize,FontID,FontColor);
  WindowRedraw();
  
  
   int    digits   = MarketInfo(Symbol(),MODE_DIGITS);
   double modifier = 1;
   
      if (digits==3 || digits==5) modifier = 10.0;
      else 
            if (ObjectFind(indNames+"16") != -1)
                ObjectDelete(indNames+"16");
      
      //
      //
      //
      //
      //
            
      DataBar  = iBarShift(NULL,DataPeriod,Time[0]);
         double HiDAILY  = iHigh (NULL,DataPeriod,DataBar);
         double LoDAILY  = iLow  (NULL,DataPeriod,DataBar); 
         double current  = iClose(NULL,DataPeriod,DataBar);
         double range    = getRange(DataPeriod)/modifier;
         double change   = (current-iOpen(NULL,DataPeriod,DataBar))/(Point*modifier);
         int    limit;
      
      //
      //
      //
      //
      //

      if (ShowInfo)
      {
          if (ObjectFind(indNames+"1") ==-1) {
                  objectCreate("1",10,10,"-------------------------------------------",10,NULL);  
                  objectCreate("3",60,32,"distance from high : ");
                  objectCreate("4",60,44,"distance from low : " );
                  objectCreate("5",10,56,"-------------------------------------------",10,NULL);
                     if (ShowSwap)
                     {
                        objectCreate("2",60,20,"change/range : ");
                        objectCreate("6",60,64,"swap long : "         );
                        objectCreate("7",60,76,"swap short : "        );
                        objectCreate("8",10,84,"-------------------------------------------",10,NULL); 
                        
                     }
                     else
                     {
                        objectCreate("2",60,20,"range : "       );
                        objectCreate("6",60,64,"distance from open : ");
                        objectCreate("8",10,76,"-------------------------------------------",10,NULL);  
                     }                     
         }

         //
         //
         //
         //
         //

         if (prevCurr != current)
            {
               prevCurr = current;
                  double currFromHi = (HiDAILY-current)/(Point*modifier);
                  double currFromLo = (current-LoDAILY)/(Point*modifier);

                  //
                  //
                  //
                  //
                  //
                  
                     objectCreate("0",128,0,Symbol(),8,"Arial bold",YellowGreen);  
                     if (ShowSwap)
                           objectCreate("10",10,20,DoubleToStr(change,0)+"/"+DoubleToStr(range,0)    ,8,"Arial bold",DarkSlateGray);
                     else  objectCreate("10",10,20,                          DoubleToStr(range,0)    ,8,"Arial bold",DarkSlateGray);
                     objectCreate("11",10,32,DoubleToStr(currFromHi,0)                         ,8,"Arial bold",YellowGreen);
                     objectCreate("12",10,44,DoubleToStr(currFromLo,0)                         ,8,"Arial bold",YellowGreen);
                     if (ShowSwap)
                     {
                        objectCreate("13",10,64,DoubleToStr(MarketInfo(Symbol(),MODE_SWAPLONG),2) ,8,"Arial bold",YellowGreen);
                        objectCreate("14",10,76,DoubleToStr(MarketInfo(Symbol(),MODE_SWAPSHORT),2),8,"Arial bold",YellowGreen);
                     }
                     else
                     {
                        if (change<0)
                              objectCreate("13",10,64,DoubleToStr(change,0) ,8,"Arial bold",Red);
                        else  objectCreate("13",10,64,DoubleToStr(change,0) ,8,"Arial bold",Gold);
                     }
                  string currentValue = DoubleToStr(current,digits);                  
                     objectCreate("15",10, 0,currentValue,8,"Arial bold",YellowGreen);  
                     if (modifier !=1)
                        objectCreate("16",10, 0,StringSubstr(currentValue,StringLen(currentValue)-1),8,"Arial bold",Gold);  
            }   
      }           
      setBarIndicator(change,HiDAILY,LoDAILY,iOpen(NULL,DataPeriod,DataBar),current);
      for(i=0,limit=Bars-1;i<4;i++) SetIndexDrawBegin(i,limit); 
   return(0);
  }

//+------------------------------------------------------------------+
//+------------------------------------------------------------------+  
//
//
//
//
//

void setBarIndicator(double change, double hi,double low, double open, double close)
{
   string name  = indNames+"-1";
   int    shift = 0;
   
   if (change>0) { DayHigh[shift]  = hi;  DayLow[shift] = low; }            
   else          { DayHigh[shift]  = low; DayLow[shift] = hi;  }            
                   DayOpen[shift]  = open;
                   DayClose[shift] = close;

   if (ShowBackground)
      {                         
         if (ObjectFind(name) == -1)
             ObjectCreate(name,OBJ_RECTANGLE,0,0,0);
             ObjectSet(name,OBJPROP_TIME1,iTime(NULL,DataPeriod,DataBar));
             ObjectSet(name,OBJPROP_TIME2,iTime(NULL,         0,0));
             ObjectSet(name,OBJPROP_PRICE1,hi);
             ObjectSet(name,OBJPROP_PRICE2,low);
             ObjectSet(name,OBJPROP_COLOR,zoneColor);
      }             
}

//
//
//
//
//

double getRange(int period)
{
      double range = iHigh(NULL, period, DataBar) - iLow(NULL, period, DataBar);
      return (NormalizeDouble(range/Point,0));
}

//
//
//
//
//

void objectCreate(string name,int x,int y,string text="-",int size=8,
                  string font="Arial",color colour=DimGray,int window = 0)
{
   if (ObjectFind(indNames+name) == -1)
   {
      ObjectCreate(indNames+name,OBJ_LABEL,window,0,0);
         ObjectSet(indNames+name,OBJPROP_CORNER,1);
         ObjectSet(indNames+name,OBJPROP_XDISTANCE,x);
         ObjectSet(indNames+name,OBJPROP_YDISTANCE,y);
   }               
   ObjectSetText(indNames+name,text,size,font,colour);
}

//+------------------------------------------------------------------+
//| Custom functions                                                 |
//+------------------------------------------------------------------+

string TimeToString1( datetime when ) {
   string timeStr;
   int hour = TimeHour( when );
   if ( !Display_Times_With_AMPM ) 
     {
      if (Display_Time_With_Seconds) timeStr = (TimeToStr( when, TIME_MINUTES|TIME_SECONDS));
      else timeStr = (TimeToStr( when, TIME_MINUTES));
     }
   else
     {
      // User wants 12HourTime format with "AM" or "PM".   
      // FYI, if >12:00, subtract 12 hours in seconds which is 12*60*60=43200
      if (Display_Time_With_Seconds)
        {
         if ( hour >  12 || hour == 0) timeStr = TimeToStr( (when - 43200), TIME_MINUTES|TIME_SECONDS);
         else timeStr = TimeToStr( when, TIME_MINUTES|TIME_SECONDS);
         if ( hour >= 12) timeStr = StringConcatenate(timeStr, " PM");
         else timeStr = StringConcatenate(timeStr, " AM");
        }
      else
        {
         if ( hour >  12 || hour == 0) timeStr = TimeToStr( (when - 43200), TIME_MINUTES);
         else timeStr = TimeToStr( when, TIME_MINUTES);
         if ( hour >= 12) timeStr = StringConcatenate(timeStr, " PM");
         else timeStr = StringConcatenate(timeStr, " AM");
        }
     }
   return (timeStr);
} // end of TimeToString1


string FormatDateTime(int nYear,int nMonth,int nDay,int nHour,int nMin,int nSec)
  {
   string sMonth,sDay,sHour,sMin,sSec;
   sMonth=100+nMonth;
   sMonth=StringSubstr(sMonth,1);
   sDay=100+nDay;
   sDay=StringSubstr(sDay,1);
   sHour=100+nHour;
   sHour=StringSubstr(sHour,1);
   sMin=100+nMin;
   sMin=StringSubstr(sMin,1);
   sSec=100+nSec;
   sSec=StringSubstr(sSec,1);
   return(StringConcatenate(nYear,".",sMonth,".",sDay," ",sHour,":",sMin,":",sSec));
} // end of FormatDateTime
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
datetime TimeArrayToTime(int& localTimeArray[])
{
   //---- parse date and time from array
   
   int    nYear,nMonth,nDOW,nDay,nHour,nMin,nSec,nMilliSec;
   //string sMilliSec;

   nYear=localTimeArray[0]&0x0000FFFF;
   nMonth=localTimeArray[0]>>16;
   //nDOW=localTimeArray[1]&0x0000FFFF;
   nDay=localTimeArray[1]>>16;
   nHour=localTimeArray[2]&0x0000FFFF;
   nMin=localTimeArray[2]>>16;
   nSec=localTimeArray[3]&0x0000FFFF;
   nMilliSec=localTimeArray[3]>>16;
   string LocalTimeS = FormatDateTime(nYear,nMonth,nDay,nHour,nMin,nSec);
   datetime localTime = StrToTime( LocalTimeS );
   return(localTime);
} // end of TimeArrayToTime