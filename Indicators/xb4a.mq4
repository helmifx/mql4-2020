
#property copyright "xbox forex"
#property link      "http://community.strangled.net"

#property indicator_chart_window
#property indicator_buffers 2
#property indicator_color1 Red
#property indicator_color2 Blue

double g_ibuf_76[];
double g_ibuf_80[];
double g_ibuf_84[];
extern int XBMAPeriod = 34;

int init() {
   IndicatorBuffers(5);
   SetIndexBuffer(0, g_ibuf_76);
   SetIndexBuffer(1, g_ibuf_80);
   SetIndexBuffer(2, g_ibuf_84);
   SetIndexStyle(0, DRAW_LINE);
   SetIndexStyle(1, DRAW_LINE);
   return (0);
}

int start() {
   for (int li_4 = Bars - 10; li_4 >= 0; li_4--) g_ibuf_84[li_4] = iMA(NULL, 0, XBMAPeriod, 0, MODE_EMA, PRICE_CLOSE, li_4);
   for (int li_8 = Bars - 10; li_8 >= 0; li_8--) {
      g_ibuf_76[li_8] = g_ibuf_84[li_8];
      g_ibuf_80[li_8] = g_ibuf_84[li_8];
      if (g_ibuf_84[li_8] > g_ibuf_84[li_8 + 1]) {
         g_ibuf_76[li_8] = EMPTY_VALUE;
         g_ibuf_80[li_8 + 1] = g_ibuf_84[li_8 + 1];
      } else {
         if (g_ibuf_84[li_8] < g_ibuf_84[li_8 + 1]) {
            g_ibuf_80[li_8] = EMPTY_VALUE;
            g_ibuf_76[li_8 + 1] = g_ibuf_84[li_8 + 1];
         }
      }
   }
   return (0);
}


