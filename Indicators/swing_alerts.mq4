//+------------------------------------------------------------------+
//|                                            TalalAlAjmi.mq4 |
//|                                                     Talal AlAjmi |
//|                                            Talal@vimarkets.com.kw |
//+------------------------------------------------------------------+
#property copyright "Talal AlAjmi"
#property link      "Talal@vimarkets.com.kw"

// Talal@vimarkets.com.kw

#property indicator_chart_window
#property indicator_buffers 4
#property indicator_color1 DarkSlateBlue
#property indicator_color2 Maroon
#property indicator_color3 Yellow
#property indicator_color4 MediumTurquoise
#property indicator_width1 6
#property indicator_width2 6
#property indicator_width3 1
#property indicator_width4 1

//----
input bool   EnableAlerts = true;
input bool   EnableSoundAlerts  = true;
input bool   EnableEmailAlerts  = true;
input bool   EnablePushAlerts=true;
extern int ma_1 = 28;
extern int ma_2 = 144;
extern int method= 3;
extern int price = 0;
extern int signal= 14;
extern int s_method= 0;
extern int signal2 = 70;
extern int s_method2= 0;
extern color color1 = DarkSlateBlue;
extern color color2 = Maroon;
extern color color3 = Yellow;
extern color color4 = MediumTurquoise;

//---- buffers
double ExtMapBuffer1[];
double ExtMapBuffer2[];
double MACD_Map[];
double SignalLine[];
double SignalLine2[];
double SignalMap2[];
double SignalMap[];

int MACrossTriggered,MACurrentDirection,MALastDirection;
bool MAFirstTime=true;
int MADirection;

datetime LastAlertTime = D'01.01.1970';
int LastAlertDirection = 0;

string Text;

//----
int ExtCountedBars=0;
//\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n
int init()
  {
//---- indicators
   IndicatorBuffers(7);

   SetIndexStyle(0,DRAW_HISTOGRAM,0,6,color1);
   SetIndexBuffer(0,ExtMapBuffer1);
   SetIndexStyle(1,DRAW_HISTOGRAM,0,6,color2);
   SetIndexBuffer(1,ExtMapBuffer2);
   SetIndexStyle(2,DRAW_LINE,0,1,color3);
   SetIndexBuffer(2,SignalLine);
   SetIndexStyle(3,DRAW_LINE,0,1,color4);
   SetIndexBuffer(3,SignalLine2);

   SetIndexBuffer(4,MACD_Map);
   SetIndexStyle(4,DRAW_NONE);
   SetIndexBuffer(5,SignalMap);
   SetIndexStyle(5,DRAW_NONE);
   SetIndexBuffer(6,SignalMap2);
   SetIndexStyle(6,DRAW_NONE);

   IndicatorShortName("Talal@vimarkets.com.kw");
   SetIndexLabel(0,"Talal@vimarkets.com.kw");
   SetIndexLabel(1,"Talal@vimarkets.com.kw");
   SetIndexLabel(2,"Talal@vimarkets.com.kw");
   SetIndexLabel(3,"Talal@vimarkets.com.kw");
//----
   SetIndexDrawBegin(0,10);
   SetIndexDrawBegin(1,10);
   SetIndexDrawBegin(2,signal);
   SetIndexDrawBegin(3,signal2);
//---- indicator buffers mapping

   Comment("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n @TalalSAlAjmi");

   return(0);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int start()
  {
   if(Bars<=10) return(0);
   ExtCountedBars=IndicatorCounted();
//---- check for possible errors
   if(ExtCountedBars<0) return(-1);
//---- last counted bar will be recounted
   if(ExtCountedBars>0) ExtCountedBars--;
   int pos=Bars-ExtCountedBars-1;
   while(pos>=0)
     {
      ExtMapBuffer1[pos]=iMA(Symbol(),0,ma_1,0,method,price,pos);
      ExtMapBuffer2[pos]=iMA(Symbol(),0,ma_2,0,method,price,pos);
      MACD_Map[pos]=ExtMapBuffer1[pos]-ExtMapBuffer2[pos];
      pos--;
     }
// ñèãíàëüíûå ëèíèè
   pos=Bars-ExtCountedBars-1;

   while(pos>=0)
     {
      SignalMap[pos]=iMAOnArray(MACD_Map,0,signal,0,s_method,pos);
      SignalLine[pos]=NormalizeDouble(ExtMapBuffer2[pos]+SignalMap[pos],Digits);

      SignalMap2[pos]=iMAOnArray(MACD_Map,0,signal2,0,s_method2,pos);
      SignalLine2[pos]=NormalizeDouble(SignalMap2[pos]+ExtMapBuffer2[pos],Digits);
      pos--;
     }

   MACrossTriggered=MACross(SignalLine[0],SignalLine2[0]);

   if(MACrossTriggered==1 && (Time[0]>LastAlertTime) && LastAlertDirection!=1) // Upwards
     {
      Text="Cross Upwards! At Time :  " +TimeToString(Time[0],TIME_DATE|TIME_MINUTES);
      if(EnableAlerts) Alert(Text);
      if(EnableEmailAlerts) SendMail("Cross Alert!",Text);
      if(EnableSoundAlerts) PlaySound("alert.wav");
      if(EnablePushAlerts) SendNotification(Text);
      LastAlertTime=Time[0];
      LastAlertDirection=1;
     }
   else if(MACrossTriggered==2 && (Time[0]>LastAlertTime) && LastAlertDirection!=2) // Downwards
     {
      Text="Cross Downwards! At Time :  " +TimeToString(Time[0],TIME_DATE|TIME_MINUTES);
      if(EnableAlerts) Alert(Text);
      if(EnableEmailAlerts) SendMail("Cross Alert!",Text);
      if(EnableSoundAlerts) PlaySound("alert.wav");
      if(EnablePushAlerts) SendNotification(Text);
      LastAlertTime=Time[0];
      LastAlertDirection=2;
     }

   return(0);
  }
//+------------------------------------------------------------------+
// Start of  MACross()                            |
//+------------------------------------------------------------------+
int MACross(double yellow,double Red2)
  {
//----
   if(yellow>Red2)
      MACurrentDirection=1;  // line1 above line2
   if(yellow<Red2)
      MACurrentDirection=2;  // line1 below line2
//----
   if(MAFirstTime==true) // Need to check if this is the first time the function is run
     {
      MAFirstTime=false; // Change variable to false
      MALastDirection=MACurrentDirection; // Set new direction
      return (0);
     }

   if(MACurrentDirection!=MALastDirection && MAFirstTime==false) // If not the first time and there is a direction change
     {
      MALastDirection=MACurrentDirection; // Set new direction
      return(MACurrentDirection); // 1 for up, 2 for down
     }
   else
     {
      return(0);  // No direction change
     }
  }
//+------------------------------------------------------------------+
// End of  MACross()                            |
//+------------------------------------------------------------------+
