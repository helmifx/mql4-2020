
#property copyright "xbox forex"
#property link      "http://community.strangled.net"

#property indicator_chart_window
#property indicator_buffers 4
#property indicator_color1 Red
#property indicator_color2 Black
#property indicator_color3 Blue
#property indicator_color4 Black

double g_ibuf_76[];
double g_ibuf_80[];
double g_ibuf_84[];
double g_ibuf_88[];
extern int Rperiod = 18;
extern int LSMA_Period = 20;
int gi_104;
int gi_108;
int gi_112;
int gi_116;
int gi_120;
int gi_124;
int gi_128;
double gd_132;
double gd_140;
double g_ibuf_156[];
double g_ibuf_160[];
double gda_unused_164[];
double g_ibuf_168[];
double gda_unused_172[];

int init() {
  
   IndicatorBuffers(7);
   SetIndexBuffer(0, g_ibuf_76);
   SetIndexStyle(0, DRAW_LINE, STYLE_SOLID, 1);
   SetIndexBuffer(1, g_ibuf_80);
   SetIndexStyle(1, DRAW_NONE);
   SetIndexBuffer(2, g_ibuf_84);
   SetIndexStyle(2, DRAW_LINE, STYLE_SOLID, 1);
   SetIndexBuffer(3, g_ibuf_88);
   SetIndexStyle(3, DRAW_NONE);
   SetIndexBuffer(4, g_ibuf_160);
   SetIndexBuffer(5, g_ibuf_156);
   SetIndexBuffer(6, g_ibuf_168);
   return (0);
}

int start() {

   gi_104 = Bars - Rperiod - 5;
   gi_124 = Rperiod;
   gi_128 = LSMA_Period;
   gi_120 = gi_104 - gi_124 - 1;
   for (gi_108 = gi_120; gi_108 >= 0; gi_108--) {
      g_ibuf_160[1] = 0;
      for (gi_112 = gi_124; gi_112 >= 1; gi_112--) {
         gd_132 = gi_124 + 1;
         gd_132 /= 3.0;
         gd_140 = 0;
         gd_140 = (gi_112 - gd_132) * (Close[gi_124 - gi_112 + gi_108]);
         g_ibuf_160[1] += gd_140;
      }
      g_ibuf_156[gi_108] = 6.0 * g_ibuf_160[1] / (gi_124 * (gi_124 + 1));
      gi_116 = gi_108;
      g_ibuf_168[gi_108] = g_ibuf_156[gi_116 + 1] + 2.0 * (g_ibuf_156[gi_116] - (g_ibuf_156[gi_116 + 1])) / (gi_128 + 1);
      g_ibuf_76[gi_108] = g_ibuf_156[gi_108];
      g_ibuf_80[gi_108] = g_ibuf_168[gi_108];
      g_ibuf_84[gi_108] = g_ibuf_156[gi_108];
      g_ibuf_88[gi_108] = g_ibuf_168[gi_108];
      if (g_ibuf_156[gi_108] < g_ibuf_168[gi_108]) {
         g_ibuf_88[gi_108] = EMPTY_VALUE;
         g_ibuf_84[gi_108] = EMPTY_VALUE;
      }
   }
   return (0);
}


