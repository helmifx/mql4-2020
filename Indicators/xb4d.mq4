
#property copyright "xbox forex"
#property link      "http://community.strangled.net"

#property indicator_separate_window
#property indicator_buffers 3
#property indicator_color1 Black
#property indicator_color2 Blue
#property indicator_color3 Red

extern int period = 27;
double g_ibuf_80[];
double g_ibuf_84[];
double g_ibuf_88[];
string gs_92 = "";
string gs_xb4_ind_100 = "XB4 ind";

int init() {
     SetIndexStyle(0, DRAW_HISTOGRAM);
   SetIndexStyle(1, DRAW_HISTOGRAM);
   SetIndexStyle(2, DRAW_HISTOGRAM);
   IndicatorDigits(Digits + 1);
   SetIndexBuffer(0, g_ibuf_80);
   SetIndexBuffer(1, g_ibuf_84);
   SetIndexBuffer(2, g_ibuf_88);
   IndicatorShortName(gs_xb4_ind_100);
   SetIndexLabel(1, NULL);
   SetIndexLabel(2, NULL);
   return (0);
}

int start() {
   double ld_8;
   double ld_16;
   double ld_80;
   int li_4 = IndicatorCounted();
   double ld_32 = 0;
   double ld_40 = 0;
   double ld_unused_48 = 0;
   double ld_unused_56 = 0;
   double ld_64 = 0;
   double ld_unused_72 = 0;
   double l_low_88 = 0;
   double l_high_96 = 0;
   creataalltext();
   int li_104 = 16777215;
   if (li_4 > 0) li_4--;
   int li_0 = Bars - li_4;
   for (int li_108 = 0; li_108 < li_0; li_108++) {
      l_high_96 = High[iHighest(NULL, 0, MODE_HIGH, period, li_108)];
      l_low_88 = Low[iLowest(NULL, 0, MODE_LOW, period, li_108)];
      ld_80 = (High[li_108] + Low[li_108]) / 2.0;
      ld_32 = 0.66 * ((ld_80 - l_low_88) / (l_high_96 - l_low_88) - 0.5) + 0.67 * ld_40;
      ld_32 = MathMin(MathMax(ld_32, -0.999), 0.999);
      g_ibuf_80[li_108] = MathLog((ld_32 + 1.0) / (1 - ld_32)) / 2.0 + ld_64 / 2.0;
      ld_40 = ld_32;
      ld_64 = g_ibuf_80[li_108];
   }
   bool li_112 = TRUE;
   for (li_108 = li_0 - 2; li_108 >= 0; li_108--) {
      ld_16 = g_ibuf_80[li_108];
      ld_8 = g_ibuf_80[li_108 + 1];
      if ((ld_16 < 0.0 && ld_8 > 0.0) || ld_16 < 0.0) li_112 = FALSE;
      if ((ld_16 > 0.0 && ld_8 < 0.0) || ld_16 > 0.0) li_112 = TRUE;
      if (!li_112) {
         g_ibuf_88[li_108] = ld_16;
         g_ibuf_84[li_108] = 0.0;
         gs_92 = "SHORT";
         li_104 = 65535;
      } else {
         g_ibuf_84[li_108] = ld_16;
         g_ibuf_88[li_108] = 0.0;
         gs_92 = "LONG";
         li_104 = 65280;
      }
   }
   settext("xboxforex_ind", gs_92, 12, li_104, 10, 15);
   return (0);
}


void creataalltext() {
   createtext("xboxforex_ind");
   settext("xboxforex_ind", "", 12, White, 10, 15);
}

void createtext(string a_name_0) {
   ObjectCreate(a_name_0, OBJ_LABEL, WindowFind(gs_xb4_ind_100), 0, 0);
}

void settext(string a_name_0, string a_text_8, int a_fontsize_16, color a_color_20, int a_x_24, int a_y_28) {
   ObjectSet(a_name_0, OBJPROP_XDISTANCE, a_x_24);
   ObjectSet(a_name_0, OBJPROP_YDISTANCE, a_y_28);
   ObjectSetText(a_name_0, a_text_8, a_fontsize_16, "Arial", a_color_20);
}