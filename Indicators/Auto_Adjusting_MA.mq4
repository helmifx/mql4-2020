//+------------------------------------------------------------------+
//|                                            Auto_Adjusting_MA.mq4 |
//|                        Copyright 2017, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2017, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property strict
#property indicator_chart_window
#property indicator_color1 Yellow

extern string MA_Settings = "Settings for MA ";
extern string info = "Enter what you want your moving ";
extern string info2 = "average to be on the 1 Hour Timeframe";
extern int MyMAPeriod = 21;
extern int MAShift = 0;
extern int MAMethod = 1;
extern int MAAppliedTo = 0;


 
double MyMaMultiplier;
double MA_Array[];
int AdjustedMA;
//+------------------------------------------------------------------+
//| Custom indicator initialization function                         |
//+------------------------------------------------------------------+
int init()
  {
  		IndicatorDigits(5);
		int period = Period();
		SetIndexBuffer(0,MA_Array);
		SetIndexStyle (0,DRAW_LINE,STYLE_SOLID,1,indicator_color1);
		SetIndexDrawBegin(0,MyMAPeriod);
		SetIndexLabel(0,"Auto_Adjusting_MA"); 
		
   return(0);
  }
  
  int deinit()
  {
  
  return (0);
  }
  
  
  
	int start()
	{
  	
  	int counted_bars = IndicatorCounted();
  
  	if(counted_bars <0) 
  	{
  		return (-1) ;
  	}
  	
  	if(counted_bars > 0)
  	{
  	counted_bars--;
  	}
  	
  	int uncountedbars = Bars - counted_bars;

	//	Print(uncountedbars);
  			
  	for(int i = 0 ; i < uncountedbars ; i++)
  	{
  	MA_Array[i] = iMA(NULL,0,MyMAPeriod,MAShift,MAMethod,MAAppliedTo,i);
  	}
 	return (0);
 
  }

  
//+------------------------------------------------------------------+
