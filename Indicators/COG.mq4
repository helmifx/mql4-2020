//+------------------------------------------------------------------+
//| Polynominal Regression Analysis.                                 |
//| Sometimes this implementation's visual structure is              |
//| identified as the Center of Gravity, or COG, indicator           |
//| Original Publication may be: https://www.mql5.com/en/code/8417   |
//| "boris", 2008.09.12 06:16                                        |
//+------------------------------------------------------------------+
#property copyright "https://www.forexfactory.com/lukeb"
#property link      "https://www.forexfactory.com/showthread.php?t=651904"
#property version    "1.00"
//*******************************************
#property indicator_chart_window
#property indicator_buffers 5
enum ENUM_COG_BUFFS { COG_CNTR,     COG_HIGH_SQ,   COG_LOW_SQ,    COG_HIGH_STD,   COG_LOW_STD, endCOGBuffs};
color cogColor[] =  { clrRoyalBlue, clrLimeGreen,  clrLimeGreen,  clrGoldenrod,   clrGoldenrod            };
string cogName[]  = { "COG_CNTR",   "COG_HIGH_SQ", "COG_LOW_SQ",  "COG_HIGH_STD", "COG_LOW_STD"           };
ENUM_LINE_STYLE cogStyle = STYLE_SOLID;
struct IndiStruct
 {
   double indiBuff[];
 } cogBuff[endCOGBuffs];
//-----------------------------------
input uint     uniqueID = 732;
input int     bars_back = 125;
input int    twoDefault = 2;
input int      startBar = 0;
input double       kstd = 2.0;
input bool      HideOld = true;  // Hide old bar display as new bars are grown.
//-----------------------
double ai[10][10], b[10], x[10], sx[20];
int    oldestBar;
int threeDefault;
//-------------------
static string shortName = IntegerToString(uniqueID)+"COG";
//*******************************************
//+------------------------------------------------------------------+
//| Custom indicator initialization function                         |
//+------------------------------------------------------------------+
int OnInit()
 {
   int initSuccess = INIT_SUCCEEDED;
   IndicatorShortName(shortName);
   if( Bars<(bars_back+2) )
    {
      initSuccess = INIT_FAILED;
      Print("Insufficient History, ", IntegerToString(Bars)," bars found, ",IntegerToString(bars_back+2)," bars required.");
    }
   else
    {
      for( ENUM_COG_BUFFS buffNum = 0; buffNum < endCOGBuffs; buffNum++)
       {
         setIndiParms(cogBuff[buffNum], cogColor[buffNum], cogName[buffNum], cogStyle, buffNum);  
       }
      oldestBar = bars_back;
      threeDefault = twoDefault + 1;
      initializeSX_ary(sx, oldestBar, startBar, threeDefault);
    }
   //
   return(INIT_SUCCEEDED);
 }
void setIndiParms(IndiStruct& buffStruct, const color& indiColor, const string& indiName,  const ENUM_LINE_STYLE& indiStyle, const int buffNum)
 {
   const static int width = 1;
   SetIndexBuffer(buffNum, buffStruct.indiBuff);
   SetIndexLabel(buffNum, indiName);
   SetIndexStyle(buffNum,DRAW_LINE, indiStyle,width,indiColor);
   SetIndexEmptyValue(buffNum, EMPTY_VALUE);
 }
//+------------------------------------------------------------------+
//| Expert Exit Function                                             |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
 {
   //
 }
//+------------------------------------------------------------------+
//| Timer function                                                   |
//+------------------------------------------------------------------+
void OnTimer()
 {
   //
 }
//+------------------------------------------------------------------+
//| ChartEvent function                                              |
//+------------------------------------------------------------------+
void OnChartEvent(const int id, const long &lparam, const double &dparam, const string &sparam)
 {
   //
 }
//+------------------------------------------------------------------+
//| Custom indicator iteration function                              |
//+------------------------------------------------------------------+
int OnCalculate(const int rates_total, const int prev_calculated, const datetime &time[], const double &open[], const double &high[],
                const double &low[], const double &close[], const long &tick_volume[], const long &volume[], const int &spread[])
 {
   setSYX_b_Value(b,oldestBar, startBar, threeDefault);
   loadAI_ary(ai,sx, threeDefault);
   //===============Gauss========================================================================================================
   calculateGause(ai,b,threeDefault);
   //
   loadX_ary(x, b, ai, threeDefault);
   //===========================================================================================================================
   loadCOG_Center( cogBuff[COG_CNTR].indiBuff, x, startBar, oldestBar, twoDefault);
   //-----------------------------------Std-----------------------------------------------------------------------------------
   double sq = getSQ(startBar, oldestBar, cogBuff[COG_CNTR].indiBuff, kstd);
   double std = iStdDev(NULL, 0, oldestBar, MODE_SMA, 0, PRICE_CLOSE, startBar) * kstd;
   loadSideBands(startBar, oldestBar, sq, std);
   //----------------------------------------------------------------------------------------------------------------------------
   if( HideOld )
    {
      SetIndexDrawBegin(0, Bars - oldestBar - 1);
      SetIndexDrawBegin(1, Bars - oldestBar - 1);
      SetIndexDrawBegin(2, Bars - oldestBar - 1);
      SetIndexDrawBegin(3, Bars - oldestBar - 1);
      SetIndexDrawBegin(4, Bars - oldestBar - 1); 
    }
   return(rates_total);
 }
//+------------------------------------------------------------------+
//| Indicator Routines                                               |
//+------------------------------------------------------------------+
void initializeSX_ary(double& ary_SX[], const int& oldBar, const int& firstBar, const int& calcLimit)
 { // Nothing varies in this after program initialization - run once in Init.
   ary_SX[1] = oldBar + 1;
   for(int outer = 1; outer <= ((calcLimit*2) - 2); outer++)         // calclimit is three by default
    {
      double total = 0.0;
      for(int inner = firstBar; inner <= firstBar + oldBar; inner++)    // startBar is 0 by default; oldestBar is 125 by default
       {
         total += MathPow(inner, outer);
       }
      ary_SX[outer + 1] = total;
    }
 }
//+------------------------------------------------------------------+
void setSYX_b_Value(double& ary_b[], const int& oldBar, const int& firstBar, const int& calcLimit)
 {
   for(int outer = 1; outer <= calcLimit; outer++)
    {
      double total = 0.00000;
      for(int inner = firstBar; inner <= firstBar + oldBar; inner++)   // startBar is 0 by default; oldestBar is 125 by default
       {
         if(outer == 1)
            total += Close[inner];
         else
            total += Close[inner] * MathPow(inner, outer - 1);
       }
      ary_b[outer] = total;
    }
 }
//+------------------------------------------------------------------+
void loadAI_ary(double& ary_AI[][],double& ary_SX[], const int& calcLimit)
 {
   for(int outer = 1; outer <= calcLimit; outer++)
    {
      for(int inner = 1; inner <= calcLimit; inner++)
       {
         int position = inner + outer - 1;
         ary_AI[inner][outer] = ary_SX[position];
       }
    }
 }
//+------------------------------------------------------------------+
void calculateGause(double& ary_ai[][],double& ary_b[],const int& calcLimit)
 {
   for(int outer = 1; outer <= calcLimit - 1; outer++)
    {
      int ai_idx = findAI_idx(ary_ai, outer, calcLimit);
      if(ai_idx == 0)
       {
         return;
       }
      if(ai_idx != outer)
       {
         tradePositions(ary_ai,ary_b, ai_idx, outer, calcLimit);
       }
      loadGause(ary_ai, ary_b, outer, calcLimit);
    }
 }
int findAI_idx(double& ary_ai[][], const int& outer, const int& calcLimit)
 {
   int ai_idx = 0; double largestVal = 0.0;
   for(int inner = outer; inner <= calcLimit; inner++)
    {
      if(MathAbs(ai[inner][outer]) > largestVal)
       {
         largestVal = MathAbs(ary_ai[inner][outer]);
         ai_idx = inner;
       }
    }
   return(ai_idx);
 }
void tradePositions(double& ary_ai[][], double& ary_b[], const int& ai_idx, const int& outer, const int& calcLimit)
 {
   double outerVal;
   for(int idx = 1; idx <= calcLimit; idx++)
    {
      outerVal = ary_ai[outer][idx];
      ary_ai[outer][idx] = ary_ai[ai_idx][idx];
      ary_ai[ai_idx][idx] = outerVal;
    }
   outerVal  = b[outer];
   b[outer]  = b[ai_idx];
   b[ai_idx] = outerVal;
 }
void loadGause(double& ary_ai[][], double& ary_b[], const int& outer, const int& calcLimit)
 {
   for(int range = outer + 1; range <= calcLimit; range++)
     {
      double aiQuotient = ary_ai[range][outer] / ary_ai[outer][outer];
      for(int idx = 1; idx <= calcLimit; idx++)
       {
         if(idx == outer)
            ary_ai[range][idx] = 0;
         else
            ary_ai[range][idx] = ary_ai[range][idx] - aiQuotient * ary_ai[outer][idx];
       }
      ary_b[range] = ary_b[range] - aiQuotient * ary_b[outer];
     }
 }
//+------------------------------------------------------------------+
void loadX_ary( double& x_ary[], double& b_ary[], double& ai_ary[][], const int& calcLimit)
 {
   x[calcLimit] = b[calcLimit] / ai[calcLimit][calcLimit];
   for(int outer = calcLimit - 1; outer >= 1; outer--)
    {
      double total = 0;
      for(int inner = 1; inner <= calcLimit - outer; inner++)
       {
         total        = total + ai_ary[outer][outer + inner] * x_ary[outer + inner];
         x_ary[outer] = (1 / ai_ary[outer][outer]) * (b_ary[outer] - total);
       }
    }
 }
//+------------------------------------------------------------------+
void loadCOG_Center( double& cogCenter[], double& x_ary[], const int& firstBar, const int& oldBar, const int&calcLimit)
 {
   for(int outer = firstBar; outer <= firstBar + oldBar; outer++)  // startBar is 0 by default; oldestBar is 125 by default
    {
      double total = 0;
      for(int inner = firstBar+1; inner <= calcLimit; inner++)
       {
         total += x_ary[inner + firstBar+1] * MathPow(outer, inner);
       }
      cogCenter[outer] = x_ary[firstBar+1] + total;
    } 
 }
//+------------------------------------------------------------------+
double getSQ( const int& firstBar, const int& oldBar, double& cogCntr[], const double& sqMultiplier)
 {
   double squares = 0.0;
   for(int idx = firstBar; idx <= firstBar + oldBar; idx++)  // startBar is 0 by default; oldestBar is 125 by default
    {
      squares += MathPow(Close[idx] - cogCntr[idx], 2);  // why to the 2nd power, is it related to sqMultiplier being 2?
    }
   squares = MathSqrt(squares / (oldBar + 1)) * sqMultiplier;
   return(squares);
 }
//+------------------------------------------------------------------+
void loadSideBands( const int& firstBar, const int& oldBar, const double& square, const double& deviation)
 {
   for(int idx = firstBar; idx <= firstBar + oldBar; idx++)  // startBar is 0 by default; oldestBar is 125 by default
    {
      cogBuff[COG_HIGH_SQ].indiBuff[idx]  = cogBuff[COG_CNTR].indiBuff[idx] + square;
      cogBuff[COG_LOW_SQ].indiBuff[idx]   = cogBuff[COG_CNTR].indiBuff[idx] - square;
      cogBuff[COG_HIGH_STD].indiBuff[idx] = cogBuff[COG_CNTR].indiBuff[idx] + deviation;
      cogBuff[COG_LOW_STD].indiBuff[idx]  = cogBuff[COG_CNTR].indiBuff[idx] - deviation;
    }
 }
//+------------------------------------------------------------------+
//| End Indicator Routines                                           |
//+------------------------------------------------------------------+
