#property  copyright "ANG3110@latchess.com"
//-------------ang_Amp_ZZ---------------
#property indicator_chart_window
#property indicator_buffers 5
#property indicator_color1 DimGray
#property indicator_color2 DimGray 
#property indicator_color3 Blue
#property indicator_color4 Red
#property indicator_color5 Green

//--------------------------------
extern int cbars=1000;
extern int from=0;
extern int Length=10;
//---------------------------------
double ha[],la[],zz[],un[],dn[];
double hi,li,hm,lm,di,j;
int f,f1,ai,bi,aii,bii,f0,aibar,bibar;
//----------------------------------------------

int init()  {   
   SetIndexStyle(0,DRAW_LINE,STYLE_SOLID,2);   
   SetIndexBuffer(0,ha);   
   
   SetIndexStyle(1,DRAW_LINE,STYLE_SOLID,2);   
   SetIndexBuffer(1,la);
   
   SetIndexStyle(2,DRAW_SECTION,STYLE_SOLID,2);
   SetIndexBuffer(2,zz);
   SetIndexEmptyValue(2,0);
   
   SetIndexStyle(3,DRAW_ARROW,STYLE_SOLID,2);
   SetIndexBuffer(3,un);
   SetIndexEmptyValue(3,0);
   
   SetIndexStyle(4,DRAW_ARROW,STYLE_SOLID,2);
   SetIndexBuffer(4,dn);
   SetIndexEmptyValue(4,0);
   SetIndexArrow(4,234);
   SetIndexArrow(3,233);
   return(0);  
}

//================================
int start()  {   
   ArrayInitialize(zz,0);
   ArrayInitialize(ha,0);
   ArrayInitialize(la,0);
   
   int shift,Swing,Swing_n,uzl,i,zu,zd,mv;
   double LL,HH,BH,BL,NH,NL; 
   double Uzel[10000][3]; 
   string text;
// loop from first bar to current bar (with shift=0) 
   Swing_n=0;Swing=0;uzl=0; 
   
   int barn = from + cbars;
   BH =High[barn];BL=Low[barn];zu=barn;zd=barn; 

   for(shift=barn;shift>=from;shift--) { 
      LL=10000000;HH=-100000000; 
      for(i=shift+Length;i>=shift+1;i--) { 
         if (Low[i]< LL) {LL=Low[i];} 
         if (High[i]>HH) {HH=High[i];} 
         } 
      if (Low[shift]<LL && High[shift]>HH){ 
         Swing=2; 
         if (Swing_n==1) {zu=shift+1;} 
         if (Swing_n==-1) {zd=shift+1;} 
         }
      else{ 
         if (Low[shift]<LL) {Swing=-1;} 
         if (High[shift]>HH) {Swing=1;} 
         } 
      if (Swing!=Swing_n && Swing_n!=0) { 
      if (Swing==2) {
         Swing=-Swing_n;BH = High[shift];BL = Low[shift]; 
         } 
      uzl=uzl+1; 
      if(Swing==1) {
         Uzel[uzl][1]=zd;
         Uzel[uzl][2]=BL;
         } 
      if(Swing==-1) {
         Uzel[uzl][1]=zu;
         Uzel[uzl][2]=BH; 
         } 
      BH = High[shift];
      BL = Low[shift]; 
      } 

   if (Swing==1) { 
      if(High[shift]>=BH) {
         BH=High[shift];zu=shift;
         }
      } 
   if(Swing==-1) {
      if(Low[shift]<=BL) {BL=Low[shift]; zd=shift;}} 
      Swing_n=Swing; 
      } 
   
   for(i=1;i<=uzl;i++) { 
      //text=DoubleToStr(Uzel[i][1],0);
      //text=;
      mv=StrToInteger(DoubleToStr(Uzel[i][1],0));
      zz[mv]=Uzel[i][2];
      } 
   //--------------------------------
   int dir = 0;
   for(i=barn; i>=from; i--){ 
      dn[i] = 0;
      un[i] = 0;
      ha[i]=zz[i];
      la[i]=1000000;
      for(int n=i+100; n>i; n--) {
         if(zz[n]>ha[i]) ha[i] = zz[n];
         if(zz[n]!=0 && la[i]>zz[n]) la[i]=zz[n];
         }
      //*   
      if(MathAbs(Close[i]-Open[i])>MathAbs(Close[i+1]-Open[i+1])){
         if(Close[i]<la[i] && Open[i]>la[i]){
            if(dir!=1){
               dn[i] = la[i];
               //dir = 1;
               }
            }
         else if(Close[i]>ha[i] && Open[i]<ha[i]){
            if(dir!=-1){
               un[i] = ha[i];
               //dir = -1;
               }
            }
         }
      //*/
      }
   

//------------------------------------
return(0);  
}