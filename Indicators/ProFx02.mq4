
#property indicator_chart_window
#property indicator_buffers 4
#property indicator_color1 Gray
#property indicator_color2 Green
#property indicator_color3 Gray
#property indicator_color4 Green


int g_ma_method_76 = MODE_EMA;
int g_period_80 = 15;
double g_ibuf_84[];
double g_ibuf_88[];
double g_ibuf_92[];
double g_ibuf_96[];
int gi_100 = 0;

int init() {

   SetIndexStyle(0, DRAW_HISTOGRAM, STYLE_SOLID, 1);
   SetIndexBuffer(0, g_ibuf_84);
   SetIndexStyle(1, DRAW_HISTOGRAM, STYLE_SOLID, 1);
   SetIndexBuffer(1, g_ibuf_88);
   SetIndexStyle(2, DRAW_HISTOGRAM, STYLE_SOLID, 3);
   SetIndexBuffer(2, g_ibuf_92);
   SetIndexStyle(3, DRAW_HISTOGRAM, STYLE_SOLID, 3);
   SetIndexBuffer(3, g_ibuf_96);
   SetIndexDrawBegin(0, 5);
   SetIndexBuffer(0, g_ibuf_84);
   SetIndexBuffer(1, g_ibuf_88);
   SetIndexBuffer(2, g_ibuf_92);
   SetIndexBuffer(3, g_ibuf_96);
   return (0);
}

int deinit() {

   return (0);
}

int start() {
   double l_ima_4;
   double l_ima_12;
   double l_ima_20;
   double l_ima_28;
   double ld_36;
   double ld_44;
   double ld_52;
   double ld_60;

   if (Bars <= 10) return (0);
   gi_100 = IndicatorCounted();
   if (gi_100 < 0) return (-1);
   if (gi_100 > 0) gi_100--;
   for (int li_68 = Bars - gi_100 - 1; li_68 >= 0; li_68--) {
      l_ima_4 = iMA(NULL, 0, g_period_80, 0, g_ma_method_76, PRICE_CLOSE, li_68);
      l_ima_12 = iMA(NULL, 0, g_period_80, 0, g_ma_method_76, PRICE_LOW, li_68);
      l_ima_20 = iMA(NULL, 0, g_period_80, 0, g_ma_method_76, PRICE_OPEN, li_68);
      l_ima_28 = iMA(NULL, 0, g_period_80, 0, g_ma_method_76, PRICE_HIGH, li_68);
      ld_36 = (g_ibuf_92[li_68 + 1] + (g_ibuf_96[li_68 + 1])) / 2.0;
      ld_60 = (l_ima_4 + l_ima_28 + l_ima_20 + l_ima_12) / 4.0;
      ld_44 = MathMax(l_ima_28, MathMax(ld_36, ld_60));
      ld_52 = MathMin(l_ima_20, MathMin(ld_36, ld_60));
      if (ld_36 < ld_60) {
         g_ibuf_84[li_68] = ld_52;
         g_ibuf_88[li_68] = ld_44;
      } else {
         g_ibuf_84[li_68] = ld_44;
         g_ibuf_88[li_68] = ld_52;
      }
      g_ibuf_92[li_68] = ld_36;
      g_ibuf_96[li_68] = ld_60;
   }
   return (0);
}