//+------------------------------------------------------------------+
//|                                                          Try.mq4 |
//|                        Copyright 2017, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property indicator_chart_window
#property indicator_buffers 4
#property indicator_color1 Magenta
#property indicator_color2 Red
#property indicator_color3 Gold
#property indicator_color4 Aqua
//--- input parameters
extern int       Fast_Period=5;
extern int       Slow_Period=20;
//--- buffers
double Fast_Above[], Fast_Below[], Slow_Below[], Slow_Above[];
//+------------------------------------------------------------------+
//| Custom indicator initialization function                         |
//+------------------------------------------------------------------+
int init()
  {
//---- indicators
   SetIndexBuffer(0, Fast_Below);
   SetIndexBuffer(1, Fast_Above);
   SetIndexBuffer(2, Slow_Above);
   SetIndexBuffer(3, Slow_Below);
   SetIndexStyle(0, DRAW_LINE, STYLE_SOLID, 3);
   SetIndexStyle(1, DRAW_LINE, STYLE_SOLID, 3);
   SetIndexStyle(2, DRAW_LINE, STYLE_SOLID, 3);
   SetIndexStyle(3, DRAW_LINE, STYLE_SOLID, 3);
   SetIndexLabel(0, "Fast Below");
   SetIndexLabel(1, "Fast Above");
   SetIndexLabel(2, "Slow Below");
   SetIndexLabel(3, "Slow Above");
   
//----
   return(0);
  }
//+------------------------------------------------------------------+
//| Custom indicator deinitialization function                       |
//+------------------------------------------------------------------+
int deinit()
  {
   return(0);
  }
//+------------------------------------------------------------------+
//| Custom indicator iteration function                              |
//+------------------------------------------------------------------+
int start()
  {
   int pos, counted_bars=IndicatorCounted();
   double fast, slow;
//----
   for (pos = 0; pos <= Bars - counted_bars - 1; pos ++)
     {
      fast = iMA (Symbol(),Period(), Fast_Period, 0 ,MODE_SMA, PRICE_CLOSE, pos);
      slow = iMA (Symbol(),Period(), Slow_Period, 0 ,MODE_SMA, PRICE_CLOSE, pos); 
      if (fast >= slow)
        {
        Fast_Above[pos] = fast;
        Fast_Below[pos] = EMPTY_VALUE; // EMPTY_VALUE // ==>> thanks to dabbler for questioning these 2 EMPTY_VALUEs ...
        Slow_Below[pos] = slow;                // ==>> ... if you use this 2 EMPTY_VALUE, ...
        Slow_Above[pos] = EMPTY_VALUE; // EMPTY_VALUE // ==>> ... the 2 lines will not visibly crossed. Try it for fun
        }
        else
        {
        Fast_Above[pos] = EMPTY_VALUE;
        Fast_Below[pos] = fast;
        Slow_Below[pos] = EMPTY_VALUE;
        Slow_Above[pos] = slow;
        }
     }
//----
   return(0);
  }
//+------------------------------------------------------------------+