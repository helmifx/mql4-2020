//+------------------------------------------------------------------+
//|                                        Renko to CSV for backtest |
//|               Copyright � 2015, Mateo Duque, ekud_87@hotmail.com |
//+------------------------------------------------------------------+
#property copyright "Copyright � 2015, Mateo Duque"
#property link      "ekud_87@hotmail.com"

#property indicator_chart_window


extern   string   PARAMETROS  =  "*****PARAMETROS *****";
extern   bool     REPLACE_VOL =  false;


ulong    TIEMPO[];
double   ABRE[]; 
double   ALTO[];
double   BAJO[];
double   CIERRA[];
int      VOLUMEN[];
 
int      LIMITE = 0;      

bool PRIMER_USO = false;

//+------------------------------------------------------------------+
//+------------------------------------------------------------------+

int init()  
{
   
   PRIMER_USO = false;
   
   return(0);
}


//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
int start() 
{
   if (PRIMER_USO == false)
   {
      int counted_bars=IndicatorCounted();
      int limit;
            
      PRIMER_USO = true;
      
      if (counted_bars<0) return(-1);
      if (counted_bars>0) counted_bars--;
      limit=Bars-1;
      if(counted_bars>=1) limit=Bars-counted_bars-1;
      if (limit<0) limit=0;
      
      LIMITE = limit;
      
      OBTENER_DATOS(); 
      CORREGIR_ERRORES();  
      GUARDAR_CSV();  
   }
      
   return(0);
}


//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
void OBTENER_DATOS()
{
   int i;
   
   ArrayResize(TIEMPO,LIMITE);
   ArrayResize(ABRE,LIMITE);
   ArrayResize(ALTO,LIMITE);
   ArrayResize(BAJO,LIMITE);
   ArrayResize(CIERRA,LIMITE);
   ArrayResize(VOLUMEN,LIMITE);
   
   if (REPLACE_VOL== true)
   {
      for (i=LIMITE;i>=1;i--)   
      {
         TIEMPO[i] = Time[i];
         ABRE[i] = Open[i];
         ALTO[i] = High[i];
         BAJO[i] = Low[i];
         CIERRA[i] = Close[i];
         VOLUMEN[i+1] = Time[i-1]-Time[i];
      }
   }
   else
   {
      for (i=LIMITE;i>=1;i--)   
      {
         TIEMPO[i] = Time[i];
         ABRE[i] = Open[i];
         ALTO[i] = High[i];
         BAJO[i] = Low[i];
         CIERRA[i] = Close[i];
         VOLUMEN[i] = Volume[i];
      }
   }
   
}


//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
void CORREGIR_ERRORES()
{
   int i;
   
   MqlDateTime VELA_1;
   MqlDateTime VELA_2;
   
   for (i=(LIMITE-1);i>=1;i--)   
   {
      TimeToStruct(TIEMPO[i],VELA_1);
      TimeToStruct(TIEMPO[i-1],VELA_2);
      
      VELA_1.sec = 0;
      VELA_2.sec = 0;
      
      TIEMPO[i] = StructToTime(VELA_1);
      TIEMPO[i-1] = StructToTime(VELA_2);
      
      if(TIEMPO[i]>=TIEMPO[i-1])
      {
         TIEMPO[i-1] = TIEMPO[i] + 60;
      }
      
   }   
   
}


//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
int GUARDAR_CSV()
{   
   int i;
   string INFO = "";
   int filehandle = FileOpen("Renko_Backtest_"+Symbol()+".csv",FILE_WRITE|FILE_CSV,",");
    
   if(filehandle!=INVALID_HANDLE) 
   {   
      for (i=(LIMITE-1);i>=1;i--)   
      {         
         FileWrite(filehandle,
                   TimeToStr(TIEMPO[i],TIME_DATE),
                   TimeToStr(TIEMPO[i],TIME_MINUTES),
                   DoubleToStr(ABRE[i],Digits),
                   DoubleToStr(ALTO[i],Digits),
                   DoubleToStr(BAJO[i],Digits),
                   DoubleToStr(CIERRA[i],Digits),
                   IntegerToString(VOLUMEN[i],5)
                   );         
      }
   } 
      
   FileClose(filehandle);    
      
   return(1);
 
}