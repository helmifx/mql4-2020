
#property copyright "xbox forex"
#property link      "http://community.strangled.net"

#property indicator_separate_window
#property indicator_minimum 0.0
#property indicator_maximum 100.0
#property indicator_buffers 2
#property indicator_color1 Blue
#property indicator_color2 Red

extern int K = 5;
extern int D = 3;
extern int Slowing = 3;
extern bool Alert_Over = FALSE;
extern bool Alert_Cross = FALSE;
extern double Overbuy_Level = 80.0;
extern double Oversell_Level = 20.0;
extern double Crossover_Gap = 0.0;
extern int Shift_Bars = 0;
extern int Bars_Count = 0;
extern int MA_Mode = 0;
double g_ibuf_132[];
double g_ibuf_136[];
string gs_140 = "";
string gs_148 = "XB4 Stochastic";
int gi_156;
int gi_160;

int init() {
  
   IndicatorBuffers(2);
   SetIndexStyle(0, DRAW_LINE, STYLE_SOLID, 1);
   SetIndexDrawBegin(0, K + Slowing);
   SetIndexBuffer(0, g_ibuf_132);
   SetIndexLabel(0, "K");
   SetIndexStyle(1, DRAW_LINE, STYLE_DOT, 1);
   SetIndexDrawBegin(1, K + D + Slowing);
   SetIndexBuffer(1, g_ibuf_136);
   SetIndexLabel(1, "D");
   SetLevelValue(0, Oversell_Level);
   SetLevelValue(1, Overbuy_Level);
   SetLevelValue(2, 50);
   IndicatorShortName(gs_148);
   return (0);
}

int start() {
   int li_0;
   int l_shift_4;
   int li_8 = IndicatorCounted();
   if (li_8 > 0) li_8--;
   if (Bars_Count > 0 && Bars_Count <= Bars) li_0 = Bars_Count - li_8;
   else li_0 = Bars - li_8;
   creataalltext();
   int li_12 = 16777215;
   while (li_0 >= 0) {
      l_shift_4 = li_0 + Shift_Bars;
      g_ibuf_132[li_0] = iStochastic(Symbol(), Period(), K, D, Slowing, MA_Mode, 0, MODE_MAIN, l_shift_4);
      g_ibuf_136[li_0] = iStochastic(Symbol(), Period(), K, D, Slowing, MA_Mode, 0, MODE_SIGNAL, l_shift_4);
      li_0--;
   }
   gs_140 = "";
   if (g_ibuf_132[0] >= Overbuy_Level) {
      gs_140 = "OVERBOUGHT";
      if (Alert_Over == TRUE && gi_156 != 1) {
         gi_156 = 1;
         Alert("xStoch Overbought K: ", Symbol(), " ", g_ibuf_132[0]);
      }
   } else {
      if (g_ibuf_132[0] <= Oversell_Level) {
         gs_140 = "OVERSOLD";
         if (Alert_Over == TRUE && gi_156 != -1) {
            gi_156 = -1;
            Alert("xStoch Oversold K: ", Symbol(), " ", g_ibuf_132[0]);
         }
      }
   }
   if (g_ibuf_132[1] < g_ibuf_136[0] + Crossover_Gap && g_ibuf_132[0] >= g_ibuf_136[0] + Crossover_Gap) {
      if (g_ibuf_132[0] >= Oversell_Level && g_ibuf_132[0] <= Overbuy_Level) gs_140 = "LONG";
      if (Alert_Cross == TRUE && gi_160 != 1) {
         gi_160 = 1;
         Alert("xStoch crossed - Direction Long ", Symbol());
      }
   } else {
      if (g_ibuf_132[1] > g_ibuf_136[0] - Crossover_Gap && g_ibuf_132[0] <= g_ibuf_136[0] - Crossover_Gap) {
         if (g_ibuf_132[0] >= Oversell_Level && g_ibuf_132[0] <= Overbuy_Level) gs_140 = "SHORT";
         if (Alert_Cross == TRUE && gi_160 != -1) {
            gi_160 = -1;
            Alert("xStoch crossed - Direction Short ", Symbol());
         }
      }
   }
   if (gs_140 == "OVERSOLD" || gs_140 == "SHORT") li_12 = 255;
   if (gs_140 == "OVERBOUGHT" || gs_140 == "LONG") li_12 = 65280;
   settext("xboxforex_stoch", gs_140, 12, li_12, 10, 35);
   return (0);
}

void creataalltext() {
   createtext("xboxforex_stoch");
   settext("xboxforex_stoch", " ", 12, White, 10, 35);
}

void createtext(string a_name_0) {
   ObjectCreate(a_name_0, OBJ_LABEL, WindowFind(gs_148), 0, 0);
}

void settext(string a_name_0, string a_text_8, int a_fontsize_16, color a_color_20, int a_x_24, int a_y_28) {
   ObjectSet(a_name_0, OBJPROP_XDISTANCE, a_x_24);
   ObjectSet(a_name_0, OBJPROP_YDISTANCE, a_y_28);
   ObjectSetText(a_name_0, a_text_8, a_fontsize_16, "Arial", a_color_20);
}


