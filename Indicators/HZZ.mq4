//+------------------------------------------------------------------+
//|                                                         2HZZ.mq4 |
//|                                                           Candid |
//|                                         http://candid.110mb.com/ |
//+------------------------------------------------------------------+
#property copyright "Candid"
#property link      "http://candid.110mb.com/"

#property indicator_chart_window
#property indicator_buffers 1
#property indicator_color1 Red
#property indicator_width1 2

extern int H = 33;
extern int MinBars = 0;
extern bool SaveData = true;

double dH;
bool UpZ;
double CurMax,CurMin;
int CurMaxBar,CurMinBar;
double Top,Bot;
double SumR,SumT,SumTD;
int ZCnt;
int h = -1;
string FileName;

int PreBars;
datetime BarTime;
int StartPos;
int pos;

double HZZ[];
//+------------------------------------------------------------------+
//| Custom indicator initialization function                         |
//+------------------------------------------------------------------+
int init() {
  if (SaveData) FileName = "HZZ.txt";

  SetIndexBuffer(0,HZZ);
  SetIndexStyle(0,DRAW_SECTION,EMPTY,2);
  SetIndexEmptyValue(0,0.0);

  return(0);
}
//+------------------------------------------------------------------+
//| Custom indicator deinitialization function                       |
//+------------------------------------------------------------------+
int deinit() {
Comment("");
  if (SaveData) FileClose(h);
  return(0);
}
//+------------------------------------------------------------------+
//| Custom indicator reset function                                  |
//+------------------------------------------------------------------+
int Reset() {
  if (SaveData) {
    FileClose(h);
    h = FileOpen(FileName,FILE_CSV|FILE_WRITE,';');
  }
  if (MinBars==0) StartPos = Bars-1;
  else StartPos = MinBars;
  PreBars = 0;
  BarTime = 0;
  dH = H*Point;
  UpZ = true;
  CurMaxBar = 1;
  CurMinBar = 1;
  CurMax = High[StartPos];
  CurMin = Low[StartPos];
  Top = CurMin+dH;
  Bot = CurMax-dH;
  ZCnt = 0;
  SumR = 0.0;
  SumT = 0.0;
  SumTD = 0.0;
  StartPos++;
  return(StartPos);
}
//+------------------------------------------------------------------+
//| Custom indicator iteration function                              |
//+------------------------------------------------------------------+
int start() {
  if (Bars == PreBars) return(0);
  if (Bars < MinBars) {
    Alert(": ������������ ����� �� �������");
    return(0);
  }  
  if (Bars-PreBars == 1 && BarTime==Time[1]) StartPos = 1;
  else StartPos = Reset();
  PreBars = Bars;  
  BarTime=Time[0];
  for (pos=StartPos;pos>0;pos--) {
    HZZ[pos] = 0;
    if (UpZ) {
      if (High[pos]>CurMax) {
        CurMax = High[pos];
        CurMaxBar = Bars-pos;
        Bot = CurMax-dH;
      } else {
        if (Low[pos]<Bot) {
          ZCnt++;
          if (ZCnt > 3) {
            SumR += CurMax-CurMin;
            SumT += CurMaxBar-CurMinBar;
            SumTD += Bars-pos-CurMaxBar;
            if (SaveData) FileWrite(h,Time[Bars-CurMaxBar],NormalizeDouble(CurMax,Digits));
          }
          UpZ = false;
          HZZ[Bars-CurMaxBar]=CurMax;
          CurMinBar = Bars-pos;
          CurMin = Low[pos];
          Top = CurMin+dH;
        }
      }
    }  else {
      if (Low[pos]<CurMin) {
        CurMinBar = Bars-pos;
        CurMin = Low[pos];
        Top = CurMin+dH;
      } else {
        if (High[pos]>Top) {
          ZCnt++;
          if (ZCnt > 3) {
            SumR += CurMax-CurMin;
            SumT += CurMinBar-CurMaxBar;
            SumTD += Bars-pos-CurMinBar;
            if (SaveData) FileWrite(h,Time[Bars-CurMinBar],NormalizeDouble(CurMin,Digits));
          }
          UpZ = true;
          HZZ[Bars-CurMinBar]=CurMin;
          CurMaxBar = Bars-pos;
          CurMax = High[pos];
          Bot = CurMax-dH;
        }
      }
    }
  }  //  pos=StartPos;pos>0;pos--) 
  if (ZCnt > 3) {
    int NZ = ZCnt-3;
    Comment("������� ������: ",SumR/NZ,", ������� ������������: ",SumT/NZ,", ������� ��������: ",SumTD/NZ);  
  }
  return(0);
}
//+------------------------------------------------------------------+