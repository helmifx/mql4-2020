
#property copyright "xbox forex"
#property link      "http://community.strangled.net"

#property indicator_chart_window
#property indicator_buffers 2
#property indicator_color1 Lime
#property indicator_color2 Yellow

double g_ibuf_76[];
double g_ibuf_80[];
extern int FasterMA = 18;
extern int SlowerMA = 34;
extern bool SoundON = FALSE;
double g_time_96;
double gd_104;
double gd_112;
string gs_xb4a_120 = "xb4a";
string gs_xb4b_128 = "xb4b";

int init() {
   SetIndexStyle(0, DRAW_ARROW);
   SetIndexArrow(0, 233);
   SetIndexBuffer(0, g_ibuf_76);
   SetIndexStyle(1, DRAW_ARROW);
   SetIndexArrow(1, 234);
   SetIndexBuffer(1, g_ibuf_80);
   return (0);
}

int deinit() {
   return (0);
}

int start() {
   double l_icustom_12;
   double l_icustom_20;
   double l_icustom_28;
   double l_icustom_36;
   double l_icustom_44;
   double l_icustom_52;
   int li_60 = IndicatorCounted();
   if (li_60 < 0) return (-1);
   if (li_60 > 0) li_60--;
   int li_0 = Bars - li_60;
   for (int l_index_4 = 0; l_index_4 <= li_0; l_index_4++) {
      l_icustom_12 = iCustom(NULL, 0, gs_xb4b_128, FasterMA, 0, l_index_4);
      l_icustom_28 = iCustom(NULL, 0, gs_xb4b_128, FasterMA, 0, l_index_4 + 1);
      l_icustom_44 = iCustom(NULL, 0, gs_xb4b_128, FasterMA, 0, l_index_4 - 1);
      l_icustom_20 = iCustom(NULL, 0, gs_xb4a_120, SlowerMA, 2, l_index_4);
      l_icustom_36 = iCustom(NULL, 0, gs_xb4a_120, SlowerMA, 2, l_index_4 + 1);
      l_icustom_52 = iCustom(NULL, 0, gs_xb4a_120, SlowerMA, 2, l_index_4 - 1);
      if (l_icustom_12 > l_icustom_20 && l_icustom_28 < l_icustom_36 && l_icustom_44 > l_icustom_52) g_ibuf_76[l_index_4] = (l_icustom_20 + l_icustom_12) / 2.0;
      else
         if (l_icustom_12 < l_icustom_20 && l_icustom_28 > l_icustom_36 && l_icustom_44 < l_icustom_52) g_ibuf_80[l_index_4] = (l_icustom_20 + l_icustom_12) / 2.0;
      if (SoundON == TRUE && l_index_4 == 1 && g_ibuf_76[l_index_4] > g_ibuf_80[l_index_4] && g_time_96 != Time[0]) {
         Alert("Fast MA ", gd_104, " Crossed down Main MA", gd_112, " Price going Down on ", Symbol(), " ", Period());
         g_time_96 = Time[0];
      }
      if (SoundON == TRUE && l_index_4 == 1 && g_ibuf_76[l_index_4] < g_ibuf_80[l_index_4] && g_time_96 != Time[0]) {
         Alert("Fast MA ", gd_104, " Crossed up Main MA", gd_112, " Price going Up on ", Symbol(), " ", Period());
         g_time_96 = Time[0];
      }
   }
   return (0);
}