//+------------------------------------------------------------------+
//|                                                 RSI+MA+LABEL.mq4 |
//|                      Copyright © 2004, MetaQuotes Software Corp. |
//|                                       http://www.metaquotes.net/ |
//|                                           Modified LHC 28.01.2009|
//+------------------------------------------------------------------+
#property copyright "Copyright © 2004, MetaQuotes Software Corp."
#property link      "http://www.metaquotes.net/"

#property indicator_separate_window

#property indicator_buffers 2
#property indicator_minimum 0
#property indicator_maximum 100
#property indicator_color1 DodgerBlue
#property indicator_color2 Red
#property indicator_style2 STYLE_SOLID
#property indicator_level1 30
#property indicator_level2 70
#property indicator_levelcolor DimGray



//--- input parameters
extern int RSIPeriod=21; // RSI Period
extern ENUM_APPLIED_PRICE RSIAppliedPrice = 0; // RSI Applied Price
extern string sep=""; // ----------
extern int RSIMAPeriod= 10; // RSI MA Period
extern ENUM_MA_METHOD RSIMAMethod=0; //RSI MA Method

//--- buffers
double RSIBuffer[];
double MAofRSIBuffer[];

color tColor;
color tColor1;
color tColor3;
color tColor4;
//+------------------------------------------------------------------+
//| Custom indicator initialization function                         |
//+------------------------------------------------------------------+
int init()
  {

//--- 2 buffers used for counting.
   IndicatorBuffers(2);
   SetIndexBuffer(0,RSIBuffer);
   SetIndexBuffer(1,MAofRSIBuffer);

//--- indicator line
   SetIndexDrawBegin(0,RSIPeriod);
   SetIndexDrawBegin(1,RSIPeriod);

//--- name for DataWindow and indicator subwindow label
   IndicatorShortName("RSI MA("+RSIPeriod+")");
   SetIndexLabel(0,"RSI("+RSIPeriod+")");
   SetIndexLabel(1,"MA("+RSIMAPeriod+")");
   return(0);
  }
//+------------------------------------------------------------------+
//| Relative Strength Index                                          |
//+------------------------------------------------------------------+
int start()
  {
   int i;
   int limit;
   int counted_bars=IndicatorCounted();

//---- check for possible errors
   if(counted_bars<0) return(-1);

//---- last counted bar will be recounted
   if(counted_bars>0) counted_bars--;
   limit=Bars-counted_bars;

//--- main loops 1 and 2
   for(i=0; i<limit; i++)
     {
      RSIBuffer[i]=iRSI(Symbol(),0,RSIPeriod,RSIAppliedPrice,i);
     }

   for(i=0; i<limit; i++)
     {
      MAofRSIBuffer[i]=iMAOnArray(RSIBuffer,0,RSIMAPeriod,0,RSIMAMethod,i);
     }

   return(0);
  }
//+------------------------------------------------------------------+
