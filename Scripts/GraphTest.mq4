//+------------------------------------------------------------------+
//|                                                    GraphTest.mq4 |
//|                        Copyright 2017, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2017, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property strict

datetime              time1=0;           // first point time 
double                price1=0;          // first point price 
datetime              time2=0;           // second point time 
double                price2=0;
//+------------------------------------------------------------------+
//| Script program start function                                    |
//+------------------------------------------------------------------+
void OnStart()
  {
  /*
   //ObjectCreate("Label_Obj_MACD", OBJ_ARROW_BUY, 0, 0, 0);
  // ObjectSet("Label_Obj_MACD", OBJPROP_CORNER, 1);    
   //ObjectSet("Label_Obj_MACD", OBJPROP_XDISTANCE, 100);
  // ObjectSet("Label_Obj_MACD", OBJPROP_YDISTANCE, 15);
   
  //	ChartSetInteger(ChartID(),CHART_FOREGROUND,0,false);

	if(!time1) 
      time1=TimeCurrent(); 
//--- if the first point's price is not set, it will have Bid value 
   if(!price1) 
      price1=SymbolInfoDouble(Symbol(),SYMBOL_BID); 
//--- if the second point's time is not set, it is located 9 bars left from the second one 
   if(!time2) 
     { 
      //--- array for receiving the open time of the last 10 bars 
      datetime temp[10]; 
      CopyTime(Symbol(),Period(),time1,10,temp); 
      //--- set the second point 9 bars left from the first one 
      time2=temp[0]; 
     } 
//--- if the second point's price is not set, move it 300 points lower than the first one 
   if(!price2) 
      price2=Close[10];

	
	if(ObjectCreate(ChartID(),"obj_rect",OBJ_RECTANGLE,0,time1,price1,time2,price2))
	{
		Comment(__FUNCTION__, 
            ": failed to create a rectangle! Error code = ",GetLastError()); 
           

	}else
	{
			Comment(__FUNCTION__, 
            ": failed to create a rectangle! Error code = ",GetLastError()); 
	}
	ObjectSetInteger(ChartID(),"obj_rect",OBJPROP_COLOR,Red); 
	 ObjectSetInteger(ChartID(),"obj_rect",OBJPROP_SELECTABLE,True); 
   //ObjectSetInteger(ChartID(),"obj_rect",OBJPROP_SELECTED,True); 
	//ObjectSetInteger(ChartID(),"obj_rect",OBJPROP_XDISTANCE,100);
	//ObjectSetInteger(ChartID(),"obj_rect",OBJPROP_YDISTANCE,100);
	//ObjectSetInteger(ChartID(),"obj_rect",OBJPROP_BACK,true);
	//ObjectSetInteger(ChartID(),"obj_rect",OBJPROP_XSIZE,300);
	//ObjectSetInteger(ChartID(),"obj_rect",OBJPROP_YSIZE,100);
   
   //double value = iAO(NULL,0,1);
   //Comment(value);
 Print ("1. GMT = ", TimeToString(TimeGMT()));
   Print ("2. Local Time (EDT) = ", TimeToString(TimeLocal()));
   Print ("3. Daylight Saving Adjustment: ", TimeDaylightSavings()/3600);
   Print ("4. GMTOffset for TimeLocal = ", TimeGMTOffset()/3600);
   Print ("5. Server Time (EEST) = ", TimeToString(TimeCurrent()));
   
   int local = (TimeLocal() - TimeGMT()) / 3600;
   int server = MathRound((TimeCurrent() - TimeGMT()) / 3600.0);
   string prt = "TimeLocal is ";
   if (local > 0)
      prt = StringConcatenate(prt, "GMT+", MathAbs(local));
   else if (local < 0)
      prt = StringConcatenate(prt, "GMT-", MathAbs(local));
   else
      prt = StringConcatenate(prt, "GMT");
      
   prt = StringConcatenate(prt, " and TimeCurrent is ");
   if (server > 0)
      prt = StringConcatenate(prt, "GMT+", MathAbs(server));
   else if (local < 0)
      prt = StringConcatenate(prt, "GMT-", MathAbs(server));
   else
      prt = StringConcatenate(prt, "GMT");
    
   Print (prt);
*/

double jaw_val=iAlligator(NULL,0,13,8,8,5,5,3,MODE_SMMA,PRICE_MEDIAN,MODE_GATORJAW,1);
Comment(NormalizeDouble(jaw_val,Digits));
  }
//+------------------------------------------------------------------+
