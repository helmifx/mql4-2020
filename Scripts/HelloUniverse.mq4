//+------------------------------------------------------------------+
//|                                                HelloUniverse.mq4 |
//|                        Copyright 2017, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2017, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property strict
//+------------------------------------------------------------------+
//| Script program start function                                    |
//+------------------------------------------------------------------+
void OnStart()
  {
	double spread = MarketInfo(Symbol(),MODE_SPREAD);
	double minlot = MarketInfo(Symbol(),MODE_MINLOT);
	double maxlot = MarketInfo(Symbol(),MODE_MAXLOT);
	double step = MarketInfo(Symbol(),MODE_LOTSTEP);
	double margin = MarketInfo(Symbol(),MODE_MARGINREQUIRED);
	
	//ask helmi why we used "/10" with the spread value the market gave us.  is it because the spread in pips , 
	//or because its in points and we need to get the value in pips with it.
	
   int info = MessageBox(IntegerToString(Period()) + " M\n"+ Symbol() +"\nSpread "+DoubleToString(spread/10,1) + 
   "\nMinLot "+DoubleToString(minlot,1) +"\nMaxLot "+DoubleToString(maxlot,1)+
   "\nSpreadStep "+DoubleToString(step,1) +"\nMargin "+DoubleToString(margin,1)    ,"INFORMATION");
   
  }
  
  
  
  void OnDeinit(const int reason)
  {
  
  Print("Deinit was called");
  
  }
//+------------------------------------------------------------------+
