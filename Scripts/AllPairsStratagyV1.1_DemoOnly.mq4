//+------------------------------------------------------------------+
//|                                         Abd_AllPairsStratagy.mq4 |
//|                   							   Copyright 2017 HelmiFX..|
//|                                         		  "www.helmifx.com"  |
//|Abd Loulou
//+------------------------------------------------------------------+


//+------------------------------------------------------------------+
//									 استراتيجية اتجاه لجميع الأزواج			    | 
//			Abd Loulou																	|	
//+------------------------------------------------------------------+

//+-----------------------------------------------------------------------------------------+
/*																										:شروط الدخول *
يشكل خطين الموفنج الأكسبونيشال 28 و 18 قناة يتداول السعر فوقها أو تحتها, حيث تواجد السعر
 تحت الخطين يدل على الهبوط وتواجد السعر فوق الخطين يدل على الصعود 
 , وسوف يشكل خطين لينيرويتد 12 و 5 نقطة دخول سواء في البيع أو الشراء .
 
 
 																										:شراء
    ستكون نقطة الدخول عندما يقطع خطين الموفنج 12 و5 لينرويتد خطين الموفنج إكسبونيشال 28 و 18 بالاتجاه الصاعدويكون مؤشر
			 فوق مستوى 50 RSI																																							
														
 																										:بيع
 																										
 																											
 																										
 																										
 																										
//+-----------------------------------------------------------------------------------------+
---------------------------------------------------------------------------------------------
//+-----------------------------------------------------------------------------------------+

																										شروط الخروج *




ADDED
TBD
//+-----------------------------------------------------------------------------------------+
*/

#property copyright "Copyright 2017 HelmiFX."
#property link      "www.helmifx.com"
#property strict
#property description "Euro Stratagy : Expert that implements on EURUSD and EURJPY on 15M timeframe"

#property description "Find us on  :\n"
#property description "Facbook : HelmiFx"
#property description "Twitter : @HelmiForex"  
#property description "Youtube : HelmiForex" 
#property icon "photo.ico"
#include <Arrays\ArrayInt.mqh>
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
enum ENUM_STOPLOSSMETHOD
  {
   Swing=0,// Swing 
   SLFixedPips=1//Fixed Pips
  };
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
enum ENUM_TAKEPROFITMOETHOD
  {
   MultiplyStopLoss=0,// Multiply Stop Loss
   TPFixedPips=1 // Fixed Pips
  };
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
enum ENUM_PERIODS
  {
   M1=1,M5=5,M15=15,M30=30,H1=60,H4=240,D1=1440,W1=10080,MN=43200
  };
//+------------------------------------------------------------------+
//| Setup                                               
//+------------------------------------------------------------------+
extern string  Header1="**************************************************************************************"; // ----------Trading Rules -----------
extern int     EMAFast=18;         //Exponantial MA Fast
extern int     EMASlow= 28;         //Exponantial MA Slow
extern int     LWMAFast= 5;         //Linear Weighted MA Fast
extern int     LWMASlow=12;          //Linear Weighted MA Slow
extern bool    CloseOnMAs=false;
extern int MaxPositionsAllowed=1;  // Max Positions Allowed

extern string  Header2="**************************************************************************************"; //----------Lots -----------
extern double  Lot=1;
extern bool    MultiplyLot=false;
extern double  LotMultiplier=2;
extern bool    isPositionSizing=true;
extern string  Note1="*******************************************"; // PositionSizing Settings if used
extern double Risk=0.5;         //Risk in Percentage
extern double incrementFactor=0.1;  // increment factor on risk after each position.
extern string  Header3="**************************************************************************************"; //----------TP & SL Settings-----------
extern ENUM_STOPLOSSMETHOD   StopLossMethod=0;
extern string  Note2="*******************************************"; // if SAR Stop Loss is used

extern string  SARStopLossExplaination="Explaination : Setting Stop Loss according to the SAR Value.";
extern string  Note3="*******************************************"; // if Swing Stop Loss is used
extern string  SwingStopLossExplaination="Explaination : Setting Stop Loss accourding to the last swing.";
extern int     SwingCandlesCount=12;       // Number of bars of the Swing.

extern string  Note4="*******************************************"; // if Fixed Stop Loss is used
extern double  FixedStopLoss=25;//Fixed Stop Loss; Hard Stop in Pips.
extern string  Note5="*******************************************"; // --- 
extern double  ClosingOffset=3;       //StopLoss Closing Offset in Pips.
extern string  Note6="*******************************************"; // --- 
extern ENUM_TAKEPROFITMOETHOD TakeProfitMethod=0;
extern string  Note7="*******************************************";// if Multiply Stop Loss is used
extern string  TakeProfitMultiplyStopLossExplaination=""; // Explaination : Take Profit is multiply the Stop Loss
extern double  TakeProfitMultiplier=2;
extern string  Note8="*******************************************";  // if Fixed TakeProfit is used
extern double  FixedTakeProfit=30; // Fixed Take Profit; Hard Take Profit in Pips.

extern string  Header5="**************************************************************************************"; // ----------RSI Settings-----------
extern int              RSIPeriod=21;       //RSI Period
extern int              RSILimit=50;         //RSI Limit 
extern ENUM_PERIODS     RSIApplyPeriod=15;   //RSI TimeFrame

extern string  Header6="**************************************************************************************"; // ----------Trailing Stop Settings-----------
extern bool    UseTrailingStops=true;
extern double  TrailingStopOffset=10;

extern string  Header7="**************************************************************************************";; // ----------Set Max Loss Limit-----------
extern bool    IsLossLimitActivated=false;
extern double  LossLimit=0;
extern bool    IsProfitLimitActivated=false;
extern double  ProfitLimit=0;

extern string  Header8="**************************************************************************************"; // ----------Chart Info -----------
extern int     xDistance=1; //Chart info distance from top right corner

extern string  Header9="**************************************************************************************"; // ----------EA General -----------
extern int     MagicNumber=656668;
extern int     Slippage=3;
extern bool    OnJournaling=true; // Add EA updates in the Journal Tab

string  InternalHeader1="----------Errors Handling Settings-----------";

int     RetryInterval=100; // Pause Time before next retry (in milliseconds)
int     MaxRetriesPerTick=10;

string  InternalHeader2="----------Service Variables-----------";

//+------------------------------------------------------------------+
//|General Variables                                                                 |
//+------------------------------------------------------------------+

double StopBuy,StopSell;
double TakeBuy,TakeSell;

int P,YenPairAdjustFactor;

bool entrySellLock=false;
bool entryBuyLock=false;

CArrayInt Orders;

int OrderNumber;

double TrailingStopList[][2];

double positonLotMultiplier=1;
//+------------------------------------------------------------------+
//|Cross Variables                                                               |
//+------------------------------------------------------------------+


double MyRSI;
double FastEMA,SlowEMA,FastLWMA,SlowLWMA;

int ECrossTriggered,ECurrentDirection,ELastDirection;
bool EFirstTime=true;

int LWCrossTriggered,LWCurrentDirection,LWLastDirection;
bool LWFirstTime=true;

int EDirection;
int LWDirection;

//+------------------------------------------------------------------+
//|Swing Variables                                                                  |
//+------------------------------------------------------------------+

double SwingCandlesHighestHigh;         //	Stores the high values of candles specified in 'SwingCandlesCount'
double SwingCandlesLowestLow;         //	Stores the low values of candles specified in 'SwingCandlesCount'

//+------------------------------------------------------------------+
//|Chart Display Variables                                                                  |
//+------------------------------------------------------------------+

double lastClosedProfit=0;   //GetLastManually Closed profit
double cycleProfit; //Get All Opened Profit
double cycleLots;    //Get All Opened Lots
double netProfit;    // All the profit of all opened positions by the same MagicNumber
string ObjName=IntegerToString(ChartID(),0,' ');   //The global object name 
//+------------------------------------------------------------------+
//|//Pip Display Variables                                                                  |
//+------------------------------------------------------------------+
double pipValue;
double pipValue1Lot;
//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
int init()
  {
   P=GetP(); // To account for 5 digit brokers. Used to convert pips to decimal place
   YenPairAdjustFactor=GetYenAdjustFactor(); // Adjust for YenPair
   ChartSettings();

   if(!CheckAccount()) //Check if the user account is live or not. 
      return (INIT_FAILED);


   DrawChartInfo();            //Drawing Chart Info

   if(UseTrailingStops) ArrayResize(TrailingStopList,MaxPositionsAllowed,0);
   return(0);
  }
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+

void OnDeinit(const int reason)
  {
//---
   if(reason!=5 && reason!=3)
      ObjectsDeleteAll(0,0,OBJ_LABEL);
  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
int start()
  {

   cycleProfit=GetCycleProfit(0)+GetCycleProfit(1);               //Set Cycle Profits onto the chart.
   cycleLots=GetCycleLots(0)+GetCycleLots(1);                  //Set Cycle Lots onto the chart.
   pipValue=(((MarketInfo(Symbol(),MODE_TICKVALUE)*Point*P)/MarketInfo(Symbol(),MODE_TICKSIZE))*cycleLots);// pip value of 1 lot
   pipValue1Lot=(((MarketInfo(Symbol(),MODE_TICKVALUE)*Point*P)/MarketInfo(Symbol(),MODE_TICKSIZE))*1);// pip value of 1 lot

   netProfit=cycleProfit+lastClosedProfit;

   UpdateChartInfo();

   if(IsLossLimitBreached(IsLossLimitActivated,LossLimit,OnJournaling)==true || IsProfitLimitBreached(IsProfitLimitActivated,ProfitLimit,OnJournaling)==true)
     {
      return 0;
     }

   CheckSLTPExit();

//----------Entry & Exit Variables-----------

   FastEMA=iMA(Symbol(),Period(),EMAFast,0,MODE_EMA,PRICE_CLOSE,1);

   SlowEMA=iMA(Symbol(),Period(),EMASlow,0,MODE_EMA,PRICE_CLOSE,1);

   FastLWMA=iMA(Symbol(),Period(),LWMAFast,0,MODE_LWMA,PRICE_CLOSE,1);

   SlowLWMA=iMA(Symbol(),Period(),LWMASlow,0,MODE_LWMA,PRICE_CLOSE,1);

   MyRSI=iRSI(Symbol(),RSIApplyPeriod,RSIPeriod,PRICE_CLOSE,1);

   ECrossTriggered=ECross(FastEMA,SlowEMA);                //Checking for a cross on the E tunnel
   EDirection=ELastDirection;                  //Setting the the direction of E tunnel if Up=1 or Down=2 From ECross

   LWCrossTriggered=LWCross(FastLWMA,SlowLWMA);               //Checking for a cross on the LW MAs 
   LWDirection=LWLastDirection;               //Setting the the direction of LW MAs if Up=1 or Down=2 From LWCross

//----------TP & SL Variables-----------

   SwingCandlesHighestHigh=High[iHighest(Symbol(),Period(),MODE_HIGH,SwingCandlesCount,1)];
   SwingCandlesLowestLow=Low[iLowest(Symbol(),Period(),MODE_LOW,SwingCandlesCount,1)];

   StopBuy=GetBuyStopLoss(StopLossMethod,FixedStopLoss,P);
   StopSell=GetSellStopLoss(StopLossMethod,FixedStopLoss,P);

   TakeBuy=GetBuyTakeProfit(TakeProfitMethod,TakeProfitMultiplier,FixedTakeProfit,StopBuy);
   TakeSell=GetSellTakeProfit(TakeProfitMethod,TakeProfitMultiplier,FixedTakeProfit,StopSell);

   if(UseTrailingStops)
     {
      UpdateTrailingList(OnJournaling,RetryInterval,MagicNumber);
      ReviewTrailingStop(OnJournaling,TrailingStopOffset,RetryInterval,MagicNumber,P);
     }

//----------Exit Rules (All Opened Positions)-----------
   if(!isNewBar())
     {
      return(0);
     }

   if(CountPosOrders(MagicNumber,OP_BUY)>=1 && ExitSignal(LWCrossTriggered)==2 && CloseOnMAs)
     { // Close Long Positions
      CloseOrderPosition(OP_BUY,OnJournaling,MagicNumber,Slippage,P);
     }
   if(CountPosOrders(MagicNumber,OP_SELL)>=1 && ExitSignal(LWCrossTriggered)==1 && CloseOnMAs)
     { // Close Short Positions
      CloseOrderPosition(OP_SELL,OnJournaling,MagicNumber,Slippage,P);
     }

//----------Entry Rules (Market) -----------

   if(IsMaxPositionsReached(MaxPositionsAllowed,MagicNumber,OnJournaling)==False)
     {
      if(entryBuyLock==false)
        {
         if(EntrySignal(ECrossTriggered,EDirection,LWCrossTriggered,LWDirection,MyRSI)==1)
           { // Open Long Positions
            OrderNumber=OpenPositionMarket(OP_BUY,GetLot(isPositionSizing,Lot*positonLotMultiplier,StopBuy),StopBuy,TakeBuy,MagicNumber,Slippage,OnJournaling,P,MaxRetriesPerTick);
            if(UseTrailingStops) SetTrailingStop(OnJournaling,RetryInterval,MagicNumber,P,OrderNumber);
            entrySellLock=true;
            Orders.Add(OrderNumber);
            Risk+=incrementFactor;

           }
        }
      if(entrySellLock==false)
        {
         if(EntrySignal(ECrossTriggered,EDirection,LWCrossTriggered,LWDirection,MyRSI)==2)
           { // Open Short Positions
            OrderNumber=OpenPositionMarket(OP_SELL,GetLot(isPositionSizing,Lot*positonLotMultiplier,StopSell),StopSell,TakeSell,MagicNumber,Slippage,OnJournaling,P,MaxRetriesPerTick);
            if(UseTrailingStops) SetTrailingStop(OnJournaling,RetryInterval,MagicNumber,P,OrderNumber);
            entryBuyLock=true;
            Orders.Add(OrderNumber);
            Risk+=incrementFactor;

           }
        }
     }

   if(CountPosOrders(MagicNumber,OP_BUY)+CountPosOrders(MagicNumber,OP_SELL)==0)
     {
      entryBuyLock=false;
      entrySellLock=false;
     }

   if(CountPosOrders(MagicNumber,OP_BUY)==0)
     {
      entrySellLock=false;
     }

   if(CountPosOrders(MagicNumber,OP_SELL)==0)
     {
      entryBuyLock=false;
     }

   return (0);
  }
//+------------------------------------------------------------------+   
//|End of Start()                                                        |
//+------------------------------------------------------------------+


//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//|                     FUNCTIONS LIBRARY                                   
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

/*

Content:
1) EntrySignal
2) ExitSignal
3) GetLot
4) CheckLot
5) CountPosOrders
6) IsMaxPositionsReached
7) BothLWAboveOrBelow
8) FastLWAboveOrBelow
9) OpenPositionMarket
10) CloseOrderPosition
11) GetP
12) GetYenAdjustFactor
13) GetSellStopLoss
14) GetBuyStopLoss
15) GetSellTakeProfit
16) GetBuyTakeProfit
17) ECrossed
18) LWCrossed
19) UpdateTrailingList
20) ReviewTrailingStop
21) SetTrailingStop
22) IsLossLimitBreached
23) HandleTradingEnvironment
24) GetErrorDescription
25) CheckAccount

*/

//+------------------------------------------------------------------+
// Start of EntrySignal()                                    			|
//+------------------------------------------------------------------+
int EntrySignal(int ECrossOccured,int DirectionOfE,int LWCrossOccured,int DirectionOfLW,double CurrentRSI)
  {
// Type: Customisable 
// Modify this function to suit your trading robot

// This function checks for entry signals

   int output=0;

   int LimitRSI=RSILimit;

   if(CurrentRSI>LimitRSI)
     {                     //rsi value > rsi limit  => long trade 
      if(ECrossOccured==1 && LWCrossOccured==1)
        {          // both LW MAs and E tunnel crossed down at the same point.
         output=1;
        }
      if(DirectionOfE==1 && LWCrossOccured==1 && BothLWAboveOrBelow()==1)
        {        //E tunnel is rising , and there was a cross up with both LW above the tunnel
         output=1;
        }

      if(DirectionOfLW==1 && FastLWAboveOrBelow()==1)
        {         // LW MAs already cross up ,Fast LW above the tunnel
         output=1;
        }

/*    if(ECrossOccured==1 && DirectionOfLW==1 && AboveOrBelow()==1)
        {         // LW MAs already cross up , then later E tunnel crossed up with both LW above the tunnel
         output=1;
        }*/

        }else if(CurrentRSI<LimitRSI){            //rsi value < rsi limit  => short trade 
      if(ECrossOccured==2 && LWCrossOccured==2)
        {          // both LW MAs and E tunnel crossed down at the same point.
         output=2;
        }
      if(DirectionOfE==2 && LWCrossOccured==2 && BothLWAboveOrBelow()==2)
        {         //E tunnel is dropping , and there was a cross down with both LW above the tunnel
         output=2;
        }

      if(DirectionOfLW==2 && FastLWAboveOrBelow()==2)
        {         // LW MAs already cross down, Fast LW below the tunnel
         output=2;
        }

/*  if(ECrossOccured==2 && DirectionOfLW==2 && AboveOrBelow()==2)
        {         // LW MAs already cross down, then later E tunnel crossed down with both LW above the tunnel
         output=2;
        }*/
     }
   return (output);
  }
//+------------------------------------------------------------------+
// End of EntrySignal()                                              |
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
// Start of ExitSignal()                                  			   |
//+------------------------------------------------------------------+
int ExitSignal(int LWCrossOccured)
  {
// Type: Customisable 
// Modify this function to suit your trading robot

// This function checks for exit signals
   int output=0;
   if(LWCrossOccured==1)
     {         //LW MAs crossed up , its an exit for short trades.
      output=1;
     }

   if(LWCrossOccured==2) //LW MAs crossed down , its an exit for long trades.
     {
      output=2;
     }

   return (output);
  }
//+------------------------------------------------------------------+
// End of ExitSignal()                                               |
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
// Start of GetLot()                                       				|
//+------------------------------------------------------------------+
double GetLot(bool isPositionSizingOn,double FixedLots,double stopLoss)
  {
// Type: Customisable 
// Modify this function to suit your trading robot

// This is our sizing algorithm

   double output;

   if(isPositionSizing)
     {
      output=((Risk*AccountEquity()/100)/(pipValue1Lot *(30)*P));
     }
   else
     {
      output=FixedLots;
     }

   output=NormalizeDouble(output,2); // Round to 2 decimal place
   return(output);
  }
//+------------------------------------------------------------------+
// End of GetLot()                                                   |
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
// Start of CheckLot()                                       				|
//+------------------------------------------------------------------+

double CheckLot(double lot,bool Journaling)
  {
// Type: Fixed Template 
// Do not edit unless you know what you're doing

// This function checks if our Lots to be trade satisfies any broker limitations

   double LotToOpen=0;
   LotToOpen=NormalizeDouble(lot,2);
   LotToOpen=MathFloor(LotToOpen/MarketInfo(Symbol(),MODE_LOTSTEP))*MarketInfo(Symbol(),MODE_LOTSTEP);

   if(LotToOpen<MarketInfo(Symbol(),MODE_MINLOT))LotToOpen=MarketInfo(Symbol(),MODE_MINLOT);
   if(LotToOpen>MarketInfo(Symbol(),MODE_MAXLOT))LotToOpen=MarketInfo(Symbol(),MODE_MAXLOT);
   LotToOpen=NormalizeDouble(LotToOpen,2);

   if(Journaling && LotToOpen!=lot)Print("EA Journaling: Trading Lot has been changed by CheckLot function. Requested lot: "+DoubleToString(Lot)+". Lot to open: "+DoubleToString(LotToOpen));

   return(LotToOpen);
  }
//+------------------------------------------------------------------+
//| End of CheckLot()
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//| Start of GetCycleLots()                                                             |
//+------------------------------------------------------------------+
double GetCycleLots(int type)
  {

// Type: Customisable 
// Modify this function to suit your trading robot

// this function returns the net profit of the opened orders 

   double Lots=0;

   for(int i=0; i<OrdersTotal(); i++)
     {
      if(OrderSelect(i,SELECT_BY_POS,MODE_TRADES)==true && OrderSymbol()==Symbol() && (OrderMagicNumber()==MagicNumber) && OrderType()==type)
         Lots+=OrderLots();
     }
   return(NormalizeDouble(Lots,2));

  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
// End of GetCycleLots()                                    		|
//+------------------------------------------------------------------+
double GetCycleProfit(int type)
  {

// Type: Customisable 
// Modify this function to suit your trading robot

// this function returns the net profit of the opened orders 

   double Profit=0;

   for(int i=0; i<OrdersTotal(); i++)
     {
      if(OrderSelect(i,SELECT_BY_POS,MODE_TRADES)==true && OrderSymbol()==Symbol() && (OrderMagicNumber()==MagicNumber) && OrderType()==type)
         Profit+=OrderProfit();
     }
   return(NormalizeDouble(Profit,1));
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
// End of GetCycleProfit()                                    			|
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|Start of CountPosOrders()	" Count Positions"
//+------------------------------------------------------------------+
int CountPosOrders(int Magic,int TYPE)
  {
// Type: Fixed Template 
// Do not edit unless you know what you're doing

// This function counts number of positions/orders of OrderType TYPE

   int orders=0;
   for(int i=0; i<OrdersTotal(); i++)
     {
      if(OrderSelect(i,SELECT_BY_POS,MODE_TRADES)==true && OrderSymbol()==Symbol() && OrderMagicNumber()==Magic && OrderType()==TYPE)
         orders++;
     }
   return(orders);

  }
//+------------------------------------------------------------------+
//| End of CountPosOrders() " Count Positions"
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//| Start of IsMaxPositionsReached()                                             
//+------------------------------------------------------------------+
bool IsMaxPositionsReached(int MaxPositions,int Magic,bool Journaling)
  {
// Type: Fixed Template 
// Do not edit unless you know what you're doing 

// This function checks the number of positions we are holding against the maximum allowed 

   int result=False;
   if(CountPosOrders(Magic,OP_BUY)+CountPosOrders(Magic,OP_SELL)>MaxPositions)
     {
      result=True;
      if(Journaling)Print("Max Orders Exceeded");
        } else if(CountPosOrders(Magic,OP_BUY)+CountPosOrders(Magic,OP_SELL)==MaxPositions) {
      result=True;
     }

   return(result);

/* Definitions: Position vs Orders
   
   Position describes an opened trade
   Order is a pending trade
   
   How to use in a sentence: Jim has 5 buy limit orders pending 10 minutes ago. The market just crashed. The orders were executed and he has 5 losing positions now lol.

*/
  }
//+------------------------------------------------------------------+
//| End of IsMaxPositionsReached()                                                
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
// Start of BothLWAboveOrBelow()                                        	 |
//+------------------------------------------------------------------+

int BothLWAboveOrBelow()
  {
// Type: Customisable 
// Modify this function to suit your trading robot

// This function satisfies the Entry Rules ( if Both LW is Above E tunnel or below). 

   double F_EMA = FastEMA;
   double S_EMA = SlowEMA;
   double F_LWMA = FastLWMA;
   double S_LWMA = SlowLWMA;

   if(F_EMA<F_LWMA && S_EMA<F_LWMA && F_EMA<S_LWMA && S_EMA<S_LWMA)
     {
      return 1; //Above

        }else if(F_EMA>F_LWMA && S_EMA>F_LWMA && F_EMA>S_LWMA && S_EMA>S_LWMA){

      return 2;   //Below
     }

   return 0;
  }
//+------------------------------------------------------------------+
// End of BothLWAboveOrBelow()                                           |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
// Start of BothLWAboveOrBelow()                                        	 |
//+------------------------------------------------------------------+

int FastLWAboveOrBelow()
  {
// Type: Customisable 
// Modify this function to suit your trading robot

// This function satisfies the Entry Rules ( if Fast LW is Above E tunnel or below). 

   double F_EMA = FastEMA;
   double S_EMA = SlowEMA;
   double F_LWMA= FastLWMA;

   if(F_EMA<F_LWMA && S_EMA<F_LWMA)
     {
      return 1; //Above

        }else if(F_EMA>F_LWMA && S_EMA>F_LWMA){

      return 2;   //Below
     }

   return 0;
  }
//+------------------------------------------------------------------+
// End of BothLWAboveOrBelow()                                           |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//|Start of OpenPositionMarket()
//+------------------------------------------------------------------+
int OpenPositionMarket(int TYPE,double LOT,double SL,double TP,int Magic,int Slip,bool Journaling,int K,int Max_Retries_Per_Tick)
  {
// Type: Fixed Template 
// Do not edit unless you know what you're doing 

// This function submits new orders

   int tries=0;
   string symbol=Symbol();
   int cmd=TYPE;
   double volume=CheckLot(LOT,Journaling);
   if(MarketInfo(symbol,MODE_MARGINREQUIRED)*volume>AccountFreeMargin())
     {
      Print("Can not open a trade. Not enough free margin to open "+volume+" on "+symbol);
      return(-1);
     }
   int slippage=Slip*K; // Slippage is in points. 1 point = 0.0001 on 4 digit broker and 0.00001 on a 5 digit broker
   string comment=" "+TYPE+"(#"+Magic+")";
   int magic=Magic;
   datetime expiration=0;
   color arrow_color=0;if(TYPE==OP_BUY)arrow_color=DodgerBlue;if(TYPE==OP_SELL)arrow_color=DeepPink;
   double stoploss=0;
   double takeprofit=0;
   double initTP = TP;
   double initSL = SL;
   int Ticket=-1;
   double price=0;

   while(tries<Max_Retries_Per_Tick) // Edits stops and take profits before the market order is placed
     {
      RefreshRates();
      if(TYPE==OP_BUY)price=Ask;if(TYPE==OP_SELL)price=Bid;

      // Sets Take Profits and Stop Loss. Check against Stop Level Limitations.
      if(TYPE==OP_BUY && SL!=0)
        {
         stoploss=NormalizeDouble(Ask-SL*K*Point,Digits);
         if(Bid-stoploss<=MarketInfo(Symbol(),MODE_STOPLEVEL)*Point)
           {
            stoploss=NormalizeDouble(Bid-MarketInfo(Symbol(),MODE_STOPLEVEL)*Point,Digits);
            if(Journaling)Print("EA Journaling: Stop Loss changed from "+initSL+" to "+MarketInfo(Symbol(),MODE_STOPLEVEL)/K+" pips");
           }
        }
      if(TYPE==OP_SELL && SL!=0)
        {
         stoploss=NormalizeDouble(Bid+SL*K*Point,Digits);
         if(stoploss-Ask<=MarketInfo(Symbol(),MODE_STOPLEVEL)*Point)
           {
            stoploss=NormalizeDouble(Ask+MarketInfo(Symbol(),MODE_STOPLEVEL)*Point,Digits);
            if(Journaling)Print("EA Journaling: Stop Loss changed from "+initSL+" to "+MarketInfo(Symbol(),MODE_STOPLEVEL)/K+" pips");
           }
        }
      if(TYPE==OP_BUY && TP!=0)
        {
         takeprofit=NormalizeDouble(Ask+TP*K*Point,Digits);
         if(takeprofit-Bid<=MarketInfo(Symbol(),MODE_STOPLEVEL)*Point)
           {
            takeprofit=NormalizeDouble(Ask+MarketInfo(Symbol(),MODE_STOPLEVEL)*Point,Digits);
            if(Journaling)Print("EA Journaling: Take Profit changed from "+initTP+" to "+MarketInfo(Symbol(),MODE_STOPLEVEL)/K+" pips");
           }
        }
      if(TYPE==OP_SELL && TP!=0)
        {
         takeprofit=NormalizeDouble(Bid-TP*K*Point,Digits);
         if(Ask-takeprofit<=MarketInfo(Symbol(),MODE_STOPLEVEL)*Point)
           {
            takeprofit=NormalizeDouble(Bid-MarketInfo(Symbol(),MODE_STOPLEVEL)*Point,Digits);
            if(Journaling)Print("EA Journaling: Take Profit changed from "+initTP+" to "+MarketInfo(Symbol(),MODE_STOPLEVEL)/K+" pips");
           }
        }
      if(Journaling)Print("EA Journaling: Trying to place a market order...");
      HandleTradingEnvironment(Journaling,RetryInterval);
      Ticket=OrderSend(symbol,cmd,volume,price,slippage,stoploss,takeprofit,comment,magic,expiration,arrow_color);
      if(Ticket>0)break;
      tries++;
     }

   if(Journaling && Ticket<0)Print("EA Journaling: Unexpected Error has happened. Error Description: "+GetErrorDescription(GetLastError()));
   if(Journaling && Ticket>0)
     {
      Print("EA Journaling: Order successfully placed. Ticket: "+Ticket);
     }
   return(Ticket);
  }
//+------------------------------------------------------------------+
//|End of OpenPositionMarket()
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|Start of CloseOrderPosition()
//+------------------------------------------------------------------+
bool CloseOrderPosition(int TYPE,bool Journaling,int Magic,int Slip,int K)
  {
// Type: Fixed Template 
// Do not edit unless you know what you're doing
   int ordersPos=OrdersTotal();

   for(int i=ordersPos-1; i>=0; i--)
     {
      // Note: Once pending orders become positions, OP_BUYLIMIT AND OP_BUYSTOP becomes OP_BUY, OP_SELLLIMIT and OP_SELLSTOP becomes OP_SELL
      if(TYPE==OP_BUY || TYPE==OP_SELL)
        {
         if(OrderSelect(i,SELECT_BY_POS,MODE_TRADES)==true && OrderSymbol()==Symbol() && OrderMagicNumber()==Magic && OrderType()==TYPE)
           {
            bool Closing=false;
            double Price=0;
            color arrow_color=0;if(TYPE==OP_BUY)arrow_color=MediumSeaGreen;if(TYPE==OP_SELL)arrow_color=DarkOrange;
            if(Journaling)Print("EA Journaling: Trying to close position "+OrderTicket()+" ...");
            HandleTradingEnvironment(Journaling,RetryInterval);
            if(TYPE==OP_BUY)Price=Bid; if(TYPE==OP_SELL)Price=Ask;
            Closing=OrderClose(OrderTicket(),OrderLots(),Price,Slip*K,arrow_color);
            if(Journaling && !Closing)Print("EA Journaling: Unexpected Error has happened. Error Description: "+GetErrorDescription(GetLastError()));
            if(Journaling && Closing)Print("EA Journaling: Position successfully closed.");
           }
        }
     }
   if(CountPosOrders(Magic, TYPE)==0)return(true); else return(false);
  }
//+------------------------------------------------------------------+
//| End of CloseOrderPosition()
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
// Start of getP()                                                   |
//+------------------------------------------------------------------+

int GetP()
  {

   int output;

   if(Digits==5 || Digits==3) output=10;else output=1;

   return(output);
  }
//+------------------------------------------------------------------+
// End of GetP()                                                    	|
//+------------------------------------------------------------------+


//+------------------------------------------------------------------+
// Start of GetYenAdjustFactor()                                     |
//+------------------------------------------------------------------+

int GetYenAdjustFactor()
  {

   int output=1;

   if(Digits==3 || Digits==2) output=100;

   return(output);

  }
//+------------------------------------------------------------------+
// End of GetYenAdjustFactor()                                       |
//+------------------------------------------------------------------+


//+------------------------------------------------------------------+
//Start of GetSellStopLoss()                                      |
//+------------------------------------------------------------------+

double GetSellStopLoss(int stopLossMethod,double fixedStop,int K)
  { // K represents our P multiplier to adjust for broker digits
// Type: Customisable 
// Modify this function to suit your trading robot

// This Function get the highest high values of the Swing Candles.
   double StopL;

   if(stopLossMethod==1)
     {
      StopL=fixedStop; // If Swing Stop Loss not activated. Stop Loss = Fixed Pips Stop Loss
     }

   else if(stopLossMethod==0)
     {
      if(SwingCandlesHighestHigh<Bid)
        {
         StopL=fixedStop;
        }
      else
        {
         StopL=(SwingCandlesHighestHigh-Bid)/(K*Point); // Stop Loss in Pips From the Highest High Price on 'SwingCandleCount'
        }
     }

   return(StopL+ClosingOffset);
  }
//+------------------------------------------------------------------+
// End of GetSellStopLoss()                                      |
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//Start of GetBuyStopLoss()                                    |
//+------------------------------------------------------------------+
double GetBuyStopLoss(int stopLossMethod,double fixedStop,int K)
  { // K represents our P multiplier to adjust for broker digits
// Type: Customisable 
// Modify this function to suit your trading robot

// This Function get the highest high values of the Swing Candles.
   double StopL;

   if(stopLossMethod==1)
     {
      StopL=fixedStop; // If Swing Stop Loss not activated. Stop Loss = Fixed Pips Stop Loss
     }

   else if(stopLossMethod==0)
     {

      if(SwingCandlesLowestLow>Ask)
        {
         StopL=fixedStop;
        }
      else
        {
         StopL=(Ask-SwingCandlesLowestLow)/(K*Point); // Stop Loss in Pips From the Lowest Low Price on 'SwingCandleCount'
        }
     }

   return(StopL+ClosingOffset);
  }
//+------------------------------------------------------------------+
// End of GetBuyStopLoss()                                  |
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
// Start of GetSellTakeProfit()                                  |
//+------------------------------------------------------------------+

double GetSellTakeProfit(int takeProfitMethod,double takeProfitMultiplier,double fixedProfit,double sellStopLoss)
  {

// Type: Customisable 
// Modify this function to suit your trading robot

//This function get the Sell TakeProfit Value
   double takeP;

   if(takeProfitMethod==0) // if the takeprofit value is double the stop loss
     {
      takeP=sellStopLoss*takeProfitMultiplier;
     }
   else
     {
      takeP=fixedProfit;            //else make it fixed in points.
     }

   return (takeP);

  }
//+------------------------------------------------------------------+
// End of GetSellTakeProfit()                                  |
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
// Start of GetBuyTakeProfit()                                  |
//+------------------------------------------------------------------+

double GetBuyTakeProfit(int takeProfitMethod,double takeProfitMultiplier,double fixedProfit,double buyStopLoss)
  {
// Type: Customisable 
// Modify this function to suit your trading robot

//This function get the Sell TakeProfit Value
   double takeP;

   if(takeProfitMethod==0) // if the takeprofit value is double the stop loss
     {
      takeP=buyStopLoss*takeProfitMultiplier;
     }
   else
     {
      takeP=fixedProfit;            //else make it fixed in points.
     }

   return (takeP);

  }
//+------------------------------------------------------------------+
// End of GetBuyTakeProfit()                                  |
//+------------------------------------------------------------------+



//+------------------------------------------------------------------+
// Start of  ECross()                            |
//+------------------------------------------------------------------+

// If Output is 0: No cross happened
//If Output is 1: Line 1 crossed Line 2 from Bottom
//If Output is 2: Line 1 crossed Line 2 from top }


int ECross(double F_EMA,double S_EMA)
  {
// Type: Customizable
// Do not edit unless you know what you're doing

// This function determines if a cross happened between 2 lines/data set here : FastEMA,SlowEMA
//----
   if(F_EMA>S_EMA)
      ECurrentDirection=1;  // line1 above line2
   if(F_EMA<S_EMA)
      ECurrentDirection=2;  // line1 below line2
//----
   if(EFirstTime==true) // Need to check if this is the first time the function is run
     {
      EFirstTime=false; // Change variable to false
      ELastDirection=ECurrentDirection; // Set new direction
      return (0);
     }

   if(ECurrentDirection!=ELastDirection && EFirstTime==false) // If not the first time and there is a direction change
     {
      ELastDirection=ECurrentDirection; // Set new direction
      return(ECurrentDirection); // 1 for up, 2 for down
     }
   else
     {
      return(0);  // No direction change
     }
  }
//+------------------------------------------------------------------+
// End of  ECross()                            |
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
// Start of  LWCross()                            |
//+------------------------------------------------------------------+
int LWCross(double F_LWMA,double S_LWMA)
  {
// Type: Customizable
// Do not edit unless you know what you're doing

// This function determines if a cross happened between 2 lines/data set here : FastLWMA and SlowLWMA
//----
   if(F_LWMA>S_LWMA)
      LWCurrentDirection=1;  // line1 above line2
   if(F_LWMA<S_LWMA)
      LWCurrentDirection=2;  // line1 below line2
//----
   if(LWFirstTime==true) // Need to check if this is the first time the function is run
     {
      LWFirstTime=false; // Change variable to false
      LWLastDirection=LWCurrentDirection; // Set new direction
      return (0);
     }

   if(LWCurrentDirection!=LWLastDirection && LWFirstTime==false) // If not the first time and there is a direction change
     {
      LWLastDirection=LWCurrentDirection; // Set new direction
      return(LWCurrentDirection); // 1 for up, 2 for down
     }
   else
     {
      return(0);  // No direction change
     }
  }
//+------------------------------------------------------------------+
// End of LWCross                                                      |
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//| Start of UpdateTrailingList()                              
//+------------------------------------------------------------------+

void UpdateTrailingList(bool Journaling,int Retry_Interval,int Magic)
  {
// Type: Fixed Template 
// Do not edit unless you know what you're doing

// This function clears the elements of your VolTrailingList if the corresponding positions has been closed

   int ordersPos=OrdersTotal();
   int orderTicketNumber;
   bool doesPosExist;

// Check the VolTrailingList, match with current list of positions. Make sure the all the positions exists. 
// If it doesn't, it means there are positions that have been closed

   for(int x=0; x<ArrayRange(TrailingStopList,0); x++)
     { // Looping through all order number in list

      doesPosExist=False;
      orderTicketNumber=TrailingStopList[x,0];

      if(orderTicketNumber!=0)
        { // Order exists
         for(int y=ordersPos-1; y>=0; y--)
           { // Looping through all current open positions
            if(OrderSelect(y,SELECT_BY_POS,MODE_TRADES)==true && OrderSymbol()==Symbol() && OrderMagicNumber()==Magic)
              {
               if(orderTicketNumber==OrderTicket())
                 { // Checks order number in list against order number of current positions
                  doesPosExist=True;
                  break;
                 }
              }
           }

         if(doesPosExist==False)
           { // Deletes elements if the order number does not match any current positions
            TrailingStopList[x,0] = 0;
            TrailingStopList[x,1] = 0;
           }
        }
     }
  }
//+------------------------------------------------------------------+
//| End of UpdateTrailingList                                        
//+------------------------------------------------------------------+



void ReviewTrailingStop(bool Journaling,double TrailingStop_Offset,int Retry_Interval,int Magic,int K)
  {
// Type: Fixed Template 
// Do not edit unless you know what you're doing 

// This function updates volatility trailing stops levels for all positions (using OrderModify) if appropriate conditions are met

   bool doesTrailingRecordExist;
   int posTicketNumber;
   for(int i=OrdersTotal()-1; i>=0; i--)
     { // Looping through all orders

      if(OrderSelect(i,SELECT_BY_POS,MODE_TRADES)==true && OrderSymbol()==Symbol() && OrderMagicNumber()==Magic)
        {
         doesTrailingRecordExist=False;
         posTicketNumber=OrderTicket();
         for(int x=0; x<ArrayRange(TrailingStopList,0); x++)
           { // Looping through all order number in list 

            if(posTicketNumber==TrailingStopList[x,0])
              { // If condition holds, it means the position have a volatility trailing stop level attached to it

               doesTrailingRecordExist=True;
               bool Modify=false;
               RefreshRates();

               // We update the volatility trailing stop record using OrderModify.
               if(OrderType()==OP_BUY && (Bid-TrailingStopList[x,1]>(TrailingStop_Offset*K*Point)))
                 {
                  if(TrailingStopList[x,1]!=Bid-(TrailingStop_Offset*K*Point))
                    {
                     // if(Journaling)Print("EA Journaling: Trying to modify order "+OrderTicket()+" ...");
                     HandleTradingEnvironment(Journaling,Retry_Interval);
                     Modify=OrderModify(OrderTicket(),OrderOpenPrice(),TrailingStopList[x,1],OrderTakeProfit(),0,CLR_NONE);
                     if(Journaling && !Modify)Print("EA Journaling: Unexpected Error has happened. Error Description: "+GetErrorDescription(GetLastError()));
                     if(Journaling && Modify) Print("EA Journaling: Order successfully modified, trailing stop changed.");

                     TrailingStopList[x,1]=Bid-(TrailingStop_Offset*K*Point);
                    }
                 }
               if(OrderType()==OP_SELL && ((TrailingStopList[x,1]-Ask>(TrailingStop_Offset*K*Point))))
                 {
                  if(TrailingStopList[x,1]!=Ask+(TrailingStop_Offset*K*Point))
                    {
                     //if(Journaling)Print("EA Journaling: Trying to modify order "+OrderTicket()+" ...");
                     HandleTradingEnvironment(Journaling,Retry_Interval);
                     Modify=OrderModify(OrderTicket(),OrderOpenPrice(),TrailingStopList[x,1],OrderTakeProfit(),0,CLR_NONE);
                     if(Journaling && !Modify)Print("EA Journaling: Unexpected Error has happened. Error Description: "+GetErrorDescription(GetLastError()));
                     if(Journaling && Modify) Print("EA Journaling: Order successfully modified, trailing stop changed.");
                     TrailingStopList[x,1]=Ask+(TrailingStop_Offset*K*Point);
                    }
                 }
               break;
              }
           }
         // If order does not have a record attached to it. Alert the trader.
         //if(!doesTrailingRecordExist && Journaling) Print("EA Journaling: Error. Order "+posTicketNumber+" has no volatility trailing stop attached to it.");
        }
     }
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//| End of Review Volatility Trailing Stop
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//| Start of SetTrailingStop
//+------------------------------------------------------------------+

void SetTrailingStop(bool Journaling,int Retry_Interval,int Magic,int K,int OrderNum)
  {
// Type: Fixed Template 
// Do not edit unless you know what you're doing 

// This function adds new volatility trailing stop level using OrderModify()
   double trailingStopLossLimit=0;
   bool Modify=False;
   bool IsTrailingStopAdded=False;
   if(OrderSelect(OrderNum,SELECT_BY_TICKET)==true && OrderSymbol()==Symbol() && OrderMagicNumber()==Magic)
     {
      RefreshRates();
      //+------------------------------------------------------------------+
      //|                                                                  |
      //+------------------------------------------------------------------+
      if(OrderType()==OP_BUY || OrderType()==OP_BUYSTOP)
        {
         trailingStopLossLimit=OrderOpenPrice();// virtual stop loss.
         IsTrailingStopAdded=True;
        }
      //+------------------------------------------------------------------+
      //|                                                                  |
      //+------------------------------------------------------------------+
      if(OrderType()==OP_SELL || OrderType()==OP_SELLSTOP)
        {
         trailingStopLossLimit=OrderOpenPrice();// virtual stop loss.
         IsTrailingStopAdded=True;
        }
      //+------------------------------------------------------------------+
      //|                                                                  |
      //+------------------------------------------------------------------+
      // Records trailingStopLossLimit for future use
      if(IsTrailingStopAdded==True)
        {
         for(int x=0; x<ArrayRange(TrailingStopList,0); x++) // Loop through elements in VolTrailingList
           {
            if(TrailingStopList[x,0]==0) // Checks if the element is empty
              {
               TrailingStopList[x,0]=OrderNum; // Add order number
               TrailingStopList[x,1]=trailingStopLossLimit; // Add Trailing Stop into the List
               Modify=true;
               if(Journaling && Modify) Print("Trailing Stop For "+OrderNum+" Has been Set successfully to "+TrailingStopOffset);
               break;
              }
           }
        }
     }

   if(Journaling && !Modify) Print("Couldnt set Trailing Stop For "+OrderNum+" !");
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//| End of SetTrailingStop
//+------------------------------------------------------------------+




//+------------------------------------------------------------------+
//| Start of IsLossLimitBreached()                                    
//+------------------------------------------------------------------+
bool IsLossLimitBreached(bool LossLimitActivated,double LossLimitPercentage,bool Journaling,int EntrySignalTrigger)
  {

// Type: Fixed Template 
// Do not edit unless you know what you're doing

// This function determines if our maximum loss threshold is breached

   static bool firstTick=False;
   static double initialCapital=0;
   double profitAndLoss=0;
   double profitAndLossPrint=0;
   bool output=False;

   if(LossLimitActivated==False) return(output);

   if(firstTick==False)
     {
      initialCapital=AccountEquity();
      firstTick=True;
     }

   profitAndLoss=(AccountEquity()/initialCapital)-1;

   if(profitAndLoss<-LossLimitPercentage/100)
     {
      output=True;
      profitAndLossPrint=NormalizeDouble(profitAndLoss,4)*100;
      if(Journaling)if(EntrySignalTrigger!=0) Print("Entry trade triggered but not executed. Loss threshold breached. Current Loss: "+profitAndLossPrint+"%");
     }

   return(output);
  }
//+------------------------------------------------------------------+
//|End of IsLossLimitBreached()                                      
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//| Start of HandleTradingEnvironment()                                         
//+------------------------------------------------------------------+
void HandleTradingEnvironment(bool Journaling,int Retry_Interval)
  {
// Type: Fixed Template 
// Do not edit unless you know what you're doing 

// This function checks for errors

   if(IsTradeAllowed()==true)return;
   if(!IsConnected())
     {
      if(Journaling)Print("EA Journaling: Terminal is not connected to server...");
      return;
     }
   if(!IsTradeAllowed() && Journaling)Print("EA Journaling: Trade is not alowed for some reason...");
   if(IsConnected() && !IsTradeAllowed())
     {
      while(IsTradeContextBusy()==true)
        {
         if(Journaling)Print("EA Journaling: Trading context is busy... Will wait a bit...");
         Sleep(Retry_Interval);
        }
     }
   RefreshRates();
  }
//+------------------------------------------------------------------+
//| End of HandleTradingEnvironment()                              
//+------------------------------------------------------------------+  
//+------------------------------------------------------------------+
//| Start of GetErrorDescription()                                               
//+------------------------------------------------------------------+
string GetErrorDescription(int error)
  {
// Type: Fixed Template 
// Do not edit unless you know what you're doing

// This function returns the exact error

   string ErrorDescription="";
//---
   switch(error)
     {
      case 0:     ErrorDescription = "NO Error. Everything should be good.";                                    break;
      case 1:     ErrorDescription = "No error returned, but the result is unknown";                            break;
      case 2:     ErrorDescription = "Common error";                                                            break;
      case 3:     ErrorDescription = "Invalid trade parameters";                                                break;
      case 4:     ErrorDescription = "Trade server is busy";                                                    break;
      case 5:     ErrorDescription = "Old version of the client terminal";                                      break;
      case 6:     ErrorDescription = "No connection with trade server";                                         break;
      case 7:     ErrorDescription = "Not enough rights";                                                       break;
      case 8:     ErrorDescription = "Too frequent requests";                                                   break;
      case 9:     ErrorDescription = "Malfunctional trade operation";                                           break;
      case 64:    ErrorDescription = "Account disabled";                                                        break;
      case 65:    ErrorDescription = "Invalid account";                                                         break;
      case 128:   ErrorDescription = "Trade timeout";                                                           break;
      case 129:   ErrorDescription = "Invalid price";                                                           break;
      case 130:   ErrorDescription = "Invalid stops";                                                           break;
      case 131:   ErrorDescription = "Invalid trade volume";                                                    break;
      case 132:   ErrorDescription = "Market is closed";                                                        break;
      case 133:   ErrorDescription = "Trade is disabled";                                                       break;
      case 134:   ErrorDescription = "Not enough money";                                                        break;
      case 135:   ErrorDescription = "Price changed";                                                           break;
      case 136:   ErrorDescription = "Off quotes";                                                              break;
      case 137:   ErrorDescription = "Broker is busy";                                                          break;
      case 138:   ErrorDescription = "Requote";                                                                 break;
      case 139:   ErrorDescription = "Order is locked";                                                         break;
      case 140:   ErrorDescription = "Long positions only allowed";                                             break;
      case 141:   ErrorDescription = "Too many requests";                                                       break;
      case 145:   ErrorDescription = "Modification denied because order too close to market";                   break;
      case 146:   ErrorDescription = "Trade context is busy";                                                   break;
      case 147:   ErrorDescription = "Expirations are denied by broker";                                        break;
      case 148:   ErrorDescription = "Too many open and pending orders (more than allowed)";                    break;
      case 4000:  ErrorDescription = "No error";                                                                break;
      case 4001:  ErrorDescription = "Wrong function pointer";                                                  break;
      case 4002:  ErrorDescription = "Array index is out of range";                                             break;
      case 4003:  ErrorDescription = "No memory for function call stack";                                       break;
      case 4004:  ErrorDescription = "Recursive stack overflow";                                                break;
      case 4005:  ErrorDescription = "Not enough stack for parameter";                                          break;
      case 4006:  ErrorDescription = "No memory for parameter string";                                          break;
      case 4007:  ErrorDescription = "No memory for temp string";                                               break;
      case 4008:  ErrorDescription = "Not initialized string";                                                  break;
      case 4009:  ErrorDescription = "Not initialized string in array";                                         break;
      case 4010:  ErrorDescription = "No memory for array string";                                              break;
      case 4011:  ErrorDescription = "Too long string";                                                         break;
      case 4012:  ErrorDescription = "Remainder from zero divide";                                              break;
      case 4013:  ErrorDescription = "Zero divide";                                                             break;
      case 4014:  ErrorDescription = "Unknown command";                                                         break;
      case 4015:  ErrorDescription = "Wrong jump (never generated error)";                                      break;
      case 4016:  ErrorDescription = "Not initialized array";                                                   break;
      case 4017:  ErrorDescription = "DLL calls are not allowed";                                               break;
      case 4018:  ErrorDescription = "Cannot load library";                                                     break;
      case 4019:  ErrorDescription = "Cannot call function";                                                    break;
      case 4020:  ErrorDescription = "Expert function calls are not allowed";                                   break;
      case 4021:  ErrorDescription = "Not enough memory for temp string returned from function";                break;
      case 4022:  ErrorDescription = "System is busy (never generated error)";                                  break;
      case 4050:  ErrorDescription = "Invalid function parameters count";                                       break;
      case 4051:  ErrorDescription = "Invalid function parameter value";                                        break;
      case 4052:  ErrorDescription = "String function internal error";                                          break;
      case 4053:  ErrorDescription = "Some array error";                                                        break;
      case 4054:  ErrorDescription = "Incorrect series array using";                                            break;
      case 4055:  ErrorDescription = "Custom indicator error";                                                  break;
      case 4056:  ErrorDescription = "Arrays are incompatible";                                                 break;
      case 4057:  ErrorDescription = "Global variables processing error";                                       break;
      case 4058:  ErrorDescription = "Global variable not found";                                               break;
      case 4059:  ErrorDescription = "Function is not allowed in testing mode";                                 break;
      case 4060:  ErrorDescription = "Function is not confirmed";                                               break;
      case 4061:  ErrorDescription = "Send mail error";                                                         break;
      case 4062:  ErrorDescription = "String parameter expected";                                               break;
      case 4063:  ErrorDescription = "Integer parameter expected";                                              break;
      case 4064:  ErrorDescription = "Double parameter expected";                                               break;
      case 4065:  ErrorDescription = "Array as parameter expected";                                             break;
      case 4066:  ErrorDescription = "Requested history data in updating state";                                break;
      case 4067:  ErrorDescription = "Some error in trading function";                                          break;
      case 4099:  ErrorDescription = "End of file";                                                             break;
      case 4100:  ErrorDescription = "Some file error";                                                         break;
      case 4101:  ErrorDescription = "Wrong file name";                                                         break;
      case 4102:  ErrorDescription = "Too many opened files";                                                   break;
      case 4103:  ErrorDescription = "Cannot open file";                                                        break;
      case 4104:  ErrorDescription = "Incompatible access to a file";                                           break;
      case 4105:  ErrorDescription = "No order selected";                                                       break;
      case 4106:  ErrorDescription = "Unknown symbol";                                                          break;
      case 4107:  ErrorDescription = "Invalid price";                                                           break;
      case 4108:  ErrorDescription = "Invalid ticket";                                                          break;
      case 4109:  ErrorDescription = "EA is not allowed to trade is not allowed. ";                             break;
      case 4110:  ErrorDescription = "Longs are not allowed. Check the expert properties";                      break;
      case 4111:  ErrorDescription = "Shorts are not allowed. Check the expert properties";                     break;
      case 4200:  ErrorDescription = "Object exists already";                                                   break;
      case 4201:  ErrorDescription = "Unknown object property";                                                 break;
      case 4202:  ErrorDescription = "Object does not exist";                                                   break;
      case 4203:  ErrorDescription = "Unknown object type";                                                     break;
      case 4204:  ErrorDescription = "No object name";                                                          break;
      case 4205:  ErrorDescription = "Object coordinates error";                                                break;
      case 4206:  ErrorDescription = "No specified subwindow";                                                  break;
      case 4207:  ErrorDescription = "Some error in object function";                                           break;
      default:    ErrorDescription = "No error or error is unknown";
     }
   return(ErrorDescription);
  }
//+------------------------------------------------------------------+
//| End of GetErrorDescription()                                         
//+------------------------------------------------------------------+


//+------------------------------------------------------------------+
//| Start of ChartSettings()                                         
//+------------------------------------------------------------------+
void ChartSettings()
  {
   ChartSetInteger(0,CHART_SHOW_GRID,0);
   ChartSetInteger(0,CHART_MODE,CHART_CANDLES);
   ChartSetInteger(0,CHART_AUTOSCROLL,0,True);
   WindowRedraw();
  }
//+------------------------------------------------------------------+
//| End of ChartSettings()                                         
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//| Start of CloseAllPositions()
//+------------------------------------------------------------------+ 
void CloseAllPositions()
  {
// Type: Fixed Template 
// Do not edit unless you know what you're doing

// This function closes all positions 

   CloseOrderPosition(OP_BUY,OnJournaling,MagicNumber,Slippage,P);

   CloseOrderPosition(OP_SELL,OnJournaling,MagicNumber,Slippage,P);

   CloseOrderPosition(OP_BUYSTOP,OnJournaling,MagicNumber,Slippage,P);

   CloseOrderPosition(OP_SELLSTOP,OnJournaling,MagicNumber,Slippage,P);

   return;

  }
//+------------------------------------------------------------------+
//| End of CloseAllPositions()
//+------------------------------------------------------------------+ 
//+------------------------------------------------------------------+
//| Start of CheckAccount()                                         
//+------------------------------------------------------------------+


bool CheckAccount()
  {

   if(AccountInfoInteger(ACCOUNT_TRADE_MODE)==ACCOUNT_TRADE_MODE_DEMO)
     {
      return true;
     }
   else
     {
      MessageBox("You cant Proceed.\n HelmiFX","Notice",MB_ICONSTOP|MB_OK);
      return false;
     }

   return false;
  }
//+------------------------------------------------------------------+
//| End of CheckAccount()                                         
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//| Start of IsLossLimitBreached()                                    
//+------------------------------------------------------------------+
bool IsLossLimitBreached(bool LossLimitActivated,double LossLimitValue,bool Journaling)
  {

// Type: Fixed Template 
// Do not edit unless you know what you're doing

// This function determines if our maximum Profit threshold is breached
//static bool firstTime=false;
   double lossPrint;
   bool output=False;

   if(LossLimitActivated==False) return (output);
//if(firstTime == true) return(true);

   if(cycleProfit<(-1 *LossLimitValue))
     {
      output=True;
      //firstTime=true;
      CloseAllPositions();
      lossPrint=NormalizeDouble(cycleProfit,4);
      if(Journaling) Print("Loss threshold breached. Current Loss: "+lossPrint);
     }

   return (output);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//|End of IsLossLimitBreached()                                      
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//| Start of IsProfitLimitBreached()                                    
//+------------------------------------------------------------------+
bool IsProfitLimitBreached(bool ProfitLimitActivated,double ProfitLimitValue,bool Journaling)
  {

// Type: Fixed Template 
// Do not edit unless you know what you're doing

// This function determines if our maximum Profit threshold is breached
   static bool firstTime=false;
   double profitPrint;
   bool output=False;

   if(ProfitLimitActivated==False) return (output);
//if(firstTime == true) return(true);

   if(cycleProfit>ProfitLimitValue)
     {
      output=True;
      //firstTime=true;
      CloseAllPositions();
      profitPrint=NormalizeDouble(cycleProfit,4);
      if(Journaling) Print("Profit threshold breached. Current Profit: "+profitPrint);
     }

   return (output);
  }
//+------------------------------------------------------------------+
//|End of IsProfitLimitBreached                                                                   |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//|Start of DrawChartInfo                                                                  |
//+------------------------------------------------------------------+

void DrawChartInfo()
  {

   ObjectCreate(ChartID(),ObjName+"Findus1",OBJ_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"Findus1",OBJPROP_TEXT,"Find us on:");
   ObjectSetInteger(ChartID(),ObjName+"Findus1",OBJPROP_CORNER,CORNER_RIGHT_LOWER);
   ObjectSetInteger(ChartID(),ObjName+"Findus1",OBJPROP_XDISTANCE,1);
   ObjectSetInteger(ChartID(),ObjName+"Findus1",OBJPROP_YDISTANCE,48);
   ObjectSetString(ChartID(),ObjName+"Findus1",OBJPROP_FONT,"Verdana");
   ObjectSetInteger(ChartID(),ObjName+"Findus1",OBJPROP_FONTSIZE,8);
   ObjectSetInteger(ChartID(),ObjName+"Findus1",OBJPROP_ANCHOR,ANCHOR_RIGHT);
   ObjectSetInteger(ChartID(),ObjName+"Findus1",OBJPROP_COLOR,White);

   ObjectCreate(ChartID(),ObjName+"Findus2",OBJ_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"Findus2",OBJPROP_TEXT,"Facbook : HelmiFx");
   ObjectSetInteger(ChartID(),ObjName+"Findus2",OBJPROP_CORNER,CORNER_RIGHT_LOWER);
   ObjectSetInteger(ChartID(),ObjName+"Findus2",OBJPROP_XDISTANCE,1);
   ObjectSetInteger(ChartID(),ObjName+"Findus2",OBJPROP_YDISTANCE,36);
   ObjectSetString(ChartID(),ObjName+"Findus2",OBJPROP_FONT,"Verdana");
   ObjectSetInteger(ChartID(),ObjName+"Findus2",OBJPROP_FONTSIZE,8);
   ObjectSetInteger(ChartID(),ObjName+"Findus2",OBJPROP_ANCHOR,ANCHOR_RIGHT);
   ObjectSetInteger(ChartID(),ObjName+"Findus2",OBJPROP_COLOR,White);

   ObjectCreate(ChartID(),ObjName+"Findus3",OBJ_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"Findus3",OBJPROP_TEXT,"Twitter : @HelmiForex");
   ObjectSetInteger(ChartID(),ObjName+"Findus3",OBJPROP_CORNER,CORNER_RIGHT_LOWER);
   ObjectSetInteger(ChartID(),ObjName+"Findus3",OBJPROP_XDISTANCE,1);
   ObjectSetInteger(ChartID(),ObjName+"Findus3",OBJPROP_YDISTANCE,24);
   ObjectSetString(ChartID(),ObjName+"Findus3",OBJPROP_FONT,"Verdana");
   ObjectSetInteger(ChartID(),ObjName+"Findus3",OBJPROP_FONTSIZE,8);
   ObjectSetInteger(ChartID(),ObjName+"Findus3",OBJPROP_ANCHOR,ANCHOR_RIGHT);
   ObjectSetInteger(ChartID(),ObjName+"Findus3",OBJPROP_COLOR,White);

   ObjectCreate(ChartID(),ObjName+"Findus4",OBJ_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"Findus4",OBJPROP_TEXT,"Youtube : HelmiForex");
   ObjectSetInteger(ChartID(),ObjName+"Findus4",OBJPROP_CORNER,CORNER_RIGHT_LOWER);
   ObjectSetInteger(ChartID(),ObjName+"Findus4",OBJPROP_XDISTANCE,1);
   ObjectSetInteger(ChartID(),ObjName+"Findus4",OBJPROP_YDISTANCE,12);
   ObjectSetString(ChartID(),ObjName+"Findus4",OBJPROP_FONT,"Verdana");
   ObjectSetInteger(ChartID(),ObjName+"Findus4",OBJPROP_FONTSIZE,8);
   ObjectSetInteger(ChartID(),ObjName+"Findus4",OBJPROP_ANCHOR,ANCHOR_RIGHT);
   ObjectSetInteger(ChartID(),ObjName+"Findus4",OBJPROP_COLOR,White);
   ObjectCreate(ChartID(),ObjName+"HelmiFX1",OBJ_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"HelmiFX1",OBJPROP_TEXT,"HelmiFX");
   ObjectSetInteger(ChartID(),ObjName+"HelmiFX1",OBJPROP_CORNER,CORNER_RIGHT_UPPER);
   ObjectSetInteger(ChartID(),ObjName+"HelmiFX1",OBJPROP_XDISTANCE,xDistance);
   ObjectSetInteger(ChartID(),ObjName+"HelmiFX1",OBJPROP_YDISTANCE,12);
   ObjectSetString(ChartID(),ObjName+"HelmiFX1",OBJPROP_FONT,"Verdana");
   ObjectSetInteger(ChartID(),ObjName+"HelmiFX1",OBJPROP_FONTSIZE,8);
   ObjectSetInteger(ChartID(),ObjName+"HelmiFX1",OBJPROP_ANCHOR,ANCHOR_RIGHT_UPPER);
   ObjectSetInteger(ChartID(),ObjName+"HelmiFX1",OBJPROP_COLOR,White);

   ObjectCreate(ChartID(),ObjName+"HelmiFX2",OBJ_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"HelmiFX2",OBJPROP_TEXT,"www.helmifx.com");
   ObjectSetInteger(ChartID(),ObjName+"HelmiFX2",OBJPROP_CORNER,CORNER_RIGHT_UPPER);
   ObjectSetInteger(ChartID(),ObjName+"HelmiFX2",OBJPROP_XDISTANCE,xDistance);
   ObjectSetInteger(ChartID(),ObjName+"HelmiFX2",OBJPROP_YDISTANCE,37);
   ObjectSetString(ChartID(),ObjName+"HelmiFX2",OBJPROP_FONT,"Verdana");
   ObjectSetInteger(ChartID(),ObjName+"HelmiFX2",OBJPROP_FONTSIZE,8);
   ObjectSetInteger(ChartID(),ObjName+"HelmiFX2",OBJPROP_ANCHOR,ANCHOR_RIGHT);
   ObjectSetInteger(ChartID(),ObjName+"HelmiFX2",OBJPROP_COLOR,White);

   ObjectCreate(ChartID(),ObjName+"Symbol",OBJ_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"Symbol",OBJPROP_TEXT,Symbol());
   ObjectSetInteger(ChartID(),ObjName+"Symbol",OBJPROP_CORNER,CORNER_RIGHT_UPPER);
   ObjectSetInteger(ChartID(),ObjName+"Symbol",OBJPROP_XDISTANCE,xDistance);
   ObjectSetInteger(ChartID(),ObjName+"Symbol",OBJPROP_YDISTANCE,62);
   ObjectSetString(ChartID(),ObjName+"Symbol",OBJPROP_FONT,"Verdana");
   ObjectSetInteger(ChartID(),ObjName+"Symbol",OBJPROP_FONTSIZE,12);
   ObjectSetInteger(ChartID(),ObjName+"Symbol",OBJPROP_ANCHOR,ANCHOR_RIGHT);
   ObjectSetInteger(ChartID(),ObjName+"Symbol",OBJPROP_COLOR,Yellow);

   ObjectCreate(ChartID(),ObjName+"PipValueOf1Lot",OBJ_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"PipValueOf1Lot",OBJPROP_TEXT,"Pip Value of 1 Lot is "+DoubleToStr(pipValue1Lot,2)+"$");
   ObjectSetInteger(ChartID(),ObjName+"PipValueOf1Lot",OBJPROP_CORNER,CORNER_RIGHT_UPPER);
   ObjectSetInteger(ChartID(),ObjName+"PipValueOf1Lot",OBJPROP_XDISTANCE,xDistance);
   ObjectSetInteger(ChartID(),ObjName+"PipValueOf1Lot",OBJPROP_YDISTANCE,87);
   ObjectSetString(ChartID(),ObjName+"PipValueOf1Lot",OBJPROP_FONT,"Verdana");
   ObjectSetInteger(ChartID(),ObjName+"PipValueOf1Lot",OBJPROP_FONTSIZE,12);
   ObjectSetInteger(ChartID(),ObjName+"PipValueOf1Lot",OBJPROP_ANCHOR,ANCHOR_RIGHT);
   ObjectSetInteger(ChartID(),ObjName+"PipValueOf1Lot",OBJPROP_COLOR,Pink);

   ObjectCreate(ChartID(),ObjName+"PipValue",OBJ_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"PipValue",OBJPROP_TEXT,"Total Lots = "+DoubleToStr(cycleLots,2)+" Lot = "+DoubleToStr(pipValue,2)+"$");
   ObjectSetInteger(ChartID(),ObjName+"PipValue",OBJPROP_CORNER,CORNER_RIGHT_UPPER);
   ObjectSetInteger(ChartID(),ObjName+"PipValue",OBJPROP_XDISTANCE,xDistance);
   ObjectSetInteger(ChartID(),ObjName+"PipValue",OBJPROP_YDISTANCE,112);
   ObjectSetString(ChartID(),ObjName+"PipValue",OBJPROP_FONT,"Verdana");
   ObjectSetInteger(ChartID(),ObjName+"PipValue",OBJPROP_FONTSIZE,10);
   ObjectSetInteger(ChartID(),ObjName+"PipValue",OBJPROP_ANCHOR,ANCHOR_RIGHT);
   ObjectSetInteger(ChartID(),ObjName+"PipValue",OBJPROP_COLOR,DeepPink);

   ObjectCreate(ChartID(),ObjName+"Equity",OBJ_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"Equity",OBJPROP_TEXT,"Account Equity : "+AccountEquity());
   ObjectSetInteger(ChartID(),ObjName+"Equity",OBJPROP_CORNER,CORNER_RIGHT_UPPER);
   ObjectSetInteger(ChartID(),ObjName+"Equity",OBJPROP_XDISTANCE,xDistance);
   ObjectSetInteger(ChartID(),ObjName+"Equity",OBJPROP_YDISTANCE,137);
   ObjectSetString(ChartID(),ObjName+"Equity",OBJPROP_FONT,"Verdana");
   ObjectSetInteger(ChartID(),ObjName+"Equity",OBJPROP_FONTSIZE,10);
   ObjectSetInteger(ChartID(),ObjName+"Equity",OBJPROP_ANCHOR,ANCHOR_RIGHT);
   ObjectSetInteger(ChartID(),ObjName+"Equity",OBJPROP_COLOR,BurlyWood);

   ObjectCreate(ChartID(),ObjName+"NetProfit",OBJ_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"NetProfit",OBJPROP_TEXT,"Net Profit "+DoubleToString(netProfit,1));
   ObjectSetInteger(ChartID(),ObjName+"NetProfit",OBJPROP_CORNER,CORNER_RIGHT_UPPER);
   ObjectSetInteger(ChartID(),ObjName+"NetProfit",OBJPROP_XDISTANCE,xDistance);
   ObjectSetInteger(ChartID(),ObjName+"NetProfit",OBJPROP_YDISTANCE,162);
   ObjectSetString(ChartID(),ObjName+"NetProfit",OBJPROP_FONT,"Verdana");
   ObjectSetInteger(ChartID(),ObjName+"NetProfit",OBJPROP_FONTSIZE,11);
   ObjectSetInteger(ChartID(),ObjName+"NetProfit",OBJPROP_ANCHOR,ANCHOR_RIGHT);
   ObjectSetInteger(ChartID(),ObjName+"NetProfit",OBJPROP_COLOR,DarkTurquoise);

   ObjectCreate(ChartID(),ObjName+"ClosedPositionsProfit",OBJ_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"ClosedPositionsProfit",OBJPROP_TEXT,"Closed Positions Profit "+DoubleToString(lastClosedProfit,1));
   ObjectSetInteger(ChartID(),ObjName+"ClosedPositionsProfit",OBJPROP_CORNER,CORNER_RIGHT_UPPER);
   ObjectSetInteger(ChartID(),ObjName+"ClosedPositionsProfit",OBJPROP_XDISTANCE,xDistance);
   ObjectSetInteger(ChartID(),ObjName+"ClosedPositionsProfit",OBJPROP_YDISTANCE,187);
   ObjectSetString(ChartID(),ObjName+"ClosedPositionsProfit",OBJPROP_FONT,"Verdana");
   ObjectSetInteger(ChartID(),ObjName+"ClosedPositionsProfit",OBJPROP_FONTSIZE,11);
   ObjectSetInteger(ChartID(),ObjName+"ClosedPositionsProfit",OBJPROP_ANCHOR,ANCHOR_RIGHT);
   ObjectSetInteger(ChartID(),ObjName+"ClosedPositionsProfit",OBJPROP_COLOR,Blue);

   ObjectCreate(ChartID(),ObjName+"CurrentCycleProfit",OBJ_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"CurrentCycleProfit",OBJPROP_TEXT,"Current Cycle Profit "+DoubleToString(cycleProfit,1));
   ObjectSetInteger(ChartID(),ObjName+"CurrentCycleProfit",OBJPROP_CORNER,CORNER_RIGHT_UPPER);
   ObjectSetInteger(ChartID(),ObjName+"CurrentCycleProfit",OBJPROP_XDISTANCE,xDistance);
   ObjectSetInteger(ChartID(),ObjName+"CurrentCycleProfit",OBJPROP_YDISTANCE,212);
   ObjectSetString(ChartID(),ObjName+"CurrentCycleProfit",OBJPROP_FONT,"Verdana");
   ObjectSetInteger(ChartID(),ObjName+"CurrentCycleProfit",OBJPROP_FONTSIZE,11);
   ObjectSetInteger(ChartID(),ObjName+"CurrentCycleProfit",OBJPROP_ANCHOR,ANCHOR_RIGHT);
   ObjectSetInteger(ChartID(),ObjName+"CurrentCycleProfit",OBJPROP_COLOR,Teal);

   double ChartRSI=iRSI(Symbol(),RSIApplyPeriod,RSIPeriod,PRICE_CLOSE,1);
   ObjectCreate(ChartID(),ObjName+"RSIValue",OBJ_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"RSIValue",OBJPROP_TEXT,"RSI Value : "+DoubleToStr(ChartRSI,Digits));
   ObjectSetInteger(ChartID(),ObjName+"RSIValue",OBJPROP_CORNER,CORNER_LEFT_UPPER);
   ObjectSetInteger(ChartID(),ObjName+"RSIValue",OBJPROP_XDISTANCE,xDistance);
   ObjectSetInteger(ChartID(),ObjName+"RSIValue",OBJPROP_YDISTANCE,225);
   ObjectSetString(ChartID(),ObjName+"RSIValue",OBJPROP_FONT,"Verdana");
   ObjectSetInteger(ChartID(),ObjName+"RSIValue",OBJPROP_FONTSIZE,10);
   ObjectSetInteger(ChartID(),ObjName+"RSIValue",OBJPROP_ANCHOR,ANCHOR_LEFT);

   if(ChartRSI>RSILimit)
     {
      ObjectSetInteger(ChartID(),ObjName+"RSIValue",OBJPROP_COLOR,Lime);
     }
   else
     {
      ObjectSetInteger(ChartID(),ObjName+"RSIValue",OBJPROP_COLOR,Red);
     }

   ObjectCreate(ChartID(),ObjName+"fastEMA",OBJ_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"fastEMA",OBJPROP_TEXT,"Fast EMA : "+DoubleToStr(FastEMA,Digits));
   ObjectSetInteger(ChartID(),ObjName+"fastEMA",OBJPROP_CORNER,CORNER_LEFT_UPPER);
   ObjectSetInteger(ChartID(),ObjName+"fastEMA",OBJPROP_XDISTANCE,xDistance);
   ObjectSetInteger(ChartID(),ObjName+"fastEMA",OBJPROP_YDISTANCE,250);
   ObjectSetString(ChartID(),ObjName+"fastEMA",OBJPROP_FONT,"Verdana");
   ObjectSetInteger(ChartID(),ObjName+"fastEMA",OBJPROP_FONTSIZE,10);
   ObjectSetInteger(ChartID(),ObjName+"fastEMA",OBJPROP_ANCHOR,ANCHOR_LEFT);
   ObjectSetInteger(ChartID(),ObjName+"fastEMA",OBJPROP_COLOR,LightCoral);

   ObjectCreate(ChartID(),ObjName+"slowEMA",OBJ_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"slowEMA",OBJPROP_TEXT,"Slow EMA : "+DoubleToStr(SlowEMA,Digits));
   ObjectSetInteger(ChartID(),ObjName+"slowEMA",OBJPROP_CORNER,CORNER_LEFT_UPPER);
   ObjectSetInteger(ChartID(),ObjName+"slowEMA",OBJPROP_XDISTANCE,xDistance);
   ObjectSetInteger(ChartID(),ObjName+"slowEMA",OBJPROP_YDISTANCE,275);
   ObjectSetString(ChartID(),ObjName+"slowEMA",OBJPROP_FONT,"Verdana");
   ObjectSetInteger(ChartID(),ObjName+"slowEMA",OBJPROP_FONTSIZE,10);
   ObjectSetInteger(ChartID(),ObjName+"slowEMA",OBJPROP_ANCHOR,ANCHOR_LEFT);
   ObjectSetInteger(ChartID(),ObjName+"slowEMA",OBJPROP_COLOR,LightCoral);

   ObjectCreate(ChartID(),ObjName+"fastLWMA",OBJ_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"fastLWMA",OBJPROP_TEXT,"Fast LWMA : "+DoubleToStr(FastLWMA,Digits));
   ObjectSetInteger(ChartID(),ObjName+"fastLWMA",OBJPROP_CORNER,CORNER_LEFT_UPPER);
   ObjectSetInteger(ChartID(),ObjName+"fastLWMA",OBJPROP_XDISTANCE,xDistance);
   ObjectSetInteger(ChartID(),ObjName+"fastLWMA",OBJPROP_YDISTANCE,300);
   ObjectSetString(ChartID(),ObjName+"fastLWMA",OBJPROP_FONT,"Verdana");
   ObjectSetInteger(ChartID(),ObjName+"fastLWMA",OBJPROP_FONTSIZE,10);
   ObjectSetInteger(ChartID(),ObjName+"fastLWMA",OBJPROP_ANCHOR,ANCHOR_LEFT);
   ObjectSetInteger(ChartID(),ObjName+"fastLWMA",OBJPROP_COLOR,Gold);

   ObjectCreate(ChartID(),ObjName+"slowLWMA",OBJ_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"slowLWMA",OBJPROP_TEXT,"Slow LWMA : "+DoubleToStr(SlowLWMA,Digits));
   ObjectSetInteger(ChartID(),ObjName+"slowLWMA",OBJPROP_CORNER,CORNER_LEFT_UPPER);
   ObjectSetInteger(ChartID(),ObjName+"slowLWMA",OBJPROP_XDISTANCE,xDistance);
   ObjectSetInteger(ChartID(),ObjName+"slowLWMA",OBJPROP_YDISTANCE,325);
   ObjectSetString(ChartID(),ObjName+"slowLWMA",OBJPROP_FONT,"Verdana");
   ObjectSetInteger(ChartID(),ObjName+"slowLWMA",OBJPROP_FONTSIZE,10);
   ObjectSetInteger(ChartID(),ObjName+"slowLWMA",OBJPROP_ANCHOR,ANCHOR_LEFT);
   ObjectSetInteger(ChartID(),ObjName+"slowLWMA",OBJPROP_COLOR,LightSeaGreen);

  }
//+------------------------------------------------------------------+
// End of DrawChartInfo()                                          |
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
// Start of UpdateChartInfo()                                          |
//+------------------------------------------------------------------+
void UpdateChartInfo()
  {

   ObjectSetString(ChartID(),ObjName+"PipValueOf1Lot",OBJPROP_TEXT,"Pip Value of 1 Lot is "+DoubleToStr(pipValue1Lot,2)+"$");
   ObjectSetString(ChartID(),ObjName+"PipValue",OBJPROP_TEXT,"Total Lots = "+cycleLots+" Lot = "+DoubleToStr(pipValue,2)+"$");
   ObjectSetString(ChartID(),ObjName+"ClosedPositionsProfit",OBJPROP_TEXT,"Closed Positions Profit "+DoubleToString(lastClosedProfit,2));
   ObjectSetString(ChartID(),ObjName+"CurrentCycleProfit",OBJPROP_TEXT,"Current Cycle Profit "+DoubleToString(cycleProfit,2));
   ObjectSetString(ChartID(),ObjName+"NetProfit",OBJPROP_TEXT,"Net Profit "+DoubleToString(netProfit,1));
   ObjectSetString(ChartID(),ObjName+"Equity",OBJPROP_TEXT,"Account Equity : "+DoubleToStr(AccountEquity(),2));

   double ChartRSI=iRSI(Symbol(),RSIApplyPeriod,RSIPeriod,PRICE_CLOSE,1);
   ObjectSetString(ChartID(),ObjName+"RSIValue",OBJPROP_TEXT,"RSI Value : "+DoubleToStr(ChartRSI,Digits));
   if(ChartRSI>RSILimit)
     {
      ObjectSetInteger(ChartID(),ObjName+"RSIValue",OBJPROP_COLOR,Lime);
     }
   else
     {
      ObjectSetInteger(ChartID(),ObjName+"RSIValue",OBJPROP_COLOR,Red);
     }

   ObjectSetString(ChartID(),ObjName+"fastEMA",OBJPROP_TEXT,"Fast EMA : "+DoubleToStr(FastEMA,Digits));
   ObjectSetString(ChartID(),ObjName+"slowEMA",OBJPROP_TEXT,"Slow EMA : "+DoubleToStr(SlowEMA,Digits));
   ObjectSetString(ChartID(),ObjName+"fastLWMA",OBJPROP_TEXT,"Fast LWMA : "+DoubleToStr(FastLWMA,Digits));
   ObjectSetString(ChartID(),ObjName+"slowLWMA",OBJPROP_TEXT,"Slow LWMA : "+DoubleToStr(SlowLWMA,Digits));

  }
//+------------------------------------------------------------------+
// End of UpdateChartInfo()                                          |
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//| Start of LogInfo()                                                                  |
//+------------------------------------------------------------------+

void LogInfo()
  {

   int  spread=(int)MarketInfo(Symbol(),MODE_SPREAD);
   string todayTime=TimeToString(TimeCurrent(),TIME_DATE);
   int file_handle=FileOpen("LondonBreakOut//LondonBreakOutLog"+todayTime+".txt",FILE_READ|FILE_WRITE|FILE_TXT,';');

   if(file_handle!=INVALID_HANDLE)
     {
      PrintFormat("Information are being logged,File path:\\Files\\LondonBreakOut\\LondonBreakOutLog.txt",TerminalInfoString(TERMINAL_DATA_PATH));
      FileSeek(file_handle,0,SEEK_END);
      FileWrite(file_handle,"  ----------------------------------"+TimeCurrent()+"  ------------------------------------------------");
      FileWrite(file_handle,"  Account balance = "+AccountBalance());
      FileWrite(file_handle,"  Account equity = "+AccountEquity());
      FileWrite(file_handle,"  Account margin = "+AccountMargin());
      FileWrite(file_handle,"  Account free margin = "+AccountFreeMargin());
      FileWrite(file_handle,"  Spread = "+spread);
      FileWrite(file_handle,"  Symbol = "+Symbol());
      FileWrite(file_handle,"  Timeframe = "+Period());
      FileWrite(file_handle,"  Cycle Lots  = "+cycleLots);
      FileWrite(file_handle,"  Cycle Profit  = "+cycleProfit);
      FileWrite(file_handle,"  Closed Positions Profit  = "+lastClosedProfit);
      FileWrite(file_handle,"  Net Profit  = "+netProfit);
      FileWrite(file_handle,"  ---------------------------------------------------------------------------------");
      //--- close the file
      FileClose(file_handle);
      Print("Data is written,file is closed");
     }
   else
      PrintFormat("Failed to open file, Error code = %d",GetLastError());

  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//|End of LogInfo()                                                             |
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//| Start of CheckSLTPExit()                                         |
//+------------------------------------------------------------------+

void CheckSLTPExit()
  {

   for(int i=Orders.Total()-1; i>=0; i--)
     {
      if(OrderSelect(Orders.At(i),SELECT_BY_TICKET,MODE_TRADES)==true && OrderSymbol()==Symbol() && (OrderMagicNumber()==MagicNumber))
        {
         if(OrderCloseTime()!=0)
           {
            if(OrderType()==OP_BUY || OrderType()==OP_SELL) //
              {

               if(OrderProfit()<=0 && MultiplyLot)
                 {
                  positonLotMultiplier*=LotMultiplier;
                 }
               else if(OrderProfit()>0 && MultiplyLot)
                 {
                  positonLotMultiplier=1;
                 }
               lastClosedProfit+=OrderProfit();
               Orders.Delete(i);
              }
           }
        }
     }

  }
//+------------------------------------------------------------------+
//| End of CheckSLTPExit()                                         |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//| Start of isNewBar()                                         
//+------------------------------------------------------------------+
bool isNewBar()
  {
//--- memorize the time of opening of the last bar in the static variable
   static datetime last_time=0;
//--- current time
   datetime lastbar_time=SeriesInfoInteger(Symbol(),Period(),SERIES_LASTBAR_DATE);

//--- if it is the first call of the function
   if(last_time==0)
     {
      //--- set the time and exit
      last_time=lastbar_time;
      return(false);
     }

//--- if the time differs
   if(last_time!=lastbar_time)
     {
      //--- memorize the time and return true
      last_time=lastbar_time;
      return(true);
     }
//--- if we passed to this line, then the bar is not new; return false
   return(false);
  }
//+------------------------------------------------------------------+
//| End of isNewBar()                                         
//+------------------------------------------------------------------+
