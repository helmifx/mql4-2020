//+------------------------------------------------------------------+
//|                                                      ProjectName |
//|                                      Copyright 2012, CompanyName |
//|                                       http://www.companyname.net |
//+------------------------------------------------------------------+
#property copyright "Abd Alrahman Loulou"
#property strict
#property description "Hedge"
#property version "1.1"


#include <Arrays\ArrayInt.mqh>
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
enum ENTRYMETHOD
  {

   RSI=0,// RSI;Auto
   Buttons=1,// Buttons;Manual

  };
//+------------------------------------------------------------------+
//| Setup                                               
//+------------------------------------------------------------------+
extern string  Header1="*************************************"; // ----------Trading Rules -----------
extern bool    AutomaticEntry=true; // Launch a cycle automatically
extern string  Header3="*************************************"; //----------Lots -----------
extern double  Lot=0.01; // Initial Lot
extern string  Header2="*************************************"; //----------SL Settings-----------
extern double  FixedStopLoss=45; // SL: Fixed Stop Loss in Pips.
extern double  StopLossAdjust=2; // SL_Adjust : Adjusting oposite order when SL is hit.
extern string  Header4="*************************************"; //----------TP Settings-----------
extern double  FixedTakeProfit=90; // TP: Fixed Take Profit in Pips.
extern string  Header9="*************************************"; // ----------Chart Info -----------
extern int     xDistance=1; //Chart info distance from top right corner
extern string  Header10="*************************************"; // ----------EA General -----------
extern bool    OpenOnStart=true; // Launch a cycle on expert start
extern int     MagicNumber=656668;
extern bool    OnJournaling=true; // Add EA updates in the Journal Tab

string  InternalHeader1="----------Errors Handling Settings-----------";

int     RetryInterval=100; // Pause Time before next retry (in milliseconds)
int     MaxRetriesPerTick=10;

string  InternalHeader2="----------Service Variables-----------";

//+------------------------------------------------------------------+
//|General Variables                                                                 |
//+------------------------------------------------------------------+

int P,YenPairAdjustFactor;

CArrayInt Orders;

int OrderNumber;

int  Slippage=3; // ignored

int BuyOrder;
int SellOrder;

//+------------------------------------------------------------------+
//|Chart Display Variables                                                                  |
//+------------------------------------------------------------------+

double buyLastClosedProfit=0;
double sellLastClosedProfit=0;
double cycleProfit; //Get All Opened Profit
double cycleProfitSince;
double buyCycleProfit;
double sellCycleProfit;
double cycleLots;
double buyCycleLots;    //Get All Buy  Opened Lots
double sellCycleLots;    //Get All Sell Opened Lots
double netProfit;    // All the profit of all opened positions by the same MagicNumber
string ObjName=IntegerToString(ChartID(),0,' ');   //The global object name 

//+------------------------------------------------------------------+
//|//Pip Display Variables                                           |
//+------------------------------------------------------------------+
double pipValue;
double buyPipValue;
double sellPipValue;
double pipValue1Lot;
//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
int init()
  {

   P=GetP(); // To account for 5 digit brokers. Used to convert pips to decimal place
   YenPairAdjustFactor=GetYenAdjustFactor(); // Adjust for YenPair
   ChartSettings();
   DrawChartInfo();            //Drawing Chart Info

   DrawButton("Start",Green,1,60,CORNER_LEFT_LOWER,"Open Buy and Sell Orders","Start",200,60,16,Black);
   DrawButton("Stop",Red,210,60,CORNER_LEFT_LOWER,"Close All","Stop",200,60,16,Black);

   if(AutomaticEntry)
     {
      StartCycle();
     }

   return(INIT_SUCCEEDED);


  }
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+

void OnDeinit(const int reason)
  {
//---
   if(reason!=5 && reason!=3)
     {
      ObjectsDeleteAll(0,0,OBJ_LABEL);
      ObjectsDeleteAll(0,0,OBJ_RECTANGLE_LABEL);
      ObjectsDeleteAll(0,0,OBJ_ARROW);
      ObjectsDeleteAll(0,0,OBJ_HLINE);
      ObjectsDeleteAll(0,0,OBJ_TREND);
      ObjectsDeleteAll(ChartID(),0);
     }
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void OnChartEvent(const int id,
                  const long &lparam,
                  const double &dparam,
                  const string &sparam)
  {
   if(id==CHARTEVENT_OBJECT_CLICK)
     {
      string clickedChartObject=sparam;
      bool expertOnSelected;
      if(clickedChartObject==ObjName+"Start")
        {
         StartCycle();
        }

      if(clickedChartObject==ObjName+"Stop")
        {
         if(MessageBox("Are you sure you want to close all orders?","Warning",MB_OKCANCEL)==1)
           {
            BuyOrder =0;
            SellOrder=0;
            CloseAllPositions();

           }
        }
     }
  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
int start()
  {

   if(MQLInfoInteger(MQL_TESTER)) //if in test mode.
     {
      ApplyTestMode();
     }

   cycleProfit=GetCycleProfit(0)+GetCycleProfit(1);               //Set Cycle Profits onto the chart.
   buyCycleProfit=GetCycleProfit(0);
   sellCycleProfit=GetCycleProfit(1);
   buyCycleLots=GetCycleLots(0);                //Set Cycle Lots onto the chart.
   sellCycleLots=GetCycleLots(1);
   cycleLots=GetCycleLots(0)+GetCycleLots(1);
   pipValue=(((MarketInfo(Symbol(),MODE_TICKVALUE)*Point*P)/MarketInfo(Symbol(),MODE_TICKSIZE))*cycleLots);// pip value of 1 lot
   buyPipValue=(((MarketInfo(Symbol(),MODE_TICKVALUE)*Point*P)/MarketInfo(Symbol(),MODE_TICKSIZE))*buyCycleLots);// pip value of 1 lot
   sellPipValue=(((MarketInfo(Symbol(),MODE_TICKVALUE)*Point*P)/MarketInfo(Symbol(),MODE_TICKSIZE))*sellCycleLots);// pip value of 1 lot
   pipValue1Lot=(((MarketInfo(Symbol(),MODE_TICKVALUE)*Point*P)/MarketInfo(Symbol(),MODE_TICKSIZE))*1);// pip value of 1 lot
   netProfit=cycleProfit+buyLastClosedProfit+sellLastClosedProfit;
   UpdateChartInfo();

   CheckSLTPExit();

   if(BuyOrder==0 && SellOrder==0 && AutomaticEntry)
     {
      StartCycle();
     }

   return (0);
  }
//+------------------------------------------------------------------+   
//|End of Start()                                                        |
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
// Start of CheckLot()                                       				|
//+------------------------------------------------------------------+

double CheckLot(double lot,bool Journaling)
  {
// Type: Fixed Template 
// Do not edit unless you know what you're doing

// This function checks if our Lots to be trade satisfies any broker limitations

   double LotToOpen=0;
   LotToOpen=NormalizeDouble(lot,2);
   LotToOpen=MathFloor(LotToOpen/MarketInfo(Symbol(),MODE_LOTSTEP))*MarketInfo(Symbol(),MODE_LOTSTEP);

   if(LotToOpen<MarketInfo(Symbol(),MODE_MINLOT))LotToOpen=MarketInfo(Symbol(),MODE_MINLOT);
   if(LotToOpen>MarketInfo(Symbol(),MODE_MAXLOT))LotToOpen=MarketInfo(Symbol(),MODE_MAXLOT);
   LotToOpen=NormalizeDouble(LotToOpen,2);

   if(Journaling && LotToOpen!=lot)Print("EA Journaling: Trading Lot has been changed by CheckLot function. Requested lot: "+DoubleToString(Lot)+". Lot to open: "+DoubleToString(LotToOpen));

   return(LotToOpen);
  }
//+------------------------------------------------------------------+
//| End of CheckLot()
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//| Start of GetCycleLots()                                                             |
//+------------------------------------------------------------------+
double GetCycleLots(int type)
  {

// Type: Customisable 
// Modify this function to suit your trading robot

// this function returns the net profit of the opened orders 

   double Lots=0;

   for(int i=0; i<OrdersTotal(); i++)
     {
      if(OrderSelect(i,SELECT_BY_POS,MODE_TRADES)==true && OrderSymbol()==Symbol() && (OrderMagicNumber()==MagicNumber) && OrderType()==type)
         Lots+=OrderLots();
     }
   return(NormalizeDouble(Lots,2));

  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void StartCycle()
  {
   OrderNumber=OpenPositionMarket(OP_BUY,Lot,FixedStopLoss,FixedTakeProfit,MagicNumber,Slippage,OnJournaling,P,MaxRetriesPerTick);
   Orders.Add(OrderNumber);
   BuyOrder=OrderNumber;
   OrderNumber=OpenPositionMarket(OP_SELL,Lot,FixedStopLoss,FixedTakeProfit,MagicNumber,Slippage,OnJournaling,P,MaxRetriesPerTick);
   Orders.Add(OrderNumber);
   SellOrder=OrderNumber;
  }
//+------------------------------------------------------------------+
// End of GetCycleLots()                                    		|
//+------------------------------------------------------------------+
double GetCycleProfit(int type)
  {

// Type: Customisable 
// Modify this function to suit your trading robot

// this function returns the net profit of the opened orders 

   double Profit=0;

   for(int i=0; i<OrdersTotal(); i++)
     {
      if(OrderSelect(i,SELECT_BY_POS,MODE_TRADES)==true && OrderSymbol()==Symbol() && (OrderMagicNumber()==MagicNumber) && OrderType()==type)
         Profit+=OrderProfit();
     }
   return(NormalizeDouble(Profit,1));
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
// End of GetCycleProfit()                                    			|
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|Start of CountPosOrders()	" Count Positions"
//+------------------------------------------------------------------+
int CountPosOrders(int Magic,int TYPE)
  {
// Type: Fixed Template 
// Do not edit unless you know what you're doing

// This function counts number of positions/orders of OrderType TYPE

   int orders=0;
   for(int i=0; i<OrdersTotal(); i++)
     {
      if(OrderSelect(i,SELECT_BY_POS,MODE_TRADES)==true && OrderSymbol()==Symbol() && OrderMagicNumber()==Magic && OrderType()==TYPE)
         orders++;
     }
   return(orders);

  }
//+------------------------------------------------------------------+
//| End of CountPosOrders() " Count Positions"
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|Start of OpenPositionMarket()
//+------------------------------------------------------------------+
int OpenPositionMarket(int TYPE,double LOT,double SL,double TP,int Magic,int Slip,bool Journaling,int K,int Max_Retries_Per_Tick)
  {
// Type: Fixed Template 
// Do not edit unless you know what you're doing 

// This function submits new orders

   int tries=0;
   string symbol=Symbol();
   int cmd=TYPE;
   double volume=CheckLot(LOT,Journaling);
   if(MarketInfo(symbol,MODE_MARGINREQUIRED)*volume>AccountFreeMargin())
     {
      Print("Can not open a trade. Not enough free margin to open "+volume+" on "+symbol);
      return(-1);
     }
   int slippage=Slip*K; // Slippage is in points. 1 point = 0.0001 on 4 digit broker and 0.00001 on a 5 digit broker
   string comment=" "+TYPE+"(#"+Magic+")";
   int magic=Magic;
   datetime expiration=0;
   color arrow_color=0;if(TYPE==OP_BUY)arrow_color=DodgerBlue;if(TYPE==OP_SELL)arrow_color=DeepPink;
   double stoploss=0;
   double takeprofit=0;
   double initTP = TP;
   double initSL = SL;
   int Ticket=-1;
   double price=0;

   while(tries<Max_Retries_Per_Tick) // Edits stops and take profits before the market order is placed
     {
      RefreshRates();
      if(TYPE==OP_BUY)price=Ask;if(TYPE==OP_SELL)price=Bid;

      // Sets Take Profits and Stop Loss. Check against Stop Level Limitations.
      if(TYPE==OP_BUY && SL!=0)
        {
         stoploss=NormalizeDouble(Ask-SL*K*Point,Digits);
         if(Bid-stoploss<=MarketInfo(Symbol(),MODE_STOPLEVEL)*Point)
           {
            stoploss=NormalizeDouble(Bid-MarketInfo(Symbol(),MODE_STOPLEVEL)*Point,Digits);
            if(Journaling)Print("EA Journaling: Stop Loss changed from "+initSL+" to "+MarketInfo(Symbol(),MODE_STOPLEVEL)/K+" pips");
           }
        }
      if(TYPE==OP_SELL && SL!=0)
        {
         stoploss=NormalizeDouble(Bid+SL*K*Point,Digits);
         if(stoploss-Ask<=MarketInfo(Symbol(),MODE_STOPLEVEL)*Point)
           {
            stoploss=NormalizeDouble(Ask+MarketInfo(Symbol(),MODE_STOPLEVEL)*Point,Digits);
            if(Journaling)Print("EA Journaling: Stop Loss changed from "+initSL+" to "+MarketInfo(Symbol(),MODE_STOPLEVEL)/K+" pips");
           }
        }
      if(TYPE==OP_BUY && TP!=0)
        {
         takeprofit=NormalizeDouble(Ask+TP*K*Point,Digits);
         if(takeprofit-Bid<=MarketInfo(Symbol(),MODE_STOPLEVEL)*Point)
           {
            takeprofit=NormalizeDouble(Ask+MarketInfo(Symbol(),MODE_STOPLEVEL)*Point,Digits);
            if(Journaling)Print("EA Journaling: Take Profit changed from "+initTP+" to "+MarketInfo(Symbol(),MODE_STOPLEVEL)/K+" pips");
           }
        }
      if(TYPE==OP_SELL && TP!=0)
        {
         takeprofit=NormalizeDouble(Bid-TP*K*Point,Digits);
         if(Ask-takeprofit<=MarketInfo(Symbol(),MODE_STOPLEVEL)*Point)
           {
            takeprofit=NormalizeDouble(Bid-MarketInfo(Symbol(),MODE_STOPLEVEL)*Point,Digits);
            if(Journaling)Print("EA Journaling: Take Profit changed from "+initTP+" to "+MarketInfo(Symbol(),MODE_STOPLEVEL)/K+" pips");
           }
        }
      if(Journaling)Print("EA Journaling: Trying to place a market order...");
      HandleTradingEnvironment(Journaling,RetryInterval);
      Ticket=OrderSend(symbol,cmd,volume,price,slippage,stoploss,takeprofit,comment,magic,expiration,arrow_color);
      if(Ticket>0)break;
      tries++;
     }

   if(Journaling && Ticket<0)Print("EA Journaling: Unexpected Error has happened. Error Description: "+GetErrorDescription(GetLastError()));
   if(Journaling && Ticket>0)
     {
      Print("EA Journaling: Order successfully placed. Ticket: "+Ticket);
     }
   return(Ticket);
  }
//+------------------------------------------------------------------+
//|End of OpenPositionMarket()
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|Start of CloseOrderPosition()
//+------------------------------------------------------------------+
bool CloseOrderPosition(int TYPE,bool Journaling,int Magic,int Slip,int K)
  {
// Type: Fixed Template 
// Do not edit unless you know what you're doing
   int ordersPos=OrdersTotal();

   for(int i=ordersPos-1; i>=0; i--)
     {
      // Note: Once pending orders become positions, OP_BUYLIMIT AND OP_BUYSTOP becomes OP_BUY, OP_SELLLIMIT and OP_SELLSTOP becomes OP_SELL
      if(TYPE==OP_BUY || TYPE==OP_SELL)
        {
         if(OrderSelect(i,SELECT_BY_POS,MODE_TRADES)==true && OrderSymbol()==Symbol() && OrderMagicNumber()==Magic && OrderType()==TYPE)
           {
            bool Closing=false;
            double Price=0;
            color arrow_color=0;if(TYPE==OP_BUY)arrow_color=MediumSeaGreen;if(TYPE==OP_SELL)arrow_color=DarkOrange;
            if(Journaling)Print("EA Journaling: Trying to close position "+OrderTicket()+" ...");
            HandleTradingEnvironment(Journaling,RetryInterval);
            if(TYPE==OP_BUY)Price=Bid; if(TYPE==OP_SELL)Price=Ask;
            Closing=OrderClose(OrderTicket(),OrderLots(),Price,Slip*K,arrow_color);
            if(Journaling && !Closing)Print("EA Journaling: Unexpected Error has happened. Error Description: "+GetErrorDescription(GetLastError()));
            if(Journaling && Closing)Print("EA Journaling: Position successfully closed.");
           }
        }
     }
   if(CountPosOrders(Magic, TYPE)==0)return(true); else return(false);
  }
//+------------------------------------------------------------------+
//| End of CloseOrderPosition()
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
// Start of getP()                                                   |
//+------------------------------------------------------------------+

int GetP()
  {

   int output;

   if(Digits==5 || Digits==3) output=10;else output=1;

   return(output);
  }
//+------------------------------------------------------------------+
// End of GetP()                                                    	|
//+------------------------------------------------------------------+


//+------------------------------------------------------------------+
// Start of GetYenAdjustFactor()                                     |
//+------------------------------------------------------------------+

int GetYenAdjustFactor()
  {

   int output=1;

   if(Digits==3 || Digits==2) output=100;

   return(output);

  }
//+------------------------------------------------------------------+
// End of GetYenAdjustFactor()                                       |
//+------------------------------------------------------------------+



//+------------------------------------------------------------------+
//| Start of HandleTradingEnvironment()                                         
//+------------------------------------------------------------------+
void HandleTradingEnvironment(bool Journaling,int Retry_Interval)
  {
// Type: Fixed Template 
// Do not edit unless you know what you're doing 

// This function checks for errors

   if(IsTradeAllowed()==true)return;
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
   if(!IsConnected())
     {
      if(Journaling)Print("EA Journaling: Terminal is not connected to server...");
      return;
     }
   if(!IsTradeAllowed() && Journaling)Print("EA Journaling: Trade is not alowed for some reason...");
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
   if(IsConnected() && !IsTradeAllowed())
     {
      while(IsTradeContextBusy()==true)
        {
         if(Journaling)Print("EA Journaling: Trading context is busy... Will wait a bit...");
         Sleep(Retry_Interval);
        }
     }
   RefreshRates();
  }
//+------------------------------------------------------------------+
//| End of HandleTradingEnvironment()                              
//+------------------------------------------------------------------+  
//+------------------------------------------------------------------+
//| Start of GetErrorDescription()                                               
//+------------------------------------------------------------------+
string GetErrorDescription(int error)
  {
// Type: Fixed Template 
// Do not edit unless you know what you're doing

// This function returns the exact error

   string ErrorDescription="";
//---
   switch(error)
     {
      case 0:     ErrorDescription = "NO Error. Everything should be good.";                                    break;
      case 1:     ErrorDescription = "No error returned, but the result is unknown";                            break;
      case 2:     ErrorDescription = "Common error";                                                            break;
      case 3:     ErrorDescription = "Invalid trade parameters";                                                break;
      case 4:     ErrorDescription = "Trade server is busy";                                                    break;
      case 5:     ErrorDescription = "Old version of the client terminal";                                      break;
      case 6:     ErrorDescription = "No connection with trade server";                                         break;
      case 7:     ErrorDescription = "Not enough rights";                                                       break;
      case 8:     ErrorDescription = "Too frequent requests";                                                   break;
      case 9:     ErrorDescription = "Malfunctional trade operation";                                           break;
      case 64:    ErrorDescription = "Account disabled";                                                        break;
      case 65:    ErrorDescription = "Invalid account";                                                         break;
      case 128:   ErrorDescription = "Trade timeout";                                                           break;
      case 129:   ErrorDescription = "Invalid price";                                                           break;
      case 130:   ErrorDescription = "Invalid stops";                                                           break;
      case 131:   ErrorDescription = "Invalid trade volume";                                                    break;
      case 132:   ErrorDescription = "Market is closed";                                                        break;
      case 133:   ErrorDescription = "Trade is disabled";                                                       break;
      case 134:   ErrorDescription = "Not enough money";                                                        break;
      case 135:   ErrorDescription = "Price changed";                                                           break;
      case 136:   ErrorDescription = "Off quotes";                                                              break;
      case 137:   ErrorDescription = "Broker is busy";                                                          break;
      case 138:   ErrorDescription = "Requote";                                                                 break;
      case 139:   ErrorDescription = "Order is locked";                                                         break;
      case 140:   ErrorDescription = "Long positions only allowed";                                             break;
      case 141:   ErrorDescription = "Too many requests";                                                       break;
      case 145:   ErrorDescription = "Modification denied because order too close to market";                   break;
      case 146:   ErrorDescription = "Trade context is busy";                                                   break;
      case 147:   ErrorDescription = "Expirations are denied by broker";                                        break;
      case 148:   ErrorDescription = "Too many open and pending orders (more than allowed)";                    break;
      case 4000:  ErrorDescription = "No error";                                                                break;
      case 4001:  ErrorDescription = "Wrong function pointer";                                                  break;
      case 4002:  ErrorDescription = "Array index is out of range";                                             break;
      case 4003:  ErrorDescription = "No memory for function call stack";                                       break;
      case 4004:  ErrorDescription = "Recursive stack overflow";                                                break;
      case 4005:  ErrorDescription = "Not enough stack for parameter";                                          break;
      case 4006:  ErrorDescription = "No memory for parameter string";                                          break;
      case 4007:  ErrorDescription = "No memory for temp string";                                               break;
      case 4008:  ErrorDescription = "Not initialized string";                                                  break;
      case 4009:  ErrorDescription = "Not initialized string in array";                                         break;
      case 4010:  ErrorDescription = "No memory for array string";                                              break;
      case 4011:  ErrorDescription = "Too long string";                                                         break;
      case 4012:  ErrorDescription = "Remainder from zero divide";                                              break;
      case 4013:  ErrorDescription = "Zero divide";                                                             break;
      case 4014:  ErrorDescription = "Unknown command";                                                         break;
      case 4015:  ErrorDescription = "Wrong jump (never generated error)";                                      break;
      case 4016:  ErrorDescription = "Not initialized array";                                                   break;
      case 4017:  ErrorDescription = "DLL calls are not allowed";                                               break;
      case 4018:  ErrorDescription = "Cannot load library";                                                     break;
      case 4019:  ErrorDescription = "Cannot call function";                                                    break;
      case 4020:  ErrorDescription = "Expert function calls are not allowed";                                   break;
      case 4021:  ErrorDescription = "Not enough memory for temp string returned from function";                break;
      case 4022:  ErrorDescription = "System is busy (never generated error)";                                  break;
      case 4050:  ErrorDescription = "Invalid function parameters count";                                       break;
      case 4051:  ErrorDescription = "Invalid function parameter value";                                        break;
      case 4052:  ErrorDescription = "String function internal error";                                          break;
      case 4053:  ErrorDescription = "Some array error";                                                        break;
      case 4054:  ErrorDescription = "Incorrect series array using";                                            break;
      case 4055:  ErrorDescription = "Custom indicator error";                                                  break;
      case 4056:  ErrorDescription = "Arrays are incompatible";                                                 break;
      case 4057:  ErrorDescription = "Global variables processing error";                                       break;
      case 4058:  ErrorDescription = "Global variable not found";                                               break;
      case 4059:  ErrorDescription = "Function is not allowed in testing mode";                                 break;
      case 4060:  ErrorDescription = "Function is not confirmed";                                               break;
      case 4061:  ErrorDescription = "Send mail error";                                                         break;
      case 4062:  ErrorDescription = "String parameter expected";                                               break;
      case 4063:  ErrorDescription = "Integer parameter expected";                                              break;
      case 4064:  ErrorDescription = "Double parameter expected";                                               break;
      case 4065:  ErrorDescription = "Array as parameter expected";                                             break;
      case 4066:  ErrorDescription = "Requested history data in updating state";                                break;
      case 4067:  ErrorDescription = "Some error in trading function";                                          break;
      case 4099:  ErrorDescription = "End of file";                                                             break;
      case 4100:  ErrorDescription = "Some file error";                                                         break;
      case 4101:  ErrorDescription = "Wrong file name";                                                         break;
      case 4102:  ErrorDescription = "Too many opened files";                                                   break;
      case 4103:  ErrorDescription = "Cannot open file";                                                        break;
      case 4104:  ErrorDescription = "Incompatible access to a file";                                           break;
      case 4105:  ErrorDescription = "No order selected";                                                       break;
      case 4106:  ErrorDescription = "Unknown symbol";                                                          break;
      case 4107:  ErrorDescription = "Invalid price";                                                           break;
      case 4108:  ErrorDescription = "Invalid ticket";                                                          break;
      case 4109:  ErrorDescription = "EA is not allowed to trade is not allowed. ";                             break;
      case 4110:  ErrorDescription = "Longs are not allowed. Check the expert properties";                      break;
      case 4111:  ErrorDescription = "Shorts are not allowed. Check the expert properties";                     break;
      case 4200:  ErrorDescription = "Object exists already";                                                   break;
      case 4201:  ErrorDescription = "Unknown object property";                                                 break;
      case 4202:  ErrorDescription = "Object does not exist";                                                   break;
      case 4203:  ErrorDescription = "Unknown object type";                                                     break;
      case 4204:  ErrorDescription = "No object name";                                                          break;
      case 4205:  ErrorDescription = "Object coordinates error";                                                break;
      case 4206:  ErrorDescription = "No specified subwindow";                                                  break;
      case 4207:  ErrorDescription = "Some error in object function";                                           break;
      default:    ErrorDescription = "No error or error is unknown";
     }
   return(ErrorDescription);
  }
//+------------------------------------------------------------------+
//| End of GetErrorDescription()                                         
//+------------------------------------------------------------------+


//+------------------------------------------------------------------+
//| Start of ChartSettings()                                         
//+------------------------------------------------------------------+
void ChartSettings()
  {
   ChartSetInteger(0,CHART_SHOW_GRID,0);
   ChartSetInteger(0,CHART_MODE,CHART_CANDLES);
   ChartSetInteger(0,CHART_AUTOSCROLL,0,True);
   WindowRedraw();
  }
//+------------------------------------------------------------------+
//| End of ChartSettings()                                         
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//| Start of CloseAllPositions()
//+------------------------------------------------------------------+ 
void CloseAllPositions()
  {
// Type: Fixed Template 
// Do not edit unless you know what you're doing

// This function closes all positions 

   CloseOrderPosition(OP_BUY,OnJournaling,MagicNumber,Slippage,P);

   CloseOrderPosition(OP_SELL,OnJournaling,MagicNumber,Slippage,P);

   return;

  }
//+------------------------------------------------------------------+
//| End of CloseAllPositions()
//+------------------------------------------------------------------+ 




//+------------------------------------------------------------------+
//|Start of DrawChartInfo                                                                  |
//+------------------------------------------------------------------+

void DrawChartInfo()
  {

   ObjectCreate(ChartID(),ObjName+"InfoBackground",OBJ_RECTANGLE_LABEL,0,0,0);
   ObjectSetInteger(ChartID(),ObjName+"InfoBackground",OBJPROP_XSIZE,-200);
   ObjectSetInteger(ChartID(),ObjName+"InfoBackground",OBJPROP_YSIZE,300);
   ObjectSetInteger(ChartID(),ObjName+"InfoBackground",OBJPROP_XDISTANCE,xDistance);
   ObjectSetInteger(ChartID(),ObjName+"InfoBackground",OBJPROP_YDISTANCE,10);
   ObjectSetInteger(ChartID(),ObjName+"InfoBackground",OBJPROP_CORNER,CORNER_RIGHT_UPPER);
   ObjectSetInteger(ChartID(),ObjName+"InfoBackground",OBJPROP_BGCOLOR,Black);
   ObjectSetInteger(ChartID(),ObjName+"InfoBackground",OBJPROP_BORDER_COLOR,Black);
   ObjectSetInteger(ChartID(),ObjName+"InfoBackground",OBJPROP_BACK,false);
   ObjectSetInteger(ChartID(),ObjName+"InfoBackground",OBJPROP_SELECTABLE,false);
   ObjectSetInteger(ChartID(),ObjName+"InfoBackground",OBJPROP_SELECTED,false);
   ObjectSetInteger(ChartID(),ObjName+"InfoBackground",OBJPROP_HIDDEN,false);
   ObjectSetInteger(ChartID(),ObjName+"InfoBackground",OBJPROP_BORDER_TYPE,BORDER_FLAT);

   ObjectCreate(ChartID(),ObjName+"Symbol",OBJ_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"Symbol",OBJPROP_TEXT,Symbol()+", S: "+DoubleToString((MarketInfo(Symbol(),MODE_SPREAD)/P),2)+", MN: "+MagicNumber);
   ObjectSetInteger(ChartID(),ObjName+"Symbol",OBJPROP_CORNER,CORNER_RIGHT_UPPER);
   ObjectSetInteger(ChartID(),ObjName+"Symbol",OBJPROP_XDISTANCE,1);
   ObjectSetInteger(ChartID(),ObjName+"Symbol",OBJPROP_YDISTANCE,37);
   ObjectSetString(ChartID(),ObjName+"Symbol",OBJPROP_FONT,"Berlin Sans FB");
   ObjectSetInteger(ChartID(),ObjName+"Symbol",OBJPROP_FONTSIZE,13);
   ObjectSetInteger(ChartID(),ObjName+"Symbol",OBJPROP_ANCHOR,ANCHOR_RIGHT);
   ObjectSetInteger(ChartID(),ObjName+"Symbol",OBJPROP_COLOR,Yellow);

   ObjectCreate(ChartID(),ObjName+"pipValue1Lot",OBJ_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"pipValue1Lot",OBJPROP_TEXT,"Pip Value Per Lot : "+DoubleToStr(pipValue1Lot,2));
   ObjectSetInteger(ChartID(),ObjName+"pipValue1Lot",OBJPROP_CORNER,CORNER_RIGHT_UPPER);
   ObjectSetInteger(ChartID(),ObjName+"pipValue1Lot",OBJPROP_XDISTANCE,1);
   ObjectSetInteger(ChartID(),ObjName+"pipValue1Lot",OBJPROP_YDISTANCE,62);
   ObjectSetString(ChartID(),ObjName+"pipValue1Lot",OBJPROP_FONT,"Berlin Sans FB");
   ObjectSetInteger(ChartID(),ObjName+"pipValue1Lot",OBJPROP_FONTSIZE,13);
   ObjectSetInteger(ChartID(),ObjName+"pipValue1Lot",OBJPROP_ANCHOR,ANCHOR_RIGHT);
   ObjectSetInteger(ChartID(),ObjName+"pipValue1Lot",OBJPROP_COLOR,White);

   ObjectCreate(ChartID(),ObjName+"BuyPipValue",OBJ_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"BuyPipValue",OBJPROP_TEXT,"Buy "+CountPosOrders(MagicNumber,OP_BUY)+" = "+DoubleToStr(buyCycleLots,2)+" L = "+DoubleToStr(buyPipValue,2)+"$");
   ObjectSetInteger(ChartID(),ObjName+"BuyPipValue",OBJPROP_CORNER,CORNER_RIGHT_UPPER);
   ObjectSetInteger(ChartID(),ObjName+"BuyPipValue",OBJPROP_XDISTANCE,xDistance);
   ObjectSetInteger(ChartID(),ObjName+"BuyPipValue",OBJPROP_YDISTANCE,87);
   ObjectSetInteger(ChartID(),ObjName+"BuyPipValue",OBJPROP_FONTSIZE,12);
   ObjectSetInteger(ChartID(),ObjName+"BuyPipValue",OBJPROP_ANCHOR,ANCHOR_RIGHT);
   ObjectSetInteger(ChartID(),ObjName+"BuyPipValue",OBJPROP_COLOR,DeepPink);
   ObjectSetString(ChartID(),ObjName+"BuyPipValue",OBJPROP_FONT,"Berlin Sans FB");

   ObjectCreate(ChartID(),ObjName+"SellPipValue",OBJ_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"SellPipValue",OBJPROP_TEXT,"Sell "+CountPosOrders(MagicNumber,OP_SELL)+" = "+DoubleToStr(sellCycleLots,2)+" L = "+DoubleToStr(sellPipValue,2)+"$");
   ObjectSetInteger(ChartID(),ObjName+"SellPipValue",OBJPROP_CORNER,CORNER_RIGHT_UPPER);
   ObjectSetInteger(ChartID(),ObjName+"SellPipValue",OBJPROP_XDISTANCE,xDistance);
   ObjectSetInteger(ChartID(),ObjName+"SellPipValue",OBJPROP_YDISTANCE,112);
   ObjectSetInteger(ChartID(),ObjName+"SellPipValue",OBJPROP_FONTSIZE,12);
   ObjectSetInteger(ChartID(),ObjName+"SellPipValue",OBJPROP_ANCHOR,ANCHOR_RIGHT);
   ObjectSetInteger(ChartID(),ObjName+"SellPipValue",OBJPROP_COLOR,MediumVioletRed);
   ObjectSetString(ChartID(),ObjName+"SellPipValue",OBJPROP_FONT,"Berlin Sans FB");

   ObjectCreate(ChartID(),ObjName+"Equity",OBJ_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"Equity",OBJPROP_TEXT,"Equity : "+DoubleToStr(AccountEquity(),2));
   ObjectSetInteger(ChartID(),ObjName+"Equity",OBJPROP_CORNER,CORNER_RIGHT_UPPER);
   ObjectSetInteger(ChartID(),ObjName+"Equity",OBJPROP_XDISTANCE,xDistance);
   ObjectSetInteger(ChartID(),ObjName+"Equity",OBJPROP_YDISTANCE,137);
   ObjectSetInteger(ChartID(),ObjName+"Equity",OBJPROP_FONTSIZE,12);
   ObjectSetInteger(ChartID(),ObjName+"Equity",OBJPROP_ANCHOR,ANCHOR_RIGHT);
   ObjectSetInteger(ChartID(),ObjName+"Equity",OBJPROP_COLOR,BurlyWood);
   ObjectSetString(ChartID(),ObjName+"Equity",OBJPROP_FONT,"Berlin Sans FB");

   ObjectCreate(ChartID(),ObjName+"ClosedBuyPositionsProfit",OBJ_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"ClosedBuyPositionsProfit",OBJPROP_TEXT,"Closed Buy "+DoubleToString(buyLastClosedProfit,2));
   ObjectSetInteger(ChartID(),ObjName+"ClosedBuyPositionsProfit",OBJPROP_CORNER,CORNER_RIGHT_UPPER);
   ObjectSetInteger(ChartID(),ObjName+"ClosedBuyPositionsProfit",OBJPROP_XDISTANCE,xDistance);
   ObjectSetInteger(ChartID(),ObjName+"ClosedBuyPositionsProfit",OBJPROP_YDISTANCE,162);
   ObjectSetInteger(ChartID(),ObjName+"ClosedBuyPositionsProfit",OBJPROP_FONTSIZE,12);
   ObjectSetInteger(ChartID(),ObjName+"ClosedBuyPositionsProfit",OBJPROP_ANCHOR,ANCHOR_RIGHT);
   ObjectSetInteger(ChartID(),ObjName+"ClosedBuyPositionsProfit",OBJPROP_COLOR,Blue);
   ObjectSetString(ChartID(),ObjName+"ClosedBuyPositionsProfit",OBJPROP_FONT,"Berlin Sans FB");

   ObjectCreate(ChartID(),ObjName+"ClosedSellPositionsProfit",OBJ_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"ClosedSellPositionsProfit",OBJPROP_TEXT,"Closed Sell "+DoubleToString(sellLastClosedProfit,2));
   ObjectSetInteger(ChartID(),ObjName+"ClosedSellPositionsProfit",OBJPROP_CORNER,CORNER_RIGHT_UPPER);
   ObjectSetInteger(ChartID(),ObjName+"ClosedSellPositionsProfit",OBJPROP_XDISTANCE,xDistance);
   ObjectSetInteger(ChartID(),ObjName+"ClosedSellPositionsProfit",OBJPROP_YDISTANCE,187);
   ObjectSetInteger(ChartID(),ObjName+"ClosedSellPositionsProfit",OBJPROP_FONTSIZE,12);
   ObjectSetInteger(ChartID(),ObjName+"ClosedSellPositionsProfit",OBJPROP_ANCHOR,ANCHOR_RIGHT);
   ObjectSetInteger(ChartID(),ObjName+"ClosedSellPositionsProfit",OBJPROP_COLOR,Chartreuse);
   ObjectSetString(ChartID(),ObjName+"ClosedSellPositionsProfit",OBJPROP_FONT,"Berlin Sans FB");

   ObjectCreate(ChartID(),ObjName+"CurrentBuyPositionsProfit",OBJ_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"CurrentBuyPositionsProfit",OBJPROP_TEXT,"Current Buy "+DoubleToString(buyCycleProfit,2));
   ObjectSetInteger(ChartID(),ObjName+"CurrentBuyPositionsProfit",OBJPROP_CORNER,CORNER_RIGHT_UPPER);
   ObjectSetInteger(ChartID(),ObjName+"CurrentBuyPositionsProfit",OBJPROP_XDISTANCE,xDistance);
   ObjectSetInteger(ChartID(),ObjName+"CurrentBuyPositionsProfit",OBJPROP_YDISTANCE,212);
   ObjectSetInteger(ChartID(),ObjName+"CurrentBuyPositionsProfit",OBJPROP_FONTSIZE,12);
   ObjectSetInteger(ChartID(),ObjName+"CurrentBuyPositionsProfit",OBJPROP_ANCHOR,ANCHOR_RIGHT);
   ObjectSetInteger(ChartID(),ObjName+"CurrentBuyPositionsProfit",OBJPROP_COLOR,GreenYellow);
   ObjectSetString(ChartID(),ObjName+"CurrentBuyPositionsProfit",OBJPROP_FONT,"Berlin Sans FB");

   ObjectCreate(ChartID(),ObjName+"CurrentSellPositionsProfit",OBJ_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"CurrentSellPositionsProfit",OBJPROP_TEXT,"Current Sell "+DoubleToString(sellCycleProfit,2));
   ObjectSetInteger(ChartID(),ObjName+"CurrentSellPositionsProfit",OBJPROP_CORNER,CORNER_RIGHT_UPPER);
   ObjectSetInteger(ChartID(),ObjName+"CurrentSellPositionsProfit",OBJPROP_XDISTANCE,xDistance);
   ObjectSetInteger(ChartID(),ObjName+"CurrentSellPositionsProfit",OBJPROP_YDISTANCE,237);
   ObjectSetInteger(ChartID(),ObjName+"CurrentSellPositionsProfit",OBJPROP_FONTSIZE,12);
   ObjectSetInteger(ChartID(),ObjName+"CurrentSellPositionsProfit",OBJPROP_ANCHOR,ANCHOR_RIGHT);
   ObjectSetInteger(ChartID(),ObjName+"CurrentSellPositionsProfit",OBJPROP_COLOR,IndianRed);
   ObjectSetString(ChartID(),ObjName+"CurrentSellPositionsProfit",OBJPROP_FONT,"Berlin Sans FB");

   ObjectCreate(ChartID(),ObjName+"NetProfit",OBJ_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"NetProfit",OBJPROP_TEXT,"Net Profit "+DoubleToString(netProfit,2));
   ObjectSetInteger(ChartID(),ObjName+"NetProfit",OBJPROP_CORNER,CORNER_RIGHT_UPPER);
   ObjectSetInteger(ChartID(),ObjName+"NetProfit",OBJPROP_XDISTANCE,xDistance);
   ObjectSetInteger(ChartID(),ObjName+"NetProfit",OBJPROP_YDISTANCE,262);
   ObjectSetInteger(ChartID(),ObjName+"NetProfit",OBJPROP_FONTSIZE,12);
   ObjectSetInteger(ChartID(),ObjName+"NetProfit",OBJPROP_ANCHOR,ANCHOR_RIGHT);
   ObjectSetInteger(ChartID(),ObjName+"NetProfit",OBJPROP_COLOR,DarkTurquoise);
   ObjectSetString(ChartID(),ObjName+"NetProfit",OBJPROP_FONT,"Berlin Sans FB");
  }
//+------------------------------------------------------------------+
// End of DrawChartInfo()                                          |
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
// Start of UpdateChartInfo()                                          |
//+------------------------------------------------------------------+
void UpdateChartInfo()
  {
   ObjectSetString(ChartID(),ObjName+"Symbol",OBJPROP_TEXT,Symbol()+", S: "+DoubleToString((MarketInfo(Symbol(),MODE_SPREAD)/P),2)+", MN: "+MagicNumber);
   ObjectSetString(ChartID(),ObjName+"pipValue1Lot",OBJPROP_TEXT,"Pip Value Per Lot : "+DoubleToStr(pipValue1Lot,2));
   ObjectSetString(ChartID(),ObjName+"ClosedBuyPositionsProfit",OBJPROP_TEXT,"Closed Buy "+DoubleToString(buyLastClosedProfit,2));
   ObjectSetString(ChartID(),ObjName+"ClosedSellPositionsProfit",OBJPROP_TEXT,"Closed Sell "+DoubleToString(sellLastClosedProfit,2));
   ObjectSetString(ChartID(),ObjName+"CurrentBuyPositionsProfit",OBJPROP_TEXT,"Current Buy "+DoubleToString(buyCycleProfit,2));
   ObjectSetString(ChartID(),ObjName+"CurrentSellPositionsProfit",OBJPROP_TEXT,"Current Sell "+DoubleToString(sellCycleProfit,2));
   ObjectSetString(ChartID(),ObjName+"BuyPipValue",OBJPROP_TEXT,"Buy "+CountPosOrders(MagicNumber,OP_BUY)+" = "+DoubleToStr(buyCycleLots,2)+" L = "+DoubleToStr(buyPipValue,2)+"$");
   ObjectSetString(ChartID(),ObjName+"SellPipValue",OBJPROP_TEXT,"Sell "+CountPosOrders(MagicNumber,OP_SELL)+" = "+DoubleToStr(sellCycleLots,2)+" L = "+DoubleToStr(sellPipValue,2)+"$");
   ObjectSetString(ChartID(),ObjName+"NetProfit",OBJPROP_TEXT,"Net Profit "+DoubleToString(netProfit,2));
   ObjectSetString(ChartID(),ObjName+"Equity",OBJPROP_TEXT,"Equity : "+DoubleToStr(AccountEquity(),2));
   ObjectSetString(ChartID(),ObjName+"Balance",OBJPROP_TEXT,"Balance : "+DoubleToStr(AccountBalance(),2));
  }
//+------------------------------------------------------------------+
// End of UpdateChartInfo()                                          |
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//| Start of CheckSLTPExit()                                         |
//+------------------------------------------------------------------+

void CheckSLTPExit()
  {

   for(int i=Orders.Total()-1; i>=0; i--)
     {
      if(OrderSelect(Orders.At(i),SELECT_BY_TICKET,MODE_TRADES)==true && OrderSymbol()==Symbol() && (OrderMagicNumber()==MagicNumber))
        {
         if(OrderCloseTime()!=0)
           {
            if(OrderType()==OP_BUY)
              {
               buyLastClosedProfit+=OrderProfit();
               Orders.Delete(i);

               if(OrderProfit()<0 && SellOrder!=0 && StopLossAdjust!=0)
                 {
                  if(OrderSelect(SellOrder,SELECT_BY_TICKET,MODE_TRADES)==true && OrderSymbol()==Symbol() && (OrderMagicNumber()==MagicNumber))
                    {
                     bool Modify;
                     double newSL=OrderOpenPrice()-((StopLossAdjust+(MarketInfo(Symbol(),MODE_SPREAD)/P))*Point*P);
                     Modify=OrderModify(OrderTicket(),OrderOpenPrice(),newSL,OrderTakeProfit(),0,CLR_NONE);
                     if(OnJournaling && !Modify)Print("EA Journaling: Unexpected Error has happened. Error Description: "+GetErrorDescription(GetLastError()));
                     if(OnJournaling && Modify) Print("EA Journaling: Order successfully modified");
                    }
                 }

               BuyOrder=0;
              }
            else if(OrderType()==OP_SELL)
              {
               sellLastClosedProfit+=OrderProfit();
               Orders.Delete(i);

               if(OrderProfit()<0 && BuyOrder!=0 && StopLossAdjust!=0)
                 {
                  if(OrderSelect(BuyOrder,SELECT_BY_TICKET,MODE_TRADES)==true && OrderSymbol()==Symbol() && (OrderMagicNumber()==MagicNumber))
                    {
                     bool Modify;
                     double newSL=OrderOpenPrice()+(StopLossAdjust*Point*P);
                     Modify=OrderModify(OrderTicket(),OrderOpenPrice(),newSL,OrderTakeProfit(),0,CLR_NONE);
                     if(OnJournaling && !Modify)Print("EA Journaling: Unexpected Error has happened. Error Description: "+GetErrorDescription(GetLastError()));
                     if(OnJournaling && Modify) Print("EA Journaling: Order successfully modified");
                    }
                 }
               SellOrder=0;
              }
           }

        }
     }
  }
//+------------------------------------------------------------------+
//| End of CheckSLTPExit()                                         |
//+------------------------------------------------------------------+
void DrawButton(string name,color clr,int x,int y,ENUM_BASE_CORNER c,string tooltip,string text,int xsize,int ysize,int fontSize,color textColor)
  {
//---
   name=ObjName+name;
   int windows=0;
   ObjectDelete(name);
   ObjectCreate(name,OBJ_BUTTON,windows,0,0);
   ObjectSet(name,OBJPROP_BGCOLOR,clr);
   ObjectSetString(0,name,OBJPROP_TEXT,text);
   ObjectSet(name,OBJPROP_COLOR,textColor);
   ObjectSet(name,OBJPROP_BORDER_COLOR,White);
   ObjectSet(name,OBJPROP_BORDER_TYPE,BORDER_FLAT);
   ObjectSetString(0,name,OBJPROP_FONT,"Berlin Sans FB Demi Bold");
   ObjectSet(name,OBJPROP_FONTSIZE,fontSize);
   ObjectSet(name,OBJPROP_XSIZE,xsize);
   ObjectSet(name,OBJPROP_YSIZE,ysize);
   ObjectSet(name,OBJPROP_CORNER,c);
   ObjectSet(name,OBJPROP_XDISTANCE,x);
   ObjectSet(name,OBJPROP_YDISTANCE,y);
   ObjectSet(name,OBJPROP_WIDTH,0);
   ObjectSetString(0,name,OBJPROP_TOOLTIP,tooltip);
   ObjectSet(name,OBJPROP_SELECTABLE,0);
//---
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
void ApplyTestMode()
  {
   if(ObjectGetInteger(0,ObjName+"Start",OBJPROP_STATE)==true)
     {
      StartCycle();
      DrawButton("Start",Green,1,60,CORNER_LEFT_LOWER,"Open 1Buy,1Sell Orders","Start",200,60,16,Black);

     }

   if(ObjectGetInteger(0,ObjName+"Stop",OBJPROP_STATE)==true)
     {
      BuyOrder=0;
      SellOrder=0;
      CloseAllPositions();
      DrawButton("Stop",Red,210,60,CORNER_LEFT_LOWER,"Close All","Stop",200,60,16,Black);
     }

  }
//+------------------------------------------------------------------+
