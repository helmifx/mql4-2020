//+------------------------------------------------------------------+
//|                                                  PanelDialog.mqh |
//|                   Copyright 2009-2015, MetaQuotes Software Corp. |
//|                                              http://www.mql5.com |
//+------------------------------------------------------------------+
#include <Controls\Dialog.mqh>
#include <Controls\Button.mqh>
#include <Controls\Panel.mqh>
#include <Controls\Edit.mqh>
#include <Controls\Label.mqh>
#include <Controls\ListView.mqh>
#include <Controls\ComboBox.mqh>
#include <Controls\SpinEdit.mqh>
#include <Controls\RadioGroup.mqh>
#include <Controls\CheckGroup.mqh>
#include <Controls\Picture.mqh>
#include <Controls\Rect.mqh>
//+------------------------------------------------------------------+
//| defines                                                          |
//+------------------------------------------------------------------+
//--- indents and gaps
#define INDENT_LEFT                         (11)      // indent from left (with allowance for border width)
#define INDENT_TOP                          (11)      // indent from top (with allowance for border width)
#define INDENT_RIGHT                        (11)      // indent from right (with allowance for border width)
#define INDENT_BOTTOM                       (11)      // indent from bottom (with allowance for border width)
#define CONTROLS_GAP_X                      (10)      // gap by X coordinate
#define CONTROLS_GAP_Y                      (10)      // gap by Y coordinate
//--- for buttons
#define BUTTON_WIDTH                        (60)     // size by X coordinate
#define BUTTON_HEIGHT                       (20)      // size by Y coordinate
//--- for the indication area
#define EDIT_HEIGHT                         (20)      // size by Y coordinate
//--- for group controls
#define GROUP_WIDTH                         (150)     // size by X coordinate
#define GROUP_HEIGHT                        (57)      // size by Y coordinate

#define TEXT_COLOR 								C'191,181,88' //C'115,106,78' 

#define BORDER_COLOR 							C'138,134,91' 
#define LOCK_BUTTON_COLOUR						C'138,134,91'
//#define PANEL_COLOUR 							C'89,97,125'
//#define PANEL_COLOUR 							C'51,51,45'
#define PANEL_COLOUR 							C'115,115,110'
#define BASEPANEL_COLOUR 						C'64,64,61'

#define TEXT_FONT									"Segoe UI Semibold"

#resource "Save3.bmp"
#resource "Save2.bmp"
#resource "divider.bmp"
//+------------------------------------------------------------------+
//| Class CPanelDialog                                               |
//| Usage: main dialog of the SimplePanel application                |
//+------------------------------------------------------------------+
class CPanelDialog : public CAppDialog
  {
private:
/* CCheckGroup       m_check_group;                   // the CheckGroup object
    CRadioGroup       m_radio_group;                   // the radio buttons group object
    CButton           m_button_notification;                       // the button object*/

   CBmpButton        m_button_hedge_plus;
   CBmpButton        m_button_hedge_minus;
   CBmpButton        m_button_lot_plus;
   CBmpButton        m_button_lot_minus;
   CBmpButton        m_button_pipstep_plus;
   CBmpButton        m_button_pipstep_minus;
   CBmpButton        m_button_takeprofit_plus;
   CBmpButton        m_button_takeprofit_minus;
   CLabel            m_label1;                           //a label
   CLabel            m_label2;
   CLabel            m_label3;
   CLabel            m_label4;
   CLabel            m_label_lot;
   CLabel            m_label_pipstep;
   CLabel            m_label_takeprofit;
   CEdit             m_edit1;
   CEdit             m_edit2;
   CPicture          m_picture;
   CPanel            m_panelbase;
   CPanel            m_panel1;
   CPanel            m_panel2;
   // bool              mMail;
   //  bool              mPush;
   //  bool              mAlert_;

   double            mLots;
   int               mPipStep;
   double            mTakeProfit;
   int               mNumberOfOrders;
   double            mHedgeLotType;
   int               mMagicNumber;
   bool              mAveragePricePrint;
   bool              mModification;       // Values have changed

   //--- get check for element
   virtual int       GetCheck(const int idx);
public:
                     CPanelDialog(void);
                    ~CPanelDialog(void);
   //--- create
   virtual bool      Create(const long chart,const string name,const int subwin,const int x1,const int y1,const int x2,const int y2);
   //--- chart event handler
   virtual bool      OnEvent(const int id,const long &lparam,const double &dparam,const string &sparam);

   virtual bool      Initialization(const int NumberOfOrders,const int Magic,const bool AveragePricePrint,const double HedgeLotType,const int PipStep,const double Lot,const double TakeProfit);
   virtual void      GetValues(int &NumberOfOrders,int &Magic,bool &AveragePricePrint,double &HedgeLotType,int &PipStep,double &Lot,double &TakeProfit);

   virtual bool      Modification(void) const { return(mModification);          }
   virtual void      Modification(bool value) { mModification=value;            }
   bool              mApplyHedge;
   bool              mApplyClose;
   string            GetEdit1Text() { return m_edit1.Text();}
   string            GetEdit2Text() { return m_edit2.Text();}
   string            GetEditHedgeText() { return m_edit_hedge.Text();}
   string            GetEditLotText() { return m_edit_lot.Text();}
   string            GetEditPipStepText() { return m_edit_pipstep.Text();}
   string            GetEditTakeProfitText() { return m_edit_takeprofit.Text();}
   bool              isButton1Pressed(void){ return m_button1.Pressed();}
   bool              isButton2Pressed(void){ return m_button2.Pressed();}
   bool              isButton3Pressed(void){ return m_button3.Pressed();}
   bool              isButton4Pressed(void){ return m_button4.Pressed();}
   bool              isButtonHedgePlusPressed(void) { return m_button_hedge_plus.Pressed();}
   bool              isButtonHedgeMinusPressed(void) { return m_button_hedge_minus.Pressed();}
   bool              isButtonPipStepPlusPressed(void) { return m_button_pipstep_plus.Pressed();}
   bool              isButtonPipStepMinusPressed(void) { return m_button_pipstep_minus.Pressed();}
   bool              isButtonLotPlusPressed(void) { return m_button_lot_plus.Pressed();}
   bool              isButtonLotMinusPressed(void) { return m_button_lot_minus.Pressed();}
   bool              isButtonTakeProfitPlusPressed(void) { return m_button_takeprofit_plus.Pressed();}
   bool              isButtonTakeProfitMinusPressed(void) { return m_button_takeprofit_minus.Pressed();}
   bool              isButtonModifyPressed(void) { return m_button_modify.Pressed();}

   void              ButtonModifyPressed(bool state);
   void              Button1Pressed(bool state);
   void              Button2Pressed(bool state);
   void              Button3Pressed(bool state);
   void              Button4Pressed(bool state);
   void              ButtonHedgePlusPressed(bool state);
   void              ButtonHedgeMinusPressed(bool state);
   void              ButtonPipStepPlusPressed(bool state);
   void              ButtonPipStepMinusPressed(bool state);
   void              ButtonLotPlusPressed(bool state);
   void              ButtonLotMinusPressed(bool state);
   void              ButtonTakeProfitPlusPressed(bool state);
   void              ButtonTakeProfitMinusPressed(bool state);

   string            HedgeTypesNames[3];
   double            HedgeTypesValues[3];
   int               HedgeTypesArrayIndex;

   CEdit             m_edit_hedge;
   CEdit             m_edit_lot;
   CEdit             m_edit_pipstep;
   CEdit             m_edit_takeprofit;

   CButton           m_button1;                       // the button object
   CButton           m_button2;                       // the button object
   CButton           m_button3;                       // the fixed button object
   CButton           m_button4;                       // the fixed button object
   CBmpButton        m_button_modify;

protected:
   //--- create dependent controls
   bool              CreatePanel1(void);
   bool              CreatePanel2(void);
   bool 					CreateBasePanel(void);
   bool              CreateButtonModify(void);
   bool              CreateButton1(void);
   bool              CreateButton2(void);
   bool              CreateButton3(void);
   bool              CreateButton4(void);
   bool              CreateButtonHedgePlus(void);
   bool              CreateButtonHedgeMinus(void);
   bool              CreateButtonLotPlus(void);
   bool              CreateButtonLotMinus(void);
   bool              CreateButtonPipStepPlus(void);
   bool              CreateButtonPipStepMinus(void);
   bool              CreateButtonTakeProfitPlus(void);
   bool              CreateButtonTakeProfitMinus(void);
   bool              CreateLabel1(void);
   bool              CreateLabel2(void);
   bool              CreateLabel3(void);
   bool              CreateLabel4(void);
   bool              CreateLabelLot(void);
   bool              CreateLabelPipStep(void);
   bool              CreateLabelTakeProfit(void);
   bool              CreateEdit1(void);
   bool              CreateEdit2(void);
   bool              CreateEditHedge(void);
   bool              CreateEditLot(void);
   bool              CreateEditPipStep(void);
   bool              CreateEditTakeProfit(void);

   //--- internal event handlers
   // virtual bool      OnResize(void);
   //--- set check for element
   bool              SetCheck(const int idx,const bool check);
   //--- handlers of the dependent controls events
   // void              OnChangeCheckGroup(void);
   // void              OnClickButtonNotification(void);
   void              OnClickButtonModify(void);
   void              OnClickButton1(void);
   void              OnClickButton2(void);
   void              OnClickButton3(void);
   void              OnClickButton4(void);
   void              OnClickButtonHedgePlus(void);
   void              OnClickButtonHedgeMinus(void);
   void              OnClickButtonLotPlus(void);
   void              OnClickButtonLotMinus(void);
   void              OnClickButtonPipStepPlus(void);
   void              OnClickButtonPipStepMinus(void);
   void              OnClickButtonTakeProfitPlus(void);
   void              OnClickButtonTakeProfitMinus(void);
   void              OnChangeEdit1(void);
   void              OnChangeEdit2(void);
   void              OnChangeEditPipStep(void);
   void              OnChangeEditLot(void);
   void              OnChangeEditTakeProfit(void);
   //   bool              OnDefault(const int id,const long &lparam,const double &dparam,const string &sparam);

  };
//+------------------------------------------------------------------+
//| Event Handling                                                   |
//+------------------------------------------------------------------+
EVENT_MAP_BEGIN(CPanelDialog)

ON_EVENT(ON_CLICK,m_button_modify,OnClickButtonModify)
ON_EVENT(ON_CLICK,m_button1,OnClickButton1)
ON_EVENT(ON_CLICK,m_button2,OnClickButton2)
ON_EVENT(ON_CLICK,m_button3,OnClickButton3)
ON_EVENT(ON_CLICK,m_button_hedge_plus,OnClickButtonHedgePlus)
ON_EVENT(ON_CLICK,m_button_hedge_minus,OnClickButtonHedgeMinus)
ON_EVENT(ON_CLICK,m_button_lot_plus,OnClickButtonLotPlus)
ON_EVENT(ON_CLICK,m_button_lot_minus,OnClickButtonLotMinus)
ON_EVENT(ON_CLICK,m_button_pipstep_plus,OnClickButtonPipStepPlus)
ON_EVENT(ON_CLICK,m_button_pipstep_minus,OnClickButtonPipStepMinus)
ON_EVENT(ON_CLICK,m_button_takeprofit_plus,OnClickButtonTakeProfitPlus)
ON_EVENT(ON_CLICK,m_button_takeprofit_minus,OnClickButtonTakeProfitMinus)
ON_EVENT(ON_CLICK,m_button4,OnClickButton4)
ON_EVENT(ON_CLICK,m_button4,OnClickButton4)
ON_EVENT(ON_END_EDIT,m_edit1,OnChangeEdit1)
ON_EVENT(ON_END_EDIT,m_edit2,OnChangeEdit2)
ON_EVENT(ON_END_EDIT,m_edit_pipstep,OnChangeEditPipStep)
ON_EVENT(ON_END_EDIT,m_edit_lot,OnChangeEditLot)
ON_EVENT(ON_END_EDIT,m_edit_takeprofit,OnChangeEditTakeProfit)
EVENT_MAP_END(CAppDialog)
//+------------------------------------------------------------------+
//| Constructor                                                      |
//+------------------------------------------------------------------+
CPanelDialog::CPanelDialog(void):mModification(false),mNumberOfOrders(5),mMagicNumber(656668),mAveragePricePrint(false),mHedgeLotType(1),mApplyHedge(false),mApplyClose(false)
  {
   HedgeTypesNames[0] = "Full";
   HedgeTypesNames[1] = "Half";
   HedgeTypesNames[2] = "Quartar";

   HedgeTypesValues[0] = 1;
   HedgeTypesValues[1] = 0.5;
   HedgeTypesValues[2] = 0.25;

   HedgeTypesArrayIndex=0;

  }
//+------------------------------------------------------------------+
//| Destructor                                                       |
//+------------------------------------------------------------------+
CPanelDialog::~CPanelDialog(void)
  {
  }
//+------------------------------------------------------------------+
//| Create                                                           |
//+------------------------------------------------------------------+
bool CPanelDialog::Create(const long chart,const string name,const int subwin,const int x1,const int y1,const int x2,const int y2)
  {
   if(!CAppDialog::Create(chart,name,subwin,x1,y1,x2,y2))
      return(false);
   if(!CreateBasePanel())
      return (false);
   if(!CreatePanel1())
      return (false);
   if(!CreatePanel2())
      return (false);
   if(!CreateButtonModify())
      return(false);
   if(!CreateButton1())
      return(false);
   if(!CreateButton2())
      return(false);
   if(!CreateButton3())
      return(false);
   if(!CreateButton4())
      return(false);
   if(!CreateEditLot())
      return(false);
   if(!CreateEditPipStep())
      return(false);
   if(!CreateEditTakeProfit())
      return(false);
   if(!CreateEditHedge())
      return(false);
   if(!CreateButtonHedgePlus())
      return(false);
   if(!CreateButtonHedgeMinus())
      return(false);
   if(!CreateButtonLotPlus())
      return(false);
   if(!CreateButtonLotMinus())
      return(false);
   if(!CreateButtonPipStepPlus())
      return(false);
   if(!CreateButtonPipStepMinus())
      return(false);
   if(!CreateButtonTakeProfitPlus())
      return(false);
   if(!CreateButtonTakeProfitMinus())
      return(false);
   if(!CreateLabel1())
      return(false);
   if(!CreateLabel2())
      return(false);
   if(!CreateLabelLot())
      return(false);
   if(!CreateLabelPipStep())
      return(false);
   if(!CreateLabelTakeProfit())
      return(false);
   if(!CreateEdit1())
      return(false);
   if(!CreateEdit2())
      return(false);


   return(true);
  }
//+------------------------------------------------------------------+
//| Create the display field                                         |
//+------------------------------------------------------------------+

void CPanelDialog::GetValues(int &NumberOfOrders,int &Magic,bool &AveragePricePrint,double &HedgeLotType,int &PipStep,double &Lots,double&TakeProfit)
  {
/*
   Mail = mMail;
   Push = mPush;
   Alert_=mAlert_;
*/
   NumberOfOrders=mNumberOfOrders;
   Magic=mMagicNumber;
   HedgeLotType=mHedgeLotType;
   AveragePricePrint=mAveragePricePrint;

   PipStep=mPipStep;
   Lots=mLots;
   TakeProfit=mTakeProfit;

  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+

bool CPanelDialog::Initialization(const int NumberOfOrders,const int Magic,const bool AveragePricePrint,const double HedgeLotType,const int PipStep,const double Lots,const double TakeProfit)
  {

   mNumberOfOrders=NumberOfOrders;
   mMagicNumber=Magic;
   mAveragePricePrint=AveragePricePrint;
   mHedgeLotType=HedgeLotType;

   mPipStep=PipStep;
   mLots=Lots;
   mTakeProfit=TakeProfit;

//---
   m_edit1.Text(IntegerToString(mNumberOfOrders));
   m_edit2.Text(IntegerToString(mMagicNumber));
   m_edit_hedge.Text(HedgeTypesNames[HedgeTypesArrayIndex]);
   m_edit_pipstep.Text(IntegerToString(mPipStep));
   m_edit_lot.Text(DoubleToStr(mLots,2));
   m_edit_takeprofit.Text(DoubleToStr(mTakeProfit,1));

//---
   return(true);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool CPanelDialog::CreateLabelPipStep(void)
  {
//--- coordinates
   int x1=INDENT_LEFT;
   int y1=INDENT_TOP;
   int x2=x1+20;
   int y2=y1+20;
//--- create
   if(!m_label_pipstep.Create(m_chart_id,m_name+"LabelPipStep",m_subwin,x1,y1,x2,y2))
      return(false);
   if(!m_label_pipstep.Text("Pip Step"))
      return(false);
   if(!Add(m_label_pipstep))
      return(false);
   m_label_pipstep.Color(TEXT_COLOR);
   m_label_pipstep.Font(TEXT_FONT);
//--- succeed
   return(true);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool CPanelDialog::CreateEditPipStep(void)
  {
//--- coordinates
   int x1=INDENT_LEFT+BUTTON_WIDTH;
   int y1=INDENT_TOP;
   int x2=x1+BUTTON_WIDTH;
   int y2=y1+BUTTON_HEIGHT;
//--- create
   if(!m_edit_pipstep.Create(m_chart_id,m_name+"EditPipStep",m_subwin,x1,y1,x2,y2))
      return(false);
   if(!Add(m_edit_pipstep))
      return(false);
   m_edit_pipstep.ColorBorder(BORDER_COLOR);
   m_edit_pipstep.FontSize(10);
   m_edit_pipstep.Font(TEXT_FONT);

   m_edit_pipstep.Alignment(WND_ALIGN_RIGHT,0,0,INDENT_RIGHT,0);
//--- succeed
   return(true);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool CPanelDialog::CreateButtonPipStepPlus(void)
  {
//--- coordinates
   int x1=INDENT_LEFT+2*BUTTON_WIDTH-14;
   int y1=INDENT_TOP;
   int x2=x1+6;
   int y2=y1+3;
//--- create
   if(!m_button_pipstep_plus.Create(m_chart_id,m_name+"ButtonPipStepPlus",m_subwin,x1,y1,x2,y2))
      return(false);
   m_button_pipstep_plus.BmpNames("::res\\SpinInc.bmp","::res\\SpinInc.bmp");
   if(!Add(m_button_pipstep_plus))
      return(false);
   m_button_pipstep_plus.Alignment(WND_ALIGN_RIGHT,0,0,INDENT_RIGHT,0);
//--- succeed
   return(true);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool CPanelDialog::CreateButtonPipStepMinus(void)
  {
//--- coordinates
   int x1=INDENT_LEFT+2*BUTTON_WIDTH-14;
   int y1=INDENT_TOP+BUTTON_HEIGHT-8;
   int x2=x1+6;
   int y2=y1+3;
//--- create
   if(!m_button_pipstep_minus.Create(m_chart_id,m_name+"ButtonPipStepMinus",m_subwin,x1,y1,x2,y2))
      return(false);
   m_button_pipstep_minus.BmpNames("::res\\SpinDec.bmp","::res\\SpinDec.bmp");

   if(!Add(m_button_pipstep_minus))
      return(false);
   m_button_pipstep_minus.Alignment(WND_ALIGN_RIGHT,0,0,INDENT_RIGHT,0);
//--- succeed
   return(true);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool CPanelDialog::CreateButtonModify(void)
  {
//--- coordinates
   int x1=INDENT_LEFT+2*BUTTON_WIDTH+3*CONTROLS_GAP_X+5;
   int y1=INDENT_TOP+BUTTON_HEIGHT +CONTROLS_GAP_Y;
   int x2=x1+20;
   int y2=y1+50;
//--- create
   if(!m_button_modify.Create(m_chart_id,m_name+"ButtonModify1",m_subwin,x1,y1,x2,y2))
      return(false);
   m_button_modify.BmpNames("::Save3.bmp","::Save2.bmp");
   if(!Add(m_button_modify))
      return(false);
   m_button_modify.ColorBorder(BORDER_COLOR);

   m_button_modify.Alignment(WND_ALIGN_RIGHT,0,0,INDENT_RIGHT,0);
//--- succeed

   return(true);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool CPanelDialog::CreateBasePanel(void)
  {
//--- coordinates
   int x1=0;
   int y1=0;
   int x2=x1+222;
   int y2=y1+315;
//--- create
   if(!m_panelbase.Create(m_chart_id,m_name+"BasePanel",m_subwin,x1,y1,x2,y2))
      return(false);
   if(!Add(m_panelbase))
      return(false);
   m_panelbase.ColorBackground(BASEPANEL_COLOUR);
//m_panel1.BorderType(BORDER_RAISED);

//--- succeed
   return(true);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool CPanelDialog::CreatePanel1(void)
  {
//--- coordinates
   int x1=0;
   int y1=0;
   int x2=x1+222;
   int y2=y1+101;
//--- create
   if(!m_panel1.Create(m_chart_id,m_name+"Panel1",m_subwin,x1,y1,x2,y2))
      return(false);
   if(!Add(m_panel1))
      return(false);
   m_panel1.ColorBackground(PANEL_COLOUR);
//m_panel1.BorderType(BORDER_RAISED);

//--- succeed
   return(true);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool CPanelDialog::CreatePanel2(void)
  {
//--- coordinates
//int y1=INDENT_TOP+5*BUTTON_HEIGHT+7*CONTROLS_GAP_Y;
//int y1=INDENT_TOP+7*BUTTON_HEIGHT+10*CONTROLS_GAP_Y;

   int x1=0;
   int y1=171;
   int x2=x1+222;
   int y2=y1+70;
//--- create
   if(!m_panel2.Create(m_chart_id,m_name+"Panel2",m_subwin,x1,y1,x2,y2))
      return(false);
   if(!Add(m_panel2))
      return(false);
   m_panel2.ColorBackground(PANEL_COLOUR);
//m_panel2.BorderType(BORDER_RAISED);

//--- succeed
   return(true);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool CPanelDialog::CreateLabelLot(void)
  {
//--- coordinates
   int x1=INDENT_LEFT;
   int y1=INDENT_TOP+ BUTTON_HEIGHT+CONTROLS_GAP_Y;
   int x2=x1+20;
   int y2=y1+20;
//--- create
   if(!m_label_lot.Create(m_chart_id,m_name+"LabelLot",m_subwin,x1,y1,x2,y2))
      return(false);
   if(!m_label_lot.Text("Lot"))
      return(false);
   if(!Add(m_label_lot))
      return(false);
   m_label_lot.Color(TEXT_COLOR);
   m_label_lot.Font(TEXT_FONT);
//--- succeed
   return(true);
  }
//|                                                                  |
//+------------------------------------------------------------------+
bool CPanelDialog::CreateEditLot(void)
  {
//--- coordinates
   int x1=INDENT_LEFT+BUTTON_WIDTH;
   int y1=INDENT_TOP+BUTTON_HEIGHT+CONTROLS_GAP_Y;
   int x2=x1+BUTTON_WIDTH;
   int y2=y1+BUTTON_HEIGHT;
//--- create
   if(!m_edit_lot.Create(m_chart_id,m_name+"EditLot",m_subwin,x1,y1,x2,y2))
      return(false);
   if(!Add(m_edit_lot))
      return(false);
   m_edit_lot.Alignment(WND_ALIGN_RIGHT,0,0,INDENT_RIGHT,0);

   m_edit_lot.FontSize(10);
   m_edit_lot.Font(TEXT_FONT);
   m_edit_lot.ColorBorder(BORDER_COLOR);




//--- succeed
   return(true);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool CPanelDialog::CreateButtonLotPlus(void)
  {
//--- coordinates
   int x1=INDENT_LEFT+2*BUTTON_WIDTH-14;
   int y1=INDENT_TOP+BUTTON_HEIGHT+CONTROLS_GAP_Y;
   int x2=x1+6;
   int y2=y1+3;
//--- create
   if(!m_button_lot_plus.Create(m_chart_id,m_name+"ButtonLotPlus",m_subwin,x1,y1,x2,y2))
      return(false);
   m_button_lot_plus.BmpNames("::res\\SpinInc.bmp","::res\\SpinInc.bmp");
   if(!Add(m_button_lot_plus))
      return(false);
   m_button_lot_plus.Alignment(WND_ALIGN_RIGHT,0,0,INDENT_RIGHT,0);
//--- succeed
   return(true);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool CPanelDialog::CreateButtonLotMinus(void)
  {
//--- coordinates
   int x1=INDENT_LEFT+2*BUTTON_WIDTH-14;
   int y1=INDENT_TOP+2*BUTTON_HEIGHT-8+CONTROLS_GAP_Y;
   int x2=x1+6;
   int y2=y1+3;
//--- create
   if(!m_button_lot_minus.Create(m_chart_id,m_name+"ButtonLotMinus",m_subwin,x1,y1,x2,y2))
      return(false);
   m_button_lot_minus.BmpNames("::res\\SpinDec.bmp","::res\\SpinDec.bmp");
   if(!Add(m_button_lot_minus))
      return(false);
   m_button_lot_minus.Alignment(WND_ALIGN_RIGHT,0,0,INDENT_RIGHT,0);
//--- succeed
   return(true);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool CPanelDialog::CreateLabelTakeProfit(void)
  {
//--- coordinates
   int x1=INDENT_LEFT;
   int y1=INDENT_TOP+2*BUTTON_HEIGHT+2*CONTROLS_GAP_Y;
   int x2=x1+20;
   int y2=y1+20;
//--- create
   if(!m_label_takeprofit.Create(m_chart_id,m_name+"LabelTakeProfit",m_subwin,x1,y1,x2,y2))
      return(false);
   if(!m_label_takeprofit.Text("InitialTP"))
      return(false);
   if(!Add(m_label_takeprofit))
      return(false);
   m_label_takeprofit.Color(TEXT_COLOR);
   m_label_takeprofit.Font(TEXT_FONT);
//--- succeed
   return(true);
  }
//|                                                                  |
//+------------------------------------------------------------------+
bool CPanelDialog::CreateEditTakeProfit(void)
  {
//--- coordinates
   int x1=INDENT_LEFT+BUTTON_WIDTH;
   int y1=INDENT_TOP+2*BUTTON_HEIGHT+2*CONTROLS_GAP_Y;
   int x2=x1+BUTTON_WIDTH;
   int y2=y1+BUTTON_HEIGHT;
//--- create
   if(!m_edit_takeprofit.Create(m_chart_id,m_name+"EditTakeProfit",m_subwin,x1,y1,x2,y2))
      return(false);
   if(!Add(m_edit_takeprofit))
      return(false);
   m_edit_takeprofit.Alignment(WND_ALIGN_RIGHT,0,0,INDENT_RIGHT,0);

   m_edit_takeprofit.FontSize(10);
   m_edit_takeprofit.Font(TEXT_FONT);
   m_edit_takeprofit.ColorBorder(BORDER_COLOR);


//--- succeed
   return(true);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool CPanelDialog::CreateButtonTakeProfitPlus(void)
  {
//--- coordinates
   int x1=INDENT_LEFT+2*BUTTON_WIDTH-14;
   int y1=INDENT_TOP+2*BUTTON_HEIGHT+2*CONTROLS_GAP_Y;
   int x2=x1+6;
   int y2=y1+3;
///--- create
   if(!m_button_takeprofit_plus.Create(m_chart_id,m_name+"ButtonTakeProfitPlus",m_subwin,x1,y1,x2,y2))
      return(false);
   m_button_takeprofit_plus.BmpNames("::res\\SpinInc.bmp","::res\\SpinInc.bmp");
   if(!Add(m_button_takeprofit_plus))
      return(false);
   m_button_takeprofit_plus.Alignment(WND_ALIGN_RIGHT,0,0,INDENT_RIGHT,0);
//--- succeed
   return(true);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool CPanelDialog::CreateButtonTakeProfitMinus(void)
  {
//--- coordinates
   int x1=INDENT_LEFT+2*BUTTON_WIDTH-14;
   int y1=INDENT_TOP+3*BUTTON_HEIGHT-8+2*CONTROLS_GAP_Y;
   int x2=x1+6;
   int y2=y1+3;
//--- create
   if(!m_button_takeprofit_minus.Create(m_chart_id,m_name+"ButtonTakeProfitMinus",m_subwin,x1,y1,x2,y2))
      return(false);
   m_button_takeprofit_minus.BmpNames("::res\\SpinDec.bmp","::res\\SpinDec.bmp");
   if(!Add(m_button_takeprofit_minus))
      return(false);
   m_button_takeprofit_minus.Alignment(WND_ALIGN_RIGHT,0,0,INDENT_RIGHT,0);
//--- succeed
   return(true);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool CPanelDialog::CreateLabel1(void)
  {
//--- coordinates
   int x1=INDENT_LEFT;
   int y1=INDENT_TOP+3*BUTTON_HEIGHT+4*CONTROLS_GAP_Y;
   int x2=x1+20;
   int y2=y1+20;
//--- create
   if(!m_label1.Create(m_chart_id,m_name+"Label1",m_subwin,x1,y1,x2,y2))
      return(false);
   if(!m_label1.Text("Hedge#"))
      return(false);
   if(!Add(m_label1))
      return(false);
   m_label1.Color(TEXT_COLOR);
   m_label1.Font(TEXT_FONT);

//--- succeed
   return(true);
  }
//+------------------------------------------------------------------+
//| Create the display field "Edit1"                                 |
//+------------------------------------------------------------------+
bool CPanelDialog::CreateEdit1(void)
  {
//--- coordinates
   int x1=INDENT_LEFT+BUTTON_WIDTH;
   int y1=INDENT_TOP+3*BUTTON_HEIGHT+4*CONTROLS_GAP_Y;
   int x2=x1+BUTTON_WIDTH;
   int y2=y1+BUTTON_HEIGHT;
//--- create
   if(!m_edit1.Create(m_chart_id,m_name+"Edit1",m_subwin,x1,y1,x2,y2))
      return(false);
   if(!m_edit1.ReadOnly(false))
      return(false);
   if(!Add(m_edit1))
      return(false);


   m_edit1.FontSize(10);

   m_edit1.Font(TEXT_FONT);
   m_edit1.ColorBorder(BORDER_COLOR);

   m_edit1.Alignment(WND_ALIGN_RIGHT,0,0,INDENT_RIGHT,0);
//--- succeed
   return(true);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool CPanelDialog::CreateEditHedge(void)
  {
//--- coordinates
   int x1=INDENT_LEFT+2*BUTTON_WIDTH +CONTROLS_GAP_X;
   int y1=INDENT_TOP+3*BUTTON_HEIGHT+4*CONTROLS_GAP_Y;
   int x2=x1+BUTTON_WIDTH+ 10;
   int y2=y1+BUTTON_HEIGHT;
//--- create
   if(!m_edit_hedge.Create(m_chart_id,m_name+"EditHedge",m_subwin,x1,y1,x2,y2))
      return(false);
   if(!m_edit_hedge.ReadOnly(true))
      return(false);
   if(!Add(m_edit_hedge))
      return(false);


   m_edit_hedge.FontSize(10);
   m_edit_hedge.Font(TEXT_FONT);
   m_edit_hedge.ColorBorder(BORDER_COLOR);



   m_edit_hedge.Alignment(WND_ALIGN_RIGHT,0,0,INDENT_RIGHT,0);
//--- succeed
   return(true);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool CPanelDialog::CreateButtonHedgePlus(void)
  {
//--- coordinates
   int x1=INDENT_LEFT+3*BUTTON_WIDTH+CONTROLS_GAP_X-4;
   int y1=INDENT_TOP+3*BUTTON_HEIGHT+4*CONTROLS_GAP_Y;
   int x2=x1+6;
   int y2=y1+3;
//--- create
   if(!m_button_hedge_plus.Create(m_chart_id,m_name+"ButtonHedgePlus",m_subwin,x1,y1,x2,y2))
      return(false);
   m_button_hedge_plus.BmpNames("::res\\SpinInc.bmp","::res\\SpinInc.bmp");

   if(!Add(m_button_hedge_plus))
      return(false);
   m_button_hedge_plus.Alignment(WND_ALIGN_RIGHT,0,0,INDENT_RIGHT,0);
//--- succeed
   return(true);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool CPanelDialog::CreateButtonHedgeMinus(void)
  {
//--- coordinates
   int x1=INDENT_LEFT+3*BUTTON_WIDTH+CONTROLS_GAP_X-4;
   int y1=INDENT_TOP+4*BUTTON_HEIGHT+4*CONTROLS_GAP_Y-8;
   int x2=x1+6;
   int y2=y1+3;
//--- create
   if(!m_button_hedge_minus.Create(m_chart_id,m_name+"ButtonHedgeMinus",m_subwin,x1,y1,x2,y2))
      return(false);
   m_button_hedge_minus.BmpNames("::res\\SpinDec.bmp","::res\\SpinDec.bmp");
   if(!Add(m_button_hedge_minus))
      return(false);
   m_button_hedge_minus.Alignment(WND_ALIGN_RIGHT,0,0,INDENT_RIGHT,0);
//--- succeed
   return(true);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool CPanelDialog::CreateButton1(void)
  {
//--- coordinates
   int x1=INDENT_LEFT+3*CONTROLS_GAP_X;
   int y1=INDENT_TOP+4*BUTTON_HEIGHT+5*CONTROLS_GAP_Y;
   int x2=x1+2*BUTTON_WIDTH+2*CONTROLS_GAP_X;
   int y2=y1+BUTTON_HEIGHT;
//--- create
   if(!m_button1.Create(m_chart_id,m_name+"Button1",m_subwin,x1,y1,x2,y2))
      return(false);
   if(!m_button1.Text("Apply"))
      return(false);
   if(!Add(m_button1))
      return(false);

   m_button1.FontSize(10);
   m_button1.Color(TEXT_COLOR);
   m_button1.Font(TEXT_FONT);
   m_button1.ColorBorder(BORDER_COLOR);

   m_button1.Alignment(WND_ALIGN_RIGHT,0,0,INDENT_RIGHT,0);
//--- succeed
   return(true);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool CPanelDialog::CreateLabel2(void)
  {
   int x1=INDENT_LEFT;
   int y1=INDENT_TOP+5*BUTTON_HEIGHT+7*CONTROLS_GAP_Y;
   int x2=x1+20;
   int y2=y1+20;
//--- create
   if(!m_label2.Create(m_chart_id,m_name+"Label2",m_subwin,x1,y1,x2,y2))
      return(false);
   if(!m_label2.Text("CloseAll"))
      return(false);
   if(!Add(m_label2))
      return(false);


   m_label2.Color(TEXT_COLOR);
   m_label2.Font(TEXT_FONT);


//--- succeed
   return(true);
  }
//+------------------------------------------------------------------+
//| Create the "Button2" button                                      |
//+------------------------------------------------------------------+

bool CPanelDialog::CreateEdit2(void)
  {
//--- coordinates
   int x1=INDENT_LEFT+BUTTON_WIDTH;
   int y1=INDENT_TOP+5*BUTTON_HEIGHT+7*CONTROLS_GAP_Y;
   int x2=x1+BUTTON_WIDTH;
   int y2=y1+BUTTON_HEIGHT;

//--- create
   if(!m_edit2.Create(m_chart_id,m_name+"Edit2",m_subwin,x1,y1,x2,y2))
      return(false);
   if(!m_edit2.ReadOnly(false))
      return(false);
   if(!Add(m_edit2))
      return(false);

   m_edit2.FontSize(10);
   m_edit2.Font(TEXT_FONT);
   m_edit2.ColorBorder(BORDER_COLOR);



   m_edit2.Alignment(WND_ALIGN_RIGHT,0,0,INDENT_RIGHT,0);
//--- succeed
   return(true);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool CPanelDialog::CreateButton2(void)
  {
//--- coordinates
   int x1=INDENT_LEFT+3*CONTROLS_GAP_X;
   int y1=INDENT_TOP+6*BUTTON_HEIGHT+8*CONTROLS_GAP_Y;
   int x2=x1+2*BUTTON_WIDTH+2*CONTROLS_GAP_X;
   int y2=y1+BUTTON_HEIGHT;
//--- create
   if(!m_button2.Create(m_chart_id,m_name+"Button2",m_subwin,x1,y1,x2,y2))
      return(false);
   if(!m_button2.Text("Apply"))
      return(false);
   if(!Add(m_button2))
      return(false);

   m_button2.FontSize(10);
   m_button2.Color(TEXT_COLOR);
   m_button2.Font(TEXT_FONT);
   m_button2.ColorBorder(BORDER_COLOR);



   m_button2.Alignment(WND_ALIGN_RIGHT,0,0,INDENT_RIGHT,0);
//--- succeed
   return(true);
  }
//+------------------------------------------------------------------+
//| Create the "Button3" fixed button                                |
//+------------------------------------------------------------------+
bool CPanelDialog::CreateButton3(void)
  {
//--- coordinates
   int x1=INDENT_LEFT;
   int y1=INDENT_TOP+7*BUTTON_HEIGHT+10*CONTROLS_GAP_Y;
   int x2=x1+3*BUTTON_WIDTH+2*CONTROLS_GAP_X;
   int y2=y1+20;
//--- create
   if(!m_button3.Create(m_chart_id,m_name+"Button3",m_subwin,x1,y1,x2,y2))
      return(false);
   if(!m_button3.Text("Print Cycle Average "))
      return(false);

   if(!Add(m_button3))
      return(false);

   m_button3.FontSize(10);
   m_button3.Color(TEXT_COLOR);
   m_button3.Font(TEXT_FONT);
   m_button3.ColorBorder(BORDER_COLOR);




   m_button3.Alignment(WND_ALIGN_RIGHT|WND_ALIGN_BOTTOM,0,0,INDENT_RIGHT,INDENT_BOTTOM);
//--- succeed
   return(true);
  }
//+------------------------------------------------------------------+
//| Create the "Button4" fixed button                                |
//+------------------------------------------------------------------+
bool CPanelDialog::CreateButton4(void)
  {
//--- coordinates
   int x1=INDENT_LEFT;
   int y1=INDENT_TOP+8*BUTTON_HEIGHT+11*CONTROLS_GAP_Y;
   int x2=x1+3*BUTTON_WIDTH+2*CONTROLS_GAP_X;
   int y2=y1+20;
//--- create
   if(!m_button4.Create(m_chart_id,m_name+"Button4",m_subwin,x1,y1,x2,y2))
      return(false);
   if(!m_button4.Text("Lock"))
      return(false);

   if(!Add(m_button4))
      return(false);

   m_button4.FontSize(10);
   m_button4.Color(TEXT_COLOR);
   m_button4.Font(TEXT_FONT);
   m_button4.ColorBorder(BORDER_COLOR);



   m_button4.Locking(true);
   m_button4.Alignment(WND_ALIGN_RIGHT|WND_ALIGN_BOTTOM,0,0,INDENT_RIGHT,INDENT_BOTTOM);
   m_button4.ColorBackground(LOCK_BUTTON_COLOUR);
//--- succeed
   return(true);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void CPanelDialog::OnClickButtonModify(void)
  {
   mModification=true;
   ButtonModifyPressed(false);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void CPanelDialog::OnClickButton1(void)
  {
   if(StringToInteger(m_edit1.Text())!=mNumberOfOrders)
     {
      mNumberOfOrders=StringToInteger(m_edit1.Text());
     }

   mApplyHedge=true;
   mModification=true;
  }
//+------------------------------------------------------------------+
//| Event handler                                                    |
//+------------------------------------------------------------------+
void CPanelDialog::OnClickButton2(void)
  {
   if(StringToInteger(m_edit2.Text())!=mMagicNumber)
     {
      mMagicNumber=StringToInteger(m_edit2.Text());

     }

   mApplyClose=true;
   mModification=true;
  }
//+------------------------------------------------------------------+
//| Event handler                                                    |
//+------------------------------------------------------------------+
void CPanelDialog::OnClickButton3(void)
  {

   mAveragePricePrint=true;
   mModification=true;

  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void CPanelDialog::OnClickButton4(void)
  {

   if(m_button4.Pressed())
     {
      m_button4.Text("Locked");
      m_button4.Color(Red);
      m_button1.Visible(false);
      m_button2.Visible(false);
      m_button3.Visible(false);

     }
   else
     {
      m_button4.Text("Unlocked");
      m_button4.Color(Black);
      m_button1.Visible(true);
      m_button2.Visible(true);
      m_button3.Visible(true);

     }

  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void CPanelDialog::OnClickButtonHedgePlus(void)
  {
   HedgeTypesArrayIndex=(HedgeTypesArrayIndex+1)%3;
   m_edit_hedge.Text(HedgeTypesNames[HedgeTypesArrayIndex]);
   mHedgeLotType=HedgeTypesValues[HedgeTypesArrayIndex];

  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void CPanelDialog::OnClickButtonHedgeMinus(void)
  {
   HedgeTypesArrayIndex--;

   if(HedgeTypesArrayIndex<0) // if it reached to its end
     {
      HedgeTypesArrayIndex=2;
     }

   m_edit_hedge.Text(HedgeTypesNames[HedgeTypesArrayIndex]);
   mHedgeLotType=HedgeTypesValues[HedgeTypesArrayIndex];

  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void CPanelDialog::OnClickButtonPipStepPlus(void)
  {
   mPipStep++;
   m_edit_pipstep.Text(IntegerToString(mPipStep));
   mModification=true;
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void CPanelDialog::OnClickButtonPipStepMinus(void)
  {
   if(mPipStep==0) // if it reached to its end
     {
      MessageBox("Pip Step value cant be below 0 ","Warning",MB_OK|MB_ICONHAND);
     }
   else
     {
      mPipStep--;
      m_edit_pipstep.Text(IntegerToString(mPipStep));
      mModification=true;
     }
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void CPanelDialog::OnClickButtonLotPlus(void)
  {
   mLots+=0.1;
   m_edit_lot.Text(DoubleToStr(mLots,2));
   mModification=true;

  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void CPanelDialog::OnClickButtonLotMinus(void)
  {
   if(mLots==0) // if it reached to its end
     {
      MessageBox("Lot value cant be below 0 ","Warning",MB_OK|MB_ICONHAND);
     }
   else
     {
      mLots-=0.1;
      m_edit_lot.Text(DoubleToStr(mLots,2));
      mModification=true;
     }

  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void CPanelDialog::OnClickButtonTakeProfitPlus(void)
  {
   mTakeProfit+=0.1;
   m_edit_takeprofit.Text(DoubleToStr(mTakeProfit,2));
   mModification=true;

  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void CPanelDialog::OnClickButtonTakeProfitMinus(void)
  {
   if(mTakeProfit==0) // if it reached to its end
     {
      MessageBox("Take Profit value cant be below 0 ","Warning",MB_OK|MB_ICONHAND);
     }
   else
     {
      mTakeProfit-=0.1;
      m_edit_takeprofit.Text(DoubleToStr(mTakeProfit,2));
      mModification=true;
     }

  }
//+------------------------------------------------------------------+
//| Event handler                                                    |
//+------------------------------------------------------------------+

void CPanelDialog::OnChangeEdit1(void)
  {
   int temp=StringToInteger(m_edit1.Text());
   if(temp==0)
     {
      MessageBox("In the input field \"#OfOrders\" not a number","Input error",0);
      m_edit1.Text(IntegerToString(mNumberOfOrders));
     }
   else
     {
      m_edit1.Text(IntegerToString(temp));
     }
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void CPanelDialog::OnChangeEdit2(void)
  {
   int temp=StringToInteger(m_edit2.Text());
   if(temp==0)
     {
      MessageBox("In the input field \"Magic#\" not a number","Input error",0);
      m_edit2.Text(IntegerToString(mMagicNumber));
     }
   else
     {
      m_edit2.Text(IntegerToString(temp));
     }
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+

void CPanelDialog::OnChangeEditPipStep(void)
  {
   int temp=StringToInteger(m_edit_pipstep.Text());
   if(temp==0)
     {
      MessageBox("In the input field \"Pip Step\" not a number","Input error",0);
      m_edit_pipstep.Text(IntegerToString(mPipStep,2));
     }
   else
     {
      m_edit_pipstep.Text(IntegerToString(temp,2));
      mPipStep=StringToDouble(m_edit_pipstep.Text());
      mModification=true;

     }
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void CPanelDialog::OnChangeEditLot(void)
  {
   double temp=StringToDouble(m_edit_lot.Text());
   if(temp==0)
     {
      MessageBox("In the input field \"Lot\" not a number","Input error",0);
      m_edit_lot.Text(DoubleToStr(mLots,2));
     }
   else
     {
      m_edit_lot.Text(DoubleToStr(temp,2));
      mLots=StringToDouble(m_edit_lot.Text());
      mModification=true;
     }
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void CPanelDialog::OnChangeEditTakeProfit(void)
  {
   double temp=StringToDouble(m_edit_takeprofit.Text());
   if(temp==0)
     {
      MessageBox("In the input field \"Initial TakeProfit\" not a number","Input error",0);
      m_edit_takeprofit.Text(DoubleToStr(mTakeProfit,1));
     }
   else
     {
      m_edit_takeprofit.Text(DoubleToStr(temp,2));
      mTakeProfit=StringToDouble(m_edit_takeprofit.Text());
      mModification=true;
     }
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
/*bool CPanelDialog::SetCheck(const int idx,const bool check)
  {
   bool rezult=m_check_group.Check(idx,check);

   return(rezult);
  }
//+------------------------------------------------------------------+
int CPanelDialog::GetCheck(const int idx)
  {
   return(m_check_group.Check(idx));
  }*/
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
void CPanelDialog::Button1Pressed(bool state)
  {
   m_button1.Pressed(state);
  }
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
void CPanelDialog::Button2Pressed(bool state)
  {
   m_button2.Pressed(state);
  }
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
void CPanelDialog::Button3Pressed(bool state)
  {
   m_button3.Pressed(state);
  }
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
void CPanelDialog::Button4Pressed(bool state)
  {
   m_button4.Pressed(state);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void CPanelDialog::ButtonHedgePlusPressed(bool state)
  {
   m_button_hedge_plus.Pressed(state);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void CPanelDialog::ButtonHedgeMinusPressed(bool state)
  {
   m_button_hedge_minus.Pressed(state);
  }
//+------------------------------------------------------------------+
void CPanelDialog::ButtonPipStepPlusPressed(bool state)
  {
   m_button_pipstep_plus.Pressed(state);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void CPanelDialog::ButtonPipStepMinusPressed(bool state)
  {
   m_button_pipstep_minus.Pressed(state);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void CPanelDialog::ButtonLotPlusPressed(bool state)
  {
   m_button_lot_plus.Pressed(state);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void CPanelDialog::ButtonLotMinusPressed(bool state)
  {
   m_button_lot_minus.Pressed(state);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void CPanelDialog::ButtonTakeProfitPlusPressed(bool state)
  {
   m_button_takeprofit_plus.Pressed(state);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void CPanelDialog::ButtonTakeProfitMinusPressed(bool state)
  {
   m_button_takeprofit_minus.Pressed(state);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void CPanelDialog::ButtonModifyPressed(bool state)
  {
   m_button_modify.Pressed(state);
  }
//+------------------------------------------------------------------+
