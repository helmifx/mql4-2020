//+------------------------------------------------------------------+
//|                                         Abd_AllPairsStratagy.mq4 |
//|                        Copyright 2017, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//|Abd Loulou
//+------------------------------------------------------------------+


//+------------------------------------------------------------------+
//									 Martingle 
//			Abd Loulou																	|	
//+------------------------------------------------------------------+

//+-----------------------------------------------------------------------------------------+

//+-----------------------------------------------------------------------------------------+

//+-----------------------------------------------------------------------------------------+

//+-----------------------------------------------------------------------------------------+

#property copyright "Copyright 2017 HelmiFX."
#property link      "www.helmifx.com"
#property strict
#property description "Martingale_StopLoss"

#property description "Find us on  :\n"
#property description "Facbook : HelmiFx"
#property description "Twitter : @HelmiForex"  
#property description "Youtube : HelmiForex" 

#property icon "photo.ico"

#include "Stack.mqh"
#include "MartingalePanelV1.2.mqh"
//+------------------------------------------------------------------+
//| Setup                                               
//+------------------------------------------------------------------+

enum ENUM_PERIODS
  {
   M1=1,M5=5,M15=15,M30=30,H1=60,H4=240,D1=1440,W1=10080,MN=43200
  };
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
enum ENUM_ORDERTYPES
  {
   SELL=OP_SELL,BUY=OP_BUY
  };
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
enum ENUM_ENTRYRULE
  {
   HeikenAshi=1,
   RSI=2,
   SpecificType=3,// Buy Or Sell
   RSICross=4
  };
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
enum ENUM_MANUALTRADING
  {
   Yes=0,No=-1
  };
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
enum ENUM_INCREMENTTAKEPROFIT
  {
   null=0,// Dont use TakeProfit Increment
   TPM=1, // TakeProfit * Multiplier
   TPA=2, // TakeProfit + Step

  };
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
enum ENUM_INCREMENTPIPSTEP
  {
   PipStepnull=0,// Dont use PipStep Increment
   PipStepTPM=1, // PipStep * Multiplier
   PipStepTPA=2, // PipStep + Step

  };
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
enum ENUM_LOTSIZING
  {
   M=1,//Multiplier
   A=2,//LotStep
   LA=3,// Multiply Sequence[thisOrder]
  };

extern string           Header1="**************************************************************************************"; // ----------Trading Rules Variables-----------
extern string             MANUALTRADINGEXP1="By allowing manual trading, you are letting manually opened";
extern string             MANUALTRADINGEXP2="/closed positions to be added/removed to the cycle.";
extern ENUM_MANUALTRADING ManualTrading=0;
extern int MaxPositionsAllowed=50;

extern string           Header2="**************************************************************************************"; // ----------Entry Rules Settings-----------
extern ENUM_ENTRYRULE   EntryRule=2;
extern string           Note1="*******************************************"; //HeikenAshi Settings if used
extern ENUM_PERIODS     HeikenAshiApplyPeriod=240;  //HeikenAshi TimeFrame
extern string           Note2="*******************************************"; // RSI Settings if used
extern int              RSIPeriod=21;       //RSI Period
extern int              RSILimit=50;         //RSI Limit 
extern ENUM_PERIODS     RSIApplyPeriod=15;   //RSI TimeFrame
extern string           Note3="*******************************************"; // if BuyOrSell is used
extern ENUM_ORDERTYPES  EntryType=OP_BUY;
extern string           Note4="*******************************************"; // if RSI Cross is used
extern int              RSICrossPeriod=21;      //Cross RSI Period
extern double           RSICrossUpperLimit=70;         //Cross RSI Upper Limit
extern double           RSICrossLowerLimit=30;         //Cross RSI Lower Limit  
extern ENUM_PERIODS     RSICrossApplyPeriod=15;         //RSI Cross TimeFrame

extern string           Header3="**************************************************************************************"; // ----------Chart Info Settings-----------
extern int              xDistance=1; //Chart info distance from top right corner
extern bool             EntryReason=false;      //Graphical reason of entry

extern string           Header4="**************************************************************************************"; // ----------Lot Sizing Settings-----------
extern double           Lot=1;
extern ENUM_LOTSIZING   LotSizingMethod=1;
extern string           Note5="*******************************************"; // if LotSizingMethod  = Last Lot * Multiplier is used
extern double           LotMultiplier=2;

extern string           Note6="*******************************************"; // if LotSizingMethod  = Last Lot + Lot Step is used
extern double           LotStep=0.1;

extern string           Note7="*******************************************"; // if LotSizingMethod  = Last Lot * Multiply Sequence is used
input string            MultiplySequence1Explaination="please use (,) to seperate numbers . example of usage : (1.6,2,1,3)";
extern string           MultiplySequence="1.6,2,1,3";

extern string           Header50="**************************************************************************************"; // ----------First Order Stop Loss Settings-----------
extern bool             UseFirstOrderStopLoss=false;
extern double           FirstOrderStopLoss=15; // First Order Stop Loss
extern string           Header5="**************************************************************************************"; // ----------Take Profit Settings-----------
extern int              PipsStep=10;
extern ENUM_INCREMENTPIPSTEP IncrementPipStep=0;
extern string           Note8="*******************************************"; // if IncrementPipStep  = PipStep * Multiply  is used
extern double             PipStepMultiplier=2;
extern string           Note9="*******************************************"; // if IncrementPipStep  = PipStepAdding  is used
extern double             PipStepAdding=1;
extern string           Note100="*******************************************"; // ---

extern double           InitialTakeProfit=10;
extern ENUM_INCREMENTTAKEPROFIT IncrementTakeProfit=0;
extern string           Note10="*******************************************"; // if IncrementTakeProfit  = TakeProfit * Multiply  is used
extern double             TakeProfitMultiplier=2;
extern string           Note11="*******************************************"; // if IncrementTakeProfit  = TakeProfit + Step  is used
extern double             TakeProfitStep=1;
extern string           Note12="*******************************************"; //---   
extern bool             CloseLastWinningTicket=false;
extern int              CloseAfter=5;                                       // on what position should i activate	

extern string           Header6="**************************************************************************************"; // ----------Trailing Stop Settings-----------
extern bool             UseTrailingStops=false;
extern double           TrailingStopOffset=10;
extern int              UseAfter=5;                            // on what position should i activate	

extern string           Header66="**************************************************************************************"; // ----------MaintainOnlyX Settings-----------
extern bool             MaintainOnlyX=false; // keep only x number of poisitons on chart
extern int                X=3;
extern string           Header7="**************************************************************************************"; // ----------Panel Settings-----------
extern bool             UsePanel=true;
extern int              ExtHedgeNumberOfOrders=10;
extern bool             ExtMailNotification=false;
extern bool             ExtPushNotification= false;
extern bool             ExtAlertNotification=true;

extern string           Header8="**************************************************************************************"; // ----------Set Max Loss Limit-----------
extern bool             IsLossLimitActivated=false;
extern double           LossLimit=0;

extern bool             IsProfitLimitActivated=false;
extern double           ProfitLimit=0;

extern string           Header9="**************************************************************************************"; // ----------EA General Settings-----------
extern int              MagicNumber=656668;
extern int              Slippage=3;
extern bool             LoggingInfo = true;
extern bool             OnJournaling=true; // Add EA updates in the Journal Tab

string InternalHeader1="----------Errors Handling Settings-----------";

int    RetryInterval=100; // Pause Time before next retry (in milliseconds)
int    MaxRetriesPerTick=10;

string InternalHeader2="----------Service Variables-----------";

//+------------------------------------------------------------------+
//|//General Variables                                                         |
//+------------------------------------------------------------------+
int     P,YenPairAdjustFactor;
double LastClose;

Stack *LastAddedPositionsStack=new Stack(MaxPositionsAllowed);
//+------------------------------------------------------------------+
//|//Lot Variables                                                                  |
//+------------------------------------------------------------------+
string multiplySequenceArray[];

//+------------------------------------------------------------------+
//|//Entry Reason Variables                                                                  |
//+------------------------------------------------------------------+
int RSICurrentDirection=0;
int RSILastDirection=0;
bool RSIFirstTime=true;
double HAOpen_1,HAClose_1;
int HAColor;
double MyRSI,MyCrossRSI1,MyCrossRSI2;
int OrdersOpened;

double buyLot;
double sellLot;
double allLot;

//+------------------------------------------------------------------+
//|//Chart Info Variables                                                                  |
//+------------------------------------------------------------------+
double lastClosedProfit=0;   //GetLastManually Closed profit
double cycleProfit; //Get All Opened Profit
double cycleLots;    //Get All Opened Lots
double netProfit;    // All the profit of all opened positions by the same MagicNumber

//+------------------------------------------------------------------+
//|//Expert Variables                                                                 |
//+------------------------------------------------------------------+                   
ENUM_ORDERTYPES OrdersType;
int OrderNumber;                                    // Order Number 
string ObjName=IntegerToString(ChartID(),0,' ');   //The global object name 
Stack *OrdersStack=new Stack(MaxPositionsAllowed);         //initialize new ordersStack
Stack *OrdersTempStack=new Stack(MaxPositionsAllowed);   //initialize new temp stack

//+------------------------------------------------------------------+
//| //Control Panel Variables                                                                 |
//+------------------------------------------------------------------+
CPanelDialog panel;
bool ExtPrintAveragePrice=false;
CPoint rect;

//+------------------------------------------------------------------+
//|//Hedge Variables                                                                  |
//+------------------------------------------------------------------+
bool   ExtonHedging=false;
double ExthedgeLotType=1; // full as an initialization.
double hedgeProfit;
double hedgeLot;
ENUM_ORDERTYPES hedgeType;
int hedgeOrderNumber;
bool HedgeOpened=false;

//+------------------------------------------------------------------+
//|//Pip Display Variables                                                                  |
//+------------------------------------------------------------------+
double pipValue;
double pipValue1Lot;

//+------------------------------------------------------------------+
//|  Trailing Stop Variables                                         |
//+------------------------------------------------------------------+

double TrailingStopList[][2];

//+------------------------------------------------------------------+
//|  Last Close Winning Variables                                    |
//+------------------------------------------------------------------+

int lastTwoPositions[2];
//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
int init()
  {
   P=GetP(); // To account for 5 digit brokers. Used to convert pips to decimal place

   YenPairAdjustFactor=GetYenAdjustFactor(); // Adjust for YenPair

   ChartInfo();         //Add Chart Info to the EA 

   MultiplySequenceSplit();      //Splits MultiplySequence

   DrawChartInfo();            //Drawing Chart Info

   if(UseTrailingStops)
     {
      ArrayInitialize(TrailingStopList,0.0);
      ArrayResize(TrailingStopList,MaxPositionsAllowed,0);
     }

   if(UninitializeReason()!=3 && UninitializeReason()!=5 && UsePanel) // creating a panel.
     {
      panel.Initialization(ExtHedgeNumberOfOrders,MagicNumber,ExtPrintAveragePrice,ExthedgeLotType,PipsStep,Lot,InitialTakeProfit);

      if(!panel.Create(0,"Martingale",0,0,75,230,420))
         return(INIT_FAILED);
      //--- run application
      if(!panel.Run())
         return(INIT_FAILED);

     }
   return(INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//|Expert deinitialization function                                  |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
  {
   if(reason!=5 && reason!=3)
     {
      delete OrdersStack;
      delete OrdersTempStack;
      delete LastAddedPositionsStack;
      panel.Destroy();
      ObjectsDeleteAll(0,0,OBJ_LABEL);
      ObjectsDeleteAll(ChartID(),0);
     }

   if(OnJournaling) Print("All Lots opened =  "+allLot);

  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
void OnChartEvent(const int id,
                  const long &lparam,
                  const double &dparam,
                  const string &sparam)
  {

   panel.ChartEvent(id,lparam,dparam,sparam);

   int tempNumberOfOrders=ExtHedgeNumberOfOrders;
   int tempMagicNumber=MagicNumber;
   int tempPipsStep=PipsStep;
   double tempInitalLot=Lot;
   double tempInitalTakeProfit=InitialTakeProfit;

   panel.GetValues(tempNumberOfOrders,tempMagicNumber,ExtPrintAveragePrice,ExthedgeLotType,tempPipsStep,tempInitalLot,tempInitalTakeProfit);

   if(panel.Modification())
     {
      if(ExtPrintAveragePrice)
        {
         PrintAveragePrice();
        }

      if(panel.mApplyHedge)
        {
         ExtHedgeNumberOfOrders=tempNumberOfOrders;
         Print("Hedge Number of Orders have been changed to "+ExtHedgeNumberOfOrders);
         ExtonHedging=true;
         panel.mApplyHedge=false;

        }

      if(panel.mApplyClose)
        {
         CloseAllMagicNumber(tempMagicNumber);
         panel.mApplyClose=false;
        }

      if(PipsStep!=tempPipsStep)
        {
         PipsStep=tempPipsStep;
         Print("Pips Offset value have been changed in expert to "+IntegerToString(tempPipsStep));
        }

      if(Lot!=tempInitalLot)
        {
         Lot=tempInitalLot;
         Print("Initial Lot value have been changed in expert to "+DoubleToStr(tempInitalLot,2));
        }

      if(InitialTakeProfit!=tempInitalTakeProfit)
        {
         InitialTakeProfit=tempInitalTakeProfit;
         Print("Initial Take Profit value have been changed in expert to "+DoubleToStr(tempInitalTakeProfit,1));
        }

      panel.Modification(false);
     }

  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int start()
  {

   if(MQLInfoInteger(MQL_TESTER)) //if in test mode.
     {
      ApplyTestMode();
      rect.x=ObjectGetInteger(ChartID(),ObjName+"Rectangle",OBJPROP_XDISTANCE,0);
      rect.y=ObjectGetInteger(ChartID(),ObjName+"Rectangle",OBJPROP_YDISTANCE,0);
      panel.Move(rect);

     }

   cycleProfit=GetCycleProfit();               //Set Cycle Profits onto the chart.
   cycleLots=GetCycleLots();                  //Set Cycle Lots onto the chart.
   pipValue=(((MarketInfo(Symbol(),MODE_TICKVALUE)*Point*P)/MarketInfo(Symbol(),MODE_TICKSIZE))*cycleLots);// pip value of 1 lot
   pipValue1Lot=(((MarketInfo(Symbol(),MODE_TICKVALUE)*Point*P)/MarketInfo(Symbol(),MODE_TICKSIZE))*1);// pip value of 1 lot
   netProfit=cycleProfit+lastClosedProfit+hedgeProfit;

   UpdateChartInfo();       // Update Chart info.

   if(IsLossLimitBreached(IsLossLimitActivated,LossLimit,OnJournaling)==true || IsProfitLimitBreached(IsProfitLimitActivated,ProfitLimit,OnJournaling)==true)
     {
      return 0;
     }

   if(LoggingInfo && !MQLInfoInteger(MQL_TESTER))
     {
      if((Minute()==00 && Seconds()==1))
         LogInfo();              // Log account info. 
     }

   if(UseTrailingStops)
     {
      UpdateTrailingList(OnJournaling,RetryInterval,MagicNumber);
      ReviewTrailingStop(OnJournaling,TrailingStopOffset,RetryInterval,MagicNumber,P);
     }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
   if(ExtonHedging && (OrdersStack.LastIndex()+1)>=ExtHedgeNumberOfOrders) //Hedging is On.
     {
      if(!HedgeOpened) //Hedging is on , dont do anything to the cycle.
        {
         //applying hedge
         ApplyHedge();
        }
      else if(HedgeOpened) //Update Hedge Profit
        {
         if(OrderSelect(hedgeOrderNumber,SELECT_BY_TICKET)==true && OrderSymbol()==Symbol() && (OrderMagicNumber()==MagicNumber) && OrderType()==hedgeType)
           {
            hedgeProfit=OrderProfit();
           }
         UpdateHedgeProfit();
        }

      //// check here if the hedge was closed, and we will be back to cycle again.
      if(OrderSelect(hedgeOrderNumber,SELECT_BY_TICKET)==true && OrderSymbol()==Symbol() && (OrderMagicNumber()==MagicNumber) && OrderType()==hedgeType)
        {
         if(OrderCloseTime()!=0) // it has been closed
           {
            HedgeOpened=false;
            ExtonHedging=false;
           }
        }
     }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
   else             //Hedging is off
     {
      if(CountPosOrders(MagicNumber,OrdersType)<(OrdersStack.LastIndex()+1)) // Check if there is an opened position closed by the user or trailing stop or winnings.
        {
         if(OnJournaling)Print("an order has been closed, Modifying TakeProfit");
         if(OnJournaling)Print((CountPosOrders(MagicNumber,OrdersType))+" is the Number of Current Positions, "+(OrdersStack.LastIndex()+1)+" is the Number of Items in Stack ");
         UpdateTakeProfit(OrdersType,OnJournaling,GetAverageTakeProfit(),MagicNumber);

         if(CountPosOrders(MagicNumber,OrdersType)>2)
           {
            lastTwoPositions[1] = OrdersStack.Pop();
            lastTwoPositions[0] = OrdersStack.Pop();
            OrdersStack.Push(lastTwoPositions[0]);
            OrdersStack.Push(lastTwoPositions[1]);
           }

        }

      else if(CountPosOrders(MagicNumber,OrdersType)>(OrdersStack.LastIndex()+1)) //check if there is a position opened by the user
        {
         OrdersStack.Clear();
         GetLastAddedPositions(MagicNumber,OrdersType); // get all opened positions that are not in the cycle 
         int lastAddedPositionsSize=LastAddedPositionsStack.LastIndex()+1;
         for(int i=0; i<lastAddedPositionsSize;i++)
           {
            int lastAddedPosition=LastAddedPositionsStack.Pop();
            if(OnJournaling) Print(lastAddedPosition+" has been  opened, Modifying TakeProfit");
            if(OnJournaling)Print((CountPosOrders(MagicNumber,OrdersType))+" is the Number of Current Positions, "+(OrdersStack.LastIndex()+1)+" is the Number of Items in Stack ");
            OrdersStack.Push(lastAddedPosition);
           }
         UpdateTakeProfit(OrdersType,OnJournaling,GetAverageTakeProfit(),MagicNumber);

         if(CountPosOrders(MagicNumber,OrdersType)>2)
           {
            lastTwoPositions[1] = OrdersStack.Pop();
            lastTwoPositions[0] = OrdersStack.Pop();
            OrdersStack.Push(lastTwoPositions[0]);
            OrdersStack.Push(lastTwoPositions[1]);
           }

        }

      if(CountPosOrders(MagicNumber,OrdersType)>=CloseAfter)
        {
         //CloseLastWinningTicketLock=false;
         if(CloseLastWinningTicket)
           {
            if(CheckLastWinningTicket())
              {
               if(OrderSelect(lastTwoPositions[1],SELECT_BY_TICKET,MODE_TRADES)==true && OrderSymbol()==Symbol() && (OrderMagicNumber()==MagicNumber || OrderMagicNumber()==ManualTrading) && OrderType()==OrdersType)
                 {
                  bool Closing=false;
                  double Price=0;
                  color arrow_color=0;if(OrderType()==OP_BUY)arrow_color=MediumSeaGreen;if(OrderType()==OP_SELL)arrow_color=Aqua;

                  HandleTradingEnvironment(OnJournaling,RetryInterval);
                  if(OrdersType==OP_BUY)Price=Bid; if(OrdersType==OP_SELL)Price=Ask;
                  if(OrderCloseTime()==0) //only if its an open position
                    {
                     Closing=OrderClose(OrderTicket(),OrderLots(),Price,Slippage*P,arrow_color);
                     if(OnJournaling)Print("EA Journaling: Trying to close position "+OrderTicket()+"... its proftiable");
                     if(OnJournaling && !Closing)Print("EA Journaling: Last Winning Position ,Unexpected Error has happened. Error Description: "+GetErrorDescription(GetLastError()));
                     if(OnJournaling && Closing)Print("EA Journaling: Last Winning Position , Position successfully closed.");
                    }
                 }

              }
           }
        }

      if(!isNewBar())
        {
         return(0);
        }

      LastClose=iClose(Symbol(),0,1);

      if(UseFirstOrderStopLoss && CountPosOrders(MagicNumber,OP_BUY)+CountPosOrders(MagicNumber,OP_SELL)==0)
        {
         OrdersStack.Clear();
         OpenInitialOrder(Lot,LotMultiplier,LotStep,InitialTakeProfit,MagicNumber,Slippage,OnJournaling,FirstOrderStopLoss);
         OrdersOpened=1;
        }
      else if(!UseFirstOrderStopLoss)
        {
         if(CheckClosed(OrdersType,MagicNumber,P,Slippage,OnJournaling,CalculatePipStep(IncrementPipStep,PipsStep),LastClose))
           {
            OrdersStack.Clear();
            OpenInitialOrder(Lot,LotMultiplier,LotStep,InitialTakeProfit,MagicNumber,Slippage,OnJournaling,0);
            OrdersOpened=1;
           }
        }
     }

   return (0);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+   
//|End of Start()                                                        |
//+------------------------------------------------------------------+


//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//|                     FUNCTIONS LIBRARY                                   
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

/*

Content:
1) CheckClosed
2) PrintLots
3) PrintAveragePrice
4) GetCycleAveragePrice
5) GetCycleLots
6) ApplyHedge
7) CloseAllMagicNumber
8) CloseAllPositions
9) GetAverageTakeProfit
10)UpdateTakeProfit
11) OpenInitialOrder
12) CheckHeikenAshi
13) DrawText
14) DrawChartInfo 
15) GetLot
16) CheckLot
17) CountPosOrders
18) IsMaxPositionsReached
19) OpenPositionMarket
20) CloseOrderPosition
21) GetP
22) GetYenAdjustFactor
23) IsLossLimitBreached
24) IsProfitLimitBreached
25) ApplyTestMode
26) HandleTradingEnvironment
27) GetErrorDescription
28) ChartInfo	
29) CheckAccount
30) isNewBar
31) RSICross
32) LogInfo
33)ClearTrailingList
34)ReviewTrailingList
35)SetTrailingStop
36)RemoveTrailingStop
37)CheckLastWinningTicket

*/

//+------------------------------------------------------------------+
// Start of CheckClosed()                                    			|
//+------------------------------------------------------------------+
bool CheckClosed(ENUM_ORDERTYPES ordersType,int Magic,int K,int Slip,bool Journaling,int pipsStep,double lastClose)
  {
// Type: Customisable 
// Modify this function to suit your trading robot

// this function check if the previous positions have been closed to TakeProfit , and if so , open an initial Order. 
//and if not check if the conditions are met to open a new Order.

   int lastOrderTicket=OrdersStack.TopEl();
   int newOrderTicket;

   if(CountPosOrders(Magic,ordersType)<=0)
     {
      return true;
     }
   else
     {
      if(OrderSelect(lastOrderTicket,SELECT_BY_TICKET,MODE_TRADES)==true && OrderSymbol()==Symbol() && (OrderMagicNumber()==Magic || OrderMagicNumber()==ManualTrading) && OrderType()==ordersType)
        {
         if(OrderType()==OP_SELL)
           {
            if(lastClose>=(OrderOpenPrice()+(pipsStep*K*Point)))
              {// last close price of the last bar is bigger than the offset, open a new position
               OrdersOpened=OrdersOpened+1;
               newOrderTicket=OpenPositionMarket(ordersType,GetLot(Lot,false,Journaling),0,0,Magic,Slip,Journaling,K,MaxRetriesPerTick);
               if(newOrderTicket!=-1)
                 {
                  lastTwoPositions[0]=lastOrderTicket;//Save the last two positions we will need it to check for closed winnings
                  lastTwoPositions[1]=newOrderTicket;

                  OrdersStack.Push(newOrderTicket);

                  if(MaintainOnlyX && OrdersStack.LastIndex()+1>X)
                    {
                     CloseFirstOrder();
                    }

                  UpdateTakeProfit(ordersType,Journaling,GetAverageTakeProfit(),Magic);

                  if(UseTrailingStops && CountPosOrders(Magic,ordersType)>UseAfter) //Trailing Stop Flag
                    {
                     SetTrailingStop(Journaling,RetryInterval,Magic,K,newOrderTicket);
                    }
                 }
               return false;
              }
           }

         else if(OrderType()==OP_BUY)
           {
            if(lastClose<=(OrderOpenPrice()-((pipsStep)*K*Point)))
              {// last close price of the last bar is bigger than the offset, open a new position
               OrdersOpened=OrdersOpened+1;
               newOrderTicket=OpenPositionMarket(ordersType,GetLot(Lot,false,Journaling),0,0,Magic,Slip,Journaling,K,MaxRetriesPerTick);
               if(newOrderTicket!=-1)
                 {
                  lastTwoPositions[0]=lastOrderTicket;      //Save the last two positions we will need it to check for closed winnings
                  lastTwoPositions[1]=newOrderTicket;

                  OrdersStack.Push(newOrderTicket);

                  if(MaintainOnlyX && OrdersStack.LastIndex()+1>X)
                    {
                     CloseFirstOrder();
                    }

                  UpdateTakeProfit(ordersType,Journaling,GetAverageTakeProfit(),Magic);

                  if(UseTrailingStops && CountPosOrders(Magic,ordersType)>UseAfter)//Trailing Stop Flag
                    {
                     SetTrailingStop(Journaling,RetryInterval,Magic,K,newOrderTicket);

                    }
                 }

               return false;

              }
           }
        }
     }
   return false;
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
// End of CloseFirstOrder()                                    				|
//+------------------------------------------------------------------+

void CloseFirstOrder()
  {
   int lastOrder=0;
   OrdersTempStack.Clear();
   int stackLastIndex=OrdersStack.LastIndex();

   for(int i=0; i<stackLastIndex;i++)
     {
      lastOrder=OrdersStack.Pop();
      OrdersTempStack.Push(lastOrder);
     }

   lastOrder=OrdersStack.Pop();
   CloseTicket(lastOrder,OnJournaling,MagicNumber,Slippage,P);

   OrdersStack.Clear();
   int tempStackLastIndex=OrdersTempStack.LastIndex();
   for(int i=0; i<=tempStackLastIndex;i++)
     {
      lastOrder=OrdersTempStack.Pop();
      OrdersStack.Push(lastOrder);
     }

   OrdersTempStack.Clear();

  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void CloseTicket(int ticket,bool Journaling,int Magic,int Slip,int K)
  {

// Type: Fixed Template 
// Do not edit unless you know what you're doing
   int ordersPos=OrdersTotal();

   if(OrderSelect(ticket,SELECT_BY_TICKET,MODE_TRADES)==true && OrderSymbol()==Symbol() && (OrderMagicNumber()==Magic || OrderMagicNumber()==ManualTrading))
     {
      bool Closing=false;
      double Price=0;
      color arrow_color=0;if(OrderType()==OP_BUY)arrow_color=MediumSeaGreen;if(OrderType()==OP_SELL)arrow_color=DarkOrange;
      if(Journaling)Print("EA Journaling: Trying to close position "+OrderTicket()+" ...");
      HandleTradingEnvironment(Journaling,RetryInterval);
      if(OrderType()==OP_BUY)Price=Bid; if(OrderType()==OP_SELL)Price=Ask;
      lastClosedProfit+=OrderProfit();
      Closing=OrderClose(OrderTicket(),OrderLots(),Price,Slip*K,arrow_color);
      if(Journaling && !Closing)Print("CEA Journaling: Unexpected Error has happened. Error Description: "+GetErrorDescription(GetLastError()));
      if(Journaling && Closing)Print("CEA Journaling: Position successfully closed.");
     }
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
// Start of PrintLots()                                    		|
//+------------------------------------------------------------------+

void PrintLots()
  {
   Print("Lots of the last Cycle : "+GetCycleLots());
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
// End of PrintLots()                                    		|
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
// Start of PrintAveragePrice()                                    		|
//+------------------------------------------------------------------+

void PrintAveragePrice()
  {
   Print("Average Price of the current cycle: "+GetCycleAveragePrice());
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
// End of PrintAveragePrice()                                    		|
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
// Start of MultiplySequenceSplit()                                    		|
//+------------------------------------------------------------------+
void MultiplySequenceSplit()
  {

// Type: Customisable 
// Modify this function to suit your trading robot

// this function splits the multiplySequence into multiplySequenceArray

   static bool firstTime=true;
   if(firstTime)
     {
      StringSplit(MultiplySequence,',',multiplySequenceArray); // Splitting the multiplication sequene of the lot
      firstTime=false;
     }

  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
// End of MultiplySequenceSplit()                                    		|
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
// Start of GetCycleAveragePrice()                                    		|
//+------------------------------------------------------------------+

double GetCycleAveragePrice()
  {
// Type: Customisable 
// Modify this function to suit your trading robot

// this function returns the average price of the current cycle
   double lotSum=0;
   double cycleSumPrice=0;
   double cycleAveragePrice=0;

   for(int i=0; i<OrdersTotal(); i++)
     {
      if(OrderSelect(i,SELECT_BY_POS,MODE_TRADES)==true && OrderSymbol()==Symbol() && (OrderMagicNumber()==MagicNumber || OrderMagicNumber()==ManualTrading) && OrderType()==OrdersType)
        {
         cycleSumPrice+=OrderLots()*OrderOpenPrice();
         lotSum+=OrderLots();
        }
     }

   if(lotSum==0)
     {
      return 0;
     }
   cycleAveragePrice=cycleSumPrice/lotSum;
   return(NormalizeDouble(cycleAveragePrice,Digits));

  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//| End of GetCycleAveragePrice()                                                             |
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//| Start of GetCycleLots()                                                             |
//+------------------------------------------------------------------+
double GetCycleLots()
  {

// Type: Customisable 
// Modify this function to suit your trading robot

// this function returns the net profit of the opened orders 

   double Lots=0;

   for(int i=0; i<OrdersTotal(); i++)
     {
      if(OrderSelect(i,SELECT_BY_POS,MODE_TRADES)==true && OrderSymbol()==Symbol() && (OrderMagicNumber()==MagicNumber || OrderMagicNumber()==ManualTrading) && OrderType()==OrdersType)
         Lots+=OrderLots();
     }
   return(NormalizeDouble(Lots,2));

  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
// End of GetCycleLots()                                    		|
//+------------------------------------------------------------------+
double GetCycleProfit()
  {

// Type: Customisable 
// Modify this function to suit your trading robot

// this function returns the net profit of the opened orders 

   double Profit=0;

   for(int i=0; i<OrdersTotal(); i++)
     {
      if(OrderSelect(i,SELECT_BY_POS,MODE_TRADES)==true && OrderSymbol()==Symbol() && (OrderMagicNumber()==MagicNumber || OrderMagicNumber()==ManualTrading) && OrderType()==OrdersType)
         Profit+=OrderProfit();
     }
   return(NormalizeDouble(Profit,1));
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
// End of GetCycleProfit()                                    			|
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
// Start of ApplyHedge()                                    			|
//+------------------------------------------------------------------+
void ApplyHedge()
  {

// Type: Customisable 
// Modify this function to suit your trading robot

// this function applies a hedge upon user request.

   UpdateTakeProfit(OrdersType,OnJournaling,0,MagicNumber);
   if(OrdersType==OP_SELL)
     {
      hedgeType=OP_BUY;
     }
   else
     {
      hedgeType=OP_SELL;
     }

   hedgeLot=GetCycleLots();

   hedgeOrderNumber=OpenPositionMarket(hedgeType,hedgeLot*ExthedgeLotType,0,0,MagicNumber,Slippage,OnJournaling,P,MaxRetriesPerTick);

   if(OnJournaling && hedgeOrderNumber<0) Print("EA Journaling: Unexpected Error has happened , Hedge position wasnt opened. Error Description: "+GetErrorDescription(GetLastError()));
   if(OnJournaling && hedgeOrderNumber>0) Print("EA Journaling: Hedge Position has been opened successfuly, OrderNumber : "+hedgeOrderNumber+" ,of Type "+hedgeType);

   if(hedgeOrderNumber>0)
     {
      HedgeOpened=true;
     }

   else
     {
      HedgeOpened=false;
     }
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
// End of ApplyHedge()                                    			|
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
// Start of CloseAllMagicNumber()                                    				|
//+------------------------------------------------------------------+
void CloseAllMagicNumber(int Magic)
  {
// Type: Customisable 
// Modify this function to suit your trading robot

// this function Closes all positions for the specific MagicNumber

   if(MQLInfoInteger(MQL_TESTER)) // if in test mode.
     {
      bool closebuy=CloseOrderPosition(OP_BUY,OnJournaling,Magic,Slippage,P);

      bool closesell=CloseOrderPosition(OP_SELL,OnJournaling,Magic,Slippage,P);

      if(OnJournaling)if(closebuy && closesell) Print("All positions with MagicNumber "+Magic+" have been closed!");
      if(OnJournaling)if(!closebuy) Print("Error Occured: None of the Buy positions have been closed!");
      if(OnJournaling)if(!closesell) Print("Error Occured: None of the Sell positions have been closed!");

      return;
     }

   if(MessageBox("Are you sure you want to close all the positions related to the MagicNumber "+Magic+" ?"+
      "\nPlease Note that with ManualTrading On, all opened manually opened Positions will be close As well!","Warning",MB_ICONSTOP|MB_OKCANCEL)==1)
     {
      bool closebuy=CloseOrderPosition(OP_BUY,OnJournaling,Magic,Slippage,P);

      bool closesell=CloseOrderPosition(OP_SELL,OnJournaling,Magic,Slippage,P);

      if(OnJournaling)if(closebuy && closesell) Print("All positions with MagicNumber "+Magic+" have been closed!");
      if(OnJournaling)if(!closebuy) Print("Error Occured: None of the Buy positions have been closed!");
      if(OnJournaling)if(!closesell) Print("Error Occured: None of the Sell positions have been closed!");
     }
   else
     {
      if(OnJournaling) Print("None of the position were closed upon your request!");
     }
   return;
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
// End of CloseAllMagicNumber()                                    			|
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//| Start of CloseAllPositions()
//+------------------------------------------------------------------+ 
void CloseAllPositions()
  {
// Type: Fixed Template 
// Do not edit unless you know what you're doing

// This function closes all positions 

   CloseOrderPosition(OP_BUY,OnJournaling,MagicNumber,Slippage,P);

   CloseOrderPosition(OP_SELL,OnJournaling,MagicNumber,Slippage,P);

   CloseOrderPosition(OP_BUYSTOP,OnJournaling,MagicNumber,Slippage,P);

   CloseOrderPosition(OP_SELLSTOP,OnJournaling,MagicNumber,Slippage,P);

   if(OnJournaling) Print("All Positions have been closed,'cause of a breach in Profit or Loss");

   return;

  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
// Start of GetAverageTakeProfit()                             		|
//+------------------------------------------------------------------+

double GetAverageTakeProfit()
  {
// Type: Customisable 
// Modify this function to suit your trading robot

// this function calculates the take profit for the average price between all opened positions.

   int lastOrder=0;
   double averageTakeProfit;
   double averagePrice;
   double sum=0;
   double lotSum=0;

   OrdersTempStack.Clear();
   int stackLastIndex=OrdersStack.LastIndex();

   for(int i=0; i<=stackLastIndex;i++)
     {
      lastOrder=OrdersStack.Pop();

      if(OrderSelect(lastOrder,SELECT_BY_TICKET)==true && OrderSymbol()==Symbol() && (OrderMagicNumber()==MagicNumber || OrderMagicNumber()==ManualTrading) && OrderType()==OrdersType)
        {
         if(OrderCloseTime()==0) // Still Open Position
           {
            sum+=OrderLots() *OrderOpenPrice();
            lotSum+=OrderLots();
            OrdersTempStack.Push(lastOrder);
           }

         else
           {
            lastClosedProfit+=OrderProfit();            //this order is Closed add its profit to the lastClosedProfit
           }
        }

     }

   if(lotSum==0)//there is no orders in the stack 
     {
      Print("No Opened Positions	");
      return (0);
     }

   averagePrice=sum/lotSum;
//Print("Average Price "+averagePrice);

   if(OrdersType==OP_BUY)
     {
      averageTakeProfit=(averagePrice+(CalculateTakeProfit(IncrementTakeProfit,InitialTakeProfit)*P*Point));
     }

   else if(OrdersType==OP_SELL)
     {
      averageTakeProfit=(averagePrice-(CalculateTakeProfit(IncrementTakeProfit,InitialTakeProfit)*P*Point));
     }

   OrdersStack.Clear();
   int tempStackLastIndex=OrdersTempStack.LastIndex();
   for(int i=0; i<=tempStackLastIndex;i++)
     {
      lastOrder=OrdersTempStack.Pop();
      OrdersStack.Push(lastOrder);
     }

   OrdersTempStack.Clear();
   averageTakeProfit=NormalizeDouble(averageTakeProfit,Digits);
   return averageTakeProfit;

  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
// End of GetAverageTakeProfit()                        					|
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
// Start of UpdateTakeProfit()                        						|
//+------------------------------------------------------------------+

void UpdateTakeProfit(ENUM_ORDERTYPES ordersType,bool Journaling,double averageTakeProfit,int Magic)
  {

// Type: Customisable 
// Modify this function to suit your trading robot

// this function updates the take profit for all opened position after it gets recalculated by GetAverageTakeProfit
   if(averageTakeProfit==0) // 
     {
      return;
     }

   int ordersPos=OrdersTotal();
   bool Modify=false;
   for(int i=ordersPos-1; i>=0; i--)
     {
      // Note: Once pending orders become positions, OP_BUYLIMIT AND OP_BUYSTOP becomes OP_BUY, OP_SELLLIMIT and OP_SELLSTOP becomes OP_SELL
      if(ordersType==OP_BUY)
        {
         if(OrderSelect(i,SELECT_BY_POS,MODE_TRADES)==true && OrderSymbol()==Symbol() && (OrderMagicNumber()==Magic || OrderMagicNumber()==ManualTrading) && OrderType()==ordersType)
           {
            if(OrderTakeProfit()!=averageTakeProfit)
              {
               //if(Journaling)Print("EA Journaling: Trying to modify order "+OrderTicket()+" ...");
               HandleTradingEnvironment(Journaling,RetryInterval);
               Modify=OrderModify(OrderTicket(),OrderOpenPrice(),OrderStopLoss(),averageTakeProfit,0,CLR_NONE);
              }
           }
        }

      if(ordersType==OP_SELL)
        {
         if(OrderSelect(i,SELECT_BY_POS,MODE_TRADES)==true && OrderSymbol()==Symbol() && (OrderMagicNumber()==Magic || OrderMagicNumber()==ManualTrading) && OrderType()==ordersType)
           {
            if(OrderTakeProfit()!=averageTakeProfit)
              {
               //if(Journaling)Print("EA Journaling: Trying to modify order "+OrderTicket()+" ...");
               HandleTradingEnvironment(Journaling,RetryInterval);
               Modify=OrderModify(OrderTicket(),OrderOpenPrice(),OrderStopLoss(),averageTakeProfit,0,CLR_NONE);
              }
           }
        }
     }

   if(Journaling && !Modify)Print("EA Journaling: Unexpected Error has happened. Error Description: "+GetErrorDescription(GetLastError()));
   if(Journaling && Modify)Print("EA Journaling: Opened Positions successfully modified, TakeProfit Changed.");

  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
// End of UpdateTakeProfit()                        						|
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
// Start of CalculateTakeProfit()                        						|
//+------------------------------------------------------------------+

double CalculateTakeProfit(ENUM_INCREMENTTAKEPROFIT incrementTakeProfit,double takeProfit)
  {
   double output=0;

   if(incrementTakeProfit==0)
     {
      output=takeProfit;
     }
   else if(incrementTakeProfit==1) // Multiply
     {
      output=takeProfit;
      for(int i=0; i<OrdersOpened-1;i++)
        {
         output*=TakeProfitMultiplier;
        }
     }

   else if(incrementTakeProfit==2)// add 
     {
      output=takeProfit;
      for(int i=0; i<OrdersOpened-1;i++)
        {
         output+=TakeProfitStep;
        }
     }

   output=NormalizeDouble(output,2); // Round to 2 decimal place
   return(output);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
// End of CalculateTakeProfit()                        						|
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
// Start of CalculatePipStep()                        						|
//+------------------------------------------------------------------+

double CalculatePipStep(ENUM_INCREMENTPIPSTEP incrementPipStep,double pipStep)
  {
   double output=0;

   if(incrementPipStep==0)
     {
      output=pipStep;
     }
   else if(incrementPipStep==1) // Multiply
     {
      output=pipStep;
      for(int i=0; i<OrdersOpened-1;i++)
        {
         output*=PipStepMultiplier;
        }
     }

   else if(incrementPipStep==2)// add 
     {
      output=pipStep;
      for(int i=0; i<OrdersOpened-1;i++)
        {
         output+=PipStepAdding;
        }
     }
   output=NormalizeDouble(output,2); // Round to 2 decimal place
   return(output);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
// End of CalculatePipStep()                        						|
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
// Start of OpenInitialOrder()                                    	|
//+------------------------------------------------------------------+
void OpenInitialOrder(double initLot,double lotMultiplier,double lotStep,double initTakeProfit,int Magic,int Slip,bool Journaling,double FirstOStopLoss)
  {

// Type: Customisable 
// Modify this function to suit your trading robot

// this function opened an initial order where the position takes initial lot and initial takeprofit pips.
   string text1;
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
   if(EntryRule==2)
     { // RSI 
      MyRSI=iRSI(Symbol(),RSIApplyPeriod,RSIPeriod,PRICE_CLOSE,1);
      //+------------------------------------------------------------------+
      //|                                                                  |
      //+------------------------------------------------------------------+
      if(MyRSI>RSILimit)
        {
         OrdersType=OP_BUY;
         if(EntryReason)
           {
            text1="MyRSI is above "+RSILimit;
           }
        }
      //+------------------------------------------------------------------+
      //|                                                                  |
      //+------------------------------------------------------------------+
      else
        {
         OrdersType=OP_SELL;
         if(EntryReason)
           {
            text1="MyRSI is below  "+RSILimit;
           }
        }
     }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
   else if(EntryRule==1)
     {// HeikenAshi
      HAOpen_1=iCustom(Symbol(),HeikenAshiApplyPeriod,"Heiken Ashi",Red,White,Red,White,2,1);
      HAClose_1=iCustom(Symbol(),HeikenAshiApplyPeriod,"Heiken Ashi",Red,White,Red,White,3,1);
      HAColor=CheckHeikenAshi(HAOpen_1,HAClose_1); // 1 White , 2 Red
      //+------------------------------------------------------------------+
      //|                                                                  |
      //+------------------------------------------------------------------+
      if(HAColor==1)
        {
         OrdersType=OP_BUY;
         if(EntryReason)
           {

            text1="HeikenAshi is White";
           }
        }
      //+------------------------------------------------------------------+
      //|                                                                  |
      //+------------------------------------------------------------------+
      else
        {
         OrdersType=OP_SELL;
         if(EntryReason)
           {
            text1="HeikenAshi is Red";
           }
        }
     }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
   else if(EntryRule==3)
     {//Specifc Type
      OrdersType=EntryType;
      text1="";
     }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
   else
     { //RSI Cross
      MyCrossRSI1=iRSI(Symbol(),RSICrossApplyPeriod,RSICrossPeriod,PRICE_CLOSE,1);
      MyCrossRSI2=iRSI(Symbol(),RSICrossApplyPeriod,RSICrossPeriod,PRICE_CLOSE,2);
      //+------------------------------------------------------------------+
      //|                                                                  |
      //+------------------------------------------------------------------+
      if(RSICross(MyCrossRSI1,MyCrossRSI2,RSICrossUpperLimit)==2) //RSI Cross above RSICrossUpperLimit
        {
         OrdersType=OP_SELL;
         text1="RSI Crossed below RSICrossUpperLimit : "+RSICrossUpperLimit;
        }
      //+------------------------------------------------------------------+
      //|                                                                  |
      //+------------------------------------------------------------------+
      else if(RSICross(MyCrossRSI1,MyCrossRSI2,RSICrossLowerLimit)==1) //RSI Cross below RSICrossLowerLimit
        {
         OrdersType=OP_BUY;
         text1="RSI Crossed above RSICrossLowerLimit : "+RSICrossLowerLimit;
        }
      //+------------------------------------------------------------------+
      //|                                                                  |
      //+------------------------------------------------------------------+
      else
        {
         return;
        }
     }

   OrderNumber=OpenPositionMarket(OrdersType,GetLot(initLot,true,Journaling),FirstOStopLoss,initTakeProfit,Magic,Slip,Journaling,P,MaxRetriesPerTick);
   OrdersStack.Push(OrderNumber);
   ClearTrailingList();
   if(UseTrailingStops && CountPosOrders(Magic,OrdersType)>UseAfter) //Trailing Stop Flag
     {
      SetTrailingStop(Journaling,RetryInterval,Magic,P,OrderNumber);
     }

   DrawText(text1);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
// End of OpenInitialOrder()                                    				|
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
// Start of CheckHeikenAshi()                                    			|
//+------------------------------------------------------------------+
int CheckHeikenAshi(double openHighPeriodShift1,double closeHighPeriodShift1)
  {
// Type: Customisable 
// Modify this function to suit your trading robot

// This function checks for the direction of the HA Indicator on the High Period

   int output=0;
   if(openHighPeriodShift1<closeHighPeriodShift1)
     {
      output=1;         //White
        }else{
      output=2;         //Red
     }

   return (output);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
// End of CheckHeikenAshi()                                          |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
// Start of DrawText()                                          |
//+------------------------------------------------------------------+

void DrawText(string text1)
  {

   string name=IntegerToString(OrderNumber,0,' ');
   ObjectCreate(ChartID(),name+"text1",OBJ_TEXT,0,TimeCurrent()-10,SymbolInfoDouble(Symbol(),SYMBOL_BID)+(0.0003));
   ObjectSetString(ChartID(),name+"text1",OBJPROP_TEXT,text1);
   ObjectSetString(ChartID(),name+"text1",OBJPROP_FONT,"Verdana");
   ObjectSetInteger(ChartID(),name+"text1",OBJPROP_FONTSIZE,6);
   ObjectSetInteger(ChartID(),name+"text1",OBJPROP_COLOR,Red);
   ObjectSetDouble(ChartID(),name+"text1",OBJPROP_ANGLE,0);

  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
// End of DrawText()                                          |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
// Start of DrawChartInfo()                                          |
//+------------------------------------------------------------------+
void DrawChartInfo()
  {

   ObjectCreate(ChartID(),ObjName+"HelmiFX1",OBJ_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"HelmiFX1",OBJPROP_TEXT,"HelmiFX");
   ObjectSetInteger(ChartID(),ObjName+"HelmiFX1",OBJPROP_CORNER,CORNER_RIGHT_UPPER);
   ObjectSetInteger(ChartID(),ObjName+"HelmiFX1",OBJPROP_XDISTANCE,xDistance);
   ObjectSetInteger(ChartID(),ObjName+"HelmiFX1",OBJPROP_YDISTANCE,12);
   ObjectSetString(ChartID(),ObjName+"HelmiFX1",OBJPROP_FONT,"Verdana");
   ObjectSetInteger(ChartID(),ObjName+"HelmiFX1",OBJPROP_FONTSIZE,8);
   ObjectSetInteger(ChartID(),ObjName+"HelmiFX1",OBJPROP_ANCHOR,ANCHOR_RIGHT_UPPER);
   ObjectSetInteger(ChartID(),ObjName+"HelmiFX1",OBJPROP_COLOR,White);

   ObjectCreate(ChartID(),ObjName+"HelmiFX2",OBJ_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"HelmiFX2",OBJPROP_TEXT,"www.helmifx.com");
   ObjectSetInteger(ChartID(),ObjName+"HelmiFX2",OBJPROP_CORNER,CORNER_RIGHT_UPPER);
   ObjectSetInteger(ChartID(),ObjName+"HelmiFX2",OBJPROP_XDISTANCE,xDistance);
   ObjectSetInteger(ChartID(),ObjName+"HelmiFX2",OBJPROP_YDISTANCE,37);
   ObjectSetString(ChartID(),ObjName+"HelmiFX2",OBJPROP_FONT,"Verdana");
   ObjectSetInteger(ChartID(),ObjName+"HelmiFX2",OBJPROP_FONTSIZE,8);
   ObjectSetInteger(ChartID(),ObjName+"HelmiFX2",OBJPROP_ANCHOR,ANCHOR_RIGHT);
   ObjectSetInteger(ChartID(),ObjName+"HelmiFX2",OBJPROP_COLOR,White);

   ObjectCreate(ChartID(),ObjName+"Symbol",OBJ_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"Symbol",OBJPROP_TEXT,Symbol());
   ObjectSetInteger(ChartID(),ObjName+"Symbol",OBJPROP_CORNER,CORNER_RIGHT_UPPER);
   ObjectSetInteger(ChartID(),ObjName+"Symbol",OBJPROP_XDISTANCE,xDistance);
   ObjectSetInteger(ChartID(),ObjName+"Symbol",OBJPROP_YDISTANCE,87);
   ObjectSetString(ChartID(),ObjName+"Symbol",OBJPROP_FONT,"Verdana");
   ObjectSetInteger(ChartID(),ObjName+"Symbol",OBJPROP_FONTSIZE,10);
   ObjectSetInteger(ChartID(),ObjName+"Symbol",OBJPROP_ANCHOR,ANCHOR_RIGHT);
   ObjectSetInteger(ChartID(),ObjName+"Symbol",OBJPROP_COLOR,Yellow);

   ObjectCreate(ChartID(),ObjName+"PipValueOf1Lot",OBJ_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"PipValueOf1Lot",OBJPROP_TEXT,"Pip Value of 1 Lot is "+DoubleToStr(pipValue1Lot,2)+"$");
   ObjectSetInteger(ChartID(),ObjName+"PipValueOf1Lot",OBJPROP_CORNER,CORNER_RIGHT_UPPER);
   ObjectSetInteger(ChartID(),ObjName+"PipValueOf1Lot",OBJPROP_XDISTANCE,xDistance);
   ObjectSetInteger(ChartID(),ObjName+"PipValueOf1Lot",OBJPROP_YDISTANCE,112);
   ObjectSetString(ChartID(),ObjName+"PipValueOf1Lot",OBJPROP_FONT,"Verdana");
   ObjectSetInteger(ChartID(),ObjName+"PipValueOf1Lot",OBJPROP_FONTSIZE,10);
   ObjectSetInteger(ChartID(),ObjName+"PipValueOf1Lot",OBJPROP_ANCHOR,ANCHOR_RIGHT);
   ObjectSetInteger(ChartID(),ObjName+"PipValueOf1Lot",OBJPROP_COLOR,Pink);

   ObjectCreate(ChartID(),ObjName+"PipValue",OBJ_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"PipValue",OBJPROP_TEXT,"Total Lots = "+DoubleToStr(cycleLots,2)+" Lot = "+DoubleToStr(pipValue,2)+"$");
   ObjectSetInteger(ChartID(),ObjName+"PipValue",OBJPROP_CORNER,CORNER_RIGHT_UPPER);
   ObjectSetInteger(ChartID(),ObjName+"PipValue",OBJPROP_XDISTANCE,xDistance);
   ObjectSetInteger(ChartID(),ObjName+"PipValue",OBJPROP_YDISTANCE,137);
   ObjectSetString(ChartID(),ObjName+"PipValue",OBJPROP_FONT,"Verdana");
   ObjectSetInteger(ChartID(),ObjName+"PipValue",OBJPROP_FONTSIZE,10);
   ObjectSetInteger(ChartID(),ObjName+"PipValue",OBJPROP_ANCHOR,ANCHOR_RIGHT);
   ObjectSetInteger(ChartID(),ObjName+"PipValue",OBJPROP_COLOR,DeepPink);

   ObjectCreate(ChartID(),ObjName+"CountPositions",OBJ_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"CountPositions",OBJPROP_TEXT,"Number of Positions : "+(CountPosOrders(MagicNumber,OP_BUY)+CountPosOrders(MagicNumber,OP_SELL)));
   ObjectSetInteger(ChartID(),ObjName+"CountPositions",OBJPROP_CORNER,CORNER_RIGHT_UPPER);
   ObjectSetInteger(ChartID(),ObjName+"CountPositions",OBJPROP_XDISTANCE,xDistance);
   ObjectSetInteger(ChartID(),ObjName+"CountPositions",OBJPROP_YDISTANCE,162);
   ObjectSetString(ChartID(),ObjName+"CountPositions",OBJPROP_FONT,"Verdana");
   ObjectSetInteger(ChartID(),ObjName+"CountPositions",OBJPROP_FONTSIZE,10);
   ObjectSetInteger(ChartID(),ObjName+"CountPositions",OBJPROP_ANCHOR,ANCHOR_RIGHT);
   ObjectSetInteger(ChartID(),ObjName+"CountPositions",OBJPROP_COLOR,Brown);

   ObjectCreate(ChartID(),ObjName+"Equity",OBJ_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"Equity",OBJPROP_TEXT,"Account Equity : "+AccountEquity());
   ObjectSetInteger(ChartID(),ObjName+"Equity",OBJPROP_CORNER,CORNER_RIGHT_UPPER);
   ObjectSetInteger(ChartID(),ObjName+"Equity",OBJPROP_XDISTANCE,xDistance);
   ObjectSetInteger(ChartID(),ObjName+"Equity",OBJPROP_YDISTANCE,187);
   ObjectSetString(ChartID(),ObjName+"Equity",OBJPROP_FONT,"Verdana");
   ObjectSetInteger(ChartID(),ObjName+"Equity",OBJPROP_FONTSIZE,10);
   ObjectSetInteger(ChartID(),ObjName+"Equity",OBJPROP_ANCHOR,ANCHOR_RIGHT);
   ObjectSetInteger(ChartID(),ObjName+"Equity",OBJPROP_COLOR,BurlyWood);

   double ChartRSI=iRSI(Symbol(),RSIApplyPeriod,RSIPeriod,PRICE_CLOSE,1);
   ObjectCreate(ChartID(),ObjName+"RSIValue",OBJ_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"RSIValue",OBJPROP_TEXT,"RSI Value : "+DoubleToStr(ChartRSI,2));
   ObjectSetInteger(ChartID(),ObjName+"RSIValue",OBJPROP_CORNER,CORNER_RIGHT_UPPER);
   ObjectSetInteger(ChartID(),ObjName+"RSIValue",OBJPROP_XDISTANCE,xDistance);
   ObjectSetInteger(ChartID(),ObjName+"RSIValue",OBJPROP_YDISTANCE,212);
   ObjectSetString(ChartID(),ObjName+"RSIValue",OBJPROP_FONT,"Verdana");
   ObjectSetInteger(ChartID(),ObjName+"RSIValue",OBJPROP_FONTSIZE,10);
   ObjectSetInteger(ChartID(),ObjName+"RSIValue",OBJPROP_ANCHOR,ANCHOR_RIGHT);

   if(ChartRSI>RSILimit)
     {
      ObjectSetInteger(ChartID(),ObjName+"RSIValue",OBJPROP_COLOR,Lime);
     }
   else
     {
      ObjectSetInteger(ChartID(),ObjName+"RSIValue",OBJPROP_COLOR,Red);
     }

   ObjectCreate(ChartID(),ObjName+"NetProfit",OBJ_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"NetProfit",OBJPROP_TEXT,"Net Profit "+DoubleToString(netProfit,1));
   ObjectSetInteger(ChartID(),ObjName+"NetProfit",OBJPROP_CORNER,CORNER_RIGHT_UPPER);
   ObjectSetInteger(ChartID(),ObjName+"NetProfit",OBJPROP_XDISTANCE,xDistance);
   ObjectSetInteger(ChartID(),ObjName+"NetProfit",OBJPROP_YDISTANCE,237);
   ObjectSetString(ChartID(),ObjName+"NetProfit",OBJPROP_FONT,"Verdana");
   ObjectSetInteger(ChartID(),ObjName+"NetProfit",OBJPROP_FONTSIZE,11);
   ObjectSetInteger(ChartID(),ObjName+"NetProfit",OBJPROP_ANCHOR,ANCHOR_RIGHT);
   ObjectSetInteger(ChartID(),ObjName+"NetProfit",OBJPROP_COLOR,DarkTurquoise);

   ObjectCreate(ChartID(),ObjName+"ClosedPositionsProfit",OBJ_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"ClosedPositionsProfit",OBJPROP_TEXT,"Closed Positions Profit "+DoubleToString(lastClosedProfit,1));
   ObjectSetInteger(ChartID(),ObjName+"ClosedPositionsProfit",OBJPROP_CORNER,CORNER_RIGHT_UPPER);
   ObjectSetInteger(ChartID(),ObjName+"ClosedPositionsProfit",OBJPROP_XDISTANCE,xDistance);
   ObjectSetInteger(ChartID(),ObjName+"ClosedPositionsProfit",OBJPROP_YDISTANCE,262);
   ObjectSetString(ChartID(),ObjName+"ClosedPositionsProfit",OBJPROP_FONT,"Verdana");
   ObjectSetInteger(ChartID(),ObjName+"ClosedPositionsProfit",OBJPROP_FONTSIZE,11);
   ObjectSetInteger(ChartID(),ObjName+"ClosedPositionsProfit",OBJPROP_ANCHOR,ANCHOR_RIGHT);
   ObjectSetInteger(ChartID(),ObjName+"ClosedPositionsProfit",OBJPROP_COLOR,Blue);

   ObjectCreate(ChartID(),ObjName+"CurrentCycleProfit",OBJ_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"CurrentCycleProfit",OBJPROP_TEXT,"Current Cycle Profit "+DoubleToString(cycleProfit,1));
   ObjectSetInteger(ChartID(),ObjName+"CurrentCycleProfit",OBJPROP_CORNER,CORNER_RIGHT_UPPER);
   ObjectSetInteger(ChartID(),ObjName+"CurrentCycleProfit",OBJPROP_XDISTANCE,xDistance);
   ObjectSetInteger(ChartID(),ObjName+"CurrentCycleProfit",OBJPROP_YDISTANCE,287);
   ObjectSetString(ChartID(),ObjName+"CurrentCycleProfit",OBJPROP_FONT,"Verdana");
   ObjectSetInteger(ChartID(),ObjName+"CurrentCycleProfit",OBJPROP_FONTSIZE,11);
   ObjectSetInteger(ChartID(),ObjName+"CurrentCycleProfit",OBJPROP_ANCHOR,ANCHOR_RIGHT);
   ObjectSetInteger(ChartID(),ObjName+"CurrentCycleProfit",OBJPROP_COLOR,Teal);

   ObjectCreate(ChartID(),ObjName+"HedgeProfit",OBJ_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"HedgeProfit",OBJPROP_TEXT,"HedgeProfit "+DoubleToString(hedgeProfit,1));
   ObjectSetInteger(ChartID(),ObjName+"HedgeProfit",OBJPROP_CORNER,CORNER_RIGHT_UPPER);
   ObjectSetInteger(ChartID(),ObjName+"HedgeProfit",OBJPROP_XDISTANCE,xDistance);
   ObjectSetInteger(ChartID(),ObjName+"HedgeProfit",OBJPROP_YDISTANCE,312);
   ObjectSetString(ChartID(),ObjName+"HedgeProfit",OBJPROP_FONT,"Verdana");
   ObjectSetInteger(ChartID(),ObjName+"HedgeProfit",OBJPROP_FONTSIZE,11);
   ObjectSetInteger(ChartID(),ObjName+"HedgeProfit",OBJPROP_ANCHOR,ANCHOR_RIGHT);
   ObjectSetInteger(ChartID(),ObjName+"HedgeProfit",OBJPROP_COLOR,Orange);

   if(MQLInfoInteger(MQL_TESTER))
     {
      ObjectCreate(ChartID(),ObjName+"Rectangle",OBJ_RECTANGLE_LABEL,0,0,0);
      ObjectSetInteger(ChartID(),ObjName+"Rectangle",OBJPROP_XSIZE,200);
      ObjectSetInteger(ChartID(),ObjName+"Rectangle",OBJPROP_YSIZE,24);
      ObjectSetInteger(ChartID(),ObjName+"Rectangle",OBJPROP_XDISTANCE,60);
      ObjectSetInteger(ChartID(),ObjName+"Rectangle",OBJPROP_YDISTANCE,10);
      // ObjectSetInteger(ChartID(),ObjName+"Rectangle",OBJPROP_FONTSIZE,8);
      ObjectSetInteger(ChartID(),ObjName+"Rectangle",OBJPROP_CORNER,CORNER_LEFT_UPPER);
      ObjectSetInteger(ChartID(),ObjName+"Rectangle",OBJPROP_COLOR,White);
      ObjectSetInteger(ChartID(),ObjName+"Rectangle",OBJPROP_BGCOLOR,White);
      ObjectSetInteger(ChartID(),ObjName+"Rectangle",OBJPROP_BACK,true);
      ObjectSetInteger(ChartID(),ObjName+"Rectangle",OBJPROP_SELECTABLE,true);
      ObjectSetInteger(ChartID(),ObjName+"Rectangle",OBJPROP_SELECTED,true);
      ObjectSetInteger(ChartID(),ObjName+"Rectangle",OBJPROP_HIDDEN,false);
      //--- set the priority for receiving the event of a mouse click in the chart
      ObjectSetInteger(ChartID(),ObjName+"Rectangle",OBJPROP_ZORDER,0);
     }
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
// End of DrawChartInfo()                                          |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
// Start of UpdateChartInfo()                                          |
//+------------------------------------------------------------------+
void UpdateChartInfo()
  {
   ObjectSetString(ChartID(),ObjName+"PipValueOf1Lot",OBJPROP_TEXT,"Pip Value of 1 Lot is "+DoubleToStr(pipValue1Lot,2)+"$");
   ObjectSetString(ChartID(),ObjName+"PipValue",OBJPROP_TEXT,"Total Lots = "+DoubleToStr(cycleLots,2)+" Lot = "+DoubleToStr(pipValue,2)+"$");
   ObjectSetString(ChartID(),ObjName+"ClosedPositionsProfit",OBJPROP_TEXT,"Closed Positions Profit "+DoubleToString(lastClosedProfit,1));
   ObjectSetString(ChartID(),ObjName+"CurrentCycleProfit",OBJPROP_TEXT,"Current Cycle Profit "+DoubleToString(cycleProfit,1));
   ObjectSetString(ChartID(),ObjName+"NetProfit",OBJPROP_TEXT,"Net Profit "+DoubleToString(netProfit,1));
   ObjectSetString(ChartID(),ObjName+"Equity",OBJPROP_TEXT,"Account Equity : "+DoubleToStr(AccountEquity(),2));

   double ChartRSI=iRSI(Symbol(),RSIApplyPeriod,RSIPeriod,PRICE_CLOSE,1);
   ObjectSetString(ChartID(),ObjName+"RSIValue",OBJPROP_TEXT,"RSI Value : "+DoubleToStr(ChartRSI,2));
   if(ChartRSI>RSILimit)
     {
      ObjectSetInteger(ChartID(),ObjName+"RSIValue",OBJPROP_COLOR,Lime);
     }
   else
     {
      ObjectSetInteger(ChartID(),ObjName+"RSIValue",OBJPROP_COLOR,Red);
     }

   ObjectSetString(ChartID(),ObjName+"CountPositions",OBJPROP_TEXT,"Number of Positions : "+(CountPosOrders(MagicNumber,OP_BUY)+CountPosOrders(MagicNumber,OP_SELL)));

  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
// End of UpdateChartInfo()                                          |
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
// Start of UpdateHedgeProfit()                                          |
//+------------------------------------------------------------------+
void UpdateHedgeProfit()
  {

   ObjectSetString(ChartID(),ObjName+"HedgeProfit",OBJPROP_TEXT,"HedgeProfit "+DoubleToString(hedgeProfit,1));

  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
// End of UpdateHedgeProfit()                                          |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
// Start of GetLot()                                       				|
//+------------------------------------------------------------------+
double GetLot(double lot,bool FirstOrder,bool Journaling)
  {
// Type: Customisable 
// Modify this function to suit your trading robot

// This is our sizing algorithm
   double output=0;
   if(FirstOrder)
     {
      output=lot;
     }

   else if(LotSizingMethod==1) //Multipli
     {
      output=lot;
      for(int i=0; i<OrdersOpened-1;i++)
        {
         output*=LotMultiplier;
        }
     }

   else if(LotSizingMethod==2) //Adding
     {
      output=lot;
      for(int i=0; i<OrdersOpened-1;i++)
        {
         output+=LotStep;
        }
     }

   else
     {
      int lastOrderTicket=OrdersStack.TopEl();
      if(OrderSelect(lastOrderTicket,SELECT_BY_TICKET)==true && OrderSymbol()==Symbol() && (OrderMagicNumber()==MagicNumber || OrderMagicNumber()==ManualTrading) && OrderType()==OrdersType)
        {
         output=OrderLots();
         output*=StringToDouble(multiplySequenceArray[OrdersStack.LastIndex()%ArrayRange(multiplySequenceArray,0)]);
        }
     }
   output=NormalizeDouble(output,2); // Round to 2 decimal place

   output=CheckLot(output,Journaling);

   return(output);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
// End of GetLot()                                                   |
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
// Start of CheckLot()                                       				|
//+------------------------------------------------------------------+

double CheckLot(double lot,bool Journaling)
  {
// Type: Fixed Template 
// Do not edit unless you know what you're doing

// This function checks if our Lots to be trade satisfies any broker limitations

   double LotToOpen=0;
   LotToOpen=NormalizeDouble(lot,2);
   LotToOpen=MathFloor(LotToOpen/MarketInfo(Symbol(),MODE_LOTSTEP))*MarketInfo(Symbol(),MODE_LOTSTEP);

   if(LotToOpen<MarketInfo(Symbol(),MODE_MINLOT))LotToOpen=MarketInfo(Symbol(),MODE_MINLOT);
   if(LotToOpen>MarketInfo(Symbol(),MODE_MAXLOT))LotToOpen=MarketInfo(Symbol(),MODE_MAXLOT);
   LotToOpen=NormalizeDouble(LotToOpen,2);

   if(Journaling && LotToOpen!=lot)Print("EA Journaling: Trading Lot has been changed by CheckLot function. Requested lot: "+DoubleToString(Lot)+". Lot to open: "+DoubleToString(LotToOpen));

   return(LotToOpen);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//| End of CheckLot()
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|Start of CountPosOrders()	" Count Positions"
//+------------------------------------------------------------------+
int CountPosOrders(int Magic,int TYPE)
  {
// Type: Fixed Template 
// Do not edit unless you know what you're doing

// This function counts number of positions/orders of OrderType TYPE

   int Orders=0;
   for(int i=0; i<OrdersTotal(); i++)
     {
      if(OrderSelect(i,SELECT_BY_POS,MODE_TRADES)==true && OrderSymbol()==Symbol() && (OrderMagicNumber()==Magic || OrderMagicNumber()==ManualTrading) && OrderType()==TYPE)
         Orders++;
     }
   return(Orders);

  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//| End of CountPosOrders() " Count Positions"
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//| Start of GetLastAddedPositions()
//+------------------------------------------------------------------+
void GetLastAddedPositions(int Magic,int TYPE)
  {
// Type: Fixed Template 
// Do not edit unless you know what you're doing

// This function get the last added order for the Magic and Type

   int OrderT;
   LastAddedPositionsStack.Clear();
   int ordersPos=OrdersTotal();

   for(int i=ordersPos-1; i>=0; i--)
      //+------------------------------------------------------------------+
      //|                                                                  |
      //+------------------------------------------------------------------+
     {
      if(OrderSelect(i,SELECT_BY_POS,MODE_TRADES)==true && OrderSymbol()==Symbol() && (OrderMagicNumber()==Magic || OrderMagicNumber()==ManualTrading) && OrderType()==TYPE)
        {
         OrderT=OrderTicket();
         LastAddedPositionsStack.Push(OrderT);
        }
     }
   return;

  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//| End of GetLastAddedPositions() " Count Positions"
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//| Start of IsMaxPositionsReached()                                             
//+------------------------------------------------------------------+
bool IsMaxPositionsReached(int MaxPositions,int Magic,bool Journaling)
  {
// Type: Fixed Template 
// Do not edit unless you know what you're doing 

// This function checks the number of positions we are holding against the maximum allowed 

   int result=False;
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
   if(CountPosOrders(Magic,OP_BUY)+CountPosOrders(Magic,OP_SELL)>MaxPositions)
     {
      result=True;
      if(Journaling)Print("Max Orders Exceeded");
        } else if(CountPosOrders(Magic,OP_BUY)+CountPosOrders(Magic,OP_SELL)==MaxPositions) {
      result=True;
     }

   return(result);

  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//| End of IsMaxPositionsReached()                                                
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|Start of OpenPositionMarket()
//+------------------------------------------------------------------+
int OpenPositionMarket(int TYPE,double LOT,double SL,double TP,int Magic,int Slip,bool Journaling,int K,int Max_Retries_Per_Tick)
  {
// Type: Fixed Template 
// Do not edit unless you know what you're doing 

// This function submits new orders

   int tries=0;
   string symbol=Symbol();
   int cmd=TYPE;
   double volume=CheckLot(LOT,Journaling);
   if(MarketInfo(symbol,MODE_MARGINREQUIRED)*volume>AccountFreeMargin())
     {
      Print("Can not open a trade. Not enough free margin to open "+volume+" on "+symbol);
      return(-1);
     }
   int slippage=Slip*K; // Slippage is in points. 1 point = 0.0001 on 4 digit broker and 0.00001 on a 5 digit broker
   string comment=" "+TYPE+"(#"+Magic+")";
   int magic=Magic;
   datetime expiration=0;
   color arrow_color=0;if(TYPE==OP_BUY)arrow_color=DodgerBlue;if(TYPE==OP_SELL)arrow_color=DeepPink;
   double stoploss=0;
   double takeprofit=0;
   double initTP = TP;
   double initSL = SL;
   int Ticket=-1;
   double price=0;

   while(tries<Max_Retries_Per_Tick) // Edits stops and take profits before the market order is placed
     {
      RefreshRates();
      if(TYPE==OP_BUY)price=Ask;if(TYPE==OP_SELL)price=Bid;
      //+------------------------------------------------------------------+
      //|                                                                  |
      //+------------------------------------------------------------------+
      // Sets Take Profits and Stop Loss. Check against Stop Level Limitations.
      if(TYPE==OP_BUY && SL!=0)
        {
         stoploss=NormalizeDouble(Ask-SL*K*Point,Digits);
         if(Bid-stoploss<=MarketInfo(Symbol(),MODE_STOPLEVEL)*Point)
           {
            stoploss=NormalizeDouble(Bid-MarketInfo(Symbol(),MODE_STOPLEVEL)*Point,Digits);
            if(Journaling)Print("EA Journaling: Stop Loss changed from "+initSL+" to "+MarketInfo(Symbol(),MODE_STOPLEVEL)/K+" pips");
           }
        }
      //+------------------------------------------------------------------+
      //|                                                                  |
      //+------------------------------------------------------------------+
      if(TYPE==OP_SELL && SL!=0)
        {
         stoploss=NormalizeDouble(Bid+SL*K*Point,Digits);
         if(stoploss-Ask<=MarketInfo(Symbol(),MODE_STOPLEVEL)*Point)
           {
            stoploss=NormalizeDouble(Ask+MarketInfo(Symbol(),MODE_STOPLEVEL)*Point,Digits);
            if(Journaling)Print("EA Journaling: Stop Loss changed from "+initSL+" to "+MarketInfo(Symbol(),MODE_STOPLEVEL)/K+" pips");
           }
        }
      //+------------------------------------------------------------------+
      //|                                                                  |
      //+------------------------------------------------------------------+
      if(TYPE==OP_BUY && TP!=0)
        {
         takeprofit=NormalizeDouble(Ask+TP*K*Point,Digits);
         if(takeprofit-Bid<=MarketInfo(Symbol(),MODE_STOPLEVEL)*Point)
           {
            takeprofit=NormalizeDouble(Ask+MarketInfo(Symbol(),MODE_STOPLEVEL)*Point,Digits);
            if(Journaling)Print("EA Journaling: Take Profit changed from "+initTP+" to "+MarketInfo(Symbol(),MODE_STOPLEVEL)/K+" pips");
           }
        }
      //+------------------------------------------------------------------+
      //|                                                                  |
      //+------------------------------------------------------------------+
      if(TYPE==OP_SELL && TP!=0)
        {
         takeprofit=NormalizeDouble(Bid-TP*K*Point,Digits);
         if(Ask-takeprofit<=MarketInfo(Symbol(),MODE_STOPLEVEL)*Point)
           {
            takeprofit=NormalizeDouble(Bid-MarketInfo(Symbol(),MODE_STOPLEVEL)*Point,Digits);
            if(Journaling)Print("EA Journaling: Take Profit changed from "+initTP+" to "+MarketInfo(Symbol(),MODE_STOPLEVEL)/K+" pips");
           }
        }
      if(Journaling)Print("EA Journaling: Trying to place a market order...");
      HandleTradingEnvironment(Journaling,RetryInterval);
      Ticket=OrderSend(symbol,cmd,volume,price,slippage,stoploss,takeprofit,comment,magic,expiration,arrow_color);
      if(Ticket>0)break;
      tries++;
     }

   if(Journaling && Ticket<0)Print("EA Journaling: Unexpected Error has happened. Error Description: "+GetErrorDescription(GetLastError()));
   if(Journaling && Ticket>0)
     {
      allLot+=volume;
      Print("EA Journaling: Order successfully placed. Ticket: "+Ticket);
     }
   return(Ticket);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//|End of OpenPositionMarket()
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|Start of CloseOrderPosition()
//+------------------------------------------------------------------+
bool CloseOrderPosition(int TYPE,bool Journaling,int Magic,int Slip,int K)
  {
// Type: Fixed Template 
// Do not edit unless you know what you're doing
   int ordersPos=OrdersTotal();

   for(int i=ordersPos-1; i>=0; i--)
      //+------------------------------------------------------------------+
      //|                                                                  |
      //+------------------------------------------------------------------+
     {
      // Note: Once pending orders become positions, OP_BUYLIMIT AND OP_BUYSTOP becomes OP_BUY, OP_SELLLIMIT and OP_SELLSTOP becomes OP_SELL
      if(TYPE==OP_BUY || TYPE==OP_SELL)
        {
         if(OrderSelect(i,SELECT_BY_POS,MODE_TRADES)==true && OrderSymbol()==Symbol() && (OrderMagicNumber()==Magic || OrderMagicNumber()==ManualTrading) && OrderType()==TYPE)
           {
            bool Closing=false;
            double Price=0;
            color arrow_color=0;if(TYPE==OP_BUY)arrow_color=MediumSeaGreen;if(TYPE==OP_SELL)arrow_color=DarkOrange;
            if(Journaling)Print("EA Journaling: Trying to close position "+OrderTicket()+" ...");
            HandleTradingEnvironment(Journaling,RetryInterval);
            if(TYPE==OP_BUY)Price=Bid; if(TYPE==OP_SELL)Price=Ask;
            Closing=OrderClose(OrderTicket(),OrderLots(),Price,Slip*K,arrow_color);
            if(Journaling && !Closing)Print("CEA Journaling: Unexpected Error has happened. Error Description: "+GetErrorDescription(GetLastError()));
            if(Journaling && Closing)Print("CEA Journaling: Position successfully closed.");
           }
        }
     }
   if(CountPosOrders(Magic, TYPE)==0)return(true); else return(false);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//| End of CloseOrderPosition()
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
// Start of getP()                                                   |
//+------------------------------------------------------------------+

int GetP()
  {

   int output;

   if(Digits==5 || Digits==3) output=10;else output=1;

   return(output);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
// End of GetP()                                                    	|
//+------------------------------------------------------------------+


//+------------------------------------------------------------------+
// Start of GetYenAdjustFactor()                                     |
//+------------------------------------------------------------------+

int GetYenAdjustFactor()
  {

   int output=1;

   if(Digits==3 || Digits==2) output=100;

   return(output);

  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
// End of GetYenAdjustFactor()                                       |
//+------------------------------------------------------------------+


//+------------------------------------------------------------------+
//| Start of IsLossLimitBreached()                                    
//+------------------------------------------------------------------+
bool IsLossLimitBreached(bool LossLimitActivated,double LossLimitValue,bool Journaling)
  {

// Type: Fixed Template 
// Do not edit unless you know what you're doing

// This function determines if our maximum Profit threshold is breached
//static bool firstTime=false;
   double lossPrint;
   bool output=False;

   if(LossLimitActivated==False) return (output);
//if(firstTime == true) return(true);

   if(cycleProfit<(-1 *LossLimitValue))
     {
      output=True;
      //firstTime=true;
      CloseAllPositions();
      lossPrint=NormalizeDouble(cycleProfit,4);
      if(Journaling) Print("Loss threshold breached. Current Loss: "+lossPrint);
     }

   return (output);
  }

//+------------------------------------------------------------------+
//|End of IsLossLimitBreached()                                      
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//| Start of IsProfitLimitBreached()                                    
//+------------------------------------------------------------------+
bool IsProfitLimitBreached(bool ProfitLimitActivated,double ProfitLimitValue,bool Journaling)
  {

// Type: Fixed Template 
// Do not edit unless you know what you're doing

// This function determines if our maximum Profit threshold is breached
   static bool firstTime=false;
   double profitPrint;
   bool output=False;

   if(ProfitLimitActivated==False) return (output);
//if(firstTime == true) return(true);

   if(cycleProfit>ProfitLimitValue)
     {
      output=True;
      //firstTime=true;
      CloseAllPositions();
      profitPrint=NormalizeDouble(cycleProfit,4);
      if(Journaling) Print("Profit threshold breached. Current Profit: "+profitPrint);
     }

   return (output);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//|End of IsProfitLimitBreached()                                      
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|Start of ApplyTestMode()                                      
//+------------------------------------------------------------------+

void ApplyTestMode()
  {

// Type: Customizable
// Do not edit unless you know what you're doing

// This function applies panel functionality on test mode.
   if(panel.isButtonModifyPressed())
     {
      double temp;
      temp=StringToDouble(panel.m_edit_pipstep.Text());

      if(temp!=PipsStep)
        {
         PipsStep=temp;
         Print("PipsStep value has been changed to "+PipsStep);
        }

      temp=StringToDouble(panel.m_edit_lot.Text());

      if(temp!=Lot)
        {
         Lot=temp;
         Print("Lot value has been changed to "+Lot);
        }

      temp=StringToDouble(panel.m_edit_takeprofit.Text());

      if(temp!=InitialTakeProfit)
        {
         InitialTakeProfit=temp;
         Print("InitialTakeProfit value has been changed to "+InitialTakeProfit);
        }

      Sleep(300);
      panel.ButtonModifyPressed(false);
     }
//---------------------------------------------------------------
   if(panel.isButton1Pressed()) // Apply Hedge
     {
      ExtHedgeNumberOfOrders=StringToInteger(panel.GetEdit1Text());

      Print("Hedge Number of Orders have been changed to "+ExtHedgeNumberOfOrders);
      ExtonHedging=true;
      panel.Button1Pressed(false);
     }
//---------------------------------------------------------------
   if(panel.isButton2Pressed()) // Close all positions of MagicNumber
     {
      CloseAllMagicNumber(StringToInteger(panel.GetEdit2Text()));
      panel.Button2Pressed(false);
     }
//---------------------------------------------------------------
   if(panel.isButton3Pressed()) // Print average Price.
     {
      PrintAveragePrice();
      panel.Button3Pressed(false);
     }
//---------------------------------------------------------------
   if(panel.isButtonHedgePlusPressed())
     {
      panel.HedgeTypesArrayIndex=(panel.HedgeTypesArrayIndex+1)%3;
      panel.m_edit_hedge.Text(panel.HedgeTypesNames[panel.HedgeTypesArrayIndex]);
      ExthedgeLotType=panel.HedgeTypesValues[panel.HedgeTypesArrayIndex];
      panel.ButtonHedgePlusPressed(false);

     }
//---------------------------------------------------------------
   if(panel.isButtonHedgeMinusPressed())
     {
      panel.HedgeTypesArrayIndex--;
      if(panel.HedgeTypesArrayIndex<0) // if it reached to its end
        {
         panel.HedgeTypesArrayIndex=2;
        }

      panel.m_edit_hedge.Text(panel.HedgeTypesNames[panel.HedgeTypesArrayIndex]);
      ExthedgeLotType=panel.HedgeTypesValues[panel.HedgeTypesArrayIndex];
      panel.ButtonHedgeMinusPressed(false);

     }
//---------------------------------------------------------------
   if(panel.isButtonPipStepPlusPressed())
     {
      int temp=StringToInteger(panel.m_edit_pipstep.Text());

      temp++;
      panel.m_edit_pipstep.Text(IntegerToString(temp));
      panel.ButtonPipStepPlusPressed(false);

     }
//---------------------------------------------------------------
   if(panel.isButtonPipStepMinusPressed())
     {
      int temp=StringToInteger(panel.m_edit_pipstep.Text());

      if(temp==0.01)
        {
         Print("PipStep value cant be below 0 ");
         panel.m_edit_pipstep.Text(IntegerToString(temp));
        }
      else
        {
         temp-=0.01;
         panel.m_edit_pipstep.Text(IntegerToString(temp));
        }

      panel.ButtonPipStepMinusPressed(false);
     }
//---------------------------------------------------------------
   if(panel.isButtonLotPlusPressed())
     {
      double temp=StringToDouble(panel.m_edit_lot.Text());

      temp+=0.1;
      panel.m_edit_lot.Text(DoubleToStr(temp,2));
      panel.ButtonLotPlusPressed(false);

     }
//---------------------------------------------------------------
   if(panel.isButtonLotMinusPressed())
     {
      double temp=StringToDouble(panel.m_edit_lot.Text());
      if(temp<=0.01)
        {
         Print("Lot value cant be below 0.01 ");
         panel.m_edit_lot.Text(DoubleToStr(temp,2));
        }
      else
        {
         temp-=0.01;
         panel.m_edit_lot.Text(DoubleToStr(temp,2));
        }
      panel.ButtonLotMinusPressed(false);
     }
//---------------------------------------------------------------
   if(panel.isButtonTakeProfitPlusPressed())
     {
      double temp=StringToDouble(panel.m_edit_takeprofit.Text());

      temp+=0.5;
      panel.m_edit_takeprofit.Text(DoubleToStr(temp,1));
      panel.ButtonTakeProfitPlusPressed(false);

     }
//---------------------------------------------------------------
   if(panel.isButtonTakeProfitMinusPressed())
     {
      double temp=StringToDouble(panel.m_edit_takeprofit.Text());
      if(temp==1)
        {
         Print("Take Profit Value cant be below 1");
         panel.m_edit_takeprofit.Text(DoubleToStr(temp,1));
        }
      else
        {
         temp-=0.5;
         panel.m_edit_takeprofit.Text(DoubleToStr(temp,1));
        }

      panel.ButtonTakeProfitMinusPressed(false);
     }
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//|End of ApplyTestMode()                                      
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//| Start of HandleTradingEnvironment()                                         
//+------------------------------------------------------------------+
void HandleTradingEnvironment(bool Journaling,int Retry_Interval)
  {
// Type: Fixed Template 
// Do not edit unless you know what you're doing 

// This function checks for errors

   if(IsTradeAllowed()==true)return;

   if(!IsConnected())
     {
      if(Journaling)Print("EA Journaling: Terminal is not connected to server...");
      return;
     }
   if(!IsTradeAllowed() && Journaling)Print("EA Journaling: Trade is not alowed for some reason...");
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
   if(IsConnected() && !IsTradeAllowed())
     {
      while(IsTradeContextBusy()==true)
        {
         if(Journaling)Print("EA Journaling: Trading context is busy... Will wait a bit...");
         Sleep(Retry_Interval);
        }
     }
   RefreshRates();
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//| End of HandleTradingEnvironment()                              
//+------------------------------------------------------------------+  
//+------------------------------------------------------------------+
//| Start of GetErrorDescription()                                               
//+------------------------------------------------------------------+
string GetErrorDescription(int error)
  {
// Type: Fixed Template 
// Do not edit unless you know what you're doing

// This function returns the exact error

   string ErrorDescription="";
//---
   switch(error)
     {
      case 0:     ErrorDescription = "NO Error. Everything should be good.";                                    break;
      case 1:     ErrorDescription = "No error returned, but the result is unknown";                            break;
      case 2:     ErrorDescription = "Common error";                                                            break;
      case 3:     ErrorDescription = "Invalid trade parameters";                                                break;
      case 4:     ErrorDescription = "Trade server is busy";                                                    break;
      case 5:     ErrorDescription = "Old version of the client terminal";                                      break;
      case 6:     ErrorDescription = "No connection with trade server";                                         break;
      case 7:     ErrorDescription = "Not enough rights";                                                       break;
      case 8:     ErrorDescription = "Too frequent requests";                                                   break;
      case 9:     ErrorDescription = "Malfunctional trade operation";                                           break;
      case 64:    ErrorDescription = "Account disabled";                                                        break;
      case 65:    ErrorDescription = "Invalid account";                                                         break;
      case 128:   ErrorDescription = "Trade timeout";                                                           break;
      case 129:   ErrorDescription = "Invalid price";                                                           break;
      case 130:   ErrorDescription = "Invalid stops";                                                           break;
      case 131:   ErrorDescription = "Invalid trade volume";                                                    break;
      case 132:   ErrorDescription = "Market is closed";                                                        break;
      case 133:   ErrorDescription = "Trade is disabled";                                                       break;
      case 134:   ErrorDescription = "Not enough money";                                                        break;
      case 135:   ErrorDescription = "Price changed";                                                           break;
      case 136:   ErrorDescription = "Off quotes";                                                              break;
      case 137:   ErrorDescription = "Broker is busy";                                                          break;
      case 138:   ErrorDescription = "Requote";                                                                 break;
      case 139:   ErrorDescription = "Order is locked";                                                         break;
      case 140:   ErrorDescription = "Long positions only allowed";                                             break;
      case 141:   ErrorDescription = "Too many requests";                                                       break;
      case 145:   ErrorDescription = "Modification denied because order too close to market";                   break;
      case 146:   ErrorDescription = "Trade context is busy";                                                   break;
      case 147:   ErrorDescription = "Expirations are denied by broker";                                        break;
      case 148:   ErrorDescription = "Too many open and pending orders (more than allowed)";                    break;
      case 4000:  ErrorDescription = "No error";                                                                break;
      case 4001:  ErrorDescription = "Wrong function pointer";                                                  break;
      case 4002:  ErrorDescription = "Array index is out of range";                                             break;
      case 4003:  ErrorDescription = "No memory for function call stack";                                       break;
      case 4004:  ErrorDescription = "Recursive stack overflow";                                                break;
      case 4005:  ErrorDescription = "Not enough stack for parameter";                                          break;
      case 4006:  ErrorDescription = "No memory for parameter string";                                          break;
      case 4007:  ErrorDescription = "No memory for temp string";                                               break;
      case 4008:  ErrorDescription = "Not initialized string";                                                  break;
      case 4009:  ErrorDescription = "Not initialized string in array";                                         break;
      case 4010:  ErrorDescription = "No memory for array string";                                              break;
      case 4011:  ErrorDescription = "Too long string";                                                         break;
      case 4012:  ErrorDescription = "Remainder from zero divide";                                              break;
      case 4013:  ErrorDescription = "Zero divide";                                                             break;
      case 4014:  ErrorDescription = "Unknown command";                                                         break;
      case 4015:  ErrorDescription = "Wrong jump (never generated error)";                                      break;
      case 4016:  ErrorDescription = "Not initialized array";                                                   break;
      case 4017:  ErrorDescription = "DLL calls are not allowed";                                               break;
      case 4018:  ErrorDescription = "Cannot load library";                                                     break;
      case 4019:  ErrorDescription = "Cannot call function";                                                    break;
      case 4020:  ErrorDescription = "Expert function calls are not allowed";                                   break;
      case 4021:  ErrorDescription = "Not enough memory for temp string returned from function";                break;
      case 4022:  ErrorDescription = "System is busy (never generated error)";                                  break;
      case 4050:  ErrorDescription = "Invalid function parameters count";                                       break;
      case 4051:  ErrorDescription = "Invalid function parameter value";                                        break;
      case 4052:  ErrorDescription = "String function internal error";                                          break;
      case 4053:  ErrorDescription = "Some array error";                                                        break;
      case 4054:  ErrorDescription = "Incorrect series array using";                                            break;
      case 4055:  ErrorDescription = "Custom indicator error";                                                  break;
      case 4056:  ErrorDescription = "Arrays are incompatible";                                                 break;
      case 4057:  ErrorDescription = "Global variables processing error";                                       break;
      case 4058:  ErrorDescription = "Global variable not found";                                               break;
      case 4059:  ErrorDescription = "Function is not allowed in testing mode";                                 break;
      case 4060:  ErrorDescription = "Function is not confirmed";                                               break;
      case 4061:  ErrorDescription = "Send mail error";                                                         break;
      case 4062:  ErrorDescription = "String parameter expected";                                               break;
      case 4063:  ErrorDescription = "Integer parameter expected";                                              break;
      case 4064:  ErrorDescription = "Double parameter expected";                                               break;
      case 4065:  ErrorDescription = "Array as parameter expected";                                             break;
      case 4066:  ErrorDescription = "Requested history data in updating state";                                break;
      case 4067:  ErrorDescription = "Some error in trading function";                                          break;
      case 4099:  ErrorDescription = "End of file";                                                             break;
      case 4100:  ErrorDescription = "Some file error";                                                         break;
      case 4101:  ErrorDescription = "Wrong file name";                                                         break;
      case 4102:  ErrorDescription = "Too many opened files";                                                   break;
      case 4103:  ErrorDescription = "Cannot open file";                                                        break;
      case 4104:  ErrorDescription = "Incompatible access to a file";                                           break;
      case 4105:  ErrorDescription = "No order selected";                                                       break;
      case 4106:  ErrorDescription = "Unknown symbol";                                                          break;
      case 4107:  ErrorDescription = "Invalid price";                                                           break;
      case 4108:  ErrorDescription = "Invalid ticket";                                                          break;
      case 4109:  ErrorDescription = "EA is not allowed to trade is not allowed. ";                             break;
      case 4110:  ErrorDescription = "Longs are not allowed. Check the expert properties";                      break;
      case 4111:  ErrorDescription = "Shorts are not allowed. Check the expert properties";                     break;
      case 4200:  ErrorDescription = "Object exists already";                                                   break;
      case 4201:  ErrorDescription = "Unknown object property";                                                 break;
      case 4202:  ErrorDescription = "Object does not exist";                                                   break;
      case 4203:  ErrorDescription = "Unknown object type";                                                     break;
      case 4204:  ErrorDescription = "No object name";                                                          break;
      case 4205:  ErrorDescription = "Object coordinates error";                                                break;
      case 4206:  ErrorDescription = "No specified subwindow";                                                  break;
      case 4207:  ErrorDescription = "Some error in object function";                                           break;
      default:    ErrorDescription = "No error or error is unknown";
     }
   return(ErrorDescription);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//| End of GetErrorDescription()                                         
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//| Start of ChartInfo()                                         
//+------------------------------------------------------------------+
void ChartInfo()
  {
   ChartSetInteger(0,CHART_SHOW_GRID,0);
   ChartSetInteger(0,CHART_MODE,CHART_CANDLES);
   ChartSetInteger(0,CHART_AUTOSCROLL,0,True);
   ChartRedraw();
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//| End of ChartInfo()                                         
//+------------------------------------------------------------------+


bool CheckAccount()
  {
   static bool firstTime=true;

   string chars[256]; int i;

/* 2017.7.31 23:59:00 */
   datetime LastAllowedDate=D'2019.02.01 23:59:59';
// // Account number, on which expert is allowed to work
// int    AllowedAccountNo=StrToInteger(/* 11111 */ chars[49]+chars[49]+chars[49]+chars[49]+chars[49]);
//if(AccountNumber()!=AllowedAccountNo)
//   {
//    Print("You don't have permission to use this script!");
//   return(false);
//  }


   Print(LastAllowedDate);
   if(firstTime)
     {

      if(AccountInfoInteger(ACCOUNT_TRADE_MODE)==ACCOUNT_TRADE_MODE_DEMO)
        {

         if(MQLInfoInteger(MQL_TESTER))
           {
            return true;
           }

         //MessageBox("Your account type is Demo. You may proceed.\n HelmiFX","Notice",MB_ICONWARNING|MB_OK);
         if(TimeCurrent()>=LastAllowedDate)
           {
            MessageBox("Demo expert period has expired on "+TimeToStr(LastAllowedDate,TIME_DATE|TIME_SECONDS),"WARNING",MB_ICONSTOP|MB_OK);
            Print("Demo expert period has expired on "+TimeToStr(LastAllowedDate,TIME_DATE|TIME_SECONDS));
            return (false);
           }
         else
           {
            return true;
           }
        }

      else if(AccountInfoInteger(ACCOUNT_TRADE_MODE)==ACCOUNT_TRADE_MODE_REAL)
        {
         if(MQLInfoInteger(MQL_TESTER))
           {
            return true;
           }
         MessageBox("Your account type is Live,You cant Proceed.\n HelmiFX","Notice",MB_ICONSTOP|MB_OK);
         Print("Your account type is Live,You cant Proceed.\n HelmiFX");
         return (false);
        }

      else if(AccountInfoInteger(ACCOUNT_TRADE_MODE)==ACCOUNT_TRADE_MODE_CONTEST)
        {
         if(MQLInfoInteger(MQL_TESTER))
           {
            return true;
           }

         MessageBox("Your account type is Contest,You cant Proceed.\n HelmiFX","Notice",MB_ICONSTOP|MB_OK);
         Print("Your account type is Contest,You cant Proceed.\n HelmiFX");

         return (false);
        }
      firstTime=false;
     }

   return false;
  }
//+------------------------------------------------------------------+
//| Start of isNewBar()                                         
//+------------------------------------------------------------------+
bool isNewBar()
  {
//--- memorize the time of opening of the last bar in the static variable
   static datetime last_time=0;
//--- current time
   datetime lastbar_time=SeriesInfoInteger(Symbol(),Period(),SERIES_LASTBAR_DATE);
//--- if it is the first call of the function
   if(last_time==0)
     {
      //--- set the time and exit
      last_time=lastbar_time;
      return(false);
     }
//--- if the time differs
   if(last_time!=lastbar_time)
     {
      //--- memorize the time and return true
      last_time=lastbar_time;
      return(true);
     }
//--- if we passed to this line, then the bar is not new; return false
   return(false);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//| End of isNewBar()                                         
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//|Start of RSICross()                                                                   |
//+------------------------------------------------------------------+


int RSICross(double RSI1,double RSI2,double Limit)
  {

//----
   if(RSI1>Limit && RSI2<Limit)
     {
      RSICurrentDirection=1;
     }  // line1 above line2

   else if(RSI1<Limit && RSI2>Limit)
     {
      RSICurrentDirection=2;
     }  // line1 below line2

   else
     {
      RSICurrentDirection=RSILastDirection;
     }
//----
   if(RSIFirstTime==true) // Need to check if this is the first time the function is run
     {
      RSIFirstTime=false; // Change variable to false
      RSILastDirection=RSICurrentDirection; // Set new direction
      return (0);
     }

   if(RSICurrentDirection!=RSILastDirection && RSIFirstTime==false) // If not the first time and there is a direction change
     {
      RSILastDirection=RSICurrentDirection; // Set new direction
      return(RSICurrentDirection); // 1 for up, 2 for down
     }

   else
     {
      return(0);  // No direction change
     }
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//|End of RSICross()                                                            |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//|Start of Notifications()                                                                |
//+------------------------------------------------------------------+

void SendNotfications(string subject,string text)
  {
   if(ExtMailNotification)
      SendMail(subject,text);
   if(ExtPushNotification)
      SendNotification(subject+" || "+text);
   if(ExtAlertNotification)
      Alert(subject+" || "+text);

  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//|Start of Notifications()                                                |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//| Start of LogInfo()                                                                  |
//+------------------------------------------------------------------+

void LogInfo()
  {

   int  spread=(int)MarketInfo(Symbol(),MODE_SPREAD);
   string todayTime=TimeToString(TimeCurrent(),TIME_DATE);
   int file_handle=FileOpen("Martingale//MartingaleLogs"+todayTime+".txt",FILE_READ|FILE_WRITE|FILE_TXT,';');

   if(file_handle!=INVALID_HANDLE)
     {
      PrintFormat("Information are being logged,File path:\\Files\\Martingale\\MartingaleLogs.txt",TerminalInfoString(TERMINAL_DATA_PATH));
      FileSeek(file_handle,0,SEEK_END);
      FileWrite(file_handle,"  ----------------------------------"+TimeCurrent()+"  ------------------------------------------------");
      FileWrite(file_handle,"  Account balance = "+AccountBalance());
      FileWrite(file_handle,"  Account equity = "+AccountEquity());
      FileWrite(file_handle,"  Account margin = "+AccountMargin());
      FileWrite(file_handle,"  Account free margin = "+AccountFreeMargin());
      FileWrite(file_handle,"  Spread = "+spread);
      FileWrite(file_handle,"  Symbol = "+Symbol());
      FileWrite(file_handle,"  Timeframe = "+Period());
      FileWrite(file_handle,"  Number of Buy Positions  = "+(CountPosOrders(MagicNumber,OP_BUY)));
      FileWrite(file_handle,"  Number of Sell Positions  = "+(CountPosOrders(MagicNumber,OP_SELL)));
      FileWrite(file_handle,"  Cycle Lots  = "+cycleLots);
      FileWrite(file_handle,"  Cycle Profit  = "+cycleProfit);
      FileWrite(file_handle,"  Closed Positions Profit  = "+lastClosedProfit);
      FileWrite(file_handle,"  Net Profit  = "+netProfit);
      FileWrite(file_handle,"  ---------------------------------------------------------------------------------");
      //--- close the file
      FileClose(file_handle);
      Print("Data is written,file is closed");
     }
   else
      PrintFormat("Failed to open file, Error code = %d",GetLastError());

  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//|End of LogInfo()                                                             |
//+------------------------------------------------------------------+


//+------------------------------------------------------------------+
//| Start of ClearTrailingList()                              
//+------------------------------------------------------------------+

void ClearTrailingList()
  {
// Type: Fixed Template 
// Do not edit unless you know what you're doing

// This function clears the elements of your VolTrailingList if the corresponding positions has been closed

   for(int x=0; x<ArrayRange(TrailingStopList,0); x++)
     { // Looping through all order number in list
      TrailingStopList[x,0] = 0;
      TrailingStopList[x,1] = 0;
     }
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//| End of ClearTrailingList                                        
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//| Start of UpdateTrailingList()                              
//+------------------------------------------------------------------+

void UpdateTrailingList(bool Journaling,int Retry_Interval,int Magic)
  {
// Type: Fixed Template 
// Do not edit unless you know what you're doing

// This function clears the elements of your VolTrailingList if the corresponding positions has been closed

   int ordersPos=OrdersTotal();
   int orderTicketNumber;
   bool doesPosExist;

// Check the VolTrailingList, match with current list of positions. Make sure the all the positions exists. 
// If it doesn't, it means there are positions that have been closed

   for(int x=0; x<ArrayRange(TrailingStopList,0); x++)
     { // Looping through all order number in list

      doesPosExist=False;
      orderTicketNumber=TrailingStopList[x,0];

      if(orderTicketNumber!=0)
        { // Order exists
         for(int y=ordersPos-1; y>=0; y--)
           { // Looping through all current open positions
            if(OrderSelect(y,SELECT_BY_POS,MODE_TRADES)==true && OrderSymbol()==Symbol() && OrderMagicNumber()==Magic)
              {
               if(orderTicketNumber==OrderTicket())
                 { // Checks order number in list against order number of current positions
                  doesPosExist=True;
                  break;
                 }
              }
           }

         if(doesPosExist==False)
           { // Deletes elements if the order number does not match any current positions
            TrailingStopList[x,0] = 0;
            TrailingStopList[x,1] = 0;
           }
        }
     }
  }
//+------------------------------------------------------------------+
//| End of UpdateTrailingList                                        
//+------------------------------------------------------------------+

void ReviewTrailingStop(bool Journaling,double TrailingStop_Offset,int Retry_Interval,int Magic,int K)
  {
// Type: Fixed Template 
// Do not edit unless you know what you're doing 

// This function updates volatility trailing stops levels for all positions (using OrderModify) if appropriate conditions are met

   bool doesTrailingRecordExist;
   int posTicketNumber;
   for(int i=OrdersTotal()-1; i>=0; i--)
     { // Looping through all orders

      if(OrderSelect(i,SELECT_BY_POS,MODE_TRADES)==true && OrderSymbol()==Symbol() && OrderMagicNumber()==Magic)
        {
         doesTrailingRecordExist=False;
         posTicketNumber=OrderTicket();
         for(int x=0; x<ArrayRange(TrailingStopList,0); x++)
           { // Looping through all order number in list 

            if(posTicketNumber==TrailingStopList[x,0])
              { // If condition holds, it means the position have a volatility trailing stop level attached to it

               doesTrailingRecordExist=True;
               bool Modify=false;
               RefreshRates();

               // We update the volatility trailing stop record using OrderModify.
               if(OrderType()==OP_BUY && (Bid-TrailingStopList[x,1]>(TrailingStop_Offset*K*Point)))
                 {
                  if(TrailingStopList[x,1]!=Bid-(TrailingStop_Offset*K*Point))
                    {
                     // if(Journaling)Print("EA Journaling: Trying to modify order "+OrderTicket()+" ...");
                     HandleTradingEnvironment(Journaling,Retry_Interval);
                     Modify=OrderModify(OrderTicket(),OrderOpenPrice(),TrailingStopList[x,1],OrderTakeProfit(),0,CLR_NONE);
                     if(Journaling && !Modify)Print("EA Journaling: Unexpected Error has happened. Error Description: "+GetErrorDescription(GetLastError()));
                     if(Journaling && Modify) Print("EA Journaling: Order successfully modified, trailing stop changed.");

                     TrailingStopList[x,1]=Bid-(TrailingStop_Offset*K*Point);
                    }
                 }
               if(OrderType()==OP_SELL && ((TrailingStopList[x,1]-Ask>(TrailingStop_Offset*K*Point))))
                 {
                  if(TrailingStopList[x,1]!=Ask+(TrailingStop_Offset*K*Point))
                    {
                     //if(Journaling)Print("EA Journaling: Trying to modify order "+OrderTicket()+" ...");
                     HandleTradingEnvironment(Journaling,Retry_Interval);
                     Modify=OrderModify(OrderTicket(),OrderOpenPrice(),TrailingStopList[x,1],OrderTakeProfit(),0,CLR_NONE);
                     if(Journaling && !Modify)Print("EA Journaling: Unexpected Error has happened. Error Description: "+GetErrorDescription(GetLastError()));
                     if(Journaling && Modify) Print("EA Journaling: Order successfully modified, trailing stop changed.");
                     TrailingStopList[x,1]=Ask+(TrailingStop_Offset*K*Point);
                    }
                 }
               break;
              }
           }
         // If order does not have a record attached to it. Alert the trader.
         //if(!doesTrailingRecordExist && Journaling) Print("EA Journaling: Error. Order "+posTicketNumber+" has no volatility trailing stop attached to it.");
        }
     }
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//| End of Review Volatility Trailing Stop
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//| Start of SetTrailingStop
//+------------------------------------------------------------------+

void SetTrailingStop(bool Journaling,int Retry_Interval,int Magic,int K,int OrderNum)
  {
// Type: Fixed Template 
// Do not edit unless you know what you're doing 

// This function adds new volatility trailing stop level using OrderModify()
   double trailingStopLossLimit=0;
   bool Modify=False;
   bool IsTrailingStopAdded=False;
   if(OrderSelect(OrderNum,SELECT_BY_TICKET)==true && OrderSymbol()==Symbol() && OrderMagicNumber()==Magic)
     {
      RefreshRates();
      //+------------------------------------------------------------------+
      //|                                                                  |
      //+------------------------------------------------------------------+
      if(OrderType()==OP_BUY || OrderType()==OP_BUYSTOP)
        {
         trailingStopLossLimit=OrderOpenPrice();// virtual stop loss.
         IsTrailingStopAdded=True;
        }
      //+------------------------------------------------------------------+
      //|                                                                  |
      //+------------------------------------------------------------------+
      if(OrderType()==OP_SELL || OrderType()==OP_SELLSTOP)
        {
         trailingStopLossLimit=OrderOpenPrice();// virtual stop loss.
         IsTrailingStopAdded=True;
        }
      //+------------------------------------------------------------------+
      //|                                                                  |
      //+------------------------------------------------------------------+
      // Records trailingStopLossLimit for future use
      if(IsTrailingStopAdded==True)
        {
         for(int x=0; x<ArrayRange(TrailingStopList,0); x++) // Loop through elements in VolTrailingList
           {
            if(TrailingStopList[x,0]==0) // Checks if the element is empty
              {
               TrailingStopList[x,0]=OrderNum; // Add order number
               TrailingStopList[x,1]=trailingStopLossLimit; // Add Trailing Stop into the List
               Modify=true;
               if(Journaling && Modify) Print("Trailing Stop For "+OrderNum+" Has been Set successfully to "+TrailingStopOffset);
               break;
              }
           }
        }
     }

   if(Journaling && !Modify) Print("Couldnt set Trailing Stop For "+OrderNum+" !");
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//| End of SetTrailingStop
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//| Start of RemoveTrailingStop
//+------------------------------------------------------------------+
void RemoveTrailingStop(int lastOrderTicket)
  {

// Type: Fixed Template 
// Do not edit unless you know what you're doing

// This function removed the trailing stop at a specific order.
   bool doesPosExist=False;
   int orderTicketNumber;
   int index;
   for(int x=0; x<ArrayRange(TrailingStopList,0); x++)
     { // Looping through all order number in list

      doesPosExist=False;
      orderTicketNumber=TrailingStopList[x,0];
      //+------------------------------------------------------------------+
      //|                                                                  |
      //+------------------------------------------------------------------+
      if(orderTicketNumber==lastOrderTicket)
        { // Order exists
         doesPosExist=True;
         index=x;
         break;
        }
     }

   if(doesPosExist==True)
     {

      bool Modify=false;
      //+------------------------------------------------------------------+
      //|                                                                  |
      //+------------------------------------------------------------------+
      if(OrderSelect(lastOrderTicket,SELECT_BY_TICKET)==true && OrderSymbol()==Symbol() && (OrderMagicNumber()==MagicNumber) && OrderType()==OrdersType)
        {
         if(OrderStopLoss()!=0)
           {
            Modify=OrderModify(OrderTicket(),OrderOpenPrice(),0,OrderTakeProfit(),0,clrNONE);
            if(OnJournaling && !Modify)Print(lastOrderTicket+" doesnt have a stop loss attached to it,Trailing Stop removed.");
           }
         else
           {
            if(OnJournaling && !Modify)Print("Trailing Stop for "+lastOrderTicket+" couldnt be removed,Stop Loss never applied on the order!");
           }

        }

      TrailingStopList[index,0] = 0;
      TrailingStopList[index,1] = 0;

      if(OnJournaling && Modify)Print("Trailing Stop For "+lastOrderTicket+"  Has been removed Successfully");

     }

   else
     {
      if(OnJournaling)Print("Last Position was closed, No trailing stop to remove!");
     }
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//| End of RemoveTrailingStop
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|Start of  CheckLastWinningTicket()  											|
//+------------------------------------------------------------------+

bool CheckLastWinningTicket()
  {

// Type: Customizable 
// Do not edit unless you know what you're doing 

// This function check if the last position can be closed if it hits the previous position OpenOrder

   double price;
   if(OrdersType==OP_BUY)
     {
      price=Bid;
      if(OrderSelect(lastTwoPositions[0],SELECT_BY_TICKET,MODE_TRADES)==true && OrderSymbol()==Symbol() && (OrderMagicNumber()==MagicNumber || OrderMagicNumber()==ManualTrading) && OrderType()==OrdersType)
        {
         if(price>=OrderOpenPrice())
           {
            return true;
           }
         else
           {
            return false;
           }
        }
     }
   if(OrdersType==OP_SELL)
     {
      price=Ask;
      if(OrderSelect(lastTwoPositions[0],SELECT_BY_TICKET,MODE_TRADES)==true && OrderSymbol()==Symbol() && (OrderMagicNumber()==MagicNumber || OrderMagicNumber()==ManualTrading) && OrderType()==OrdersType)
        {
         if(price<=OrderOpenPrice())
           {
            return true;
           }
         else
           {
            return false;
           }
        }
     }
   return false;
  }
//+------------------------------------------------------------------+
//|End of  CheckLastWinningTicket()                                                              |
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|Start of  getLastOpenedOrder()                                                              |
//+------------------------------------------------------------------+

int getLastOpenedOrder(int orderType)
  {
   int lastOpenTime=0,needleTicket=0;

   for(int i=(OrdersTotal()-1); i>=0; i --)
     {
      if(OrderSelect(i,SELECT_BY_POS,MODE_TRADES) && OrderType()==orderType)
        {
         int curOpenTime=OrderOpenTime();

         if(curOpenTime>lastOpenTime)
           {
            lastOpenTime = curOpenTime;
            needleTicket = OrderTicket();
           }
        }
     }

   Print("result : T#",needleTicket);
   return(needleTicket);
  }

//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
