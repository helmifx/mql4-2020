//+------------------------------------------------------------------+
//|                                         Abd_AllPairsStratagy.mq4 |
//|                        Copyright 2017, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//|Abd Loulou
//+------------------------------------------------------------------+


//+------------------------------------------------------------------+
//									 SimpleMartingle 
//			Abd Loulou																	|	
//+------------------------------------------------------------------+

//+-----------------------------------------------------------------------------------------+

//+-----------------------------------------------------------------------------------------+

//+-----------------------------------------------------------------------------------------+
#property copyright "Copyright 2017 HelmiFX."
#property link      "www.helmifx.com"
#property strict
#property description "Martingale\\Stack" 
//+------------------------------------------------------------------+
//| Stack                            											|
//+------------------------------------------------------------------+
class Stack
  {
public:
                     Stack(int capacity);
                    ~Stack(void);
   void              Push(int &val);
   int               PopLast(void);
   int               TopEl(void);
   int               BeforeTopEl(void);
   bool              IsFull(void);
   bool              IsEmpty(void);
   void              Clear(void);
   int               GetItem(int index);
   int               LastIndex(void);

private:
   int               data[];
   int               capacity,last;
  };
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
Stack::Stack(int cap)
  {
   capacity=cap;
   ArrayResize(data,cap,0);
   last=-1;
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
Stack::~Stack()
  {
   ArrayFree(data);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void Stack::Push(int &el)
  {
   if(IsFull()==true)
     {
      printf("\n Can't push into a full stack!");
      return;
     }
   last++;
   data[last]=el;
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int Stack::LastIndex()
  {
   return last;
  }
//+------------------------------------------------------------------+
//|                                                  |
//+------------------------------------------------------------------+
int Stack::PopLast()
  {
   if(IsEmpty()==true)
     {
      //printf("\n Can't pop from an empty stack!");
      return -1;
     }
   int el=data[last];
   last--;
   return el;
  }
//+------------------------------------------------------------------+
//|                                                  |
//+------------------------------------------------------------------+
//int Stack::PopBeforeLast()
//  {
//   if(IsEmpty()==true)
//     {
//      printf("\n Can't pop from an empty stack!");
//      return -1;
//     }
//   if(capacity<2)
//     {
//      printf("\n Stack has less than 2");
//      return -1;
//     }
//   int el=data[last-1];
//   data[last-1]=data[last];
//   last--;
//   return el;
//  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int Stack::TopEl()
  {
   if(IsEmpty()==true)
     {
      //printf("\n Stack is empty!");
      return -1;
     }
   int el=data[last];
   return el;
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int Stack::BeforeTopEl()
  {
   if(IsEmpty()==true)
     {
      //printf("\n Stack is empty!");
      return -1;
     }

   if(last<1)
     {
      //printf("\n Stack has less than 2");
      return -1;
     }
   int el=data[last-1];
   return el;
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool Stack::IsEmpty(void)
  {
   return last == -1;
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool Stack::IsFull(void)
  {
   return last+1 == capacity;
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void Stack::Clear(void)
  {
   last=-1;
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int Stack::GetItem(int index)
  {
   return index<=LastIndex()?data[index]:-1;
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
