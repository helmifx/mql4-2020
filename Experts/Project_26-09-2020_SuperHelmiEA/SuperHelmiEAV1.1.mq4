//+------------------------------------------------------------------+
//|                                                     Moving Average |
//|                        Copyright 2019, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//|Abd Loulou
//+------------------------------------------------------------------+

#property copyright "Aboabedalwrd"
#property description "SuperHelmiEA"
#property strict
#property version "1.2"

#include "Stack.mqh"

//TDL check the values of the chart.

#include <Arrays\ArrayInt.mqh>
//+------------------------------------------------------------------+
//| Enums                                                                  |
//+------------------------------------------------------------------+

enum ENUM_LOTSIZE
  {
   ENUM_LOTSIZEM=0,// Multiplier
   ENUM_LOTSIZEA=1,// Addition
   ENUM_LOTSIZEMS=2,// Multiply Sequence
  };
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
enum ENUM_PIPSTEPSIZE
  {
   ENUM_PIPSTEPSIZEATR = 0,// ATR
   ENUM_PIPSTEPSIZEF = 1 , // Fixed
  };
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
enum ENUM_OPPOSITEDIRECTIONMETHOD
  {
   ENUM_OPPOSITEDIRECTIONMETHODLEVELS=0, // Levels
   ENUM_OPPOSITEDIRECTIONMETHODSUMLOTS=1 // Sum Lots
  };

//+------------------------------------------------------------------+
//| Setup                                               
//+------------------------------------------------------------------+ 
extern string  Header0="**************************************************************************************"; // ---------- Supertrend as Filter -----------
extern bool UseSuperTrend=true; // Use SuperTrend?
extern int Nbr_Periods=50; // Nbr_Periods
extern double Multiplier=5.0; // Multiplier
extern ENUM_TIMEFRAMES Supertrend_Timeframe=0; // Timeframe
extern string  Header1="**************************************************************************************"; //---------- Buy Cycle Rules -----------
extern double BuyCycleLot=0.01; // Initial Lot
extern double BuyCycleTakeProfit=10; // Initial TakeProfit
extern double BuyCyclePipStep=10; // Initial PipStep
extern string Sep00=""; // ********* Max Lot Settings
extern bool UseBuyCycleLotMax=false; // Use Max Lot?
extern double BuyCycleLotMax=0.08; // Max Lot
extern string Sep10=""; // ********* Lot Sizing Settings
extern ENUM_LOTSIZE BuyCycleLotSizeMethod=0; // Lot Sizing Method
extern double BuyCycleLotMultiplier=2; // Lot Multiplier, when Lot Sizing Method is *Multiplier*
extern double BuyCycleLotStep=0.1;// Lot Step, when Lot Sizing Method is *Addition*
extern string BuyCycleLotMultiplySequence="1.6,2,1,3"; // Multiply Sequence, when Lot Sizing Method is *Multiply Sequence*
extern string Sep20=""; // ********* PipStep Sizing Settings
extern ENUM_PIPSTEPSIZE BuyCyclePipStepSizeMethod=0; // PipStep Sizing Method
extern string Sep30=""; // -- when Lot Sizing Method is *ATR*
extern int  BuyCycleATRPeriod=14; // ATR Period
extern double  BuyCycleATRMultiplyFactor=10000; // ATR Multiply Factor
extern double  BuyCycleATRDivisionFactor=4; // ATR Division Factor
extern ENUM_TIMEFRAMES  BuyCycleATRTimeframe=PERIOD_D1; // ATR TimeFrame
extern string Sep50=""; // ********* PipStep Decrease Settings
extern bool UseBuyCyclePipStepDecrease=false; // Use PipStep Decrease?
extern int  BuyCyclePipStepDecreaseLevels=5; // Decrease PipStep after 'X' Levels
extern double  BuyCyclePipStepDecreasePercentage=20; // Decrease PipStep in 'Y' Percentage
extern string Sep60=""; // ********* Close Last Win Order Settings
extern bool UseBuyCycleCloseLastOrderWin= true; // Use Close Last Win Order?
extern int  BuyCycleCloseLastOrderWinLevels= 5; // Close Last Win Order after 'X' Levels
extern string Sep70=""; // ********* Opposite Direction Lot Settings
extern bool UseBuyCycleOppositeDirectionLot=false; // Use Opposite Direction Lot?
extern ENUM_OPPOSITEDIRECTIONMETHOD BuyCycleOppositeDirectionLotMethod=0; // Method of allowing Opposite Direction
extern int  BuyCycleOppositeDirectionLotLevels=5; // Use Opposite Direction Lot after 'X' Levels
extern double  BuyCycleOppositeDirectionLotSumLot=5; // Use Opposite Direction Lot when bigger than 'X' Lots
extern double  BuyCycleOppositeDirectionLotPercentage=10; // Opposite Direction Lot in 'Y' Percentage
extern string Sep80=""; // ********* Magic Number
extern int     BuyCycleMagicNumber=1;
extern string  Header2="**************************************************************************************"; //---------- Sell Cycle Rules -----------
extern double SellCycleLot=0.01; // Initial Lot
extern double SellCycleTakeProfit=10; // Initial TakeProfit
extern double SellCyclePipStep=10; // Initial PipStep
extern string Sep01=""; // ********* Max Lot Settings
extern bool UseSellCycleLotMax=false; // Use Max Lot?
extern double SellCycleLotMax=0.08; // Max Lot
extern string Sep11=""; // ********* Lot Sizing Settings
extern ENUM_LOTSIZE SellCycleLotSizeMethod=0; // Lot Sizing Method
extern double SellCycleLotMultiplier=2; // Lot Multiplier, when Lot Sizing Method is *Multiplier*
extern double SellCycleLotStep=0.1;// Lot Step, when Lot Sizing Method is *Addition*
extern string SellCycleLotMultiplySequence="1.6,2,1,3"; // Multiply Sequence, when Lot Sizing Method is *Multiply Sequence*
extern string Sep21=""; // ********* PipStep Sizing Settings
extern ENUM_PIPSTEPSIZE SellCyclePipStepSizeMethod=0; // PipStep Sizing Method
extern string Sep31=""; // -- when Lot Sizing Method is *ATR*
extern int  SellCycleATRPeriod=14; // ATR Period
extern double  SellCycleATRMultiplyFactor=10000; // ATR Multiply Factor
extern double  SellCycleATRDivisionFactor=4; // ATR Division Factor
extern ENUM_TIMEFRAMES  SellCycleATRTimeframe=PERIOD_D1; // ATR TimeFrame
extern string Sep51=""; // ********* PipStep Decrease Settings
extern bool UseSellCyclePipStepDecrease=false; // Use PipStep Decrease?
extern int  SellCyclePipStepDecreaseLevels=5; // Decrease PipStep after 'X' Levels
extern double SellCyclePipStepDecreasePercentage=20; // Decrease PipStep in 'Y' Percentage
extern string Sep61=""; // ********* Close Last Win Order Settings
extern bool UseSellCycleCloseLastOrderWin= true; // Use Close Last Win Order?
extern int  SellCycleCloseLastOrderWinLevels= 5; // Close Last Win Order after 'X' Levels
extern string Sep71=""; // ********* Opposite Direction Lot Settings
extern bool UseSellCycleOppositeDirectionLot=false; // Use Opposite Direction Lot?
extern ENUM_OPPOSITEDIRECTIONMETHOD SellCycleOppositeDirectionLotMethod=0; // Method of allowing Opposite Direction
extern int  SellCycleOppositeDirectionLotLevels=5; // Use Opposite Direction Lot after 'X' Levels
extern double  SellCycleOppositeDirectionLotSumLot=5; // Use Opposite Direction Lot when bigger than 'X' Lots
extern double  SellCycleOppositeDirectionLotPercentage=10; // Opposite Direction Lot in 'Y' Percentage
extern string Sep81=""; // ********* Magic Number
extern int     SellCycleMagicNumber=2;
extern string  Header4="**************************************************************************************"; // ---------- Trailing Stop Rules -----------
extern bool    UseTrailingStops=false; // activate Trailing Stop?
extern double  TrailingStopOffset=15; // Trailing Stop Offset
extern string  Header5="**************************************************************************************"; // ---------- Profit Goal -----------
extern bool    UseProfitGoal=true; // activate Profit Goal ?
extern double  ProfitGoal=100; // Profit goal in Currency
extern string  Header6="**************************************************************************************"; // ---------- EA General -----------
extern color   Panel1LabelColor=White; // First panel label color
extern color   Panel2LabelColor=White; // Second panel label color
extern color   Panel3LabelColor=White; // Third panel label color
extern color   Panel4LabelColor=White; // Forth panel label color
extern color   Panel5LabelColor=White; // Fifth panel label color
extern string  Header7="**************************************************************************************"; // ---------- Journaling  -----------
extern bool SuperTrendJournaling=true; // Print change of SuperTrend values in Journal?
extern bool MartingaleJournaling=true; // Print entering using Martingale in Journal?
extern bool LastOrderWinJournaling=true; // Print when Last Order Win messages in Journal?
extern bool OrdersJournaling=true; // Print Orders related messages in Journal?

string  InternalHeader1="----------Errors Handling Settings-----------";

int     RetryInterval=100; // Pause Time before next retry (in milliseconds)
int     MaxRetriesPerTick=10;
int     Slippage=3;

string  InternalHeader2="----------Service Variables-----------";
string  Header9="**************************************************************************************"; // ----------Chart Info -----------
int     xDistance=1; //Chart info distance from top right corner
//+------------------------------------------------------------------+
//|General Variables                                                                 |
//+------------------------------------------------------------------+
int P;

Stack *BuyCycleOrdersStack=new Stack(1000);
Stack *SellCycleOrdersStack=new Stack(1000);         //initialize new ordersStack

int orderNumber;

double allLot=0;

datetime expertStart;
datetime cyclesStart;

double supertrendTrendUp_1;
double supertrendTrendDown_1;
double supertrendTrendUp_3;
double supertrendTrendDown_3;

double supertrendCurrentTrendUp_1;
double supertrendCurrentTrendDown_1;
double supertrendCurrentTrendUp_3;
double supertrendCurrentTrendDown_3;

int SuperTrendTriggered=-1;
int SuperTrendDirection=-1;
datetime SuperTrendDirectionChange=0;

int SuperTrendCurrentTriggered=-1;
int SuperTrendCurrentDirection=-1;
datetime SuperTrendCurrentDirectionChange=0;

//+-----------------------------------------------------------------+
//|Chart Display Variables                                                                  |
//+------------------------------------------------------------------+
double totalCycleProfit; //Get All Opened Profit
double buyCycleProfit;
double sellCycleProfit;
double totalCycleLots;
double buyCycleLots;    //Get All Buy  Opened Lots
double sellCycleLots;    //Get All Sell Opened Lots
double totalProfit;    // All the profit of all opened positions by the same MagicNumber
string ObjName=IntegerToString(ChartID(),0,' ');   //The global object name 
double totalLots;
double pipsTotal;
double spreadValue;
double buyCurrentProfit;
double sellCurrentProfit;
double buyCurrentLots;
double sellCurrentLots;
//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
int init()
  {

//Chart Properties
   DrawChartInfo();
   ChartSettings();

//Variables Initialization
   P=GetP(); // To account for 5 digit brokers. Used to convert pips to decimal place
   MultiplySequenceSplit(); // Split the multiply sequence if it was used.
   expertStart=Time[0];

//Get Super Trend previous Direction
   if(UseSuperTrend) GetFirstSuperTrendSignal();
   GetFirstCurrentSuperTrendSignal();

   return(INIT_SUCCEEDED);

  }
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
  {
//---
   if(reason!=5 && reason!=3)
     {
      //Delete all Objects on Chart
      ObjectsDeleteAll(0,0,OBJ_LABEL);
      ObjectsDeleteAll(0,0,OBJ_RECTANGLE_LABEL);
      ObjectsDeleteAll(0,0,OBJ_ARROW);
      ObjectsDeleteAll(0,0,OBJ_HLINE);
      ObjectsDeleteAll(ChartID(),0);

      //Delete Stacks
      delete BuyCycleOrdersStack;
      delete SellCycleOrdersStack;
     }

   Print("**** "+Symbol()+": All Lots opened =  "+DoubleToStr(NormalizeDouble(allLot,2),2)+" ****");

  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void OnChartEvent(const int id,
                  const long &lparam,
                  const double &dparam,
                  const string &sparam)
  {

//Setting the Live values when they change on Chart.
   if(id==CHARTEVENT_OBJECT_ENDEDIT)
     {
      if(sparam==ObjName+"LiveBuyCycleLotInput")
         BuyCycleLot=StringToDouble(ObjectGetString(0,sparam,OBJPROP_TEXT));

      if(sparam==ObjName+"LiveBuyCycleTakeProfitInput")
         BuyCycleTakeProfit=StringToDouble(ObjectGetString(0,sparam,OBJPROP_TEXT));

      if(sparam==ObjName+"LiveBuyCyclePipStepInput")
         BuyCyclePipStep=StringToDouble(ObjectGetString(0,sparam,OBJPROP_TEXT));

      if(sparam==ObjName+"LiveSellCycleLotInput")
         SellCycleLot=StringToDouble(ObjectGetString(0,sparam,OBJPROP_TEXT));

      if(sparam==ObjName+"LiveSellCycleTakeProfitInput")
         SellCycleTakeProfit=StringToDouble(ObjectGetString(0,sparam,OBJPROP_TEXT));

      if(sparam==ObjName+"LiveSellCyclePipStepInput")
         SellCyclePipStep=StringToDouble(ObjectGetString(0,sparam,OBJPROP_TEXT));
     }
  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
int start()
  {

//#If no orders are opened, set Cycle Start time to now
   if(CountPosOrders(BuyCycleMagicNumber,OP_BUY)==0 && CountPosOrders(SellCycleMagicNumber,OP_SELL)==0 && cyclesStart!=Time[0])
      cyclesStart=Time[0];

//#Check for LastOrderWin
   if(CountPosOrders(BuyCycleMagicNumber,OP_BUY)!=0 && IsLastOrderWin(OP_BUY))
     {
      if(OrdersJournaling)Print("** OrdersJournaling: Closing last winning Order "+BuyCycleOrdersStack.TopEl()+"! **");
      CloseTicket(BuyCycleOrdersStack.PopLast(),BuyCycleMagicNumber);

     }

   if(CountPosOrders(SellCycleMagicNumber,OP_SELL)!=0 && IsLastOrderWin(OP_SELL))
     {
      if(OrdersJournaling)Print("** OrdersJournaling: Closing last winning Order "+SellCycleOrdersStack.TopEl()+"! **");
      CloseTicket(SellCycleOrdersStack.PopLast(),SellCycleMagicNumber);

     }

//#Check Stacks Integrity
   CheckStacksIntegrity();

//#Chart Calculations
//Total
   totalProfit=GetTotalProfitSince(BuyCycleMagicNumber,expertStart,OP_BUY)+GetTotalProfitSince(SellCycleMagicNumber,expertStart,OP_SELL);
   totalCycleProfit=GetTotalProfitSince(BuyCycleMagicNumber,cyclesStart,OP_BUY)+GetTotalProfitSince(SellCycleMagicNumber,cyclesStart,OP_SELL);
   totalLots=GeTotalLotsSince(BuyCycleMagicNumber,expertStart,OP_BUY)+GeTotalLotsSince(SellCycleMagicNumber,expertStart,OP_SELL);
   totalCycleLots=GeTotalLotsSince(BuyCycleMagicNumber,cyclesStart,OP_BUY)+GeTotalLotsSince(SellCycleMagicNumber,cyclesStart,OP_SELL);
//Buy
   buyCurrentProfit=GetCurrentProfit(BuyCycleMagicNumber,OP_BUY);
   buyCycleProfit=GetTotalProfitSince(BuyCycleMagicNumber,cyclesStart,OP_BUY);
   buyCurrentLots=GetCurrentLots(BuyCycleMagicNumber,OP_BUY);
   buyCycleLots=GeTotalLotsSince(BuyCycleMagicNumber,cyclesStart,OP_BUY);

//Sell
   sellCurrentProfit=GetCurrentProfit(SellCycleMagicNumber,OP_SELL);
   sellCycleProfit=GetTotalProfitSince(SellCycleMagicNumber,cyclesStart,OP_SELL);
   sellCurrentLots=GetCurrentLots(SellCycleMagicNumber,OP_SELL);
   sellCycleLots=GeTotalLotsSince(SellCycleMagicNumber,cyclesStart,OP_SELL);
//Random
   spreadValue=MarketInfo(Symbol(),MODE_SPREAD)/P;

//#Update Chart Calculations
   UpdateChartInfo();

//#Check Profit Goal if its reached
   if(UseProfitGoal && totalCycleProfit>ProfitGoal)
     {
      Print("**** Profit Goal of "+ProfitGoal+" for "+Symbol()+" was reached! closing all orders. ****");
      CloseOrderPosition(OP_BUY,BuyCycleMagicNumber);
      CloseOrderPosition(OP_SELL,SellCycleMagicNumber);
      cyclesStart=Time[0];
     }

//#Retrieve the Direction of SuperTrend on its own TimeFrame
   if(UseSuperTrend)
     {
      if(isSuperTrendNewBar())
        {
         supertrendTrendUp_1=iCustom(Symbol(),Supertrend_Timeframe,"super-trend",Nbr_Periods,Multiplier,0,1);
         supertrendTrendDown_1=iCustom(Symbol(),Supertrend_Timeframe,"super-trend",Nbr_Periods,Multiplier,1,1);
         supertrendTrendUp_3=iCustom(Symbol(),Supertrend_Timeframe,"super-trend",Nbr_Periods,Multiplier,0,3);
         supertrendTrendDown_3=iCustom(Symbol(),Supertrend_Timeframe,"super-trend",Nbr_Periods,Multiplier,1,3);
         SuperTrendTriggered=SuperTrendSignal();
         if(SuperTrendTriggered!=-1 && SuperTrendTriggered!=SuperTrendDirection)
           {
            SuperTrendDirection=SuperTrendTriggered;
            SuperTrendDirectionChange=iTime(Symbol(),Supertrend_Timeframe,0);
            if(SuperTrendJournaling)Print("** SuperTrendJournaling: SuperTrend Changed to "+(SuperTrendDirection==OP_SELL?"Sell":"Buy")+" on: "+TimeToString(SuperTrendDirectionChange,TIME_DATE|TIME_MINUTES)+" **");
           }
        }
     }

//#Check Trailing stop for both cycles.
   if(UseTrailingStops)
      ReviewTrailingStop();

//#If no orders are created after TP or ProfitGoal then create new Initial Orders
   if(CountPosOrders(BuyCycleMagicNumber,OP_BUY)==0 || CountPosOrders(SellCycleMagicNumber,OP_SELL)==0)
     {
      if(CountPosOrders(BuyCycleMagicNumber,OP_BUY)==0)
         OpenInitialOrder(OP_BUY);

      if(CountPosOrders(SellCycleMagicNumber,OP_SELL)==0)
         OpenInitialOrder(OP_SELL);
     }

//#Return if we are in a bar still, we continue at the start of a new bar of the current Timeframe
   if(!isNowNewBar())
     {
      return(0);
     }

//#Retrieve the Direction of SuperTrend on the Current TimeFrame
   supertrendCurrentTrendUp_1=iCustom(Symbol(),Period(),"super-trend",Nbr_Periods,Multiplier,0,1);
   supertrendCurrentTrendDown_1=iCustom(Symbol(),Period(),"super-trend",Nbr_Periods,Multiplier,1,1);
   supertrendCurrentTrendUp_3=iCustom(Symbol(),Period(),"super-trend",Nbr_Periods,Multiplier,0,3);
   supertrendCurrentTrendDown_3=iCustom(Symbol(),Period(),"super-trend",Nbr_Periods,Multiplier,1,3);
   SuperTrendCurrentTriggered=SuperTrendCurrentSignal();

   if(SuperTrendCurrentTriggered!=-1 && SuperTrendCurrentTriggered!=SuperTrendCurrentDirection)
     {
      SuperTrendCurrentDirection=SuperTrendCurrentTriggered;
      SuperTrendCurrentDirectionChange=iTime(Symbol(),Period(),0);
      if(SuperTrendJournaling)Print("** SuperTrendJournaling: SuperTrendCurrent Changed to "+(SuperTrendCurrentDirection==OP_SELL?"Sell":"Buy")+" on: "+TimeToString(SuperTrendCurrentDirectionChange,TIME_DATE|TIME_MINUTES)+" **");
     }

//#Check for Martingale
   IsEnterMartingale(OP_BUY);
   IsEnterMartingale(OP_SELL);

   return (0);
  }
//+------------------------------------------------------------------+   
//|End of Start()                                                    |
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                         *** Stacks ***                           |
//+------------------------------------------------------------------+

void CheckStacksIntegrity()
  {
   int ticket;
//Buy
   if(CountPosOrders(BuyCycleMagicNumber,OP_BUY)<BuyCycleOrdersStack.LastIndex()+1)
     {
      BuyCycleOrdersStack.Clear();

      for(int i=OrdersTotal()-1; i>=0; i--)
        {
         if(OrderSelect(i,SELECT_BY_POS,MODE_TRADES)==true && OrderSymbol()==Symbol() && OrderMagicNumber()==BuyCycleMagicNumber && OrderType()==OP_BUY)
           {
            ticket=OrderTicket();
            BuyCycleOrdersStack.Push(ticket);
           }
        }
     }
//Sell
   if(CountPosOrders(SellCycleMagicNumber,OP_SELL)<SellCycleOrdersStack.LastIndex()+1)
     {
      SellCycleOrdersStack.Clear();
      for(int i=OrdersTotal()-1; i>=0; i--)
        {
         if(OrderSelect(i,SELECT_BY_POS,MODE_TRADES)==true && OrderSymbol()==Symbol() && OrderMagicNumber()==SellCycleMagicNumber && OrderType()==OP_SELL)
           {
            ticket=OrderTicket();
            SellCycleOrdersStack.Push(ticket);
           }
        }
     }
  }
//+------------------------------------------------------------------+
//|                         *** /Stacks ***                          |
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                         *** Martingale ***                       |
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
// Start of OpenInitialOrder()                                    	|
//+------------------------------------------------------------------+
void OpenInitialOrder(int cycle)
  {

//Initialize variables
   double lot=(cycle==OP_BUY?CalculateInitialLot(OP_BUY):CalculateInitialLot(OP_SELL));
   double takeProfit=(cycle==OP_BUY?BuyCycleTakeProfit:SellCycleTakeProfit);
   int magic=(cycle==OP_BUY?BuyCycleMagicNumber:SellCycleMagicNumber);

//Creation
   orderNumber=OpenPositionMarket(cycle,lot,0,takeProfit,magic);

//Add to all Lots
   allLot+=lot;

   if(cycle==OP_BUY) BuyCycleOrdersStack.Push(orderNumber);
   else if(cycle==OP_SELL) SellCycleOrdersStack.Push(orderNumber);

   if(MartingaleJournaling) Print("** MartingaleJournaling: "+(cycle==OP_BUY?"Buy " :"Sell ")+"Initial Order has been opened. **");

  }
//+------------------------------------------------------------------+
// End of OpenInitialOrder()                                    	   |
//+------------------------------------------------------------------+


//+------------------------------------------------------------------+   
//|Start of IsEnterMartingale()                                      |
//+------------------------------------------------------------------+

void IsEnterMartingale(int cycle)
  {

//Initializing Variables
   int magic=(cycle==OP_BUY?BuyCycleMagicNumber:SellCycleMagicNumber);
   int lastOrder=(cycle==OP_BUY?BuyCycleOrdersStack.TopEl():SellCycleOrdersStack.TopEl());
   double lot=CalculateLot(cycle);
   double pipStep=CalculatePipStep(cycle);

//If no orders were open, this function is not needed, check above OpenInitialOrder should've triggered.
   if(CountPosOrders(magic,cycle)==0 || lastOrder==-1)
      return;


   if(OrderSelect(lastOrder,SELECT_BY_TICKET)==true && OrderSymbol()==Symbol() && OrderMagicNumber()==magic && OrderType()==cycle)
     {

      if(OrderType()==OP_BUY)
        {
         if(iClose(Symbol(),Period(),1)<=(OrderOpenPrice()-((pipStep)*P*Point)))
           {
            orderNumber=OpenPositionMarket(cycle,lot,0,0,magic);
            if(orderNumber!=-1)
              {
               //Add to all Lots
               allLot+=lot;

               BuyCycleOrdersStack.Push(orderNumber);
               if(MartingaleJournaling) Print("** MartingaleJournaling: "+(cycle==0?"Buy " :"Sell ")+"Order was created with Lot "+lot+" **");
               UpdateTakeProfit(cycle);
              }
           }
        }

      else if(OrderType()==OP_SELL)
        {
         if(iClose(Symbol(),Period(),1)>=(OrderOpenPrice()+(pipStep*P*Point)))
           {
            orderNumber=OpenPositionMarket(cycle,lot,0,0,magic);
            if(orderNumber!=-1)
              {
               //Add to all Lots
               allLot+=lot;

               SellCycleOrdersStack.Push(orderNumber);
               if(MartingaleJournaling) Print("** MartingaleJournaling: "+(cycle==0?"Buy " :"Sell ")+"Order was created with Lot "+lot+" **");
               UpdateTakeProfit(cycle);
              }
           }
        }
     }

  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
// Start of GetAverageTakeProfit()                             		|
//+------------------------------------------------------------------+

double GetAverageTakeProfit(int cycle)
  {

// Get Average TakeProfit for this cycle's orders

   Stack *ordersStack=(cycle==OP_BUY?BuyCycleOrdersStack:SellCycleOrdersStack);
   int magic=(cycle==OP_BUY?BuyCycleMagicNumber:SellCycleMagicNumber);

   double sum=0;
   double lotSum=0;

   for(int i=0; i<=ordersStack.LastIndex();i++)
     {
      if(OrderSelect(ordersStack.GetItem(i),SELECT_BY_TICKET)==true && OrderSymbol()==Symbol() && OrderMagicNumber()==magic && OrderType()==cycle)
        {
         if(OrderCloseTime()==0) // Still Open Position
           {
            sum+=OrderLots()*OrderOpenPrice();
            lotSum+=OrderLots();
           }
        }
     }

   if(lotSum==0) //There're  no orders in the stack. Print and return.
     {
      Print("**** Error: GetAverageTakeProfit was called, but no Opened Orders in"+cycle==0? " Buy Stack!":" Sell Stack! ****");
      return (0);
     }

   double averagePrice=sum/lotSum;
   double averageTakeProfit=(cycle==0?averagePrice+(BuyCycleTakeProfit*P*Point):averagePrice-(SellCycleTakeProfit*P*Point));
   return NormalizeDouble(averageTakeProfit,Digits);

  }
//+------------------------------------------------------------------+
// End of GetAverageTakeProfit()                        					|
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
// Start of UpdateTakeProfit()                        						|
//+------------------------------------------------------------------+

void UpdateTakeProfit(int cycle)
  {

// Update TakeProfit for this cycle's orders

// Retrieve this cycles Average TakeProfit
   double averageTakeProfit=GetAverageTakeProfit(cycle);

// If AverageTakeProfit is 0, nothing to update.   
   if(averageTakeProfit==0)
      return;

   Stack *ordersStack=(cycle==OP_BUY?BuyCycleOrdersStack:SellCycleOrdersStack);
   int magic=(cycle==OP_BUY?BuyCycleMagicNumber:SellCycleMagicNumber);
   bool Modify=false;

   for(int i=0; i<=ordersStack.LastIndex();i++)
     {
      if(OrderSelect(ordersStack.GetItem(i),SELECT_BY_TICKET)==true && OrderSymbol()==Symbol() && OrderMagicNumber()==magic && OrderType()==cycle)
        {
         if(OrderTakeProfit()!=averageTakeProfit)
           {
            HandleTradingEnvironment();
            Modify=OrderModify(OrderTicket(),OrderOpenPrice(),OrderStopLoss(),averageTakeProfit,0,CLR_NONE);
           }
        }
     }

   if(MartingaleJournaling && !Modify)Print("** MartingaleJournaling: Unexpected Error has happened. Error Description: "+GetErrorDescription(GetLastError())+" **");
   if(MartingaleJournaling && Modify)Print("** MartingaleJournaling: Opened Positions successfully modified, TakeProfit Changed. **");

  }
//+------------------------------------------------------------------+
// End of UpdateTakeProfit()                        						|
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//|Start of  IsLastOrderWin()  											|
//+------------------------------------------------------------------+

bool IsLastOrderWin(int cycle)
  {

   if(cycle==OP_BUY) // Buy
     {
      if(UseBuyCycleCloseLastOrderWin && BuyCycleOrdersStack.BeforeTopEl()!=-1 && BuyCycleOrdersStack.LastIndex()+1>=BuyCycleCloseLastOrderWinLevels)
        {
         if(OrderSelect(BuyCycleOrdersStack.BeforeTopEl(),SELECT_BY_TICKET)==true && OrderSymbol()==Symbol() && OrderMagicNumber()==BuyCycleMagicNumber && OrderType()==OP_BUY)
            return Bid>=OrderOpenPrice()?true:false;
        }
     }
   else if(cycle==OP_SELL) // Sell
     {
      if(UseSellCycleCloseLastOrderWin && SellCycleOrdersStack.BeforeTopEl()!=-1 && SellCycleOrdersStack.LastIndex()+1>=SellCycleCloseLastOrderWinLevels)
        {
         if(OrderSelect(SellCycleOrdersStack.BeforeTopEl(),SELECT_BY_TICKET)==true && OrderSymbol()==Symbol() && OrderMagicNumber()==SellCycleMagicNumber && OrderType()==OP_SELL)
            return Ask<=OrderOpenPrice()?true:false;
        }
     }

   return false;
  }
//+------------------------------------------------------------------+
//|End of  IsLastOrderWin()                                                              |
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                         *** /Martingale ***                      |
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                         *** Lot ***                              |
//+------------------------------------------------------------------+


//+------------------------------------------------------------------+
// Start of CheckLot()                                       			|
//+------------------------------------------------------------------+
double CheckLot(double lot)
  {

// This function checks if our Lots to be trade satisfies any broker limitations

   double LotToOpen=0;
   LotToOpen=NormalizeDouble(lot,2);
   LotToOpen=MathFloor(LotToOpen/MarketInfo(Symbol(),MODE_LOTSTEP))*MarketInfo(Symbol(),MODE_LOTSTEP);

   if(LotToOpen<MarketInfo(Symbol(),MODE_MINLOT))LotToOpen=MarketInfo(Symbol(),MODE_MINLOT);
   if(LotToOpen>MarketInfo(Symbol(),MODE_MAXLOT))LotToOpen=MarketInfo(Symbol(),MODE_MAXLOT);
   LotToOpen=NormalizeDouble(LotToOpen,2);

   if(OrdersJournaling && LotToOpen!=lot)Print("** OrdersJournaling: Trading Lot has been changed by CheckLot function. Requested lot: "+DoubleToString(lot)+". Lot to open: "+DoubleToString(LotToOpen)+" **");

   return(LotToOpen);
  }
//+------------------------------------------------------------------+
//| End of CheckLot()                                                |
//+------------------------------------------------------------------+


string buyCycleMultiplySequenceArray[];
string sellCycleMultiplySequenceArray[];
//+------------------------------------------------------------------+
// Start of MultiplySequenceSplit()                                  |
//+------------------------------------------------------------------+
void MultiplySequenceSplit()
  {
   if(BuyCycleLotSizeMethod==2) // Multiply Sequence
      StringSplit(BuyCycleLotMultiplySequence,',',buyCycleMultiplySequenceArray); // Splitting the multiplication sequene of the lot

   if(SellCycleLotSizeMethod==2) // Multiply Sequence
      StringSplit(SellCycleLotMultiplySequence,',',sellCycleMultiplySequenceArray); // Splitting the multiplication sequene of the lot
  }
//+------------------------------------------------------------------+
// End of MultiplySequenceSplit()                                    |
//+------------------------------------------------------------------+

/*enum ENUM_OPPOSITEDIRECTIONMETHOD
  {
   ENUM_OPPOSITEDIRECTIONMETHODLEVELS=0, // Levels
   ENUM_OPPOSITEDIRECTIONMETHODSUMLOTS=1 // Sum Lots
  };
*/

/*ENUM_LOTSIZE
  {
   ENUM_LOTSIZEM=0,// Multiplier
   ENUM_LOTSIZEA=1,// Addition
   ENUM_LOTSIZEMS=2,// Multiply Sequence
  };
*/

//+------------------------------------------------------------------+
// Start of CalculateInitialLot()                                       	   |
//+------------------------------------------------------------------+
double CalculateInitialLot(int cycle)
  {

   double lot;

//Initialize variables
   int lotSizingMethod=cycle==OP_BUY?BuyCycleLotSizeMethod:SellCycleLotSizeMethod;
   int magic=cycle==OP_BUY?BuyCycleMagicNumber:SellCycleMagicNumber;
   Stack *ordersStack=cycle==OP_BUY?BuyCycleOrdersStack:SellCycleOrdersStack;

   bool useOppositeDirectionLot=cycle==OP_BUY?UseBuyCycleOppositeDirectionLot:UseSellCycleOppositeDirectionLot;
   int oppositeDirectionMethod=cycle==OP_BUY?BuyCycleOppositeDirectionLotMethod:SellCycleOppositeDirectionLotMethod;
   int oppositeDirectionLotLevels=cycle==OP_BUY?BuyCycleOppositeDirectionLotLevels:SellCycleOppositeDirectionLotLevels;
   double oppositeDirectionLotSumLot=cycle==OP_BUY?BuyCycleOppositeDirectionLotSumLot:SellCycleOppositeDirectionLotSumLot;
   double oppositeDirectionLotPercentage=cycle==OP_BUY?BuyCycleOppositeDirectionLotPercentage:SellCycleOppositeDirectionLotPercentage;
   Stack *oppositeDirectionOrdersStack=cycle==OP_BUY?SellCycleOrdersStack:BuyCycleOrdersStack;
   int oppositeCycle=cycle==OP_BUY?OP_SELL:OP_BUY;
   int oppositeMagic = cycle==OP_BUY?SellCycleMagicNumber:BuyCycleMagicNumber;
   double oppositeDirectionOrdersStackSumLot=0;

   bool useLotMax=cycle==OP_BUY?UseBuyCycleLotMax:UseSellCycleLotMax;
   double lotMax=cycle==OP_BUY?BuyCycleLotMax:SellCycleLotMax;

// If use Opposite

   if(!oppositeDirectionOrdersStack.IsEmpty() && useOppositeDirectionLot)
     {
      for(int i=0;i<=oppositeDirectionOrdersStack.LastIndex();i++)
        {
         if(OrderSelect(oppositeDirectionOrdersStack.GetItem(i),SELECT_BY_TICKET)==true && OrderSymbol()==Symbol() && OrderMagicNumber()==oppositeMagic && OrderType()==oppositeCycle)
           {          
            oppositeDirectionOrdersStackSumLot+=OrderLots();
           }
        }
      // If the criteria is met
      if(((oppositeDirectionOrdersStack.LastIndex()+1>=oppositeDirectionLotLevels && oppositeDirectionMethod==0)
         || (oppositeDirectionOrdersStackSumLot>=oppositeDirectionLotSumLot && oppositeDirectionMethod==1)))
        {

         lot=oppositeDirectionOrdersStackSumLot*(oppositeDirectionLotPercentage/100);
        }
      else
        {
         lot=cycle==OP_BUY?BuyCycleLot:SellCycleLot;

         //Check the direction of the SuperTrend
         if(cycle==SuperTrendDirection)
            lot*=2;
        }
     }

//Else
   else
     {
      lot=cycle==OP_BUY?BuyCycleLot:SellCycleLot;

      //Check the direction of the SuperTrend
      if(cycle==SuperTrendDirection)
         lot*=2;
     }

//Check Lot Max
   if(useLotMax && lot>lotMax)
      lot=lotMax;

   return(CheckLot(NormalizeDouble(lot,2)));
  }
//+------------------------------------------------------------------+
//|End of CalculateInitialLot()                                      |
//+------------------------------------------------------------------+

/*ENUM_LOTSIZE
  {
   ENUM_LOTSIZEM=0,// Multiplier
   ENUM_LOTSIZEA=1,// Addition
   ENUM_LOTSIZEMS=2,// Multiply Sequence
  };
*/
//+------------------------------------------------------------------+
// Start of CalculateLot()                                       	   |
//+------------------------------------------------------------------+
double CalculateLot(int cycle)
  {

   double lot;

//Initialize variables
   double initialLot=cycle==OP_BUY?BuyCycleLot:SellCycleLot;
   int lotSizingMethod=cycle==OP_BUY?BuyCycleLotSizeMethod:SellCycleLotSizeMethod;
   int magic=cycle==OP_BUY?BuyCycleMagicNumber:SellCycleMagicNumber;
   Stack *ordersStack=cycle==OP_BUY?BuyCycleOrdersStack:SellCycleOrdersStack;

   bool useLotMax=cycle==OP_BUY?UseBuyCycleLotMax:UseSellCycleLotMax;
   double lotMax=cycle==OP_BUY?BuyCycleLotMax:SellCycleLotMax;

//#If no Items exist 
   if(ordersStack.IsEmpty())
      return(CheckLot(NormalizeDouble(CalculateInitialLot(cycle),2)));

//#Else check which method to use 

//Check the direction of the SuperTrend
   lot=initialLot;

   if(cycle==SuperTrendDirection)
      lot*=2;

   if(lotSizingMethod==0)
     { //Multiply 
      for(int i=0;i<=ordersStack.LastIndex();i++)
        {
         lot*=(cycle==OP_BUY?BuyCycleLotMultiplier:SellCycleLotMultiplier);
        }
     }

   else if(lotSizingMethod==1)
     { //Adding
      for(int i=0;i<=ordersStack.LastIndex();i++)
        {
         lot+=(cycle==OP_BUY?BuyCycleLotStep:SellCycleLotStep);
        }
     }

   else
     {
      if(OrderSelect(ordersStack.TopEl(),SELECT_BY_TICKET)==true && OrderSymbol()==Symbol() && OrderMagicNumber()==magic && OrderType()==cycle)
        {
         lot=OrderLots();
         lot*=(cycle==OP_BUY?StringToDouble(buyCycleMultiplySequenceArray[ordersStack.LastIndex()%ArrayRange(buyCycleMultiplySequenceArray,0)]):StringToDouble(sellCycleMultiplySequenceArray[ordersStack.LastIndex()%ArrayRange(sellCycleMultiplySequenceArray,0)]));
        }
     }

//Check Lot Max
   if(useLotMax && lot>lotMax)
      lot=lotMax;

   return(CheckLot(NormalizeDouble(lot,2)));
  }
//+------------------------------------------------------------------+
//|End of CalculateLot()                                             |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//|                         *** /Lot ***                             |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//|                         *** PipStep ***                          |
//+------------------------------------------------------------------+

/*
enum ENUM_PIPSTEPSIZE
  {
   ENUM_PIPSTEPSIZEATR = 0,// ATR
   ENUM_PIPSTEPSIZEF = 1 , // Fixed
  };
*/
//+------------------------------------------------------------------+
//|Start of CalculatePipStep()                                       |
//+------------------------------------------------------------------+

double CalculatePipStep(int cycle)
  {

   double pipStep;

//Initialize variables
   int pipStepSizingMethod=cycle==0?BuyCyclePipStepSizeMethod:SellCyclePipStepSizeMethod;
   bool pipStepDecrease=cycle==0?UseBuyCyclePipStepDecrease:UseSellCyclePipStepDecrease;
   int pipStepDecreaseLevels=cycle==0?BuyCyclePipStepDecreaseLevels:SellCyclePipStepDecreaseLevels;
   double pipStepDecreasePercentage=cycle==0?BuyCyclePipStepDecreasePercentage:SellCyclePipStepDecreasePercentage;
   Stack *ordersStack=cycle==0?BuyCycleOrdersStack:SellCycleOrdersStack;

// ATR
   if(pipStepSizingMethod==0)
      pipStep=(iATR(Symbol(),cycle==0?BuyCycleATRTimeframe:SellCycleATRTimeframe,cycle==0?BuyCycleATRPeriod:SellCycleATRPeriod,1)*(cycle==0?BuyCycleATRMultiplyFactor:SellCycleATRMultiplyFactor))/(cycle==0?BuyCycleATRDivisionFactor:SellCycleATRDivisionFactor);

// Fixed
   else if(pipStepSizingMethod==1)
      pipStep=cycle==0?BuyCyclePipStep:SellCyclePipStep;

//Check PipStep Decrease 
   if(pipStepDecrease && ordersStack.LastIndex()+1>=pipStepDecreaseLevels)
      pipStep-=(pipStepDecreasePercentage/100);

   return(NormalizeDouble(pipStep,2));
  }
//+------------------------------------------------------------------+
// End of CalculatePipStep()                        						|
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                         *** /PipStep ***                         |
//+------------------------------------------------------------------+


//+------------------------------------------------------------------+
//|                    *** Chart Calculations ***                    |
//+------------------------------------------------------------------+


//+------------------------------------------------------------------+
//| Start of GetCurrentLots()                                          |
//+------------------------------------------------------------------+
double GetCurrentLots(int magicNumber,int type)
  {

   double Lots=0;

   for(int i=0; i<OrdersTotal(); i++)
     {
      if(OrderSelect(i,SELECT_BY_POS,MODE_TRADES)==true && OrderSymbol()==Symbol() && (OrderMagicNumber()==magicNumber) && OrderType()==type)
         Lots+=OrderLots();
     }
   return(NormalizeDouble(Lots,2));

  }
//+------------------------------------------------------------------+
// End of GetCurrentLots()                                    		   |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//| Start of GetCurrentProfit()                                        |
//+------------------------------------------------------------------+
double GetCurrentProfit(int magicNumber,int type)
  {

   double Profit=0;

   for(int i=0; i<OrdersTotal(); i++)
     {
      if(OrderSelect(i,SELECT_BY_POS,MODE_TRADES)==true && OrderSymbol()==Symbol() && OrderMagicNumber()==magicNumber && OrderType()==type)
         Profit=Profit+OrderProfit();
     }
   return(NormalizeDouble(Profit,2));
  }
//+------------------------------------------------------------------+
//| Start of GetCurrentProfit()                                        |
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
// Start of GetTotalProfitSince()                                    |
//+------------------------------------------------------------------+
double GetTotalProfitSince(int magic,datetime time,int ordertype)
  {

   double Profit=0;

   for(int i=OrdersTotal()-1; i>=0; i--)
     {
      if(OrderSelect(i,SELECT_BY_POS,MODE_TRADES)==true && OrderSymbol()==Symbol() && OrderMagicNumber()==magic && OrderOpenTime()>=time && OrderType()==ordertype)
         Profit=Profit+OrderProfit();
     }

   for(int i=OrdersHistoryTotal()-1; i>=0; i--)
     {
      if(OrderSelect(i,SELECT_BY_POS,MODE_HISTORY)==true && OrderSymbol()==Symbol() && OrderMagicNumber()==magic && OrderOpenTime()>=time && OrderType()==ordertype)
         Profit=Profit+OrderProfit()+OrderSwap()+OrderCommission();
     }
   return(NormalizeDouble(Profit,2));
  }
//+------------------------------------------------------------------+
// End of GetTotalProfitSince()                                    	|
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
// Start of GeTotalLotsSince()                                    	|
//+------------------------------------------------------------------+

double GeTotalLotsSince(int magicNumber,datetime time,int ordertype)
  {

// Type: Customisable 
// Modify this function to suit your trading robot

// this function returns the net profit of the opened orders 

   double Lots=0;

   for(int i=OrdersTotal()-1; i>=0; i--)
     {
      if(OrderSelect(i,SELECT_BY_POS,MODE_TRADES)==true && OrderSymbol()==Symbol() && (OrderMagicNumber()==magicNumber) && OrderOpenTime()>=time && OrderType()==ordertype)
         Lots=Lots+OrderLots();
     }

   for(int i=OrdersHistoryTotal()-1; i>=0; i--)
     {
      if(OrderSelect(i,SELECT_BY_POS,MODE_HISTORY)==true && OrderSymbol()==Symbol() && (OrderMagicNumber()==magicNumber) && OrderOpenTime()>=time && OrderType()==ordertype)
         Lots=Lots+OrderLots();
     }
   return(NormalizeDouble(Lots,2));
  }
//+------------------------------------------------------------------+
// End of GeTotalLotsSince()                                    	   |
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                    *** /Chart Calculations ***                   |
//+------------------------------------------------------------------+


//+------------------------------------------------------------------+
//|                         *** Orders ***                           |
//+------------------------------------------------------------------+


//+------------------------------------------------------------------+
//|Start of CountPosOrders()                                         |
//+------------------------------------------------------------------+
int CountPosOrders(int magic,int type)
  {

   int orders=0;
   for(int i=0; i<OrdersTotal(); i++)
     {
      if(OrderSelect(i,SELECT_BY_POS,MODE_TRADES)==true && OrderSymbol()==Symbol() && OrderMagicNumber()==magic && OrderType()==type)
         orders++;
     }
   return(orders);

  }
//+------------------------------------------------------------------+
//|Start of OpenPositionMarket()
//+------------------------------------------------------------------+
int OpenPositionMarket(int TYPE,double LOT,double SL,double TP,int Magic)
  {

// This function submits new orders

   int tries=0;
   string symbol=Symbol();
   int cmd=TYPE;
   double volume=CheckLot(LOT);
   if(MarketInfo(symbol,MODE_MARGINREQUIRED)*volume>AccountFreeMargin())
     {
      Print("**** Can not open a trade. Not enough free margin to open "+volume+" on "+symbol+" ****");
      return(-1);
     }
   int slippage=Slippage*P; // Slippage is in points. 1 point = 0.0001 on 4 digit broker and 0.00001 on a 5 digit broker
   string comment=" "+TYPE+"(#"+Magic+")";
   int magic=Magic;
   datetime expiration=0;
   color arrow_color=0;if(TYPE==OP_BUY)arrow_color=DodgerBlue;if(TYPE==OP_SELL)arrow_color=DeepPink;
   double stoploss=0;
   double takeprofit=0;
   double initTP = TP;
   double initSL = SL;
   int Ticket=-1;
   double price=0;

   while(tries<MaxRetriesPerTick) // Edits stops and take profits before the market order is placed
     {
      RefreshRates();
      if(TYPE==OP_BUY)price=Ask;if(TYPE==OP_SELL)price=Bid;

      // Sets Take Profits and Stop Loss. Check against Stop Level Limitations.
      if(TYPE==OP_BUY && SL!=0)
        {
         stoploss=NormalizeDouble(Ask-SL*P*Point,Digits);
         if(Bid-stoploss<=MarketInfo(Symbol(),MODE_STOPLEVEL)*Point)
           {
            stoploss=NormalizeDouble(Bid-MarketInfo(Symbol(),MODE_STOPLEVEL)*Point,Digits);
            if(OrdersJournaling)Print("** OrdersJournaling: Stop Loss changed from "+initSL+" to "+MarketInfo(Symbol(),MODE_STOPLEVEL)/P+" pips"+" **");
           }
        }
      if(TYPE==OP_SELL && SL!=0)
        {
         stoploss=NormalizeDouble(Bid+SL*P*Point,Digits);
         if(stoploss-Ask<=MarketInfo(Symbol(),MODE_STOPLEVEL)*Point)
           {
            stoploss=NormalizeDouble(Ask+MarketInfo(Symbol(),MODE_STOPLEVEL)*Point,Digits);
            if(OrdersJournaling)Print("** OrdersJournaling: Stop Loss changed from "+initSL+" to "+MarketInfo(Symbol(),MODE_STOPLEVEL)/P+" pips"+" **");
           }
        }
      if(TYPE==OP_BUY && TP!=0)
        {
         takeprofit=NormalizeDouble(Ask+TP*P*Point,Digits);
         if(takeprofit-Bid<=MarketInfo(Symbol(),MODE_STOPLEVEL)*Point)
           {
            takeprofit=NormalizeDouble(Ask+MarketInfo(Symbol(),MODE_STOPLEVEL)*Point,Digits);
            if(OrdersJournaling)Print("** OrdersJournaling: Take Profit changed from "+initTP+" to "+MarketInfo(Symbol(),MODE_STOPLEVEL)/P+" pips"+" **");
           }
        }
      if(TYPE==OP_SELL && TP!=0)
        {
         takeprofit=NormalizeDouble(Bid-TP*P*Point,Digits);
         if(Ask-takeprofit<=MarketInfo(Symbol(),MODE_STOPLEVEL)*Point)
           {
            takeprofit=NormalizeDouble(Bid-MarketInfo(Symbol(),MODE_STOPLEVEL)*Point,Digits);
            if(OrdersJournaling)Print("** OrdersJournaling: Take Profit changed from "+initTP+" to "+MarketInfo(Symbol(),MODE_STOPLEVEL)/P+" pips"+" **");
           }
        }
      if(OrdersJournaling)Print("** OrdersJournaling: Trying to place a market order..."+" **");
      HandleTradingEnvironment();
      Ticket=OrderSend(symbol,cmd,volume,price,slippage,stoploss,takeprofit,comment,magic,expiration,arrow_color);
      if(Ticket>0)break;
      tries++;
     }

   if(OrdersJournaling && Ticket<0)Print("** OrdersJournaling: Unexpected Error has happened. Error Description: "+GetErrorDescription(GetLastError())+" **");
   if(OrdersJournaling && Ticket>0)
     {
      Print("** OrdersJournaling: Order successfully placed. Ticket: "+Ticket+" **");
     }
   return(Ticket);
  }
//+------------------------------------------------------------------+
//|End of OpenPositionMarket()
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//|Start of CloseOrderPosition()                                     |
//+------------------------------------------------------------------+
bool CloseOrderPosition(int TYPE,int Magic)
  {
// Type: Fixed Template 
// Do not edit unless you know what you're doing
   int ordersPos=OrdersTotal();

   for(int i=ordersPos-1; i>=0; i--)
     {
      // Note: Once pending orders become positions, OP_BUYLIMIT AND OP_BUYSTOP becomes OP_BUY, OP_SELLLIMIT and OP_SELLSTOP becomes OP_SELL
      if(TYPE==OP_BUY || TYPE==OP_SELL)
        {
         if(OrderSelect(i,SELECT_BY_POS,MODE_TRADES)==true && OrderSymbol()==Symbol() && OrderMagicNumber()==Magic && OrderType()==TYPE)
           {
            bool Closing=false;
            double Price=0;
            color arrow_color=0;if(TYPE==OP_BUY)arrow_color=MediumSeaGreen;if(TYPE==OP_SELL)arrow_color=DarkOrange;
            if(OrdersJournaling)Print("** OrdersJournaling: Trying to close position "+OrderTicket()+" ..."+" **");
            HandleTradingEnvironment();
            if(TYPE==OP_BUY)Price=Bid; if(TYPE==OP_SELL)Price=Ask;
            Closing=OrderClose(OrderTicket(),OrderLots(),Price,Slippage*P,arrow_color);
            if(OrdersJournaling && !Closing)Print("** OrdersJournaling: Unexpected Error has happened. Error Description: "+GetErrorDescription(GetLastError())+" **");
            if(OrdersJournaling && Closing)Print("** OrdersJournaling: Order successfully closed."+" **");
           }
        }
      else
        {
         bool Delete=false;
         if(OrderSelect(i,SELECT_BY_POS,MODE_TRADES)==true && OrderSymbol()==Symbol() && OrderMagicNumber()==Magic && OrderType()==TYPE)
           {
            if(OrdersJournaling)Print("** OrdersJournaling: Trying to delete order "+OrderTicket()+" ..."+" **");
            HandleTradingEnvironment();
            Delete=OrderDelete(OrderTicket(),White);
            if(OrdersJournaling && !Delete)Print("** OrdersJournaling: Unexpected Error has happened Deleting the Pending Order. Error Description: "+GetErrorDescription(GetLastError())+" **");
            if(OrdersJournaling && Delete)Print("** OrdersJournaling: Order successfully deleted."+" **");
           }
        }
     }

   if(CountPosOrders(Magic, TYPE)==0)return(true); else return(false);
  }
//+------------------------------------------------------------------+
//| End of CloseOrderPosition()                                      |
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//| Start of CloseTicket()                                           |   
//+------------------------------------------------------------------+
void CloseTicket(int ticket,int magic)
  {

   int ordersPos=OrdersTotal();

   if(OrderSelect(ticket,SELECT_BY_TICKET)==true && OrderSymbol()==Symbol() && OrderMagicNumber()==magic)
     {
      bool Closing=false;
      double Price=0;
      color arrow_color=0;if(OrderType()==OP_BUY)arrow_color=MediumSeaGreen;if(OrderType()==OP_SELL)arrow_color=DarkOrange;
      if(OrdersJournaling)Print("** OrdersJournaling: Trying to close Order "+OrderTicket()+" ..."+" **");
      HandleTradingEnvironment();
      if(OrderType()==OP_BUY)Price=Bid; if(OrderType()==OP_SELL)Price=Ask;
      Closing=OrderClose(OrderTicket(),OrderLots(),Price,Slippage*P,arrow_color);
      if(OrdersJournaling && !Closing)Print("** OrdersJournaling: Unexpected Error has happened. Error Description: "+GetErrorDescription(GetLastError())+" **");
      if(OrdersJournaling && Closing)Print("** OrdersJournaling: Order successfully closed."+" **");
     }
  }
//+------------------------------------------------------------------+
//| End of CloseTicket()                                             |
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//| End of ReviewTrailingStop
//+------------------------------------------------------------------+
void ReviewTrailingStop()
  {

   if(!UseTrailingStops)
      return;

   bool doesTrailingRecordExist;
   int posTicketNumber;
   for(int i=OrdersTotal()-1; i>=0; i--)
     {
      if(OrderSelect(i,SELECT_BY_POS,MODE_TRADES)==true && OrderSymbol()==Symbol() && (OrderMagicNumber()==BuyCycleMagicNumber || OrderMagicNumber()==SellCycleMagicNumber) && OrderProfit()>0)
        {
         bool Modify=false;
         RefreshRates();

         if(OrderType()==OP_BUY)
           {
            if(OrderStopLoss()==0 && (Bid-(TrailingStopOffset*P*Point)>OrderOpenPrice()))
              {
               HandleTradingEnvironment();
               Modify=OrderModify(OrderTicket(),OrderOpenPrice(),OrderOpenPrice(),OrderTakeProfit(),0,CLR_NONE);
               if(OrdersJournaling && !Modify)Print("** OrdersJournaling: Unexpected Error has happened. Error Description: "+GetErrorDescription(GetLastError())+" **");
               if(OrdersJournaling && Modify) Print("** OrdersJournaling: Order successfully modified, trailing stop changed."+" **");
               continue;
              }
            else if(OrderStopLoss()!=0 && OrderStopLoss()!=Bid-(TrailingStopOffset*P*Point) && (Bid-(TrailingStopOffset*P*Point)>OrderStopLoss()))
              {
               HandleTradingEnvironment();
               Modify=OrderModify(OrderTicket(),OrderOpenPrice(),Bid-(TrailingStopOffset*P*Point),OrderTakeProfit(),0,CLR_NONE);
               if(OrdersJournaling && !Modify)Print("** OrdersJournaling: Unexpected Error has happened. Error Description: "+GetErrorDescription(GetLastError())+" **");
               if(OrdersJournaling && Modify) Print("** OrdersJournaling: Order successfully modified, trailing stop changed."+" **");
               continue;
              }
           }

         else if(OP_SELL)
           {
            if(OrderStopLoss()==0 && (Ask+(TrailingStopOffset*P*Point)<OrderOpenPrice()))
              {
               HandleTradingEnvironment();
               Modify=OrderModify(OrderTicket(),OrderOpenPrice(),OrderOpenPrice(),OrderTakeProfit(),0,CLR_NONE);
               if(OrdersJournaling && !Modify)Print("** OrdersJournaling: Unexpected Error has happened. Error Description: "+GetErrorDescription(GetLastError())+" **");
               if(OrdersJournaling && Modify) Print("** OrdersJournaling: Order successfully modified, trailing stop changed."+" **");
               continue;
              }
            else if(OrderStopLoss()!=0 && OrderStopLoss()!=Ask+(TrailingStopOffset*P*Point) && (Ask+(TrailingStopOffset*P*Point)<OrderStopLoss()))
              {
               HandleTradingEnvironment();
               Modify=OrderModify(OrderTicket(),OrderOpenPrice(),Ask+(TrailingStopOffset*P*Point),OrderTakeProfit(),0,CLR_NONE);
               if(OrdersJournaling && !Modify)Print("** OrdersJournaling: Unexpected Error has happened. Error Description: "+GetErrorDescription(GetLastError())+" **");
               if(OrdersJournaling && Modify) Print("** OrdersJournaling: Order successfully modified, trailing stop changed."+" **");
               continue;
              }
           }
        }
     }
  }
//+------------------------------------------------------------------+
//| End of ReviewTrailingStop
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//|                         *** /Orders ***                           |
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                         *** System ***                           |
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
// Start of getP()                                                   |
//+------------------------------------------------------------------+

int GetP()
  {

   int output;

   if(Digits==5 || Digits==3) output=10;
   else output=1;

   return(output);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
// End of GetP()                                                    	|
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//| Start of HandleTradingEnvironment()                                         
//+------------------------------------------------------------------+
void HandleTradingEnvironment()
  {

   if(IsTradeAllowed()==true)return;
   if(!IsConnected())
     {
      Print("**** Error: Terminal is not connected to server..."+" ****");
      return;
     }
   if(!IsTradeAllowed())Print("**** Error: Trade is not alowed for some reason... ****");
   if(IsConnected() && !IsTradeAllowed())
     {
      while(IsTradeContextBusy()==true)
        {
         Print("**** Error: Trading context is busy... Will wait a bit... ****");
         Sleep(RetryInterval);
        }
     }
   RefreshRates();
  }
//+------------------------------------------------------------------+
//| End of HandleTradingEnvironment()                              
//+------------------------------------------------------------------+  
//+------------------------------------------------------------------+
//| Start of GetErrorDescription()                                               
//+------------------------------------------------------------------+
string GetErrorDescription(int error)
  {
   string ErrorDescription="";
//---
   switch(error)
     {
      case 0:     ErrorDescription = "NO Error. Everything should be good.";                                    break;
      case 1:     ErrorDescription = "No error returned, but the result is unknown";                            break;
      case 2:     ErrorDescription = "Common error";                                                            break;
      case 3:     ErrorDescription = "Invalid trade parameters";                                                break;
      case 4:     ErrorDescription = "Trade server is busy";                                                    break;
      case 5:     ErrorDescription = "Old version of the client terminal";                                      break;
      case 6:     ErrorDescription = "No connection with trade server";                                         break;
      case 7:     ErrorDescription = "Not enough rights";                                                       break;
      case 8:     ErrorDescription = "Too frequent requests";                                                   break;
      case 9:     ErrorDescription = "Malfunctional trade operation";                                           break;
      case 64:    ErrorDescription = "Account disabled";                                                        break;
      case 65:    ErrorDescription = "Invalid account";                                                         break;
      case 128:   ErrorDescription = "Trade timeout";                                                           break;
      case 129:   ErrorDescription = "Invalid price";                                                           break;
      case 130:   ErrorDescription = "Invalid stops";                                                           break;
      case 131:   ErrorDescription = "Invalid trade volume";                                                    break;
      case 132:   ErrorDescription = "Market is closed";                                                        break;
      case 133:   ErrorDescription = "Trade is disabled";                                                       break;
      case 134:   ErrorDescription = "Not enough money";                                                        break;
      case 135:   ErrorDescription = "Price changed";                                                           break;
      case 136:   ErrorDescription = "Off quotes";                                                              break;
      case 137:   ErrorDescription = "Broker is busy";                                                          break;
      case 138:   ErrorDescription = "Requote";                                                                 break;
      case 139:   ErrorDescription = "Order is locked";                                                         break;
      case 140:   ErrorDescription = "Long positions only allowed";                                             break;
      case 141:   ErrorDescription = "Too many requests";                                                       break;
      case 145:   ErrorDescription = "Modification denied because order too close to market";                   break;
      case 146:   ErrorDescription = "Trade context is busy";                                                   break;
      case 147:   ErrorDescription = "Expirations are denied by broker";                                        break;
      case 148:   ErrorDescription = "Too many open and pending orders (more than allowed)";                    break;
      case 4000:  ErrorDescription = "No error";                                                                break;
      case 4001:  ErrorDescription = "Wrong function pointer";                                                  break;
      case 4002:  ErrorDescription = "Array index is out of range";                                             break;
      case 4003:  ErrorDescription = "No memory for function call stack";                                       break;
      case 4004:  ErrorDescription = "Recursive stack overflow";                                                break;
      case 4005:  ErrorDescription = "Not enough stack for parameter";                                          break;
      case 4006:  ErrorDescription = "No memory for parameter string";                                          break;
      case 4007:  ErrorDescription = "No memory for temp string";                                               break;
      case 4008:  ErrorDescription = "Not initialized string";                                                  break;
      case 4009:  ErrorDescription = "Not initialized string in array";                                         break;
      case 4010:  ErrorDescription = "No memory for array string";                                              break;
      case 4011:  ErrorDescription = "Too long string";                                                         break;
      case 4012:  ErrorDescription = "Remainder from zero divide";                                              break;
      case 4013:  ErrorDescription = "Zero divide";                                                             break;
      case 4014:  ErrorDescription = "Unknown command";                                                         break;
      case 4015:  ErrorDescription = "Wrong jump (never generated error)";                                      break;
      case 4016:  ErrorDescription = "Not initialized array";                                                   break;
      case 4017:  ErrorDescription = "DLL calls are not allowed";                                               break;
      case 4018:  ErrorDescription = "Cannot load library";                                                     break;
      case 4019:  ErrorDescription = "Cannot call function";                                                    break;
      case 4020:  ErrorDescription = "Expert function calls are not allowed";                                   break;
      case 4021:  ErrorDescription = "Not enough memory for temp string returned from function";                break;
      case 4022:  ErrorDescription = "System is busy (never generated error)";                                  break;
      case 4050:  ErrorDescription = "Invalid function parameters count";                                       break;
      case 4051:  ErrorDescription = "Invalid function parameter value";                                        break;
      case 4052:  ErrorDescription = "String function internal error";                                          break;
      case 4053:  ErrorDescription = "Some array error";                                                        break;
      case 4054:  ErrorDescription = "Incorrect series array using";                                            break;
      case 4055:  ErrorDescription = "Custom indicator error";                                                  break;
      case 4056:  ErrorDescription = "Arrays are incompatible";                                                 break;
      case 4057:  ErrorDescription = "Global variables processing error";                                       break;
      case 4058:  ErrorDescription = "Global variable not found";                                               break;
      case 4059:  ErrorDescription = "Function is not allowed in testing mode";                                 break;
      case 4060:  ErrorDescription = "Function is not confirmed";                                               break;
      case 4061:  ErrorDescription = "Send mail error";                                                         break;
      case 4062:  ErrorDescription = "String parameter expected";                                               break;
      case 4063:  ErrorDescription = "Integer parameter expected";                                              break;
      case 4064:  ErrorDescription = "Double parameter expected";                                               break;
      case 4065:  ErrorDescription = "Array as parameter expected";                                             break;
      case 4066:  ErrorDescription = "Requested history data in updating state";                                break;
      case 4067:  ErrorDescription = "Some error in trading function";                                          break;
      case 4099:  ErrorDescription = "End of file";                                                             break;
      case 4100:  ErrorDescription = "Some file error";                                                         break;
      case 4101:  ErrorDescription = "Wrong file name";                                                         break;
      case 4102:  ErrorDescription = "Too many opened files";                                                   break;
      case 4103:  ErrorDescription = "Cannot open file";                                                        break;
      case 4104:  ErrorDescription = "Incompatible access to a file";                                           break;
      case 4105:  ErrorDescription = "No order selected";                                                       break;
      case 4106:  ErrorDescription = "Unknown symbol";                                                          break;
      case 4107:  ErrorDescription = "Invalid price";                                                           break;
      case 4108:  ErrorDescription = "Invalid ticket";                                                          break;
      case 4109:  ErrorDescription = "EA is not allowed to trade is not allowed. ";                             break;
      case 4110:  ErrorDescription = "Longs are not allowed. Check the expert properties";                      break;
      case 4111:  ErrorDescription = "Shorts are not allowed. Check the expert properties";                     break;
      case 4200:  ErrorDescription = "Object exists already";                                                   break;
      case 4201:  ErrorDescription = "Unknown object property";                                                 break;
      case 4202:  ErrorDescription = "Object does not exist";                                                   break;
      case 4203:  ErrorDescription = "Unknown object type";                                                     break;
      case 4204:  ErrorDescription = "No object name";                                                          break;
      case 4205:  ErrorDescription = "Object coordinates error";                                                break;
      case 4206:  ErrorDescription = "No specified subwindow";                                                  break;
      case 4207:  ErrorDescription = "Some error in object function";                                           break;
      default:    ErrorDescription = "No error or error is unknown";
     }
   return(ErrorDescription);
  }
//+------------------------------------------------------------------+
//| End of GetErrorDescription()                                         
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//| Start of isNowNewBar()                                           |
//+------------------------------------------------------------------+
bool isNowNewBar()
  {
//--- memorize the time of opening of the last bar in the static variable
   static datetime last_time=0;
//--- current time
   datetime lastbar_time=SeriesInfoInteger(Symbol(),Period(),SERIES_LASTBAR_DATE);
//--- if it is the first call of the function
   if(last_time==0)
     {
      //--- set the time and exit
      last_time=lastbar_time;
      return(false);
     }
//--- if the time differs
   if(last_time!=lastbar_time)
     {
      //--- memorize the time and return true
      last_time=lastbar_time;
      return(true);
     }
//--- if we passed to this line, then the bar is not new; return false
   return(false);
  }
//+------------------------------------------------------------------+
//| End of isNowNewBar()                                             |                                         
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//| Start of isSuperTrendNewBar()                                    |                                         
//+------------------------------------------------------------------+
bool isSuperTrendNewBar()
  {
//--- memorize the time of opening of the last bar in the static variable
   static datetime last_time=0;
//--- current time
   datetime lastbar_time=SeriesInfoInteger(Symbol(),Supertrend_Timeframe,SERIES_LASTBAR_DATE);
//--- if it is the first call of the function
   if(last_time==0)
     {
      //--- set the time and exit
      last_time=lastbar_time;
      return(false);
     }
//--- if the time differs
   if(last_time!=lastbar_time)
     {
      //--- memorize the time and return true
      last_time=lastbar_time;
      return(true);
     }
//--- if we passed to this line, then the bar is not new; return false
   return(false);
  }
//+------------------------------------------------------------------+
//| End of isSuperTrendNewBar()                                      |                                         
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                         *** /System ***                          |
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                         *** Chart ***                            |
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//| Start of ChartSettings()                                         |                                         
//+------------------------------------------------------------------+
void ChartSettings()
  {
   ChartSetInteger(0,CHART_SHOW_GRID,0);
   ChartSetInteger(0,CHART_MODE,CHART_CANDLES);
   ChartSetInteger(0,CHART_AUTOSCROLL,0,True);
   ChartSetInteger(0,CHART_SHOW_OBJECT_DESCR,true);
   WindowRedraw();
  }
//+------------------------------------------------------------------+
//| End of ChartSettings()                                         
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|Start of DrawChartInfo                                                                  |
//+------------------------------------------------------------------+

void DrawChartInfo()
  {

// Info Background 1
   ObjectCreate(ChartID(),ObjName+"InfoBackground1",OBJ_RECTANGLE_LABEL,0,0,0);
   ObjectSetInteger(ChartID(),ObjName+"InfoBackground1",OBJPROP_XSIZE,-240);
   ObjectSetInteger(ChartID(),ObjName+"InfoBackground1",OBJPROP_YSIZE,160);
   ObjectSetInteger(ChartID(),ObjName+"InfoBackground1",OBJPROP_XDISTANCE,xDistance);
   ObjectSetInteger(ChartID(),ObjName+"InfoBackground1",OBJPROP_YDISTANCE,20);
   ObjectSetInteger(ChartID(),ObjName+"InfoBackground1",OBJPROP_CORNER,CORNER_RIGHT_UPPER);
   ObjectSetInteger(ChartID(),ObjName+"InfoBackground1",OBJPROP_BGCOLOR,Black);
   ObjectSetInteger(ChartID(),ObjName+"InfoBackground1",OBJPROP_BORDER_COLOR,Black);
   ObjectSetInteger(ChartID(),ObjName+"InfoBackground1",OBJPROP_BACK,false);
   ObjectSetInteger(ChartID(),ObjName+"InfoBackground1",OBJPROP_SELECTABLE,false);
   ObjectSetInteger(ChartID(),ObjName+"InfoBackground1",OBJPROP_SELECTED,false);
   ObjectSetInteger(ChartID(),ObjName+"InfoBackground1",OBJPROP_HIDDEN,false);
   ObjectSetInteger(ChartID(),ObjName+"InfoBackground1",OBJPROP_BORDER_TYPE,BORDER_FLAT);

   ObjectCreate(ChartID(),ObjName+"Symbol",OBJ_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"Symbol",OBJPROP_TEXT,Symbol()+"  "+spreadValue);
   ObjectSetInteger(ChartID(),ObjName+"Symbol",OBJPROP_CORNER,CORNER_RIGHT_UPPER);
   ObjectSetInteger(ChartID(),ObjName+"Symbol",OBJPROP_XDISTANCE,xDistance);
   ObjectSetInteger(ChartID(),ObjName+"Symbol",OBJPROP_YDISTANCE,37);
   ObjectSetString(ChartID(),ObjName+"Symbol",OBJPROP_FONT,"Helvetica");
   ObjectSetInteger(ChartID(),ObjName+"Symbol",OBJPROP_FONTSIZE,14);
   ObjectSetInteger(ChartID(),ObjName+"Symbol",OBJPROP_ANCHOR,ANCHOR_RIGHT);
   ObjectSetInteger(ChartID(),ObjName+"Symbol",OBJPROP_COLOR,Panel1LabelColor);

   ObjectCreate(ChartID(),ObjName+"Margin",OBJ_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"Margin",OBJPROP_TEXT,"Margin: "+DoubleToStr(AccountInfoDouble(ACCOUNT_MARGIN),2)+"/ Lvl: "+DoubleToStr(AccountInfoDouble(ACCOUNT_MARGIN_LEVEL),2));
   ObjectSetInteger(ChartID(),ObjName+"Margin",OBJPROP_CORNER,CORNER_RIGHT_UPPER);
   ObjectSetInteger(ChartID(),ObjName+"Margin",OBJPROP_XDISTANCE,xDistance);
   ObjectSetInteger(ChartID(),ObjName+"Margin",OBJPROP_YDISTANCE,62);
   ObjectSetString(ChartID(),ObjName+"Margin",OBJPROP_FONT,"Helvetica");
   ObjectSetInteger(ChartID(),ObjName+"Margin",OBJPROP_FONTSIZE,14);
   ObjectSetInteger(ChartID(),ObjName+"Margin",OBJPROP_ANCHOR,ANCHOR_RIGHT);
   ObjectSetInteger(ChartID(),ObjName+"Margin",OBJPROP_COLOR,Panel1LabelColor);

   ObjectCreate(ChartID(),ObjName+"Balance",OBJ_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"Balance",OBJPROP_TEXT,"Balance: "+DoubleToStr(AccountBalance(),2));
   ObjectSetInteger(ChartID(),ObjName+"Balance",OBJPROP_CORNER,CORNER_RIGHT_UPPER);
   ObjectSetInteger(ChartID(),ObjName+"Balance",OBJPROP_XDISTANCE,xDistance);
   ObjectSetInteger(ChartID(),ObjName+"Balance",OBJPROP_YDISTANCE,87);
   ObjectSetString(ChartID(),ObjName+"Balance",OBJPROP_FONT,"Helvetica");
   ObjectSetInteger(ChartID(),ObjName+"Balance",OBJPROP_FONTSIZE,14);
   ObjectSetInteger(ChartID(),ObjName+"Balance",OBJPROP_ANCHOR,ANCHOR_RIGHT);
   ObjectSetInteger(ChartID(),ObjName+"Balance",OBJPROP_COLOR,Panel1LabelColor);

   ObjectCreate(ChartID(),ObjName+"Equity",OBJ_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"Equity",OBJPROP_TEXT,"Equity: "+DoubleToStr(AccountEquity(),2));
   ObjectSetInteger(ChartID(),ObjName+"Equity",OBJPROP_CORNER,CORNER_RIGHT_UPPER);
   ObjectSetInteger(ChartID(),ObjName+"Equity",OBJPROP_XDISTANCE,xDistance);
   ObjectSetInteger(ChartID(),ObjName+"Equity",OBJPROP_YDISTANCE,112);
   ObjectSetString(ChartID(),ObjName+"Equity",OBJPROP_FONT,"Helvetica");
   ObjectSetInteger(ChartID(),ObjName+"Equity",OBJPROP_FONTSIZE,14);
   ObjectSetInteger(ChartID(),ObjName+"Equity",OBJPROP_ANCHOR,ANCHOR_RIGHT);
   ObjectSetInteger(ChartID(),ObjName+"Equity",OBJPROP_COLOR,Panel1LabelColor);

   ObjectCreate(ChartID(),ObjName+"TotalLots",OBJ_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"TotalLots",OBJPROP_TEXT,"Total Lots: "+DoubleToStr(totalLots,2));
   ObjectSetInteger(ChartID(),ObjName+"TotalLots",OBJPROP_CORNER,CORNER_RIGHT_UPPER);
   ObjectSetInteger(ChartID(),ObjName+"TotalLots",OBJPROP_XDISTANCE,xDistance);
   ObjectSetInteger(ChartID(),ObjName+"TotalLots",OBJPROP_YDISTANCE,137);
   ObjectSetString(ChartID(),ObjName+"TotalLots",OBJPROP_FONT,"Helvetica");
   ObjectSetInteger(ChartID(),ObjName+"TotalLots",OBJPROP_FONTSIZE,14);
   ObjectSetInteger(ChartID(),ObjName+"TotalLots",OBJPROP_ANCHOR,ANCHOR_RIGHT);
   ObjectSetInteger(ChartID(),ObjName+"TotalLots",OBJPROP_COLOR,Panel1LabelColor);

   ObjectCreate(ChartID(),ObjName+"TotalProfit",OBJ_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"TotalProfit",OBJPROP_TEXT,"Total Profit: "+DoubleToStr(totalProfit,2));
   ObjectSetInteger(ChartID(),ObjName+"TotalProfit",OBJPROP_CORNER,CORNER_RIGHT_UPPER);
   ObjectSetInteger(ChartID(),ObjName+"TotalProfit",OBJPROP_XDISTANCE,xDistance);
   ObjectSetInteger(ChartID(),ObjName+"TotalProfit",OBJPROP_YDISTANCE,162);
   ObjectSetString(ChartID(),ObjName+"TotalProfit",OBJPROP_FONT,"Helvetica");
   ObjectSetInteger(ChartID(),ObjName+"TotalProfit",OBJPROP_FONTSIZE,14);
   ObjectSetInteger(ChartID(),ObjName+"TotalProfit",OBJPROP_ANCHOR,ANCHOR_RIGHT);
   ObjectSetInteger(ChartID(),ObjName+"TotalProfit",OBJPROP_COLOR,Panel1LabelColor);

// Info Background 2
   ObjectCreate(ChartID(),ObjName+"InfoBackground2",OBJ_RECTANGLE_LABEL,0,0,0);
   ObjectSetInteger(ChartID(),ObjName+"InfoBackground2",OBJPROP_XSIZE,-240);
   ObjectSetInteger(ChartID(),ObjName+"InfoBackground2",OBJPROP_YSIZE,160);
   ObjectSetInteger(ChartID(),ObjName+"InfoBackground2",OBJPROP_XDISTANCE,xDistance);
   ObjectSetInteger(ChartID(),ObjName+"InfoBackground2",OBJPROP_YDISTANCE,185);
   ObjectSetInteger(ChartID(),ObjName+"InfoBackground2",OBJPROP_CORNER,CORNER_RIGHT_UPPER);
   ObjectSetInteger(ChartID(),ObjName+"InfoBackground2",OBJPROP_BGCOLOR,Black);
   ObjectSetInteger(ChartID(),ObjName+"InfoBackground2",OBJPROP_BORDER_COLOR,Black);
   ObjectSetInteger(ChartID(),ObjName+"InfoBackground2",OBJPROP_BACK,false);
   ObjectSetInteger(ChartID(),ObjName+"InfoBackground2",OBJPROP_SELECTABLE,false);
   ObjectSetInteger(ChartID(),ObjName+"InfoBackground2",OBJPROP_SELECTED,false);
   ObjectSetInteger(ChartID(),ObjName+"InfoBackground2",OBJPROP_HIDDEN,false);
   ObjectSetInteger(ChartID(),ObjName+"InfoBackground2",OBJPROP_BORDER_TYPE,BORDER_FLAT);

   ObjectCreate(ChartID(),ObjName+"CurrentLots",OBJ_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"CurrentLots",OBJPROP_TEXT,"Current Lots: 'B'  "+DoubleToStr(buyCurrentLots,2)+" / 'S'  "+DoubleToStr(sellCurrentLots,2));
   ObjectSetInteger(ChartID(),ObjName+"CurrentLots",OBJPROP_CORNER,CORNER_RIGHT_UPPER);
   ObjectSetInteger(ChartID(),ObjName+"CurrentLots",OBJPROP_XDISTANCE,xDistance);
   ObjectSetInteger(ChartID(),ObjName+"CurrentLots",OBJPROP_YDISTANCE,202);
   ObjectSetString(ChartID(),ObjName+"CurrentLots",OBJPROP_FONT,"Helvetica");
   ObjectSetInteger(ChartID(),ObjName+"CurrentLots",OBJPROP_FONTSIZE,11);
   ObjectSetInteger(ChartID(),ObjName+"CurrentLots",OBJPROP_ANCHOR,ANCHOR_RIGHT);
   ObjectSetInteger(ChartID(),ObjName+"CurrentLots",OBJPROP_COLOR,Panel2LabelColor);

   ObjectCreate(ChartID(),ObjName+"CycleLots",OBJ_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"CycleLots",OBJPROP_TEXT,"Cycle Lots: 'B'  "+DoubleToStr(buyCycleLots,2)+" / 'S'  "+DoubleToStr(sellCycleLots,2));
   ObjectSetInteger(ChartID(),ObjName+"CycleLots",OBJPROP_CORNER,CORNER_RIGHT_UPPER);
   ObjectSetInteger(ChartID(),ObjName+"CycleLots",OBJPROP_XDISTANCE,xDistance);
   ObjectSetInteger(ChartID(),ObjName+"CycleLots",OBJPROP_YDISTANCE,227);
   ObjectSetString(ChartID(),ObjName+"CycleLots",OBJPROP_FONT,"Helvetica");
   ObjectSetInteger(ChartID(),ObjName+"CycleLots",OBJPROP_FONTSIZE,11);
   ObjectSetInteger(ChartID(),ObjName+"CycleLots",OBJPROP_ANCHOR,ANCHOR_RIGHT);
   ObjectSetInteger(ChartID(),ObjName+"CycleLots",OBJPROP_COLOR,Panel2LabelColor);

   ObjectCreate(ChartID(),ObjName+"TotalCycleLots",OBJ_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"TotalCycleLots",OBJPROP_TEXT,"Total Cycles Lots: "+DoubleToStr(totalCycleLots,2));
   ObjectSetInteger(ChartID(),ObjName+"TotalCycleLots",OBJPROP_CORNER,CORNER_RIGHT_UPPER);
   ObjectSetInteger(ChartID(),ObjName+"TotalCycleLots",OBJPROP_XDISTANCE,xDistance);
   ObjectSetInteger(ChartID(),ObjName+"TotalCycleLots",OBJPROP_YDISTANCE,252);
   ObjectSetString(ChartID(),ObjName+"TotalCycleLots",OBJPROP_FONT,"Helvetica");
   ObjectSetInteger(ChartID(),ObjName+"TotalCycleLots",OBJPROP_FONTSIZE,11);
   ObjectSetInteger(ChartID(),ObjName+"TotalCycleLots",OBJPROP_ANCHOR,ANCHOR_RIGHT);
   ObjectSetInteger(ChartID(),ObjName+"TotalCycleLots",OBJPROP_COLOR,Panel2LabelColor);

   ObjectCreate(ChartID(),ObjName+"CurrentProfit",OBJ_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"CurrentProfit",OBJPROP_TEXT,"Current Profit: 'B'  "+DoubleToStr(buyCurrentProfit,2)+" / 'S'  "+DoubleToStr(sellCurrentProfit,2));
   ObjectSetInteger(ChartID(),ObjName+"CurrentProfit",OBJPROP_CORNER,CORNER_RIGHT_UPPER);
   ObjectSetInteger(ChartID(),ObjName+"CurrentProfit",OBJPROP_XDISTANCE,xDistance);
   ObjectSetInteger(ChartID(),ObjName+"CurrentProfit",OBJPROP_YDISTANCE,277);
   ObjectSetString(ChartID(),ObjName+"CurrentProfit",OBJPROP_FONT,"Helvetica");
   ObjectSetInteger(ChartID(),ObjName+"CurrentProfit",OBJPROP_FONTSIZE,11);
   ObjectSetInteger(ChartID(),ObjName+"CurrentProfit",OBJPROP_ANCHOR,ANCHOR_RIGHT);
   ObjectSetInteger(ChartID(),ObjName+"CurrentProfit",OBJPROP_COLOR,Panel2LabelColor);

   ObjectCreate(ChartID(),ObjName+"CycleProfit",OBJ_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"CycleProfit",OBJPROP_TEXT,"Cycle Profit: 'B'  "+DoubleToStr(buyCycleProfit,2)+" / 'S'  "+DoubleToStr(sellCycleProfit,2));
   ObjectSetInteger(ChartID(),ObjName+"CycleProfit",OBJPROP_CORNER,CORNER_RIGHT_UPPER);
   ObjectSetInteger(ChartID(),ObjName+"CycleProfit",OBJPROP_XDISTANCE,xDistance);
   ObjectSetInteger(ChartID(),ObjName+"CycleProfit",OBJPROP_YDISTANCE,302);
   ObjectSetString(ChartID(),ObjName+"CycleProfit",OBJPROP_FONT,"Helvetica");
   ObjectSetInteger(ChartID(),ObjName+"CycleProfit",OBJPROP_FONTSIZE,11);
   ObjectSetInteger(ChartID(),ObjName+"CycleProfit",OBJPROP_ANCHOR,ANCHOR_RIGHT);
   ObjectSetInteger(ChartID(),ObjName+"CycleProfit",OBJPROP_COLOR,Panel2LabelColor);

   ObjectCreate(ChartID(),ObjName+"TotalCycleProfit",OBJ_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"TotalCycleProfit",OBJPROP_TEXT,"Total Cycles Profit: "+DoubleToStr(totalCycleProfit,2));
   ObjectSetInteger(ChartID(),ObjName+"TotalCycleProfit",OBJPROP_CORNER,CORNER_RIGHT_UPPER);
   ObjectSetInteger(ChartID(),ObjName+"TotalCycleProfit",OBJPROP_XDISTANCE,xDistance);
   ObjectSetInteger(ChartID(),ObjName+"TotalCycleProfit",OBJPROP_YDISTANCE,327);
   ObjectSetString(ChartID(),ObjName+"TotalCycleProfit",OBJPROP_FONT,"Helvetica");
   ObjectSetInteger(ChartID(),ObjName+"TotalCycleProfit",OBJPROP_FONTSIZE,11);
   ObjectSetInteger(ChartID(),ObjName+"TotalCycleProfit",OBJPROP_ANCHOR,ANCHOR_RIGHT);
   ObjectSetInteger(ChartID(),ObjName+"TotalCycleProfit",OBJPROP_COLOR,Panel2LabelColor);

// Info Background 3
   ObjectCreate(ChartID(),ObjName+"InfoBackground3",OBJ_RECTANGLE_LABEL,0,0,0);
   ObjectSetInteger(ChartID(),ObjName+"InfoBackground3",OBJPROP_XSIZE,-240);
   ObjectSetInteger(ChartID(),ObjName+"InfoBackground3",OBJPROP_YSIZE,165);
   ObjectSetInteger(ChartID(),ObjName+"InfoBackground3",OBJPROP_XDISTANCE,xDistance);
   ObjectSetInteger(ChartID(),ObjName+"InfoBackground3",OBJPROP_YDISTANCE,350);
   ObjectSetInteger(ChartID(),ObjName+"InfoBackground3",OBJPROP_CORNER,CORNER_RIGHT_UPPER);
   ObjectSetInteger(ChartID(),ObjName+"InfoBackground3",OBJPROP_BGCOLOR,Black);
   ObjectSetInteger(ChartID(),ObjName+"InfoBackground3",OBJPROP_BORDER_COLOR,Black);
   ObjectSetInteger(ChartID(),ObjName+"InfoBackground3",OBJPROP_BACK,false);
   ObjectSetInteger(ChartID(),ObjName+"InfoBackground3",OBJPROP_SELECTABLE,false);
   ObjectSetInteger(ChartID(),ObjName+"InfoBackground3",OBJPROP_SELECTED,false);
   ObjectSetInteger(ChartID(),ObjName+"InfoBackground3",OBJPROP_HIDDEN,false);
   ObjectSetInteger(ChartID(),ObjName+"InfoBackground3",OBJPROP_BORDER_TYPE,BORDER_FLAT);

   ObjectCreate(ChartID(),ObjName+"LiveBuyCycleLotLabel",OBJ_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"LiveBuyCycleLotLabel",OBJPROP_TEXT,"Buy Lot: ");
   ObjectSetInteger(ChartID(),ObjName+"LiveBuyCycleLotLabel",OBJPROP_CORNER,CORNER_RIGHT_UPPER);
   ObjectSetInteger(ChartID(),ObjName+"LiveBuyCycleLotLabel",OBJPROP_XDISTANCE,210);
   ObjectSetInteger(ChartID(),ObjName+"LiveBuyCycleLotLabel",OBJPROP_YDISTANCE,367);
   ObjectSetString(ChartID(),ObjName+"LiveBuyCycleLotLabel",OBJPROP_FONT,"Helvetica");
   ObjectSetInteger(ChartID(),ObjName+"LiveBuyCycleLotLabel",OBJPROP_FONTSIZE,11);
   ObjectSetInteger(ChartID(),ObjName+"LiveBuyCycleLotLabel",OBJPROP_ANCHOR,ANCHOR_LEFT);
   ObjectSetInteger(ChartID(),ObjName+"LiveBuyCycleLotLabel",OBJPROP_COLOR,Panel3LabelColor);

   DrawEdit((ObjName+"LiveBuyCycleLotInput"),Black,190,357,CORNER_RIGHT_UPPER,"LiveBuyCycleLotInput",BuyCycleLot,90,20,10,White,Panel3LabelColor);

   ObjectCreate(ChartID(),ObjName+"LiveBuyCycleTakeProfitLabel",OBJ_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"LiveBuyCycleTakeProfitLabel",OBJPROP_TEXT,"Buy TakeProfit: ");
   ObjectSetInteger(ChartID(),ObjName+"LiveBuyCycleTakeProfitLabel",OBJPROP_CORNER,CORNER_RIGHT_UPPER);
   ObjectSetInteger(ChartID(),ObjName+"LiveBuyCycleTakeProfitLabel",OBJPROP_XDISTANCE,210);
   ObjectSetInteger(ChartID(),ObjName+"LiveBuyCycleTakeProfitLabel",OBJPROP_YDISTANCE,392);
   ObjectSetString(ChartID(),ObjName+"LiveBuyCycleTakeProfitLabel",OBJPROP_FONT,"Helvetica");
   ObjectSetInteger(ChartID(),ObjName+"LiveBuyCycleTakeProfitLabel",OBJPROP_FONTSIZE,11);
   ObjectSetInteger(ChartID(),ObjName+"LiveBuyCycleTakeProfitLabel",OBJPROP_ANCHOR,ANCHOR_LEFT);
   ObjectSetInteger(ChartID(),ObjName+"LiveBuyCycleTakeProfitLabel",OBJPROP_COLOR,Panel3LabelColor);

   DrawEdit((ObjName+"LiveBuyCycleTakeProfitInput"),Black,190,382,CORNER_RIGHT_UPPER,"LiveBuyCycleTakeProfitInput",BuyCycleTakeProfit,90,20,10,White,Panel3LabelColor);

   ObjectCreate(ChartID(),ObjName+"LiveBuyCyclePipStepLabel",OBJ_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"LiveBuyCyclePipStepLabel",OBJPROP_TEXT,"Buy PipStep: ");
   ObjectSetInteger(ChartID(),ObjName+"LiveBuyCyclePipStepLabel",OBJPROP_CORNER,CORNER_RIGHT_UPPER);
   ObjectSetInteger(ChartID(),ObjName+"LiveBuyCyclePipStepLabel",OBJPROP_XDISTANCE,210);
   ObjectSetInteger(ChartID(),ObjName+"LiveBuyCyclePipStepLabel",OBJPROP_YDISTANCE,417);
   ObjectSetString(ChartID(),ObjName+"LiveBuyCyclePipStepLabel",OBJPROP_FONT,"Helvetica");
   ObjectSetInteger(ChartID(),ObjName+"LiveBuyCyclePipStepLabel",OBJPROP_FONTSIZE,11);
   ObjectSetInteger(ChartID(),ObjName+"LiveBuyCyclePipStepLabel",OBJPROP_ANCHOR,ANCHOR_LEFT);
   ObjectSetInteger(ChartID(),ObjName+"LiveBuyCyclePipStepLabel",OBJPROP_COLOR,Panel3LabelColor);

   DrawEdit((ObjName+"LiveBuyCyclePipStepInput"),Black,190,407,CORNER_RIGHT_UPPER,"LiveBuyCyclePipStepInput",BuyCyclePipStep,90,20,10,White,Panel3LabelColor);

   ObjectCreate(ChartID(),ObjName+"LiveSellCycleLotLabel",OBJ_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"LiveSellCycleLotLabel",OBJPROP_TEXT,"Sell Lot: ");
   ObjectSetInteger(ChartID(),ObjName+"LiveSellCycleLotLabel",OBJPROP_CORNER,CORNER_RIGHT_UPPER);
   ObjectSetInteger(ChartID(),ObjName+"LiveSellCycleLotLabel",OBJPROP_XDISTANCE,210);
   ObjectSetInteger(ChartID(),ObjName+"LiveSellCycleLotLabel",OBJPROP_YDISTANCE,452);
   ObjectSetString(ChartID(),ObjName+"LiveSellCycleLotLabel",OBJPROP_FONT,"Helvetica");
   ObjectSetInteger(ChartID(),ObjName+"LiveSellCycleLotLabel",OBJPROP_FONTSIZE,11);
   ObjectSetInteger(ChartID(),ObjName+"LiveSellCycleLotLabel",OBJPROP_ANCHOR,ANCHOR_LEFT);
   ObjectSetInteger(ChartID(),ObjName+"LiveSellCycleLotLabel",OBJPROP_COLOR,Panel3LabelColor);

   DrawEdit((ObjName+"LiveSellCycleLotInput"),Black,190,442,CORNER_RIGHT_UPPER,"LiveSellCycleLotInput",SellCycleLot,90,20,10,White,Panel3LabelColor);

   ObjectCreate(ChartID(),ObjName+"LiveSellCycleTakeProfitLabel",OBJ_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"LiveSellCycleTakeProfitLabel",OBJPROP_TEXT,"Sell TakeProfit: ");
   ObjectSetInteger(ChartID(),ObjName+"LiveSellCycleTakeProfitLabel",OBJPROP_CORNER,CORNER_RIGHT_UPPER);
   ObjectSetInteger(ChartID(),ObjName+"LiveSellCycleTakeProfitLabel",OBJPROP_XDISTANCE,210);
   ObjectSetInteger(ChartID(),ObjName+"LiveSellCycleTakeProfitLabel",OBJPROP_YDISTANCE,477);
   ObjectSetString(ChartID(),ObjName+"LiveSellCycleTakeProfitLabel",OBJPROP_FONT,"Helvetica");
   ObjectSetInteger(ChartID(),ObjName+"LiveSellCycleTakeProfitLabel",OBJPROP_FONTSIZE,11);
   ObjectSetInteger(ChartID(),ObjName+"LiveSellCycleTakeProfitLabel",OBJPROP_ANCHOR,ANCHOR_LEFT);
   ObjectSetInteger(ChartID(),ObjName+"LiveSellCycleTakeProfitLabel",OBJPROP_COLOR,Panel3LabelColor);

   DrawEdit((ObjName+"LiveSellCycleTakeProfitInput"),Black,190,467,CORNER_RIGHT_UPPER,"LiveSellCycleTakeProfitInput",SellCycleTakeProfit,90,20,10,White,Panel3LabelColor);

   ObjectCreate(ChartID(),ObjName+"LiveSellCyclePipStepLabel",OBJ_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"LiveSellCyclePipStepLabel",OBJPROP_TEXT,"Sell PipStep: ");
   ObjectSetInteger(ChartID(),ObjName+"LiveSellCyclePipStepLabel",OBJPROP_CORNER,CORNER_RIGHT_UPPER);
   ObjectSetInteger(ChartID(),ObjName+"LiveSellCyclePipStepLabel",OBJPROP_XDISTANCE,210);
   ObjectSetInteger(ChartID(),ObjName+"LiveSellCyclePipStepLabel",OBJPROP_YDISTANCE,502);
   ObjectSetString(ChartID(),ObjName+"LiveSellCyclePipStepLabel",OBJPROP_FONT,"Helvetica");
   ObjectSetInteger(ChartID(),ObjName+"LiveSellCyclePipStepLabel",OBJPROP_FONTSIZE,11);
   ObjectSetInteger(ChartID(),ObjName+"LiveSellCyclePipStepLabel",OBJPROP_ANCHOR,ANCHOR_LEFT);
   ObjectSetInteger(ChartID(),ObjName+"LiveSellCyclePipStepLabel",OBJPROP_COLOR,Panel3LabelColor);

   DrawEdit((ObjName+"LiveSellCyclePipStepInput"),Black,190,492,CORNER_RIGHT_UPPER,"LiveSellCyclePipStepInput",SellCyclePipStep,90,20,10,White,Panel3LabelColor);

// Info Background 4
   ObjectCreate(ChartID(),ObjName+"InfoBackground4",OBJ_RECTANGLE_LABEL,0,0,0);
   ObjectSetInteger(ChartID(),ObjName+"InfoBackground4",OBJPROP_XSIZE,970);
   ObjectSetInteger(ChartID(),ObjName+"InfoBackground4",OBJPROP_YSIZE,-60);
   ObjectSetInteger(ChartID(),ObjName+"InfoBackground4",OBJPROP_XDISTANCE,210);
   ObjectSetInteger(ChartID(),ObjName+"InfoBackground4",OBJPROP_YDISTANCE,20);
   ObjectSetInteger(ChartID(),ObjName+"InfoBackground4",OBJPROP_CORNER,CORNER_LEFT_LOWER);
   ObjectSetInteger(ChartID(),ObjName+"InfoBackground4",OBJPROP_BGCOLOR,Black);
   ObjectSetInteger(ChartID(),ObjName+"InfoBackground4",OBJPROP_BORDER_COLOR,Black);
   ObjectSetInteger(ChartID(),ObjName+"InfoBackground4",OBJPROP_BACK,false);
   ObjectSetInteger(ChartID(),ObjName+"InfoBackground4",OBJPROP_SELECTABLE,false);
   ObjectSetInteger(ChartID(),ObjName+"InfoBackground4",OBJPROP_SELECTED,false);
   ObjectSetInteger(ChartID(),ObjName+"InfoBackground4",OBJPROP_HIDDEN,false);
   ObjectSetInteger(ChartID(),ObjName+"InfoBackground4",OBJPROP_BORDER_TYPE,BORDER_FLAT);

   ObjectCreate(ChartID(),ObjName+"OrdersNumber",OBJ_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"OrdersNumber",OBJPROP_TEXT,"Orders #: 'B' "+IntegerToString(CountPosOrders(BuyCycleMagicNumber,OP_BUY),2)+" / 'S' "+IntegerToString(CountPosOrders(SellCycleMagicNumber,OP_SELL),2));
   ObjectSetInteger(ChartID(),ObjName+"OrdersNumber",OBJPROP_CORNER,CORNER_LEFT_LOWER);
   ObjectSetInteger(ChartID(),ObjName+"OrdersNumber",OBJPROP_XDISTANCE,210);
   ObjectSetInteger(ChartID(),ObjName+"OrdersNumber",OBJPROP_YDISTANCE,50);//177
   ObjectSetString(ChartID(),ObjName+"OrdersNumber",OBJPROP_FONT,"Helvetica");
   ObjectSetInteger(ChartID(),ObjName+"OrdersNumber",OBJPROP_FONTSIZE,11);
   ObjectSetInteger(ChartID(),ObjName+"OrdersNumber",OBJPROP_ANCHOR,ANCHOR_LEFT);
   ObjectSetInteger(ChartID(),ObjName+"OrdersNumber",OBJPROP_COLOR,Panel4LabelColor);

   ObjectCreate(ChartID(),ObjName+"BuyCycleLotSizingMethod",OBJ_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"BuyCycleLotSizingMethod",OBJPROP_TEXT,"Buy Lot Method: "+(BuyCycleLotSizeMethod==0?"Multiplier":BuyCycleLotSizeMethod==1?"Addition":"Sequence"));
   ObjectSetInteger(ChartID(),ObjName+"BuyCycleLotSizingMethod",OBJPROP_CORNER,CORNER_LEFT_LOWER);
   ObjectSetInteger(ChartID(),ObjName+"BuyCycleLotSizingMethod",OBJPROP_XDISTANCE,375);
   ObjectSetInteger(ChartID(),ObjName+"BuyCycleLotSizingMethod",OBJPROP_YDISTANCE,62);//177
   ObjectSetString(ChartID(),ObjName+"BuyCycleLotSizingMethod",OBJPROP_FONT,"Helvetica");
   ObjectSetInteger(ChartID(),ObjName+"BuyCycleLotSizingMethod",OBJPROP_FONTSIZE,11);
   ObjectSetInteger(ChartID(),ObjName+"BuyCycleLotSizingMethod",OBJPROP_ANCHOR,ANCHOR_LEFT);
   ObjectSetInteger(ChartID(),ObjName+"BuyCycleLotSizingMethod",OBJPROP_COLOR,Panel4LabelColor);

   ObjectCreate(ChartID(),ObjName+"NextBuyCycleLot",OBJ_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"NextBuyCycleLot",OBJPROP_TEXT,"Next Buy Lot: "+CalculateLot(OP_BUY));
   ObjectSetInteger(ChartID(),ObjName+"NextBuyCycleLot",OBJPROP_CORNER,CORNER_LEFT_LOWER);
   ObjectSetInteger(ChartID(),ObjName+"NextBuyCycleLot",OBJPROP_XDISTANCE,375);
   ObjectSetInteger(ChartID(),ObjName+"NextBuyCycleLot",OBJPROP_YDISTANCE,37);//177
   ObjectSetString(ChartID(),ObjName+"NextBuyCycleLot",OBJPROP_FONT,"Helvetica");
   ObjectSetInteger(ChartID(),ObjName+"NextBuyCycleLot",OBJPROP_FONTSIZE,11);
   ObjectSetInteger(ChartID(),ObjName+"NextBuyCycleLot",OBJPROP_ANCHOR,ANCHOR_LEFT);
   ObjectSetInteger(ChartID(),ObjName+"NextBuyCycleLot",OBJPROP_COLOR,Panel4LabelColor);

   ObjectCreate(ChartID(),ObjName+"SellCycleLotSizingMethod",OBJ_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"SellCycleLotSizingMethod",OBJPROP_TEXT,"Sell Lot Method: "+(SellCycleLotSizeMethod==0?"Multiplier":SellCycleLotSizeMethod==1?"Addition":"Sequence"));
   ObjectSetInteger(ChartID(),ObjName+"SellCycleLotSizingMethod",OBJPROP_CORNER,CORNER_LEFT_LOWER);
   ObjectSetInteger(ChartID(),ObjName+"SellCycleLotSizingMethod",OBJPROP_XDISTANCE,575);
   ObjectSetInteger(ChartID(),ObjName+"SellCycleLotSizingMethod",OBJPROP_YDISTANCE,62);//177
   ObjectSetString(ChartID(),ObjName+"SellCycleLotSizingMethod",OBJPROP_FONT,"Helvetica");
   ObjectSetInteger(ChartID(),ObjName+"SellCycleLotSizingMethod",OBJPROP_FONTSIZE,11);
   ObjectSetInteger(ChartID(),ObjName+"SellCycleLotSizingMethod",OBJPROP_ANCHOR,ANCHOR_LEFT);
   ObjectSetInteger(ChartID(),ObjName+"SellCycleLotSizingMethod",OBJPROP_COLOR,Panel4LabelColor);

   ObjectCreate(ChartID(),ObjName+"NextSellCycleLot",OBJ_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"NextSellCycleLot",OBJPROP_TEXT,"Next Sell Lot: "+CalculateLot(OP_SELL));
   ObjectSetInteger(ChartID(),ObjName+"NextSellCycleLot",OBJPROP_CORNER,CORNER_LEFT_LOWER);
   ObjectSetInteger(ChartID(),ObjName+"NextSellCycleLot",OBJPROP_XDISTANCE,575);
   ObjectSetInteger(ChartID(),ObjName+"NextSellCycleLot",OBJPROP_YDISTANCE,37);//152
   ObjectSetString(ChartID(),ObjName+"NextSellCycleLot",OBJPROP_FONT,"Helvetica");
   ObjectSetInteger(ChartID(),ObjName+"NextSellCycleLot",OBJPROP_FONTSIZE,11);
   ObjectSetInteger(ChartID(),ObjName+"NextSellCycleLot",OBJPROP_ANCHOR,ANCHOR_LEFT);
   ObjectSetInteger(ChartID(),ObjName+"NextSellCycleLot",OBJPROP_COLOR,Panel4LabelColor);

   ObjectCreate(ChartID(),ObjName+"BuyCyclePipStepSizingMethod",OBJ_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"BuyCyclePipStepSizingMethod",OBJPROP_TEXT,"Buy PipStep Method: "+(BuyCyclePipStepSizeMethod==0?"ATR":"Fixed"));
   ObjectSetInteger(ChartID(),ObjName+"BuyCyclePipStepSizingMethod",OBJPROP_CORNER,CORNER_LEFT_LOWER);
   ObjectSetInteger(ChartID(),ObjName+"BuyCyclePipStepSizingMethod",OBJPROP_XDISTANCE,775);
   ObjectSetInteger(ChartID(),ObjName+"BuyCyclePipStepSizingMethod",OBJPROP_YDISTANCE,62);//177
   ObjectSetString(ChartID(),ObjName+"BuyCyclePipStepSizingMethod",OBJPROP_FONT,"Helvetica");
   ObjectSetInteger(ChartID(),ObjName+"BuyCyclePipStepSizingMethod",OBJPROP_FONTSIZE,11);
   ObjectSetInteger(ChartID(),ObjName+"BuyCyclePipStepSizingMethod",OBJPROP_ANCHOR,ANCHOR_LEFT);
   ObjectSetInteger(ChartID(),ObjName+"BuyCyclePipStepSizingMethod",OBJPROP_COLOR,Panel4LabelColor);

   ObjectCreate(ChartID(),ObjName+"NextBuyCyclePipStepDistance",OBJ_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"NextBuyCyclePipStepDistance",OBJPROP_TEXT,"Next Buy PipStep: "+CalculatePipStep(OP_BUY));
   ObjectSetInteger(ChartID(),ObjName+"NextBuyCyclePipStepDistance",OBJPROP_CORNER,CORNER_LEFT_LOWER);
   ObjectSetInteger(ChartID(),ObjName+"NextBuyCyclePipStepDistance",OBJPROP_XDISTANCE,775);
   ObjectSetInteger(ChartID(),ObjName+"NextBuyCyclePipStepDistance",OBJPROP_YDISTANCE,37);//127
   ObjectSetString(ChartID(),ObjName+"NextBuyCyclePipStepDistance",OBJPROP_FONT,"Helvetica");
   ObjectSetInteger(ChartID(),ObjName+"NextBuyCyclePipStepDistance",OBJPROP_FONTSIZE,11);
   ObjectSetInteger(ChartID(),ObjName+"NextBuyCyclePipStepDistance",OBJPROP_ANCHOR,ANCHOR_LEFT);
   ObjectSetInteger(ChartID(),ObjName+"NextBuyCyclePipStepDistance",OBJPROP_COLOR,Panel4LabelColor);

   ObjectCreate(ChartID(),ObjName+"SellCyclePipStepSizingMethod",OBJ_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"SellCyclePipStepSizingMethod",OBJPROP_TEXT,"Sell PipStep Method: "+(SellCyclePipStepSizeMethod==0?"ATR":"Fixed"));
   ObjectSetInteger(ChartID(),ObjName+"SellCyclePipStepSizingMethod",OBJPROP_CORNER,CORNER_LEFT_LOWER);
   ObjectSetInteger(ChartID(),ObjName+"SellCyclePipStepSizingMethod",OBJPROP_XDISTANCE,975);
   ObjectSetInteger(ChartID(),ObjName+"SellCyclePipStepSizingMethod",OBJPROP_YDISTANCE,62);//177
   ObjectSetString(ChartID(),ObjName+"SellCyclePipStepSizingMethod",OBJPROP_FONT,"Helvetica");
   ObjectSetInteger(ChartID(),ObjName+"SellCyclePipStepSizingMethod",OBJPROP_FONTSIZE,11);
   ObjectSetInteger(ChartID(),ObjName+"SellCyclePipStepSizingMethod",OBJPROP_ANCHOR,ANCHOR_LEFT);
   ObjectSetInteger(ChartID(),ObjName+"SellCyclePipStepSizingMethod",OBJPROP_COLOR,Panel4LabelColor);

   ObjectCreate(ChartID(),ObjName+"NextSellCyclePipStepDistance",OBJ_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"NextSellCyclePipStepDistance",OBJPROP_TEXT,"Next Sell PipStep: "+CalculatePipStep(OP_SELL));
   ObjectSetInteger(ChartID(),ObjName+"NextSellCyclePipStepDistance",OBJPROP_CORNER,CORNER_LEFT_LOWER);
   ObjectSetInteger(ChartID(),ObjName+"NextSellCyclePipStepDistance",OBJPROP_XDISTANCE,975);
   ObjectSetInteger(ChartID(),ObjName+"NextSellCyclePipStepDistance",OBJPROP_YDISTANCE,37);//102
   ObjectSetString(ChartID(),ObjName+"NextSellCyclePipStepDistance",OBJPROP_FONT,"Helvetica");
   ObjectSetInteger(ChartID(),ObjName+"NextSellCyclePipStepDistance",OBJPROP_FONTSIZE,11);
   ObjectSetInteger(ChartID(),ObjName+"NextSellCyclePipStepDistance",OBJPROP_ANCHOR,ANCHOR_LEFT);
   ObjectSetInteger(ChartID(),ObjName+"NextSellCyclePipStepDistance",OBJPROP_COLOR,Panel4LabelColor);

// Info Background 5
   ObjectCreate(ChartID(),ObjName+"InfoBackground5",OBJ_RECTANGLE_LABEL,0,0,0);
   ObjectSetInteger(ChartID(),ObjName+"InfoBackground5",OBJPROP_XSIZE,200);
   ObjectSetInteger(ChartID(),ObjName+"InfoBackground5",OBJPROP_YSIZE,-60);
   ObjectSetInteger(ChartID(),ObjName+"InfoBackground5",OBJPROP_XDISTANCE,xDistance);
   ObjectSetInteger(ChartID(),ObjName+"InfoBackground5",OBJPROP_YDISTANCE,20);
   ObjectSetInteger(ChartID(),ObjName+"InfoBackground5",OBJPROP_CORNER,CORNER_LEFT_LOWER);
   ObjectSetInteger(ChartID(),ObjName+"InfoBackground5",OBJPROP_BGCOLOR,Black);
   ObjectSetInteger(ChartID(),ObjName+"InfoBackground5",OBJPROP_BORDER_COLOR,Black);
   ObjectSetInteger(ChartID(),ObjName+"InfoBackground5",OBJPROP_BACK,false);
   ObjectSetInteger(ChartID(),ObjName+"InfoBackground5",OBJPROP_SELECTABLE,false);
   ObjectSetInteger(ChartID(),ObjName+"InfoBackground5",OBJPROP_SELECTED,false);
   ObjectSetInteger(ChartID(),ObjName+"InfoBackground5",OBJPROP_HIDDEN,false);
   ObjectSetInteger(ChartID(),ObjName+"InfoBackground5",OBJPROP_BORDER_TYPE,BORDER_FLAT);

   ObjectCreate(ChartID(),ObjName+"SuperTrendFilterDirection",OBJ_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"SuperTrendFilterDirection",OBJPROP_TEXT,"SuperTrend Filter: "+(SuperTrendDirection==OP_BUY?"Buy":SuperTrendDirection==OP_SELL?"Sell":"NoValue"));
   ObjectSetInteger(ChartID(),ObjName+"SuperTrendFilterDirection",OBJPROP_CORNER,CORNER_LEFT_LOWER);
   ObjectSetInteger(ChartID(),ObjName+"SuperTrendFilterDirection",OBJPROP_XDISTANCE,xDistance);
   ObjectSetInteger(ChartID(),ObjName+"SuperTrendFilterDirection",OBJPROP_YDISTANCE,62);
   ObjectSetString(ChartID(),ObjName+"SuperTrendFilterDirection",OBJPROP_FONT,"Helvetica");
   ObjectSetInteger(ChartID(),ObjName+"SuperTrendFilterDirection",OBJPROP_FONTSIZE,11);
   ObjectSetInteger(ChartID(),ObjName+"SuperTrendFilterDirection",OBJPROP_ANCHOR,ANCHOR_LEFT);
   ObjectSetInteger(ChartID(),ObjName+"SuperTrendFilterDirection",OBJPROP_COLOR,(UseSuperTrend?Panel5LabelColor:clrBeige));

   ObjectCreate(ChartID(),ObjName+"SuperTrendCurrentDirection",OBJ_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"SuperTrendCurrentDirection",OBJPROP_TEXT,"SuperTrend Current: "+(SuperTrendCurrentDirection==OP_BUY?"Buy":SuperTrendCurrentDirection==OP_SELL?"Sell":"NoValue"));
   ObjectSetInteger(ChartID(),ObjName+"SuperTrendCurrentDirection",OBJPROP_CORNER,CORNER_LEFT_LOWER);
   ObjectSetInteger(ChartID(),ObjName+"SuperTrendCurrentDirection",OBJPROP_XDISTANCE,xDistance);
   ObjectSetInteger(ChartID(),ObjName+"SuperTrendCurrentDirection",OBJPROP_YDISTANCE,37);
   ObjectSetString(ChartID(),ObjName+"SuperTrendCurrentDirection",OBJPROP_FONT,"Helvetica");
   ObjectSetInteger(ChartID(),ObjName+"SuperTrendCurrentDirection",OBJPROP_FONTSIZE,11);
   ObjectSetInteger(ChartID(),ObjName+"SuperTrendCurrentDirection",OBJPROP_ANCHOR,ANCHOR_LEFT);
   ObjectSetInteger(ChartID(),ObjName+"SuperTrendCurrentDirection",OBJPROP_COLOR,Panel5LabelColor);

//Main Label
   ObjectCreate(ChartID(),ObjName+"HelmiFX",OBJ_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"HelmiFX",OBJPROP_TEXT,"HelmiFX");
   ObjectSetInteger(ChartID(),ObjName+"HelmiFX",OBJPROP_CORNER,CORNER_RIGHT_UPPER);
   ObjectSetInteger(ChartID(),ObjName+"HelmiFX",OBJPROP_XDISTANCE,235);
   ObjectSetInteger(ChartID(),ObjName+"HelmiFX",OBJPROP_YDISTANCE,555);
   ObjectSetString(ChartID(),ObjName+"HelmiFX",OBJPROP_FONT,"Calibri");
   ObjectSetInteger(ChartID(),ObjName+"HelmiFX",OBJPROP_FONTSIZE,48);
   ObjectSetInteger(ChartID(),ObjName+"HelmiFX",OBJPROP_ANCHOR,ANCHOR_LEFT);
   ObjectSetInteger(ChartID(),ObjName+"HelmiFX",OBJPROP_COLOR,clrDarkTurquoise);

  }
//+------------------------------------------------------------------+
// End of DrawChartInfo()                                            |
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
// Start of UpdateChartInfo()                                        |
//+------------------------------------------------------------------+
void UpdateChartInfo()
  {

   ObjectSetString(ChartID(),ObjName+"Symbol",OBJPROP_TEXT,Symbol()+"  "+spreadValue);
   ObjectSetString(ChartID(),ObjName+"Balance",OBJPROP_TEXT,"Balance: "+DoubleToStr(AccountBalance(),2));
   ObjectSetString(ChartID(),ObjName+"Equity",OBJPROP_TEXT,"Equity: "+DoubleToStr(AccountEquity(),2));
   ObjectSetString(ChartID(),ObjName+"TotalLots",OBJPROP_TEXT,"Total Lots: "+DoubleToStr(totalLots,2));
   ObjectSetString(ChartID(),ObjName+"TotalProfit",OBJPROP_TEXT,"Total Profit "+DoubleToStr(totalProfit,2));
   ObjectSetString(ChartID(),ObjName+"CycleLots",OBJPROP_TEXT,"Cycle Lots: 'B' "+DoubleToStr(buyCycleLots,2)+" / 'S' "+DoubleToStr(sellCycleLots,2));
   ObjectSetString(ChartID(),ObjName+"CurrentLots",OBJPROP_TEXT,"Current Lots: 'B'  "+DoubleToStr(buyCurrentLots,2)+" / 'S'  "+DoubleToStr(sellCurrentLots,2));
   ObjectSetString(ChartID(),ObjName+"TotalCycleLots",OBJPROP_TEXT,"Total Cycles Lots: "+DoubleToStr(totalCycleLots,2));
   ObjectSetString(ChartID(),ObjName+"CycleProfit",OBJPROP_TEXT,"Cycle Profit: 'B' "+DoubleToStr(buyCycleProfit,2)+" / 'S' "+DoubleToStr(sellCycleProfit,2));
   ObjectSetString(ChartID(),ObjName+"CurrentProfit",OBJPROP_TEXT,"Current Profit: 'B'  "+DoubleToStr(buyCurrentProfit,2)+" / 'S'  "+DoubleToStr(sellCurrentProfit,2));
   ObjectSetString(ChartID(),ObjName+"TotalCycleProfit",OBJPROP_TEXT,"Total Cycles Profit: "+DoubleToStr(totalCycleProfit,2));

   ObjectSetString(ChartID(),ObjName+"SuperTrendFilterDirection",OBJPROP_TEXT,"SuperTrend Filter: "+(SuperTrendDirection==OP_BUY?"Buy":SuperTrendDirection==OP_SELL?"Sell":"NoValue"));
   ObjectSetString(ChartID(),ObjName+"SuperTrendCurrentDirection",OBJPROP_TEXT,"SuperTrend Current: "+(SuperTrendCurrentDirection==OP_BUY?"Buy":SuperTrendCurrentDirection==OP_SELL?"Sell":"NoValue"));

   ObjectSetString(ChartID(),ObjName+"NextBuyCycleLot",OBJPROP_TEXT,"Next Buy Lot:"+CalculateLot(OP_BUY));
   ObjectSetString(ChartID(),ObjName+"NextSellCycleLot",OBJPROP_TEXT,"Next Sell Lot:"+CalculateLot(OP_SELL));
   ObjectSetString(ChartID(),ObjName+"NextBuyCyclePipStepDistance",OBJPROP_TEXT,"Next Buy PipStep:"+CalculatePipStep(OP_BUY));
   ObjectSetString(ChartID(),ObjName+"NextSellCyclePipStepDistance",OBJPROP_TEXT,"Next Sell PipStep:"+CalculatePipStep(OP_SELL));

   ObjectSetString(ChartID(),ObjName+"OrdersNumber",OBJPROP_TEXT,"Orders #: 'B' "+IntegerToString(CountPosOrders(BuyCycleMagicNumber,OP_BUY),2)+" / 'S' "+IntegerToString(CountPosOrders(SellCycleMagicNumber,OP_SELL),2));

   ObjectSetString(ChartID(),ObjName+"Margin",OBJPROP_TEXT,"Margin: "+DoubleToStr(AccountInfoDouble(ACCOUNT_MARGIN),2)+"/ Lvl: "+DoubleToStr(AccountInfoDouble(ACCOUNT_MARGIN_LEVEL),2));

//Update the Edits values
   ObjectSetString(ChartID(),ObjName+"LiveBuyCycleLotInput",OBJPROP_TEXT,BuyCycleLot);
   ObjectSetString(ChartID(),ObjName+"LiveSellCycleLotInput",OBJPROP_TEXT,SellCycleLot);
   ObjectSetString(ChartID(),ObjName+"LiveBuyCycleTakeProfitInput",OBJPROP_TEXT,BuyCycleTakeProfit);
   ObjectSetString(ChartID(),ObjName+"LiveSellCycleTakeProfitInput",OBJPROP_TEXT,SellCycleTakeProfit);
   ObjectSetString(ChartID(),ObjName+"LiveBuyCyclePipStepInput",OBJPROP_TEXT,BuyCyclePipStep);
   ObjectSetString(ChartID(),ObjName+"LiveSellCyclePipStepInput",OBJPROP_TEXT,SellCyclePipStep);

  }
//+------------------------------------------------------------------+
// End of UpdateChartInfo()                                          |
//+------------------------------------------------------------------+


//+------------------------------------------------------------------+
// Start of DrawEdit()                                               |
//+------------------------------------------------------------------+

void DrawEdit(string name,color clr,int x,int y,ENUM_BASE_CORNER c,string tooltip,string text,int xsize,int ysize,int fontSize,color textColor,color borderColor)
  {
//---

   ObjectDelete(name);
   if(!ObjectCreate(ChartID(),name,OBJ_EDIT,0,0,0))
     {
      Print(__FUNCTION__,
            ": failed to create the Edit! Error code = ",GetLastError()+" ****");
      return;
     }
   ObjectSet(name,OBJPROP_BGCOLOR,clr);
   ObjectSetString(0,name,OBJPROP_TEXT,text);
   ObjectSet(name,OBJPROP_COLOR,(MQLInfoInteger(MQL_TESTER)?clrBeige:textColor));
   ObjectSet(name,OBJPROP_BORDER_COLOR,White);
   ObjectSetString(0,name,OBJPROP_FONT,"Helvetica");
   ObjectSet(name,OBJPROP_FONTSIZE,fontSize);
   ObjectSet(name,OBJPROP_XSIZE,xsize);
   ObjectSet(name,OBJPROP_YSIZE,ysize);
   ObjectSet(name,OBJPROP_CORNER,c);
   ObjectSet(name,OBJPROP_XDISTANCE,xsize);
   ObjectSet(name,OBJPROP_YDISTANCE,y);
   ObjectSet(name,OBJPROP_WIDTH,1);
   ObjectSetInteger(ChartID(),name,OBJPROP_ANCHOR,ANCHOR_LEFT);
   ObjectSetInteger(ChartID(),name,OBJPROP_BACK,false);
   ObjectSetInteger(ChartID(),name,OBJPROP_SELECTABLE,false);
   ObjectSetInteger(ChartID(),name,OBJPROP_SELECTED,false);
   ObjectSetInteger(ChartID(),name,OBJPROP_HIDDEN,false);
   ObjectSetInteger(ChartID(),name,OBJPROP_BORDER_COLOR,borderColor);
   ObjectSetInteger(ChartID(),name,OBJPROP_READONLY,MQLInfoInteger(MQL_TESTER));

//---
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
// End of DrawEdit()                                                 |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//|                         *** /Chart ***                           |
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                         *** SuperTrend ***                       |
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
// Start of SuperTrendSignal()                                       |
//+------------------------------------------------------------------+
int SuperTrendSignal()
  {
   int output=-1;

   if(supertrendTrendUp_1!=EMPTY_VALUE && supertrendTrendUp_3==EMPTY_VALUE && supertrendTrendDown_1==EMPTY_VALUE)
     {
      output=0; // Buy Signal
     }
   else if(supertrendTrendDown_1!=EMPTY_VALUE && supertrendTrendDown_3==EMPTY_VALUE && supertrendTrendUp_1==EMPTY_VALUE)
     {
      output=1; // Sell Signal
     }

   return (output);
  }
//+------------------------------------------------------------------+
// End of SuperTrendSignal()                                         |
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
// Start of SuperTrendCurrentSignal()                           		|
//+------------------------------------------------------------------+
int SuperTrendCurrentSignal()
  {
   int output=-1;

   if(supertrendCurrentTrendUp_1!=EMPTY_VALUE && supertrendCurrentTrendUp_3==EMPTY_VALUE && supertrendCurrentTrendDown_1==EMPTY_VALUE)
     {
      output=0; // Buy Signal
     }
   else if(supertrendCurrentTrendDown_1!=EMPTY_VALUE && supertrendTrendDown_3==EMPTY_VALUE && supertrendCurrentTrendUp_1==EMPTY_VALUE)
     {
      output=1; // Sell Signal
     }

   return (output);
  }
//+------------------------------------------------------------------+
// End of SuperTrendCurrentSignal()                                  |
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//| Start of GetFirstSuperTrendSignal()                              |
//+------------------------------------------------------------------+

void GetFirstSuperTrendSignal()
  {
   for(int i=3;i<100;i++)
     {
      supertrendTrendUp_1=iCustom(Symbol(),Supertrend_Timeframe,"super-trend",Nbr_Periods,Multiplier,0,i-2);
      supertrendTrendDown_1=iCustom(Symbol(),Supertrend_Timeframe,"super-trend",Nbr_Periods,Multiplier,1,i-2);
      supertrendTrendUp_3=iCustom(Symbol(),Supertrend_Timeframe,"super-trend",Nbr_Periods,Multiplier,0,i);
      supertrendTrendDown_3=iCustom(Symbol(),Supertrend_Timeframe,"super-trend",Nbr_Periods,Multiplier,1,i);
      SuperTrendTriggered=SuperTrendSignal();
      if(SuperTrendTriggered!=-1)
        {
         SuperTrendDirection=SuperTrendTriggered;
         SuperTrendDirectionChange=iTime(Symbol(),Supertrend_Timeframe,i-3);
         if(SuperTrendJournaling)Print("** SuperTrendJournaling: SuperTrend Changed to "+(SuperTrendDirection==OP_SELL?"Sell":"Buy")+" on: "+TimeToString(SuperTrendDirectionChange,TIME_DATE|TIME_MINUTES)+" **");
         break;
        }
     }
  }
//+------------------------------------------------------------------+
//| End of GetFirstSuperTrendSignal()                                |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//| Start of GetFirstCurrentSuperTrendSignal()                       |
//+------------------------------------------------------------------+

void GetFirstCurrentSuperTrendSignal()
  {
   for(int i=3;i<100;i++)
     {
      supertrendCurrentTrendUp_1=iCustom(Symbol(),Period(),"super-trend",Nbr_Periods,Multiplier,0,i-2);
      supertrendCurrentTrendDown_1=iCustom(Symbol(),Period(),"super-trend",Nbr_Periods,Multiplier,1,i-2);
      supertrendCurrentTrendUp_3=iCustom(Symbol(),Period(),"super-trend",Nbr_Periods,Multiplier,0,i);
      supertrendCurrentTrendDown_3=iCustom(Symbol(),Period(),"super-trend",Nbr_Periods,Multiplier,1,i);
      SuperTrendCurrentTriggered=SuperTrendCurrentSignal();
      if(SuperTrendCurrentTriggered!=-1)
        {
         SuperTrendCurrentDirection=SuperTrendTriggered;
         SuperTrendCurrentDirectionChange=iTime(Symbol(),Period(),i-3);
         if(SuperTrendJournaling)Print("** SuperTrendJournaling: SuperTrendCurrent Changed to "+(SuperTrendCurrentDirection==2?"Sell":"Buy")+" on: "+TimeToString(SuperTrendCurrentDirectionChange,TIME_DATE|TIME_MINUTES)+" **");
         break;
        }
     }
  }
//+------------------------------------------------------------------+
//| End of GetFirstCurrentSuperTrendSignal()                         |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//|                         *** /SuperTrend ***                      |
//+------------------------------------------------------------------+
