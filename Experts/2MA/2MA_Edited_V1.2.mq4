//+-----------------------------------------------------------------------+
//|                                         2 Moving Average _ Edited.mq4 |
//|                              Copyright 2019, MetaQuotes Software Corp.|
//|                                             https://www.mql5.com      |
//|Abd Loulou                                                             |
//+-----------------------------------------------------------------------+


#property copyright "Copyright 2019 HelmiFX."
#property link      "www.helmifx.com"
#property strict
#property description "2 Moving Average _ Edited"

#property description "Find us on  :\n"
#property description "Facbook : HelmiFx"
#property description "Twitter : @HelmiForex"  
#property description "Youtube : HelmiForex" 

#property icon "photo.ico"
#resource "picture.bmp"  

#include <Arrays\ArrayInt.mqh>
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
enum ENUM_STOPLOSSMETHOD
  {
   Swing=1,// Swing 
   SLFixedPips=2//Fixed Pips
  };
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
enum ENUM_TAKEPROFITMOETHOD
  {
   MultiplyStopLoss=0,// Multiply Stop Loss
   TPFixedPips=1 // Fixed Pips
  };
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
enum ENUM_LOTMETHOD
  {
   UsePositionSizing=0,// Use Position Sizing 
   UseFixedLots=1,// Use Fixed Lots 
  };
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
enum ENUM_ORDERTYPE
  {
   Buy=0,// Buy only
   Sell=1,// Sell only
   BuyAndSell=2,// Buy and Sell

  };

//+------------------------------------------------------------------+
//| Setup                                               
//+------------------------------------------------------------------+
extern ENUM_ORDERTYPE OrdersType=2; // Orders Type
extern string  Header1="**************************************************************************************"; // ----------Trading Rules -----------
extern string  Sep1="***************************************"; // ----------2 MA Rules -----------
extern bool Use2MA = true;
extern int     FastMA=5;
extern int     SlowMA=12;
extern ENUM_MA_METHOD MA2Type=0;
extern ENUM_APPLIED_PRICE MA2AppliedPrice=0;
extern string  Sep11="***************************************"; // ----------Small MA Rules -----------
extern bool UseSmallMA= false;
extern int     SmallMA=10;
extern ENUM_MA_METHOD MA1Type=0;
extern ENUM_TIMEFRAMES SmallMATimeFrame=0;
extern ENUM_APPLIED_PRICE MA1AppliedPrice=0;
extern string  Sep2="***************************************"; // ----------Big MA Rules -----------
extern bool UseBigMA=true; // Use Big MA?
extern int     BigMA=200;
extern ENUM_MA_METHOD BigMAType=0;
extern ENUM_TIMEFRAMES BigMATimeFrame=0;
extern ENUM_APPLIED_PRICE BigMAAppliedPrice=0;
extern string  Header2="**************************************************************************************"; // ---------- Wait Bars Rules -----------
extern string  Disc2="***************************************";  // In case of a Loss, Wait Bars will be activated.
extern bool  UseWaitBars=true; // Use Wait Bars?
extern int  NumberOfWaitBars=10; // Number of Wait Bars after a Loss
extern string  Header3="**************************************************************************************"; // ---------- Working Times Settings-----------
extern bool    useWorkingTimer=false;
extern string  Note200="Please use 24Hour format, between 0 and 24"; // --- 
extern int     Start=0;
extern int     End=6;
extern string  Header4="**************************************************************************************"; // ---------- Reflex Points Settings-----------
extern bool    UseReflexPoints=true;
extern double NumberOfReflexPoints=5; // Reflex points in Pips
extern string  Header5="**************************************************************************************"; //----------Lots -----------
extern ENUM_LOTMETHOD LotMethod=0;
extern string  Sep3="***************************************"; //  ------- Fixed Position is Used
extern double  Lot=0.01;
extern string  Sep4="***************************************"; //  ------- Position Sizing is Used
extern double  Risk=0.5;         //Risk in Percentage
extern double  incrementFactor=0.1;  // increment factor on risk after each position.
extern string  Sep5="********************************************************"; // In Case of Loss 
extern string  Sep6="***************************************"; //  ------- Multiply Lots 
extern bool    UseMultiplyLot=false; // Use Multiply Lot in case of Loss?
extern double  LotMultiplier=2;
extern string  Sep7="***************************************"; //  ------- Following Orders
extern bool  UseFollowingOrders=true; // Use Following Orders In case of a Loss?
extern int  NumberOfTheFollowingOrders=5; // Number of the following Orders
extern string  LotsOfTheFollowingOrders="0.2,0.3,0.4,0.5,0.6"; // Number of the following Orders ( Separated by ',')
extern string  Header6="**************************************************************************************"; //----------TP & SL Settings-----------
extern ENUM_STOPLOSSMETHOD   StopLossMethod=1;
extern string  Note3="*******************************************"; // if Swing Stop Loss is used
extern string  SwingStopLossExplaination="Explaination : Setting Stop Loss accourding to the last swing.";
extern int     SwingCandlesCount=12;       // Number of bars of the Swing.
extern string  Note4="*******************************************"; // if Fixed Stop Loss is used
extern double  FixedStopLoss=25;//Fixed Stop Loss; Hard Stop in Pips.
extern string  Note5="*******************************************"; // --- 
extern double  ClosingOffset=3;       //StopLoss Closing Offset in Pips.
extern string  Note6="*******************************************"; // --- 
extern ENUM_TAKEPROFITMOETHOD TakeProfitMethod=0;
extern string  Note7="*******************************************";// if Multiply Stop Loss is used
extern string  TakeProfitMultiplyStopLossExplaination=""; // Explaination : Take Profit is multiply the Stop Loss
extern double  TakeProfitMultiplier=2;
extern string  Note8="*******************************************";  // if Fixed TakeProfit is used
extern double  FixedTakeProfit=30; // Fixed Take Profit; Hard Take Profit in Pips.

extern string  Header7="**************************************************************************************"; // ----------Trailing Stop Settings-----------
extern bool    UseTrailingStops=false;
extern double  TrailingStopOffset=30;

extern string  Header8="**************************************************************************************"; // ----------Breakeven Settings-----------
extern bool    UseBreakeven=false;
extern double  BreakevenOffset=25;
extern double  BreakevenBuffer=3;
extern bool    AddSpread=true;

extern bool    UseWaitBarsOnBreakeven=false;
extern bool    UseMultiplyLotOnBreakeven=false;

extern string  Header9="**************************************************************************************";; // ----------Set Max Loss Limit-----------
extern bool    IsLossLimitActivated=false;
extern double  LossLimit=0;
extern bool    IsProfitLimitActivated=false;
extern double  ProfitLimit=0;

extern string  Header10="**************************************************************************************"; // ----------Chart Info -----------
extern int     xDistance=1; //Chart info distance from top right corner

extern string  Header11="**************************************************************************************"; // ----------EA General -----------
extern int     MagicNumber=656668;
extern int     Slippage=3;
extern bool    OnJournaling=true; // Add EA updates in the Journal Tab

string  InternalHeader1="----------Errors Handling Settings-----------";

int     RetryInterval=100; // Pause Time before next retry (in milliseconds)
int     MaxRetriesPerTick=10;

string  InternalHeader2="----------Service Variables-----------";

//+------------------------------------------------------------------+
//|General Variables                                                                 |
//+------------------------------------------------------------------+

double StopBuy,StopSell;
double TakeBuy,TakeSell;

int P,YenPairAdjustFactor;

bool entrySellLock=false;
bool entryBuyLock=false;

CArrayInt Orders;

int OrderNumber;

double TrailingStopList[][2];

double BreakevenList[][2];

double positonLotMultiplier=1;

datetime today;
datetime StartofExpert;

string FollowingOrdersLots[];
bool ActivateFollowingOrders=false;
int FollowingOrdersIndex;

bool ActivateWaitBars=false;

int BarsCounter=0;

int Current_Time;

//+------------------------------------------------------------------+
//|RSI Variables                                                                  |
//+------------------------------------------------------------------+

double MyFastMA_1,MySlowMA_1,MyBigMA_1,MySmallMA_1;

int MACrossTriggered,MACurrentDirection,MALastDirection;
bool MAFirstTime=true;
int MADirection;

int SmallMATriggered;

//+------------------------------------------------------------------+
//|Swing Variables                                                                  |
//+------------------------------------------------------------------+

double SwingCandlesHighestHigh;         //	Stores the high values of candles specified in 'SwingCandlesCount'
double SwingCandlesLowestLow;         //	Stores the low values of candles specified in 'SwingCandlesCount'

//+-----------------------------------------------------------------+
//|Chart Display Variables                                                                  |
//+------------------------------------------------------------------+
double buyLastClosedProfit=0;
double sellLastClosedProfit=0;
double cycleProfit; //Get All Opened Profit
double buyCycleProfit;
double sellCycleProfit;
double cycleLots;
double buyCycleLots;    //Get All Buy  Opened Lots
double sellCycleLots;    //Get All Sell Opened Lots
double netProfit;    // All the profit of all opened positions by the same MagicNumber
string ObjName=IntegerToString(ChartID(),0,' ');   //The global object name 
double netProfitSince;
double lotsSince;
double lotsTotal;
double pipsTotal;

//+------------------------------------------------------------------+
//|//Pip Display Variables                                           |
//+------------------------------------------------------------------+
double pipValue;
double buyPipValue;
double sellPipValue;
double pipValue1Lot;
double spreadValue;

double profitSinceLastCycle;
//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
int init()
  {

   Comment("Copyright 2019 HelmiFX.");

   if(LotsOfTheFollowingOrders!="")
     {
      StringSplit(LotsOfTheFollowingOrders,',',FollowingOrdersLots); // Splitting the multiplication sequene of the lot
      if(UseFollowingOrders && ArraySize(FollowingOrdersLots)!=NumberOfTheFollowingOrders)
        {
         MessageBox("Number of Following Orders doesnt match the Number of Lots","ERROR");
         Print("Number of Following Orders doesnt match the Number of Lots");
         return(INIT_FAILED);
        }
     }

   DrawChartInfo();            //Drawing Chart Info
   P=GetP(); // To account for 5 digit brokers. Used to convert pips to decimal place
   YenPairAdjustFactor=GetYenAdjustFactor(); // Adjust for YenPair
   ChartSettings();

   if(UseTrailingStops) ArrayResize(TrailingStopList,1000,0);
   if(UseBreakeven) ArrayResize(BreakevenList,1000,0);

   return(INIT_SUCCEEDED);


  }
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+

void OnDeinit(const int reason)
  {
//---
//---
   if(reason!=5 && reason!=3)
     {
      ObjectsDeleteAll(0,0,OBJ_LABEL);
      ObjectsDeleteAll(0,0,OBJ_RECTANGLE_LABEL);
      ObjectsDeleteAll(0,0,OBJ_ARROW);
      ObjectsDeleteAll(0,0,OBJ_HLINE);

      ObjectsDeleteAll(ChartID(),0);
     }
  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
int start()
  {
   cycleProfit=GetCycleProfit(0)+GetCycleProfit(1);               //Set Cycle Profits onto the chart.
   buyCycleProfit=GetCycleProfit(0);
   sellCycleProfit=GetCycleProfit(1);
   buyCycleLots=GetCycleLots(0);                //Set Cycle Lots onto the chart.
   sellCycleLots=GetCycleLots(1);
   cycleLots=GetCycleLots(0)+GetCycleLots(1);
   netProfitSince=GetNetProfitSince(today,OP_BUY)+GetNetProfitSince(today,OP_SELL);
   lotsSince=GetLotsSince(today,OP_BUY)+GetLotsSince(today,OP_SELL);
   lotsTotal=GetLotsSince(StartofExpert,OP_BUY)+GetLotsSince(StartofExpert,OP_SELL);
   pipValue=(((MarketInfo(Symbol(),MODE_TICKVALUE)*Point*P)/MarketInfo(Symbol(),MODE_TICKSIZE))*cycleLots);// pip value of 1 lot
   buyPipValue=(((MarketInfo(Symbol(),MODE_TICKVALUE)*Point*P)/MarketInfo(Symbol(),MODE_TICKSIZE))*buyCycleLots);// pip value of 1 lot
   sellPipValue=(((MarketInfo(Symbol(),MODE_TICKVALUE)*Point*P)/MarketInfo(Symbol(),MODE_TICKSIZE))*sellCycleLots);// pip value of 1 lot
   pipValue1Lot=(((MarketInfo(Symbol(),MODE_TICKVALUE)*Point*P)/MarketInfo(Symbol(),MODE_TICKSIZE))*1);// pip value of 1 lot
   netProfit=GetNetProfitSince(StartofExpert,OP_BUY)+GetNetProfitSince(StartofExpert,OP_SELL);
   buyLastClosedProfit = GetClosedProfitSince(StartofExpert,OP_BUY);
   sellLastClosedProfit= GetClosedProfitSince(StartofExpert,OP_SELL);
   spreadValue=MarketInfo(Symbol(),MODE_SPREAD)/P;

   if(IsLossLimitBreached(IsLossLimitActivated,LossLimit,OnJournaling)==true || IsProfitLimitBreached(IsProfitLimitActivated,ProfitLimit,OnJournaling)==true)
     {
      return 0;
     }

   UpdateChartInfo();
   CheckSLTPExit();

//----------Entry & Exit Variables-----------

   MyFastMA_1=iMA(Symbol(),Period(),FastMA,0,MA2Type,MA2AppliedPrice,0);
   MySlowMA_1=iMA(Symbol(),Period(),SlowMA,0,MA2Type,MA2AppliedPrice,0);
   MyBigMA_1=iMA(Symbol(),BigMATimeFrame,BigMA,0,BigMAType,BigMAAppliedPrice,0);
   MySmallMA_1=iMA(Symbol(),SmallMATimeFrame,SmallMA,0,MA1Type,MA1AppliedPrice,0);

//----------TP & SL Variables-----------

   SwingCandlesHighestHigh=High[iHighest(Symbol(),Period(),MODE_HIGH,SwingCandlesCount,1)];
   SwingCandlesLowestLow=Low[iLowest(Symbol(),Period(),MODE_LOW,SwingCandlesCount,1)];

   StopBuy=GetBuyStopLoss(StopLossMethod,FixedStopLoss,P);
   StopSell=GetSellStopLoss(StopLossMethod,FixedStopLoss,P);

   TakeBuy=GetBuyTakeProfit(TakeProfitMethod,TakeProfitMultiplier,FixedTakeProfit,StopBuy);
   TakeSell=GetSellTakeProfit(TakeProfitMethod,TakeProfitMultiplier,FixedTakeProfit,StopSell);

   if(UseTrailingStops)
     {
      UpdateTrailingList(OnJournaling,RetryInterval,MagicNumber);
      ReviewTrailingStop(OnJournaling,TrailingStopOffset,RetryInterval,MagicNumber,P);
     }

   if(UseBreakeven)
     {
      UpdateBreakevenList(OnJournaling,RetryInterval,MagicNumber);
      ReviewBreakeven(OnJournaling,BreakevenOffset,RetryInterval,MagicNumber,P);
     }

//----------Bars Rules-----------

   if(useWorkingTimer)
     {
      if(!isItTime()) return(0);
     }

   if(!UseReflexPoints)
     {
      if(!isNewBar())
        {
         return(0);
        }

      if(ActivateWaitBars)
        {
         BarsCounter++;
        }

      if(Use2MA)
        {
         MACrossTriggered=MACross(MyFastMA_1,MySlowMA_1);
         MADirection=MACurrentDirection;
        }

      if(UseSmallMA)
        {
         if(MySmallMA_1<=iClose(Symbol(),SmallMATimeFrame,0)) SmallMATriggered=1;
         else if(MySmallMA_1>=iClose(Symbol(),SmallMATimeFrame,0)) SmallMATriggered=2;
         else SmallMATriggered=0;
        }
     }
   else
     {
      if(isNewBar())
        {
         if(ActivateWaitBars)
           {
            BarsCounter++;
           }
         if(Use2MA)
           {
            MACrossTriggered=MACross(MyFastMA_1,MySlowMA_1);
            MADirection=MACurrentDirection;
           }

         if(UseSmallMA)
           {
            if(MySmallMA_1<=iClose(Symbol(),SmallMATimeFrame,0)) SmallMATriggered=1;
            else if(MySmallMA_1>=iClose(Symbol(),SmallMATimeFrame,0)) SmallMATriggered=2;
            else SmallMATriggered=0;
           }
        }
     }

//----------Entry Rules (Market) -----------

   if(CountPosOrders(MagicNumber,OP_BUY)+CountPosOrders(MagicNumber,OP_SELL)<1)
     {
      if(EntrySignal(MACrossTriggered,SmallMATriggered)==1 && OrdersType!=1) // Buy
        { // Open Long Positions
         OrderNumber=OpenPositionMarket(OP_BUY,GetLot(LotMethod,ActivateFollowingOrders,UseMultiplyLot,StopBuy),StopBuy,TakeBuy,MagicNumber,Slippage,OnJournaling,P,MaxRetriesPerTick);
         if(UseTrailingStops) SetTrailingStop(OnJournaling,RetryInterval,MagicNumber,P,OrderNumber);
         if(UseBreakeven) SetBreakeven(OnJournaling,RetryInterval,MagicNumber,P,OrderNumber);
         Orders.Add(OrderNumber);
         Risk+=incrementFactor;
        }

      if(EntrySignal(MACrossTriggered,SmallMATriggered)==2 && OrdersType!=0) // Sell
        { // Open Short Positions
         OrderNumber=OpenPositionMarket(OP_SELL,GetLot(LotMethod,ActivateFollowingOrders,UseMultiplyLot,StopSell),StopSell,TakeSell,MagicNumber,Slippage,OnJournaling,P,MaxRetriesPerTick);
         if(UseTrailingStops) SetTrailingStop(OnJournaling,RetryInterval,MagicNumber,P,OrderNumber);
         if(UseBreakeven) SetBreakeven(OnJournaling,RetryInterval,MagicNumber,P,OrderNumber);
         Orders.Add(OrderNumber);
         Risk+=incrementFactor;
        }
     }
   return (0);
  }
//+------------------------------------------------------------------+   
//|End of Start()                                                        |
//+------------------------------------------------------------------+


//+------------------------------------------------------------------+
// Start of EntrySignal()                                    			|
//+------------------------------------------------------------------+
int EntrySignal(int MACrossOccured,int SmallMAOccured)
  {
// Type: Customisable 
// Modify this function to suit your trading robot

// This function checks for entry signals

   int output=0;

   if(MACrossOccured==1)
     {
      if(((MyBigMA_1<=iClose(Symbol(),BigMATimeFrame,0) && UseBigMA) || !UseBigMA))
        {
         if(((BarsCounter>=NumberOfWaitBars && UseWaitBars && ActivateWaitBars) || !UseWaitBars || !ActivateWaitBars))
           {
            if(((Ask<=Close[1]-(NumberOfReflexPoints*Point*P) && UseReflexPoints) || !UseReflexPoints))
              {
               output=1;
               if(OnJournaling) Print("Entered on a cross UP");
              }
            else
              {
               if(OnJournaling)Print("There was a cross UP, But Reflex Points didnt approve.");
              }
           }
         else
           {
            if(OnJournaling) Print("There was a cross UP, But Wait Bars didnt approve.");
           }
        }
      else
        {
         if(OnJournaling) Print("There was a cross UP, But BigMA didnt approve.");
        }
     }

   else if(SmallMAOccured==1)
     {
      if(((MyBigMA_1<=iClose(Symbol(),BigMATimeFrame,0) && UseBigMA) || !UseBigMA))
        {
         if(((BarsCounter>=NumberOfWaitBars && UseWaitBars && ActivateWaitBars) || !UseWaitBars || !ActivateWaitBars))
           {
            if(((Ask<=Close[1]-(NumberOfReflexPoints*Point*P) && UseReflexPoints) || !UseReflexPoints))
              {
               output=1;
               if(OnJournaling) Print("Entered on a Small MA UP signal");

              }
            else
              {
               if(OnJournaling)Print("There was a signal UP on Small MA, But Reflex Points didnt approve.");
              }
           }
         else
           {
            if(OnJournaling) Print("There was a signal UP on Small MA, But Wait Bars didnt approve.");
           }
        }
      else
        {
         if(OnJournaling) Print("There was a signal UP on Small MA, But BigMA didnt approve.");
        }
     }

   else if(MACrossOccured==2)
     {
      if(((MyBigMA_1>=iClose(Symbol(),BigMATimeFrame,0) && UseBigMA) || !UseBigMA))
        {
         if(((BarsCounter>=NumberOfWaitBars && UseWaitBars && ActivateWaitBars) || !UseWaitBars || !ActivateWaitBars))
           {
            if(((Bid>=Close[1]+(NumberOfReflexPoints*Point*P) && UseReflexPoints) || !UseReflexPoints))
              {
               output=2;
               if(OnJournaling) Print("Entered on a cross DOWN");
              }
            else
              {
               if(OnJournaling)Print("There was a cross DOWN, But Reflex Points didnt approve.");
              }
           }
         else
           {
            if(OnJournaling) Print("There was a cross DOWN, But Wait Bars didnt approve.");
           }
        }
      else
        {
         if(OnJournaling) Print("There was a cross DOWN, But BigMA didnt approve.");
        }
     }

   else if(SmallMAOccured==2)
     {
      if(((MyBigMA_1>=iClose(Symbol(),BigMATimeFrame,0) && UseBigMA) || !UseBigMA))
        {
         if(((BarsCounter>=NumberOfWaitBars && UseWaitBars && ActivateWaitBars) || !UseWaitBars || !ActivateWaitBars))
           {
            if(((Bid>=Close[1]+(NumberOfReflexPoints*Point*P) && UseReflexPoints) || !UseReflexPoints))
              {
               output=2;
               if(OnJournaling) Print("Entered on a Small MA DOWN signal");

              }
            else
              {
               if(OnJournaling)Print("There was a signal DOWN on Small MA, But Reflex Points didnt approve.");
              }
           }
         else
           {
            if(OnJournaling) Print("There was a signal DOWN on Small MA, But Wait Bars didnt approve.");
           }
        }
      else
        {
         if(OnJournaling) Print("There was a signal DOWN on Small MA, But BigMA didnt approve.");
        }
     }

   return (output);
  }
//+------------------------------------------------------------------+
// End of EntrySignal()                                              |
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
// Start of GetLot()                                       				|
//+------------------------------------------------------------------+
double GetLot(ENUM_LOTMETHOD LotMethodValue,bool isFollowingOrdersActivated,bool isMultiplyLot,double stopLoss)
  {
// Type: Customisable 
// Modify this function to suit your trading robot

// This is our sizing algorithm

   double output;

   if(LotMethodValue==0) // Position Sizing
     {
      if(stopLoss==0)
        {
         output=Lot;
        }
      else
        {
         double Spread=MarketInfo(Symbol(),MODE_SPREAD)/P;
         output=((Risk*AccountEquity()/100)/(pipValue1Lot *(stopLoss+Spread)));
        }
     }
   else
     {
      output=Lot;
     }

   if(UseFollowingOrders && isFollowingOrdersActivated)
     {
      if(FollowingOrdersIndex<NumberOfTheFollowingOrders)
        {
         output=StringToDouble(FollowingOrdersLots[FollowingOrdersIndex]);
         Print("Before"+FollowingOrdersIndex);
         FollowingOrdersIndex++;
         Print("After"+FollowingOrdersIndex);
        }
      else
        {
         FollowingOrdersIndex=0;
         ActivateFollowingOrders=false;
         output=Lot;
        }
     }

   if(isMultiplyLot)
     {
      output*=positonLotMultiplier;
     }

   output=NormalizeDouble(output,2); // Round to 2 decimal place
   return(output);
  }
//+------------------------------------------------------------------+
// End of GetLot()                                                   |
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
// Start of CheckLot()                                       				|
//+------------------------------------------------------------------+

double CheckLot(double lot,bool Journaling)
  {
// Type: Fixed Template 
// Do not edit unless you know what you're doing

// This function checks if our Lots to be trade satisfies any broker limitations

   double LotToOpen=0;
   LotToOpen=NormalizeDouble(lot,2);
   LotToOpen=MathFloor(LotToOpen/MarketInfo(Symbol(),MODE_LOTSTEP))*MarketInfo(Symbol(),MODE_LOTSTEP);

   if(LotToOpen<MarketInfo(Symbol(),MODE_MINLOT))LotToOpen=MarketInfo(Symbol(),MODE_MINLOT);
   if(LotToOpen>MarketInfo(Symbol(),MODE_MAXLOT))LotToOpen=MarketInfo(Symbol(),MODE_MAXLOT);
   LotToOpen=NormalizeDouble(LotToOpen,2);

   if(Journaling && LotToOpen!=lot)Print("EA Journaling: Trading Lot has been changed by CheckLot function. Requested lot: "+DoubleToString(lot)+". Lot to open: "+DoubleToString(LotToOpen));

   return(LotToOpen);
  }
//+------------------------------------------------------------------+
//| End of CheckLot()
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//| Start of GetCycleLots()                                                             |
//+------------------------------------------------------------------+
double GetCycleLots(int type)
  {

// Type: Customisable 
// Modify this function to suit your trading robot

// this function returns the net profit of the opened orders 

   double Lots=0;

   for(int i=0; i<OrdersTotal(); i++)
     {
      if(OrderSelect(i,SELECT_BY_POS,MODE_TRADES)==true && OrderSymbol()==Symbol() && (OrderMagicNumber()==MagicNumber) && OrderType()==type)
         Lots+=OrderLots();
     }
   return(NormalizeDouble(Lots,2));

  }
//+------------------------------------------------------------------+
// End of GetCycleLots()                                    		|
//+------------------------------------------------------------------+
double GetCycleProfit(int type)
  {

// Type: Customisable 
// Modify this function to suit your trading robot

// this function returns the net profit of the opened orders 

   double Profit=0;

   for(int i=0; i<OrdersTotal(); i++)
     {
      if(OrderSelect(i,SELECT_BY_POS,MODE_TRADES)==true && OrderSymbol()==Symbol() && (OrderMagicNumber()==MagicNumber) && OrderType()==type)
         Profit=Profit+OrderProfit();
     }
   return(NormalizeDouble(Profit,2));
  }
//+------------------------------------------------------------------+
// End of GetCycleLots()                                    		|
//+------------------------------------------------------------------+
double GetNetProfitSince(datetime time,int ordertype)
  {

// Type: Customisable 
// Modify this function to suit your trading robot

// this function returns the net profit of the opened orders 

   double Profit=0;

   for(int i=OrdersTotal()-1; i>=0; i--)
     {
      if(OrderSelect(i,SELECT_BY_POS,MODE_TRADES)==true && OrderSymbol()==Symbol() && (OrderMagicNumber()==MagicNumber) && OrderOpenTime()>time && OrderType()==ordertype)
         Profit=Profit+OrderProfit();
     }

   for(int i=OrdersHistoryTotal()-1; i>=0; i--)
     {
      if(OrderSelect(i,SELECT_BY_POS,MODE_HISTORY)==true && OrderSymbol()==Symbol() && (OrderMagicNumber()==MagicNumber) && OrderOpenTime()>time && OrderType()==ordertype)
         Profit=Profit+OrderProfit()+OrderSwap()+OrderCommission();
     }
   return(Profit);
  }
//+------------------------------------------------------------------+
// End of GetCycleLots()                                    		|
//+------------------------------------------------------------------+
double GetLotsSince(datetime time,int ordertype)
  {

// Type: Customisable 
// Modify this function to suit your trading robot

// this function returns the net profit of the opened orders 

   double Lots=0;

   for(int i=OrdersTotal()-1; i>=0; i--)
     {
      if(OrderSelect(i,SELECT_BY_POS,MODE_TRADES)==true && OrderSymbol()==Symbol() && (OrderMagicNumber()==MagicNumber) && OrderOpenTime()>time && OrderType()==ordertype)
         Lots=Lots+OrderLots();
     }

   for(int i=OrdersHistoryTotal()-1; i>=0; i--)
     {
      if(OrderSelect(i,SELECT_BY_POS,MODE_HISTORY)==true && OrderSymbol()==Symbol() && (OrderMagicNumber()==MagicNumber) && OrderOpenTime()>time && OrderType()==ordertype)
         Lots=Lots+OrderLots();
     }
   return(Lots);
  }
//+------------------------------------------------------------------+
// End of GetCycleLots()                                    		|
//+------------------------------------------------------------------+
double GetClosedProfitSince(datetime time,int ordertype)
  {

// Type: Customisable 
// Modify this function to suit your trading robot

// this function returns the net profit of the opened orders 

   double Profit=0;

   for(int i=OrdersHistoryTotal()-1; i>=0; i--)
     {
      if(OrderSelect(i,SELECT_BY_POS,MODE_HISTORY)==true && OrderSymbol()==Symbol() && (OrderMagicNumber()==MagicNumber) && OrderOpenTime()>time && OrderType()==ordertype)
         Profit=Profit+OrderProfit()+OrderSwap()+OrderCommission();
     }
   return(Profit);
  }
//+------------------------------------------------------------------+
// End of GetCycleProfit()                                    			|
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|Start of CountPosOrders()	" Count Positions"
//+------------------------------------------------------------------+
int CountPosOrders(int Magic,int TYPE)
  {
// Type: Fixed Template 
// Do not edit unless you know what you're doing

// This function counts number of positions/orders of OrderType TYPE

   int orders=0;
   for(int i=0; i<OrdersTotal(); i++)
     {
      if(OrderSelect(i,SELECT_BY_POS,MODE_TRADES)==true && OrderSymbol()==Symbol() && OrderMagicNumber()==Magic && OrderType()==TYPE)
         orders++;
     }
   return(orders);

  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+


//+------------------------------------------------------------------+
//|Start of OpenPositionMarket()
//+------------------------------------------------------------------+
int OpenPositionMarket(int TYPE,double LOT,double SL,double TP,int Magic,int Slip,bool Journaling,int K,int Max_Retries_Per_Tick)
  {
// Type: Fixed Template 
// Do not edit unless you know what you're doing 

// This function submits new orders

   int tries=0;
   string symbol=Symbol();
   int cmd=TYPE;
   double volume=CheckLot(LOT,Journaling);
   if(MarketInfo(symbol,MODE_MARGINREQUIRED)*volume>AccountFreeMargin())
     {
      Print("Can not open a trade. Not enough free margin to open "+volume+" on "+symbol);
      return(-1);
     }
   int slippage=Slip*K; // Slippage is in points. 1 point = 0.0001 on 4 digit broker and 0.00001 on a 5 digit broker
   string comment=" "+TYPE+"(#"+Magic+")";
   int magic=Magic;
   datetime expiration=0;
   color arrow_color=0;if(TYPE==OP_BUY)arrow_color=DodgerBlue;if(TYPE==OP_SELL)arrow_color=DeepPink;
   double stoploss=0;
   double takeprofit=0;
   double initTP = TP;
   double initSL = SL;
   int Ticket=-1;
   double price=0;

   while(tries<Max_Retries_Per_Tick) // Edits stops and take profits before the market order is placed
     {
      RefreshRates();
      if(TYPE==OP_BUY)price=Ask;if(TYPE==OP_SELL)price=Bid;

      // Sets Take Profits and Stop Loss. Check against Stop Level Limitations.
      if(TYPE==OP_BUY && SL!=0)
        {
         stoploss=NormalizeDouble(Ask-SL*K*Point,Digits);
         if(Bid-stoploss<=MarketInfo(Symbol(),MODE_STOPLEVEL)*Point)
           {
            stoploss=NormalizeDouble(Bid-MarketInfo(Symbol(),MODE_STOPLEVEL)*Point,Digits);
            if(Journaling)Print("EA Journaling: Stop Loss changed from "+initSL+" to "+MarketInfo(Symbol(),MODE_STOPLEVEL)/K+" pips");
           }
        }
      if(TYPE==OP_SELL && SL!=0)
        {
         stoploss=NormalizeDouble(Bid+SL*K*Point,Digits);
         if(stoploss-Ask<=MarketInfo(Symbol(),MODE_STOPLEVEL)*Point)
           {
            stoploss=NormalizeDouble(Ask+MarketInfo(Symbol(),MODE_STOPLEVEL)*Point,Digits);
            if(Journaling)Print("EA Journaling: Stop Loss changed from "+initSL+" to "+MarketInfo(Symbol(),MODE_STOPLEVEL)/K+" pips");
           }
        }
      if(TYPE==OP_BUY && TP!=0)
        {
         takeprofit=NormalizeDouble(Ask+TP*K*Point,Digits);
         if(takeprofit-Bid<=MarketInfo(Symbol(),MODE_STOPLEVEL)*Point)
           {
            takeprofit=NormalizeDouble(Ask+MarketInfo(Symbol(),MODE_STOPLEVEL)*Point,Digits);
            if(Journaling)Print("EA Journaling: Take Profit changed from "+initTP+" to "+MarketInfo(Symbol(),MODE_STOPLEVEL)/K+" pips");
           }
        }
      if(TYPE==OP_SELL && TP!=0)
        {
         takeprofit=NormalizeDouble(Bid-TP*K*Point,Digits);
         if(Ask-takeprofit<=MarketInfo(Symbol(),MODE_STOPLEVEL)*Point)
           {
            takeprofit=NormalizeDouble(Bid-MarketInfo(Symbol(),MODE_STOPLEVEL)*Point,Digits);
            if(Journaling)Print("EA Journaling: Take Profit changed from "+initTP+" to "+MarketInfo(Symbol(),MODE_STOPLEVEL)/K+" pips");
           }
        }
      if(Journaling)Print("EA Journaling: Trying to place a market order...");
      HandleTradingEnvironment(Journaling,RetryInterval);
      Ticket=OrderSend(symbol,cmd,volume,price,slippage,stoploss,takeprofit,comment,magic,expiration,arrow_color);
      if(Ticket>0)break;
      tries++;
     }

   if(Journaling && Ticket<0)Print("EA Journaling: Unexpected Error has happened. Error Description: "+GetErrorDescription(GetLastError()));
   if(Journaling && Ticket>0)
     {
      Print("EA Journaling: Order successfully placed. Ticket: "+Ticket);
     }
   return(Ticket);
  }
//+------------------------------------------------------------------+
//|End of OpenPositionMarket()
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|Start of CloseOrderPosition()
//+------------------------------------------------------------------+
bool CloseOrderPosition(int TYPE,bool Journaling,int Magic,int Slip,int K)
  {
// Type: Fixed Template 
// Do not edit unless you know what you're doing
   int ordersPos=OrdersTotal();

   for(int i=ordersPos-1; i>=0; i--)
     {
      // Note: Once pending orders become positions, OP_BUYLIMIT AND OP_BUYSTOP becomes OP_BUY, OP_SELLLIMIT and OP_SELLSTOP becomes OP_SELL
      if(TYPE==OP_BUY || TYPE==OP_SELL)
        {
         if(OrderSelect(i,SELECT_BY_POS,MODE_TRADES)==true && OrderSymbol()==Symbol() && OrderMagicNumber()==Magic && OrderType()==TYPE)
           {
            bool Closing=false;
            double Price=0;
            color arrow_color=0;if(TYPE==OP_BUY)arrow_color=MediumSeaGreen;if(TYPE==OP_SELL)arrow_color=DarkOrange;
            if(Journaling)Print("EA Journaling: Trying to close position "+OrderTicket()+" ...");
            HandleTradingEnvironment(Journaling,RetryInterval);
            if(TYPE==OP_BUY)Price=Bid; if(TYPE==OP_SELL)Price=Ask;
            Closing=OrderClose(OrderTicket(),OrderLots(),Price,Slip*K,arrow_color);
            if(Journaling && !Closing)Print("EA Journaling: Unexpected Error has happened. Error Description: "+GetErrorDescription(GetLastError()));
            if(Journaling && Closing)Print("EA Journaling: Position successfully closed.");
           }
        }
     }
   if(CountPosOrders(Magic, TYPE)==0)return(true); else return(false);
  }
//+------------------------------------------------------------------+
//| End of CloseOrderPosition()
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
// Start of getP()                                                   |
//+------------------------------------------------------------------+

int GetP()
  {

   int output;

   if(Digits==5 || Digits==3) output=10;else output=1;

   return(output);
  }
//+------------------------------------------------------------------+
// End of GetP()                                                    	|
//+------------------------------------------------------------------+


//+------------------------------------------------------------------+
// Start of GetYenAdjustFactor()                                     |
//+------------------------------------------------------------------+

int GetYenAdjustFactor()
  {

   int output=1;

   if(Digits==3 || Digits==2) output=100;

   return(output);

  }
//+------------------------------------------------------------------+
// End of GetYenAdjustFactor()                                       |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//| Start of UpdateTrailingList()                              
//+------------------------------------------------------------------+

void UpdateTrailingList(bool Journaling,int Retry_Interval,int Magic)
  {
// Type: Fixed Template 
// Do not edit unless you know what you're doing

// This function clears the elements of your VolTrailingList if the corresponding positions has been closed

   int ordersPos=OrdersTotal();
   int orderTicketNumber;
   bool doesPosExist;

// Check the VolTrailingList, match with current list of positions. Make sure the all the positions exists. 
// If it doesn't, it means there are positions that have been closed

   for(int x=0; x<ArrayRange(TrailingStopList,0); x++)
     { // Looping through all order number in list

      doesPosExist=False;
      orderTicketNumber=TrailingStopList[x,0];

      if(orderTicketNumber!=0)
        { // Order exists
         for(int y=ordersPos-1; y>=0; y--)
           { // Looping through all current open positions
            if(OrderSelect(y,SELECT_BY_POS,MODE_TRADES)==true && OrderSymbol()==Symbol() && OrderMagicNumber()==Magic)
              {
               if(orderTicketNumber==OrderTicket())
                 { // Checks order number in list against order number of current positions
                  doesPosExist=True;
                  break;
                 }
              }
           }

         if(doesPosExist==False)
           { // Deletes elements if the order number does not match any current positions
            TrailingStopList[x,0] = 0;
            TrailingStopList[x,1] = 0;
           }
        }
     }
  }
//+------------------------------------------------------------------+
//| End of UpdateTrailingList                                        
//+------------------------------------------------------------------+



void ReviewTrailingStop(bool Journaling,double TrailingStop_Offset,int Retry_Interval,int Magic,int K)
  {
// Type: Fixed Template 
// Do not edit unless you know what you're doing 

// This function updates volatility trailing stops levels for all positions (using OrderModify) if appropriate conditions are met

   bool doesTrailingRecordExist;
   int posTicketNumber;
   for(int i=OrdersTotal()-1; i>=0; i--)
     { // Looping through all orders

      if(OrderSelect(i,SELECT_BY_POS,MODE_TRADES)==true && OrderSymbol()==Symbol() && OrderMagicNumber()==Magic)
        {
         doesTrailingRecordExist=False;
         posTicketNumber=OrderTicket();
         for(int x=0; x<ArrayRange(TrailingStopList,0); x++)
           { // Looping through all order number in list 

            if(posTicketNumber==TrailingStopList[x,0])
              { // If condition holds, it means the position have a volatility trailing stop level attached to it

               doesTrailingRecordExist=True;
               bool Modify=false;
               RefreshRates();

               // We update the volatility trailing stop record using OrderModify.
               if(OrderType()==OP_BUY && (Bid-TrailingStopList[x,1]>(TrailingStop_Offset*K*Point)))
                 {
                  if(TrailingStopList[x,1]!=Bid-(TrailingStop_Offset*K*Point))
                    {
                     // if(Journaling)Print("EA Journaling: Trying to modify order "+OrderTicket()+" ...");
                     HandleTradingEnvironment(Journaling,Retry_Interval);
                     Modify=OrderModify(OrderTicket(),OrderOpenPrice(),TrailingStopList[x,1],OrderTakeProfit(),0,CLR_NONE);
                     if(Journaling && !Modify)Print("EA Journaling: Unexpected Error has happened. Error Description: "+GetErrorDescription(GetLastError()));
                     if(Journaling && Modify) Print("EA Journaling: Order successfully modified, trailing stop changed.");

                     TrailingStopList[x,1]=Bid-(TrailingStop_Offset*K*Point);
                    }
                 }
               if(OrderType()==OP_SELL && ((TrailingStopList[x,1]-Ask>(TrailingStop_Offset*K*Point))))
                 {
                  if(TrailingStopList[x,1]!=Ask+(TrailingStop_Offset*K*Point))
                    {
                     //if(Journaling)Print("EA Journaling: Trying to modify order "+OrderTicket()+" ...");
                     HandleTradingEnvironment(Journaling,Retry_Interval);
                     Modify=OrderModify(OrderTicket(),OrderOpenPrice(),TrailingStopList[x,1],OrderTakeProfit(),0,CLR_NONE);
                     if(Journaling && !Modify)Print("EA Journaling: Unexpected Error has happened. Error Description: "+GetErrorDescription(GetLastError()));
                     if(Journaling && Modify) Print("EA Journaling: Order successfully modified, trailing stop changed.");
                     TrailingStopList[x,1]=Ask+(TrailingStop_Offset*K*Point);
                    }
                 }
               break;
              }
           }
         // If order does not have a record attached to it. Alert the trader.
         //if(!doesTrailingRecordExist && Journaling) Print("EA Journaling: Error. Order "+posTicketNumber+" has no volatility trailing stop attached to it.");
        }
     }
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//| End of Review Volatility Trailing Stop
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//| Start of SetTrailingStop
//+------------------------------------------------------------------+

void SetTrailingStop(bool Journaling,int Retry_Interval,int Magic,int K,int OrderNum)
  {
// Type: Fixed Template 
// Do not edit unless you know what you're doing 

// This function adds new volatility trailing stop level using OrderModify()
   double trailingStopLossLimit=0;
   bool Modify=False;
   bool IsTrailingStopAdded=False;
   if(OrderSelect(OrderNum,SELECT_BY_TICKET)==true && OrderSymbol()==Symbol() && OrderMagicNumber()==Magic)
     {
      RefreshRates();
      if(OrderType()==OP_BUY || OrderType()==OP_BUYSTOP)
        {
         trailingStopLossLimit=OrderOpenPrice();// virtual stop loss.
         IsTrailingStopAdded=True;
        }
      if(OrderType()==OP_SELL || OrderType()==OP_SELLSTOP)
        {
         trailingStopLossLimit=OrderOpenPrice();// virtual stop loss.
         IsTrailingStopAdded=True;
        }
      // Records trailingStopLossLimit for future use
      if(IsTrailingStopAdded==True)
        {
         for(int x=0; x<ArrayRange(TrailingStopList,0); x++) // Loop through elements in VolTrailingList
           {
            if(TrailingStopList[x,0]==0) // Checks if the element is empty
              {
               TrailingStopList[x,0]=OrderNum; // Add order number
               TrailingStopList[x,1]=trailingStopLossLimit; // Add Trailing Stop into the List
               Modify=true;
               if(Journaling && Modify) Print("Trailing Stop For "+OrderNum+" Has been Set successfully to "+TrailingStopOffset);
               break;
              }
           }
        }
     }

   if(Journaling && !Modify) Print("Couldnt set Trailing Stop For "+OrderNum+" !");
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//| End of SetTrailingStop
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//| Start of UpdateTrailingList()                              
//+------------------------------------------------------------------+

void UpdateBreakevenList(bool Journaling,int Retry_Interval,int Magic)
  {
// Type: Fixed Template 
// Do not edit unless you know what you're doing

// This function clears the elements of your BreakevenList if the corresponding positions has been closed

   int ordersPos=OrdersTotal();
   int orderTicketNumber;
   bool doesPosExist;

// Check the VolTrailingList, match with current list of positions. Make sure the all the positions exists. 
// If it doesn't, it means there are positions that have been closed

   for(int x=0; x<ArrayRange(BreakevenList,0); x++)
     { // Looping through all order number in list

      doesPosExist=False;
      orderTicketNumber=BreakevenList[x,0];

      if(orderTicketNumber!=0)
        { // Order exists
         for(int y=ordersPos-1; y>=0; y--)
           { // Looping through all current open positions
            if(OrderSelect(y,SELECT_BY_POS,MODE_TRADES)==true && OrderSymbol()==Symbol() && OrderMagicNumber()==Magic)
              {
               if(orderTicketNumber==OrderTicket())
                 { // Checks order number in list against order number of current positions
                  doesPosExist=True;
                  break;
                 }
              }
           }

         if(doesPosExist==False)
           { // Deletes elements if the order number does not match any current positions
            BreakevenList[x,0] = 0;
            BreakevenList[x,1] = 0;
           }
        }
     }
  }
//+------------------------------------------------------------------+
//| End of UpdateTrailingList                                        
//+------------------------------------------------------------------+



void ReviewBreakeven(bool Journaling,double Breakeven_Offset,int Retry_Interval,int Magic,int K)
  {
// Type: Fixed Template 
// Do not edit unless you know what you're doing 

// This function updates volatility trailing stops levels for all positions (using OrderModify) if appropriate conditions are met

   bool doesBreakevenRecordExist;
   int posTicketNumber;
   double spread=0;

   if(AddSpread)
     {
      spread=spreadValue*Point*P;
     }
   for(int i=OrdersTotal()-1; i>=0; i--)
     { // Looping through all orders

      if(OrderSelect(i,SELECT_BY_POS,MODE_TRADES)==true && OrderSymbol()==Symbol() && OrderMagicNumber()==Magic)
        {
         doesBreakevenRecordExist=False;
         posTicketNumber=OrderTicket();
         for(int x=0; x<ArrayRange(BreakevenList,0); x++)
           { // Looping through all order number in list 

            if(posTicketNumber==BreakevenList[x,0])
              { // If condition holds, it means the position have a volatility trailing stop level attached to it

               doesBreakevenRecordExist=True;
               bool Modify=false;
               RefreshRates();

               // We update the volatility trailing stop record using OrderModify.
               if(OrderType()==OP_BUY && (Bid-BreakevenList[x,1]>(Breakeven_Offset*K*Point)) && BreakevenList[x,1]!=-1)
                 {
                  // if(Journaling)Print("EA Journaling: Trying to modify order "+OrderTicket()+" ...");
                  HandleTradingEnvironment(Journaling,Retry_Interval);
                  Modify=OrderModify(OrderTicket(),OrderOpenPrice(),OrderOpenPrice()+(BreakevenBuffer *Point*P)+spread,OrderTakeProfit(),0,CLR_NONE);
                  if(Journaling && !Modify)Print("EA Journaling: Unexpected Error has happened. Error Description: "+GetErrorDescription(GetLastError()));
                  if(Journaling && Modify) Print("EA Journaling: Order successfully modified, Breakeven changed.");

                  BreakevenList[x,1]=-1;

                 }
               if(OrderType()==OP_SELL && ((BreakevenList[x,1]-Ask>(Breakeven_Offset*K*Point))) && BreakevenList[x,1]!=-1)
                 {

                  //if(Journaling)Print("EA Journaling: Trying to modify order "+OrderTicket()+" ...");
                  HandleTradingEnvironment(Journaling,Retry_Interval);
                  Modify=OrderModify(OrderTicket(),OrderOpenPrice(),OrderOpenPrice()-(BreakevenBuffer *Point*P)-spread,OrderTakeProfit(),0,CLR_NONE);
                  if(Journaling && !Modify)Print("EA Journaling: Unexpected Error has happened. Error Description: "+GetErrorDescription(GetLastError()));
                  if(Journaling && Modify) Print("EA Journaling: Order successfully modified, Breakeven changed.");
                  BreakevenList[x,1]=-1;

                 }
               break;
              }
           }

        }
     }
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//| End of Review Volatility Trailing Stop
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//| Start of SetBreakeven
//+------------------------------------------------------------------+

void SetBreakeven(bool Journaling,int Retry_Interval,int Magic,int K,int OrderNum)
  {
// Type: Fixed Template 
// Do not edit unless you know what you're doing 

// This function adds new volatility trailing stop level using OrderModify()
   double BreakevenLimit=0;
   bool Modify=False;
   bool IsBreakevenAdded=False;
   if(OrderSelect(OrderNum,SELECT_BY_TICKET)==true && OrderSymbol()==Symbol() && OrderMagicNumber()==Magic)
     {
      RefreshRates();
      if(OrderType()==OP_BUY || OrderType()==OP_BUYSTOP)
        {
         BreakevenLimit=OrderOpenPrice();// virtual stop loss.
         IsBreakevenAdded=True;
        }
      if(OrderType()==OP_SELL || OrderType()==OP_SELLSTOP)
        {
         BreakevenLimit=OrderOpenPrice();// virtual stop loss.
         IsBreakevenAdded=True;
        }
      // Records trailingStopLossLimit for future use
      if(IsBreakevenAdded==True)
        {
         for(int x=0; x<ArrayRange(BreakevenList,0); x++) // Loop through elements in VolTrailingList
           {
            if(BreakevenList[x,0]==0) // Checks if the element is empty
              {
               BreakevenList[x,0]=OrderNum; // Add order number
               BreakevenList[x,1]=BreakevenLimit; // Add Trailing Stop into the List
               Modify=true;
               if(Journaling && Modify) Print("Breakeven For "+OrderNum+" Has been Set successfully to "+BreakevenOffset);
               break;
              }
           }
        }
     }

   if(Journaling && !Modify) Print("Couldnt set Breakeven For "+OrderNum+" !");
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//| End of SetBreakeven
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//Start of GetSellStopLoss()                                      |
//+------------------------------------------------------------------+

double GetSellStopLoss(int stopLossMethod,double fixedStop,int K)
  {

   double StopL;
   if(stopLossMethod==2)
     {
      StopL=fixedStop; // If Swing Stop Loss not activated. Stop Loss = Fixed Pips Stop Loss
     }
   else
     {
      if(SwingCandlesHighestHigh<Bid)
        {
         StopL=fixedStop;
        }
      else
        {
         StopL=(SwingCandlesHighestHigh-Bid)/(K*Point); // Stop Loss in Pips From the Highest High Price on 'SwingCandleCount'
        }
     }
   return(StopL+ClosingOffset);
  }
//+------------------------------------------------------------------+
// End of GetSellStopLoss()                                      |
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//Start of GetBuyStopLoss()                                    |
//+------------------------------------------------------------------+
double GetBuyStopLoss(int stopLossMethod,double fixedStop,int K)
  {

   double StopL;
   if(stopLossMethod==2)
     {
      StopL=fixedStop; // If Swing Stop Loss not activated. Stop Loss = Fixed Pips Stop Loss
     }
   else
     {
      if(SwingCandlesLowestLow>Ask)
        {
         StopL=fixedStop;
        }
      else
        {
         StopL=(Ask-SwingCandlesLowestLow)/(K*Point); // Stop Loss in Pips From the Lowest Low Price on 'SwingCandleCount'
        }

     }

   return(StopL+ClosingOffset);
  }
//+------------------------------------------------------------------+
// End of GetBuyStopLoss()                                  |
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
// Start of GetSellTakeProfit()                                  |
//+------------------------------------------------------------------+

double GetSellTakeProfit(int takeProfitMethod,double takeProfitMultiplier,double fixedProfit,double sellStopLoss)
  {

   double takeP;

   if(takeProfitMethod==0) // if the takeprofit value is double the stop loss
     {
      takeP=sellStopLoss*takeProfitMultiplier;
     }
   else
     {
      takeP=fixedProfit;            //else make it fixed in points.
     }

   return (takeP);

  }
//+------------------------------------------------------------------+
// End of GetSellTakeProfit()                                  |
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
// Start of GetBuyTakeProfit()                                  |
//+------------------------------------------------------------------+

double GetBuyTakeProfit(int takeProfitMethod,double takeProfitMultiplier,double fixedProfit,double buyStopLoss)
  {
   double takeP;

   if(takeProfitMethod==0) // if the takeprofit value is double the stop loss
     {
      takeP=buyStopLoss*takeProfitMultiplier;
     }
   else
     {
      takeP=fixedProfit;            //else make it fixed in points.
     }

   return (takeP);

  }
//+------------------------------------------------------------------+
// End of GetBuyTakeProfit()                                  |
//+------------------------------------------------------------------+


//+------------------------------------------------------------------+
//| Start of HandleTradingEnvironment()                                         
//+------------------------------------------------------------------+
void HandleTradingEnvironment(bool Journaling,int Retry_Interval)
  {
// Type: Fixed Template 
// Do not edit unless you know what you're doing 

// This function checks for errors

   if(IsTradeAllowed()==true)return;
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
   if(!IsConnected())
     {
      if(Journaling)Print("EA Journaling: Terminal is not connected to server...");
      return;
     }
   if(!IsTradeAllowed() && Journaling)Print("EA Journaling: Trade is not alowed for some reason...");
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
   if(IsConnected() && !IsTradeAllowed())
     {
      while(IsTradeContextBusy()==true)
        {
         if(Journaling)Print("EA Journaling: Trading context is busy... Will wait a bit...");
         Sleep(Retry_Interval);
        }
     }
   RefreshRates();
  }
//+------------------------------------------------------------------+
//| End of HandleTradingEnvironment()                              
//+------------------------------------------------------------------+  
//+------------------------------------------------------------------+
//| Start of GetErrorDescription()                                               
//+------------------------------------------------------------------+
string GetErrorDescription(int error)
  {
// Type: Fixed Template 
// Do not edit unless you know what you're doing

// This function returns the exact error

   string ErrorDescription="";
//---
   switch(error)
     {
      case 0:     ErrorDescription = "NO Error. Everything should be good.";                                    break;
      case 1:     ErrorDescription = "No error returned, but the result is unknown";                            break;
      case 2:     ErrorDescription = "Common error";                                                            break;
      case 3:     ErrorDescription = "Invalid trade parameters";                                                break;
      case 4:     ErrorDescription = "Trade server is busy";                                                    break;
      case 5:     ErrorDescription = "Old version of the client terminal";                                      break;
      case 6:     ErrorDescription = "No connection with trade server";                                         break;
      case 7:     ErrorDescription = "Not enough rights";                                                       break;
      case 8:     ErrorDescription = "Too frequent requests";                                                   break;
      case 9:     ErrorDescription = "Malfunctional trade operation";                                           break;
      case 64:    ErrorDescription = "Account disabled";                                                        break;
      case 65:    ErrorDescription = "Invalid account";                                                         break;
      case 128:   ErrorDescription = "Trade timeout";                                                           break;
      case 129:   ErrorDescription = "Invalid price";                                                           break;
      case 130:   ErrorDescription = "Invalid stops";                                                           break;
      case 131:   ErrorDescription = "Invalid trade volume";                                                    break;
      case 132:   ErrorDescription = "Market is closed";                                                        break;
      case 133:   ErrorDescription = "Trade is disabled";                                                       break;
      case 134:   ErrorDescription = "Not enough money";                                                        break;
      case 135:   ErrorDescription = "Price changed";                                                           break;
      case 136:   ErrorDescription = "Off quotes";                                                              break;
      case 137:   ErrorDescription = "Broker is busy";                                                          break;
      case 138:   ErrorDescription = "Requote";                                                                 break;
      case 139:   ErrorDescription = "Order is locked";                                                         break;
      case 140:   ErrorDescription = "Long positions only allowed";                                             break;
      case 141:   ErrorDescription = "Too many requests";                                                       break;
      case 145:   ErrorDescription = "Modification denied because order too close to market";                   break;
      case 146:   ErrorDescription = "Trade context is busy";                                                   break;
      case 147:   ErrorDescription = "Expirations are denied by broker";                                        break;
      case 148:   ErrorDescription = "Too many open and pending orders (more than allowed)";                    break;
      case 4000:  ErrorDescription = "No error";                                                                break;
      case 4001:  ErrorDescription = "Wrong function pointer";                                                  break;
      case 4002:  ErrorDescription = "Array index is out of range";                                             break;
      case 4003:  ErrorDescription = "No memory for function call stack";                                       break;
      case 4004:  ErrorDescription = "Recursive stack overflow";                                                break;
      case 4005:  ErrorDescription = "Not enough stack for parameter";                                          break;
      case 4006:  ErrorDescription = "No memory for parameter string";                                          break;
      case 4007:  ErrorDescription = "No memory for temp string";                                               break;
      case 4008:  ErrorDescription = "Not initialized string";                                                  break;
      case 4009:  ErrorDescription = "Not initialized string in array";                                         break;
      case 4010:  ErrorDescription = "No memory for array string";                                              break;
      case 4011:  ErrorDescription = "Too long string";                                                         break;
      case 4012:  ErrorDescription = "Remainder from zero divide";                                              break;
      case 4013:  ErrorDescription = "Zero divide";                                                             break;
      case 4014:  ErrorDescription = "Unknown command";                                                         break;
      case 4015:  ErrorDescription = "Wrong jump (never generated error)";                                      break;
      case 4016:  ErrorDescription = "Not initialized array";                                                   break;
      case 4017:  ErrorDescription = "DLL calls are not allowed";                                               break;
      case 4018:  ErrorDescription = "Cannot load library";                                                     break;
      case 4019:  ErrorDescription = "Cannot call function";                                                    break;
      case 4020:  ErrorDescription = "Expert function calls are not allowed";                                   break;
      case 4021:  ErrorDescription = "Not enough memory for temp string returned from function";                break;
      case 4022:  ErrorDescription = "System is busy (never generated error)";                                  break;
      case 4050:  ErrorDescription = "Invalid function parameters count";                                       break;
      case 4051:  ErrorDescription = "Invalid function parameter value";                                        break;
      case 4052:  ErrorDescription = "String function internal error";                                          break;
      case 4053:  ErrorDescription = "Some array error";                                                        break;
      case 4054:  ErrorDescription = "Incorrect series array using";                                            break;
      case 4055:  ErrorDescription = "Custom indicator error";                                                  break;
      case 4056:  ErrorDescription = "Arrays are incompatible";                                                 break;
      case 4057:  ErrorDescription = "Global variables processing error";                                       break;
      case 4058:  ErrorDescription = "Global variable not found";                                               break;
      case 4059:  ErrorDescription = "Function is not allowed in testing mode";                                 break;
      case 4060:  ErrorDescription = "Function is not confirmed";                                               break;
      case 4061:  ErrorDescription = "Send mail error";                                                         break;
      case 4062:  ErrorDescription = "String parameter expected";                                               break;
      case 4063:  ErrorDescription = "Integer parameter expected";                                              break;
      case 4064:  ErrorDescription = "Double parameter expected";                                               break;
      case 4065:  ErrorDescription = "Array as parameter expected";                                             break;
      case 4066:  ErrorDescription = "Requested history data in updating state";                                break;
      case 4067:  ErrorDescription = "Some error in trading function";                                          break;
      case 4099:  ErrorDescription = "End of file";                                                             break;
      case 4100:  ErrorDescription = "Some file error";                                                         break;
      case 4101:  ErrorDescription = "Wrong file name";                                                         break;
      case 4102:  ErrorDescription = "Too many opened files";                                                   break;
      case 4103:  ErrorDescription = "Cannot open file";                                                        break;
      case 4104:  ErrorDescription = "Incompatible access to a file";                                           break;
      case 4105:  ErrorDescription = "No order selected";                                                       break;
      case 4106:  ErrorDescription = "Unknown symbol";                                                          break;
      case 4107:  ErrorDescription = "Invalid price";                                                           break;
      case 4108:  ErrorDescription = "Invalid ticket";                                                          break;
      case 4109:  ErrorDescription = "EA is not allowed to trade is not allowed. ";                             break;
      case 4110:  ErrorDescription = "Longs are not allowed. Check the expert properties";                      break;
      case 4111:  ErrorDescription = "Shorts are not allowed. Check the expert properties";                     break;
      case 4200:  ErrorDescription = "Object exists already";                                                   break;
      case 4201:  ErrorDescription = "Unknown object property";                                                 break;
      case 4202:  ErrorDescription = "Object does not exist";                                                   break;
      case 4203:  ErrorDescription = "Unknown object type";                                                     break;
      case 4204:  ErrorDescription = "No object name";                                                          break;
      case 4205:  ErrorDescription = "Object coordinates error";                                                break;
      case 4206:  ErrorDescription = "No specified subwindow";                                                  break;
      case 4207:  ErrorDescription = "Some error in object function";                                           break;
      default:    ErrorDescription = "No error or error is unknown";
     }
   return(ErrorDescription);
  }
//+------------------------------------------------------------------+
//| End of GetErrorDescription()                                         
//+------------------------------------------------------------------+


//+------------------------------------------------------------------+
//| Start of ChartSettings()                                         
//+------------------------------------------------------------------+
void ChartSettings()
  {
   ChartSetInteger(0,CHART_SHOW_GRID,0);
   ChartSetInteger(0,CHART_MODE,CHART_CANDLES);
   ChartSetInteger(0,CHART_AUTOSCROLL,0,True);
   WindowRedraw();
  }
//+------------------------------------------------------------------+
//| End of ChartSettings()                                         
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//| Start of CloseAllPositions()
//+------------------------------------------------------------------+ 
void CloseAllPositions()
  {
// Type: Fixed Template 
// Do not edit unless you know what you're doing

// This function closes all positions 

   CloseOrderPosition(OP_BUY,OnJournaling,MagicNumber,Slippage,P);

   CloseOrderPosition(OP_SELL,OnJournaling,MagicNumber,Slippage,P);

   CloseOrderPosition(OP_BUYSTOP,OnJournaling,MagicNumber,Slippage,P);

   CloseOrderPosition(OP_SELLSTOP,OnJournaling,MagicNumber,Slippage,P);

   return;

  }
//+------------------------------------------------------------------+
//| End of CloseAllPositions()
//+------------------------------------------------------------------+ 
//+------------------------------------------------------------------+
//| Start of IsLossLimitBreached()                                    
//+------------------------------------------------------------------+
bool IsLossLimitBreached(bool LossLimitActivated,double LossLimitValue,bool Journaling)
  {

// Type: Fixed Template 
// Do not edit unless you know what you're doing

// This function determines if our maximum Profit threshold is breached
//static bool firstTime=false;
   double lossPrint;
   bool output=False;

   if(LossLimitActivated==False) return (output);
//if(firstTime == true) return(true);

   if(cycleProfit<(-1 *LossLimitValue))
     {
      output=True;
      //firstTime=true;
      CloseAllPositions();
      lossPrint=NormalizeDouble(cycleProfit,4);
      if(Journaling) Print("Loss threshold breached. Current Loss: "+lossPrint);
     }

   return (output);
  }
//+------------------------------------------------------------------+
//|End of IsLossLimitBreached()                                      
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//| Start of IsProfitLimitBreached()                                    
//+------------------------------------------------------------------+
bool IsProfitLimitBreached(bool ProfitLimitActivated,double ProfitLimitValue,bool Journaling)
  {

// Type: Fixed Template 
// Do not edit unless you know what you're doing

// This function determines if our maximum Profit threshold is breached
   static bool firstTime=false;
   double profitPrint;
   bool output=False;

   if(ProfitLimitActivated==False) return (output);
//if(firstTime == true) return(true);

   if(cycleProfit>ProfitLimitValue)
     {
      output=True;
      //firstTime=true;
      CloseAllPositions();
      profitPrint=NormalizeDouble(cycleProfit,4);
      if(Journaling) Print("Profit threshold breached. Current Profit: "+profitPrint);
     }

   return (output);
  }
//+------------------------------------------------------------------+
//|End of IsProfitLimitBreached                                                                   |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//|Start of DrawChartInfo                                                                  |
//+------------------------------------------------------------------+

void DrawChartInfo()
  {

   ObjectCreate(ChartID(),ObjName+"InfoBackground",OBJ_RECTANGLE_LABEL,0,0,0);
   ObjectSetInteger(ChartID(),ObjName+"InfoBackground",OBJPROP_XSIZE,-180);
   ObjectSetInteger(ChartID(),ObjName+"InfoBackground",OBJPROP_YSIZE,310);
   ObjectSetInteger(ChartID(),ObjName+"InfoBackground",OBJPROP_XDISTANCE,xDistance);
   ObjectSetInteger(ChartID(),ObjName+"InfoBackground",OBJPROP_YDISTANCE,20);
   ObjectSetInteger(ChartID(),ObjName+"InfoBackground",OBJPROP_CORNER,CORNER_RIGHT_UPPER);
   ObjectSetInteger(ChartID(),ObjName+"InfoBackground",OBJPROP_BGCOLOR,Black);
   ObjectSetInteger(ChartID(),ObjName+"InfoBackground",OBJPROP_BORDER_COLOR,Black);
   ObjectSetInteger(ChartID(),ObjName+"InfoBackground",OBJPROP_BACK,false);
   ObjectSetInteger(ChartID(),ObjName+"InfoBackground",OBJPROP_SELECTABLE,false);
   ObjectSetInteger(ChartID(),ObjName+"InfoBackground",OBJPROP_SELECTED,false);
   ObjectSetInteger(ChartID(),ObjName+"InfoBackground",OBJPROP_HIDDEN,false);
   ObjectSetInteger(ChartID(),ObjName+"InfoBackground",OBJPROP_BORDER_TYPE,BORDER_FLAT);

   ObjectCreate(ChartID(),ObjName+"BigMA",OBJ_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"BigMA",OBJPROP_TEXT,"BigMa "+DoubleToStr(MyBigMA_1,2));
   ObjectSetInteger(ChartID(),ObjName+"BigMA",OBJPROP_CORNER,CORNER_LEFT_LOWER);
   ObjectSetInteger(ChartID(),ObjName+"BigMA",OBJPROP_XDISTANCE,xDistance);
   ObjectSetInteger(ChartID(),ObjName+"BigMA",OBJPROP_YDISTANCE,24);
   ObjectSetString(ChartID(),ObjName+"BigMA",OBJPROP_FONT,"Verdana");
   ObjectSetInteger(ChartID(),ObjName+"BigMA",OBJPROP_FONTSIZE,11);
   ObjectSetInteger(ChartID(),ObjName+"BigMA",OBJPROP_ANCHOR,ANCHOR_LEFT);
   ObjectSetInteger(ChartID(),ObjName+"BigMA",OBJPROP_COLOR,Tomato);

   ObjectCreate(ChartID(),ObjName+"HelmiFX2",OBJ_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"HelmiFX2",OBJPROP_TEXT,"www.helmifx.com");
   ObjectSetInteger(ChartID(),ObjName+"HelmiFX2",OBJPROP_CORNER,CORNER_RIGHT_UPPER);
   ObjectSetInteger(ChartID(),ObjName+"HelmiFX2",OBJPROP_XDISTANCE,xDistance);
   ObjectSetInteger(ChartID(),ObjName+"HelmiFX2",OBJPROP_YDISTANCE,37);
   ObjectSetString(ChartID(),ObjName+"HelmiFX2",OBJPROP_FONT,"Verdana");
   ObjectSetInteger(ChartID(),ObjName+"HelmiFX2",OBJPROP_FONTSIZE,8);
   ObjectSetInteger(ChartID(),ObjName+"HelmiFX2",OBJPROP_ANCHOR,ANCHOR_RIGHT);
   ObjectSetInteger(ChartID(),ObjName+"HelmiFX2",OBJPROP_COLOR,White);

   ObjectCreate(ChartID(),ObjName+"Symbol",OBJ_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"Symbol",OBJPROP_TEXT,Symbol()+"  "+spreadValue);
   ObjectSetInteger(ChartID(),ObjName+"Symbol",OBJPROP_CORNER,CORNER_RIGHT_UPPER);
   ObjectSetInteger(ChartID(),ObjName+"Symbol",OBJPROP_XDISTANCE,xDistance);
   ObjectSetInteger(ChartID(),ObjName+"Symbol",OBJPROP_YDISTANCE,62);
   ObjectSetString(ChartID(),ObjName+"Symbol",OBJPROP_FONT,"Verdana");
   ObjectSetInteger(ChartID(),ObjName+"Symbol",OBJPROP_FONTSIZE,12);
   ObjectSetInteger(ChartID(),ObjName+"Symbol",OBJPROP_ANCHOR,ANCHOR_RIGHT);
   ObjectSetInteger(ChartID(),ObjName+"Symbol",OBJPROP_COLOR,Yellow);

   ObjectCreate(ChartID(),ObjName+"BuyPipValue",OBJ_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"BuyPipValue",OBJPROP_TEXT,"Buy "+CountPosOrders(MagicNumber,OP_BUY)+" = "+DoubleToStr(buyCycleLots,2)+" L = "+DoubleToStr(buyPipValue,2)+"$");
   ObjectSetInteger(ChartID(),ObjName+"BuyPipValue",OBJPROP_CORNER,CORNER_RIGHT_UPPER);
   ObjectSetInteger(ChartID(),ObjName+"BuyPipValue",OBJPROP_XDISTANCE,xDistance);
   ObjectSetInteger(ChartID(),ObjName+"BuyPipValue",OBJPROP_YDISTANCE,87);
   ObjectSetString(ChartID(),ObjName+"BuyPipValue",OBJPROP_FONT,"Verdana");
   ObjectSetInteger(ChartID(),ObjName+"BuyPipValue",OBJPROP_FONTSIZE,10);
   ObjectSetInteger(ChartID(),ObjName+"BuyPipValue",OBJPROP_ANCHOR,ANCHOR_RIGHT);
   ObjectSetInteger(ChartID(),ObjName+"BuyPipValue",OBJPROP_COLOR,DeepPink);

   ObjectCreate(ChartID(),ObjName+"SellPipValue",OBJ_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"SellPipValue",OBJPROP_TEXT,"Sell "+CountPosOrders(MagicNumber,OP_SELL)+" = "+DoubleToStr(sellCycleLots,2)+" L = "+DoubleToStr(sellPipValue,2)+"$");
   ObjectSetInteger(ChartID(),ObjName+"SellPipValue",OBJPROP_CORNER,CORNER_RIGHT_UPPER);
   ObjectSetInteger(ChartID(),ObjName+"SellPipValue",OBJPROP_XDISTANCE,xDistance);
   ObjectSetInteger(ChartID(),ObjName+"SellPipValue",OBJPROP_YDISTANCE,112);
   ObjectSetString(ChartID(),ObjName+"SellPipValue",OBJPROP_FONT,"Verdana");
   ObjectSetInteger(ChartID(),ObjName+"SellPipValue",OBJPROP_FONTSIZE,10);
   ObjectSetInteger(ChartID(),ObjName+"SellPipValue",OBJPROP_ANCHOR,ANCHOR_RIGHT);
   ObjectSetInteger(ChartID(),ObjName+"SellPipValue",OBJPROP_COLOR,MediumVioletRed);

   ObjectCreate(ChartID(),ObjName+"Equity",OBJ_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"Equity",OBJPROP_TEXT,"Equity : "+DoubleToStr(AccountEquity(),2));
   ObjectSetInteger(ChartID(),ObjName+"Equity",OBJPROP_CORNER,CORNER_RIGHT_UPPER);
   ObjectSetInteger(ChartID(),ObjName+"Equity",OBJPROP_XDISTANCE,xDistance);
   ObjectSetInteger(ChartID(),ObjName+"Equity",OBJPROP_YDISTANCE,137);
   ObjectSetString(ChartID(),ObjName+"Equity",OBJPROP_FONT,"Verdana");
   ObjectSetInteger(ChartID(),ObjName+"Equity",OBJPROP_FONTSIZE,10);
   ObjectSetInteger(ChartID(),ObjName+"Equity",OBJPROP_ANCHOR,ANCHOR_RIGHT);
   ObjectSetInteger(ChartID(),ObjName+"Equity",OBJPROP_COLOR,BurlyWood);

   ObjectCreate(ChartID(),ObjName+"Balance",OBJ_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"Balance",OBJPROP_TEXT,"Balance : "+DoubleToStr(AccountBalance(),2));
   ObjectSetInteger(ChartID(),ObjName+"Balance",OBJPROP_CORNER,CORNER_RIGHT_UPPER);
   ObjectSetInteger(ChartID(),ObjName+"Balance",OBJPROP_XDISTANCE,xDistance);
   ObjectSetInteger(ChartID(),ObjName+"Balance",OBJPROP_YDISTANCE,162);
   ObjectSetString(ChartID(),ObjName+"Balance",OBJPROP_FONT,"Verdana");
   ObjectSetInteger(ChartID(),ObjName+"Balance",OBJPROP_FONTSIZE,10);
   ObjectSetInteger(ChartID(),ObjName+"Balance",OBJPROP_ANCHOR,ANCHOR_RIGHT);
   ObjectSetInteger(ChartID(),ObjName+"Balance",OBJPROP_COLOR,SkyBlue);

   ObjectCreate(ChartID(),ObjName+"ClosedBuyPositionsProfit",OBJ_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"ClosedBuyPositionsProfit",OBJPROP_TEXT,"Closed Buy "+DoubleToStr(buyLastClosedProfit,2));
   ObjectSetInteger(ChartID(),ObjName+"ClosedBuyPositionsProfit",OBJPROP_CORNER,CORNER_RIGHT_UPPER);
   ObjectSetInteger(ChartID(),ObjName+"ClosedBuyPositionsProfit",OBJPROP_XDISTANCE,xDistance);
   ObjectSetInteger(ChartID(),ObjName+"ClosedBuyPositionsProfit",OBJPROP_YDISTANCE,187);
   ObjectSetString(ChartID(),ObjName+"ClosedBuyPositionsProfit",OBJPROP_FONT,"Verdana");
   ObjectSetInteger(ChartID(),ObjName+"ClosedBuyPositionsProfit",OBJPROP_FONTSIZE,11);
   ObjectSetInteger(ChartID(),ObjName+"ClosedBuyPositionsProfit",OBJPROP_ANCHOR,ANCHOR_RIGHT);
   ObjectSetInteger(ChartID(),ObjName+"ClosedBuyPositionsProfit",OBJPROP_COLOR,Khaki);

   ObjectCreate(ChartID(),ObjName+"ClosedSellPositionsProfit",OBJ_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"ClosedSellPositionsProfit",OBJPROP_TEXT,"Closed Sell "+DoubleToStr(sellLastClosedProfit,2));
   ObjectSetInteger(ChartID(),ObjName+"ClosedSellPositionsProfit",OBJPROP_CORNER,CORNER_RIGHT_UPPER);
   ObjectSetInteger(ChartID(),ObjName+"ClosedSellPositionsProfit",OBJPROP_XDISTANCE,xDistance);
   ObjectSetInteger(ChartID(),ObjName+"ClosedSellPositionsProfit",OBJPROP_YDISTANCE,212);
   ObjectSetString(ChartID(),ObjName+"ClosedSellPositionsProfit",OBJPROP_FONT,"Verdana");
   ObjectSetInteger(ChartID(),ObjName+"ClosedSellPositionsProfit",OBJPROP_FONTSIZE,11);
   ObjectSetInteger(ChartID(),ObjName+"ClosedSellPositionsProfit",OBJPROP_ANCHOR,ANCHOR_RIGHT);
   ObjectSetInteger(ChartID(),ObjName+"ClosedSellPositionsProfit",OBJPROP_COLOR,Chartreuse);

   ObjectCreate(ChartID(),ObjName+"CurrentBuyPositionsProfit",OBJ_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"CurrentBuyPositionsProfit",OBJPROP_TEXT,"Current Buy "+DoubleToStr(buyCycleProfit,2));
   ObjectSetInteger(ChartID(),ObjName+"CurrentBuyPositionsProfit",OBJPROP_CORNER,CORNER_RIGHT_UPPER);
   ObjectSetInteger(ChartID(),ObjName+"CurrentBuyPositionsProfit",OBJPROP_XDISTANCE,xDistance);
   ObjectSetInteger(ChartID(),ObjName+"CurrentBuyPositionsProfit",OBJPROP_YDISTANCE,237);
   ObjectSetString(ChartID(),ObjName+"CurrentBuyPositionsProfit",OBJPROP_FONT,"Verdana");
   ObjectSetInteger(ChartID(),ObjName+"CurrentBuyPositionsProfit",OBJPROP_FONTSIZE,11);
   ObjectSetInteger(ChartID(),ObjName+"CurrentBuyPositionsProfit",OBJPROP_ANCHOR,ANCHOR_RIGHT);
   ObjectSetInteger(ChartID(),ObjName+"CurrentBuyPositionsProfit",OBJPROP_COLOR,GreenYellow);

   ObjectCreate(ChartID(),ObjName+"CurrentSellPositionsProfit",OBJ_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"CurrentSellPositionsProfit",OBJPROP_TEXT,"Current Sell "+DoubleToStr(sellCycleProfit,2));
   ObjectSetInteger(ChartID(),ObjName+"CurrentSellPositionsProfit",OBJPROP_CORNER,CORNER_RIGHT_UPPER);
   ObjectSetInteger(ChartID(),ObjName+"CurrentSellPositionsProfit",OBJPROP_XDISTANCE,xDistance);
   ObjectSetInteger(ChartID(),ObjName+"CurrentSellPositionsProfit",OBJPROP_YDISTANCE,262);
   ObjectSetString(ChartID(),ObjName+"CurrentSellPositionsProfit",OBJPROP_FONT,"Verdana");
   ObjectSetInteger(ChartID(),ObjName+"CurrentSellPositionsProfit",OBJPROP_FONTSIZE,11);
   ObjectSetInteger(ChartID(),ObjName+"CurrentSellPositionsProfit",OBJPROP_ANCHOR,ANCHOR_RIGHT);
   ObjectSetInteger(ChartID(),ObjName+"CurrentSellPositionsProfit",OBJPROP_COLOR,IndianRed);

   ObjectCreate(ChartID(),ObjName+"NetProfit",OBJ_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"NetProfit",OBJPROP_TEXT,"Net Profit "+DoubleToStr(netProfit,2));
   ObjectSetInteger(ChartID(),ObjName+"NetProfit",OBJPROP_CORNER,CORNER_RIGHT_UPPER);
   ObjectSetInteger(ChartID(),ObjName+"NetProfit",OBJPROP_XDISTANCE,xDistance);
   ObjectSetInteger(ChartID(),ObjName+"NetProfit",OBJPROP_YDISTANCE,287);
   ObjectSetString(ChartID(),ObjName+"NetProfit",OBJPROP_FONT,"Verdana");
   ObjectSetInteger(ChartID(),ObjName+"NetProfit",OBJPROP_FONTSIZE,11);
   ObjectSetInteger(ChartID(),ObjName+"NetProfit",OBJPROP_ANCHOR,ANCHOR_RIGHT);
   ObjectSetInteger(ChartID(),ObjName+"NetProfit",OBJPROP_COLOR,DarkTurquoise);

   ObjectCreate(ChartID(),ObjName+"CycleProfit",OBJ_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"CycleProfit",OBJPROP_TEXT,"Cycle Profit "+DoubleToStr(cycleProfit,2));
   ObjectSetInteger(ChartID(),ObjName+"CycleProfit",OBJPROP_CORNER,CORNER_RIGHT_UPPER);
   ObjectSetInteger(ChartID(),ObjName+"CycleProfit",OBJPROP_XDISTANCE,xDistance);
   ObjectSetInteger(ChartID(),ObjName+"CycleProfit",OBJPROP_YDISTANCE,312);
   ObjectSetString(ChartID(),ObjName+"CycleProfit",OBJPROP_FONT,"Verdana");
   ObjectSetInteger(ChartID(),ObjName+"CycleProfit",OBJPROP_FONTSIZE,11);
   ObjectSetInteger(ChartID(),ObjName+"CycleProfit",OBJPROP_ANCHOR,ANCHOR_RIGHT);
   ObjectSetInteger(ChartID(),ObjName+"CycleProfit",OBJPROP_COLOR,Pink);
   
   ObjectCreate(ChartID(),ObjName+"Logo",OBJ_BITMAP_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"Logo",OBJPROP_BMPFILE,0,"::picture.bmp");
   ObjectSetString(ChartID(),ObjName+"Logo",OBJPROP_BMPFILE,1,"::picture.bmp");
   ObjectSetInteger(ChartID(),ObjName+"Logo",OBJPROP_CORNER,CORNER_RIGHT_UPPER);
   ObjectSetInteger(ChartID(),ObjName+"Logo",OBJPROP_XDISTANCE,xDistance);
   ObjectSetInteger(ChartID(),ObjName+"Logo",OBJPROP_YDISTANCE,281);
   ObjectSetInteger(ChartID(),ObjName+"Logo",OBJPROP_ANCHOR,ANCHOR_RIGHT);

  }
//+------------------------------------------------------------------+
// End of DrawChartInfo()                                          |
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
// Start of UpdateChartInfo()                                          |
//+------------------------------------------------------------------+
void UpdateChartInfo()
  {
   ObjectSetString(ChartID(),ObjName+"ClosedBuyPositionsProfit",OBJPROP_TEXT,"Closed Buy "+DoubleToStr(buyLastClosedProfit,2));
   ObjectSetString(ChartID(),ObjName+"ClosedSellPositionsProfit",OBJPROP_TEXT,"Closed Sell "+DoubleToStr(sellLastClosedProfit,2));
   ObjectSetString(ChartID(),ObjName+"CurrentBuyPositionsProfit",OBJPROP_TEXT,"Current Buy "+DoubleToStr(buyCycleProfit,2));
   ObjectSetString(ChartID(),ObjName+"CurrentSellPositionsProfit",OBJPROP_TEXT,"Current Sell "+DoubleToStr(sellCycleProfit,2));
   ObjectSetString(ChartID(),ObjName+"BuyPipValue",OBJPROP_TEXT,"Buy "+CountPosOrders(MagicNumber,OP_BUY)+" = "+DoubleToStr(buyCycleLots,2)+" L = "+DoubleToStr(buyPipValue,2)+"$");
   ObjectSetString(ChartID(),ObjName+"SellPipValue",OBJPROP_TEXT,"Sell "+CountPosOrders(MagicNumber,OP_SELL)+" = "+DoubleToStr(sellCycleLots,2)+" L = "+DoubleToStr(sellPipValue,2)+"$");
   ObjectSetString(ChartID(),ObjName+"NetProfit",OBJPROP_TEXT,"Net Profit "+DoubleToStr(netProfit,2));
   ObjectSetString(ChartID(),ObjName+"Equity",OBJPROP_TEXT,"Equity : "+DoubleToStr(AccountEquity(),2));
   ObjectSetString(ChartID(),ObjName+"Balance",OBJPROP_TEXT,"Balance : "+DoubleToStr(AccountBalance(),2));
   ObjectSetString(ChartID(),ObjName+"CycleProfit",OBJPROP_TEXT,"Cycle Profit "+DoubleToString(cycleProfit,2));
   ObjectSetString(ChartID(),ObjName+"Symbol",OBJPROP_TEXT,Symbol()+"  "+spreadValue);
   ObjectSetString(ChartID(),ObjName+"BigMA",OBJPROP_TEXT,(UseBigMA?("BigMa "+DoubleToStr(MyBigMA_1,Digits)):" "));

  }
//+------------------------------------------------------------------+
// End of UpdateChartInfo()                                          |
//+------------------------------------------------------------------+



//+------------------------------------------------------------------+
//| Start of CheckSLTPExit()                                         |
//+------------------------------------------------------------------+

void CheckSLTPExit()
  {

   int orderType=-1;
   double orderProfit=-1;
   double orderOpenPrice=-1;
   double orderBreakEvenStopLoss=-1;
   double orderClosePrice=-1;

   for(int i=Orders.Total()-1; i>=0; i--)
     {
      if(OrderSelect(Orders.At(i),SELECT_BY_TICKET,MODE_TRADES)==true && OrderSymbol()==Symbol() && (OrderMagicNumber()==MagicNumber) && (OrderType()==OP_BUY || OrderType()==OP_SELL))
        {
         if(OrderCloseTime()!=0)
           {
            orderType=OrderType();
            orderProfit=OrderProfit();
            orderOpenPrice=OrderOpenPrice();
            orderClosePrice=OrderClosePrice();

            if(orderType==OP_BUY) orderBreakEvenStopLoss=orderOpenPrice+(BreakevenBuffer*Point*P)+(AddSpread?spreadValue*Point*P:0);
            else orderBreakEvenStopLoss=orderOpenPrice-(BreakevenBuffer*Point*P)-(AddSpread?spreadValue*Point*P:0);

            if(UseBreakeven && NormalizeDouble(orderClosePrice,Digits)==NormalizeDouble(orderBreakEvenStopLoss,Digits))
              {
               if(UseWaitBarsOnBreakeven) ActivateWaitBars=true;
               else ActivateWaitBars=false;

               if(FollowingOrdersIndex!=0)FollowingOrdersIndex--;
               else FollowingOrdersIndex=0;

               if(UseMultiplyLotOnBreakeven)positonLotMultiplier*=LotMultiplier;
               else positonLotMultiplier=1;
              }

            else
              {

               if(orderProfit<=0)
                 {
                  if(UseMultiplyLot)positonLotMultiplier*=LotMultiplier;
                  else positonLotMultiplier=1;

                  ActivateFollowingOrders=true;
                  ActivateWaitBars=true;
                  BarsCounter=0;

                 }

               else if(orderProfit>0)
                 {
                  positonLotMultiplier=1;
                  ActivateFollowingOrders=false;
                  FollowingOrdersIndex=0;
                  ActivateWaitBars=false;

                 }

              }

            Orders.Delete(i);
           }
        }
     }
  }
//+------------------------------------------------------------------+
//| End of CheckSLTPExit()                                         |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//| Start of isNewBar()                                         
//+------------------------------------------------------------------+
bool isNewBar()
  {
//--- memorize the time of opening of the last bar in the static variable
   static datetime last_time=0;
//--- current time
   datetime lastbar_time=SeriesInfoInteger(Symbol(),Period(),SERIES_LASTBAR_DATE);

//--- if it is the first call of the function
   if(last_time==0)
     {
      //--- set the time and exit
      last_time=lastbar_time;
      return(false);
     }

//--- if the time differs
   if(last_time!=lastbar_time)
     {
      //--- memorize the time and return true
      last_time=lastbar_time;
      return(true);
     }
//--- if we passed to this line, then the bar is not new; return false
   return(false);
  }
//+------------------------------------------------------------------+
//| End of isNewBar()                                         
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
// Start of  MACross()                            |
//+------------------------------------------------------------------+

// If Output is 0: No cross happened
//If Output is 1: Line 1 crossed Line 2 from Bottom
//If Output is 2: Line 1 crossed Line 2 from top }


int MACross(double fastMA,double slowMA)
  {
// Type: Customizable
// Do not edit unless you know what you're doing

// This function determines if a cross happened between 2 lines/data set here : FastEMA,SlowEMA
//----
   if(fastMA>slowMA)
      MACurrentDirection=1;  // line1 above line2
   if(fastMA<slowMA)
      MACurrentDirection=2;  // line1 below line2
//----
   if(MAFirstTime==true) // Need to check if this is the first time the function is run
     {
      MAFirstTime=false; // Change variable to false
      MALastDirection=MACurrentDirection; // Set new direction
      return (0);
     }

   if(MACurrentDirection!=MALastDirection && MAFirstTime==false) // If not the first time and there is a direction change
     {
      MALastDirection=MACurrentDirection; // Set new direction
      return(MACurrentDirection); // 1 for up, 2 for down
     }
   else
     {
      return(0);  // No direction change
     }
  }
//+------------------------------------------------------------------+
// End of  MACross()                            |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//| Start of isItTime()                                         
//+------------------------------------------------------------------+

bool isItTime()
  {
   Current_Time=TimeHour(TimeCurrent());
   if(Start<0)Start=0;
   if(End<0) End=0;
   if(Start==0 || Start>24) Start=24; if(End==0 || End>24) End=24; if(Current_Time==0) Current_Time=24;

   if(Start<End)
      if( (Current_Time < Start) || (Current_Time >= End) ) return(false);

   if(Start>End)
      if( (Current_Time < Start) && (Current_Time >= End) ) return(false);

   if(Start==End)
     {
      return false;
     }

   return(true);
  }
//+------------------------------------------------------------------+
//| End of isItTime()                                         
//+------------------------------------------------------------------+
