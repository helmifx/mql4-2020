
#property indicator_separate_window
#property indicator_buffers 3
#property indicator_color1 Black
#property indicator_color2 Green
#property indicator_color3 Gray

int gi_unused_76 = 56;
int gi_unused_80 = 56;
int gi_84 = 25;
double gd_88 = 0.1;
double gd_96 = 0.1;
string gs_unused_104 = "ProFx03";
int gi_112;
double g_ibuf_116[];
double g_ibuf_120[];
double g_ibuf_124[];
double g_ibuf_128[];

int init() {

   IndicatorBuffers(4);
   SetIndexLabel(0, "Star");
   SetIndexStyle(0, DRAW_NONE);
   SetIndexBuffer(0, g_ibuf_116);
   SetIndexStyle(1, DRAW_HISTOGRAM);
   SetIndexBuffer(1, g_ibuf_120);
   SetIndexStyle(2, DRAW_HISTOGRAM);
   SetIndexBuffer(2, g_ibuf_124);
   SetIndexStyle(3, DRAW_NONE);
   SetIndexBuffer(3, g_ibuf_128);
   SetIndexLabel(1, NULL);
   SetIndexLabel(2, NULL);
   gi_112 = gi_84 * 2 + 4;
   SetIndexDrawBegin(1, gi_112);
   SetIndexDrawBegin(2, gi_112);
   if (gd_88 >= 1.0) {
      gd_88 = 0.9999;
      Alert("PriceSmothing factor has to be smaller 1!");
   }
   if (gd_88 < 0.0) {
      gd_88 = 0;
      Alert("PriceSmothing factor must be negative!");
   }
   if (gd_96 >= 1.0) {
      gd_96 = 0.9999;
      Alert("PriceSmothing factor has to be smaller 1!");
   }
   if (gd_96 < 0.0) {
      gd_96 = 0;
      Alert("PriceSmothing factor must be negative!");
   }
   return (0);
}

int deinit() {

   return (0);
}

int start() {

   if (Bars < gi_112) {
      Alert("Not enough Bars loaded to calculate Indicator with RangePeriods=", gi_84);
      return (-1);
   }
   int li_4 = IndicatorCounted();
   if (li_4 < 0) return (-1);
   if (li_4 > 0) li_4--;
   int li_8 = Bars - li_4;
   int li_12 = Bars - li_8;
   if (li_12 < gi_84 + 1) li_8 = Bars - gi_84 - 1;
   while (li_8 >= 0) {
      CalculateCurrentBar(li_8);
      li_8--;
   }
   return (0);
}

int CalculateCurrentBar(int ai_0) {
   double ld_36;
   double ld_52;
   double l_low_4 = Low[iLowest(NULL, 0, MODE_LOW, gi_84, ai_0)];
   double ld_12 = High[iHighest(NULL, 0, MODE_HIGH, gi_84, ai_0)];
   if (ld_12 - l_low_4 < Point / 10.0) ld_12 = l_low_4 + Point / 10.0;
   double ld_20 = ld_12 - l_low_4;
   double ld_28 = (High[ai_0] + Low[ai_0]) / 2.0;
   if (ld_20 != 0.0) {
      ld_36 = (ld_28 - l_low_4) / ld_20;
      ld_36 = 2.0 * ld_36 - 1.0;
   }
   g_ibuf_128[ai_0] = gd_88 * (g_ibuf_128[ai_0 + 1]) + (1.0 - gd_88) * ld_36;
   double ld_44 = g_ibuf_128[ai_0];
   if (ld_44 > 0.99) ld_44 = 0.99;
   if (ld_44 < -0.99) ld_44 = -0.99;
   if (1 - ld_44 != 0.0) ld_52 = MathLog((ld_44 + 1.0) / (1 - ld_44));
   else Alert("Unerlaubter Zustand bei Bar Nummer ", Bars - ai_0);
   g_ibuf_116[ai_0] = gd_96 * (g_ibuf_116[ai_0 + 1]) + (1.0 - gd_96) * ld_52;
   if (Bars - ai_0 < gi_112) g_ibuf_116[ai_0] = 0;
   double ld_60 = g_ibuf_116[ai_0];
   if (ld_60 > 0.0) {
      g_ibuf_120[ai_0] = ld_60;
      g_ibuf_124[ai_0] = 0;
   } else {
      g_ibuf_120[ai_0] = 0;
      g_ibuf_124[ai_0] = ld_60;
   }
   return (0);
}