//+------------------------------------------------------------------+
//|                                         Abd_AllPairsStratagy.mq4 |
//|                        Copyright 2017, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//|Abd Loulou
//+------------------------------------------------------------------+

#property copyright "Copyright 2020 HelmiFX."
#property link      "www.helmifx.com"
#property strict
#property description "Mudaafat Expert"
#property version "1.3"


#property description "Find us on  :\n"
#property description "Facbook : HelmiFx"
#property description "Twitter : @HelmiForex"  
#property description "Youtube : HelmiForex" 

#import "urlmon.dll"
int URLDownloadToFileW(int pCaller,string szURL,string szFileName,int dwReserved,int Callback);
#import

#import "kernel32.dll"
int SystemTimeToFileTime(int &TimeArray[],int &FileTimeArray[]);
int FileTimeToLocalFileTime(int &FileTimeArray[],int &LocalFileTimeArray[]);
void GetSystemTime(int &TimeArray[]);
#import


#define INAME "Mudaafat_Expert"
#define TITLE		0
#define COUNTRY	1
#define DATE		2
#define TIME		3
#define IMPACT		4
#define FORECAST	5
#define PREVIOUS	6
#define USED      7

#property icon "photo.ico"
#resource "picture.bmp"  

#include <Arrays\ArrayInt.mqh>
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
enum ENUM_ORDERTYPES
  {
   Sell=0,// Sell Only
   Buy=1,//Buy Only
   SellAndBuy=2,// Sell and Buy
  };

//+------------------------------------------------------------------+
//| Setup                                               
//+------------------------------------------------------------------+ 

extern string  Header1="**************************************************************************************"; // ---------- Order Rules -----------
extern  ENUM_ORDERTYPES OrderTypes=2;
extern double OrdersMultiplier=2;
extern int MaxOrdersBuy=4; // Max Positions Buy at a time, then resets
extern int MaxOrdersSell=4;// Max Positions Sell at a time, then resets
extern string  Header3="**************************************************************************************"; // ---------- Moving Average Rules -----------
extern int     MA=200; // Moving Average 
extern ENUM_APPLIED_PRICE MAAppliedPrice=0; // Moving Average Applied Price
extern ENUM_MA_METHOD MAMethod=0; // Moving Average Method
extern ENUM_TIMEFRAMES MATimeFrame=0; // Moving Average Timeframe
extern string  Header4="**************************************************************************************"; //---------- Orders Rules  -----------
extern double  Lot=0.01; // Lot
extern double  LotMultiplier=2; // Lot Multiplier
extern double  LotMax=0.08; //Lot Max 
extern bool IsMultiplyTarget=true; // Multiply Small Target when multiplying Lot??
extern string  Header5="**************************************************************************************"; //---------- TP and SL Rules -----------
extern double TakeProfit=10;
extern double StopLoss=10;
extern string  Header6="**************************************************************************************"; // ---------- Working Times Settings-----------
extern bool    useWorkingTimer1=false; // Use First Timer
extern string  Note200="Please use 24Hour format, between 0 and 24"; // --- 
extern int     Start1=0;
extern int     End1=6;
extern string  Note056=""; // --------------------------- 
extern bool    useWorkingTimer2=false; // Use Second Timer
extern string  Note2012="Please use 24Hour format, between 0 and 24"; // --- 
extern int     Start2=0;
extern int     End2=6;
extern string  Header7="**************************************************************************************"; // ---------- News Settings-----------
extern bool UseNews=false;
extern bool    IncludeHigh       = true;                 // Include high
extern bool    IncludeMedium     = true;                 // Include medium
extern bool    IncludeLow        = true;                 // Include low
extern bool    IncludeSpeaks     = true;                 // Include speaks
extern bool    IncludeHolidays   = false;                // Include holidays
extern string  FindKeyword       = "";                   // Find keyword
extern string  IgnoreKeyword     = "";                   // Ignore keyword
extern int  BeforeTimer= 15; // Time Before News in Minutes
extern int  AfterTimer = 15; // Time After News in Minutes
extern string  Header8="**************************************************************************************"; // ----------Set Max Profit Limit-----------
extern bool    IsSmallProfitLimitActivated=true;
extern double  SmallProfitTarget=7; // Small Profit Limit in Currency
extern string  Note1=""; // --------------------------- 
extern bool    IsBigProfitLimitActivated=false;
extern double  BigProfitTarget=50; // Small Profit Limit in Currency
extern string  Header9="**************************************************************************************"; // ---------- Loss Target-----------
extern double LossTarget=5; // Loss Target in Currency
extern string  Header10="**************************************************************************************"; // ----------Trailing Stop Settings-----------
extern bool    UseTrailingStops=false;
extern double  TrailingStopOffset=30;
extern string  Header11="**************************************************************************************"; // ----------Breakeven Settings-----------
extern bool    UseBreakeven=false;
extern double  BreakevenOffset=25;
extern string  Header12="**************************************************************************************"; // ----------Chart Info -----------
extern int     xDistance=1; //Chart info distance from top right corner
extern string  Header13="**************************************************************************************"; // ----------EA General -----------
extern int     MagicNumber=656668;
extern bool    OnJournaling=true; // Add EA updates in the Journal Tab

string  InternalHeader1="----------Errors Handling Settings-----------";

int     RetryInterval=100; // Pause Time before next retry (in milliseconds)
int     MaxRetriesPerTick=10;
int     Slippage=3;

string  InternalHeader2="----------Service Variables-----------";

//+------------------------------------------------------------------+
//|General Variables                                                                 |
//+------------------------------------------------------------------+
int P,YenPairAdjustFactor;

CArrayInt Orders;

int OrderNumber;

double TrailingStopList[][2];

double BreakevenList[][2];

int lastSignal;
bool canTrade=true;

double MA1_0;

static datetime now;
datetime today;

int currentOrdersBuy;
double currentLotBuy;

int currentOrdersSell;
double currentLotSell;

double smallTarget;
double bigTarget;
double lossTarget;

int Current_Time1;
int Current_Time2;

//+------------------------------------------------------------------+
//| News Settings                                                                 |
//+------------------------------------------------------------------+
string xmlFileName;
string sData;
string Event[][8];
string eTitle[10],eCountry[10],eImpact[10],eForecast[10],ePrevious[10];
int eMinutes[10];
datetime eTime[10];
int anchor,x0,x1,x2,xf,xp;
int Factor;
//--- Alert
bool FirstAlert;
bool SecondAlert;
datetime AlertTime;
//--- time
datetime xmlModifed;
int TimeOfDay;
datetime Midnight;
bool IsEvent;
int UpdateHour=4;
datetime closestNews;
int EventIndex=0;

//+-----------------------------------------------------------------+
//|Chart Display Variables                                                                  |
//+------------------------------------------------------------------+
double buyLastClosedProfit=0;
double sellLastClosedProfit=0;
double cycleProfit; //Get All Opened Profit
double buyCycleProfit;
double sellCycleProfit;
double cycleLots;
double buyCycleLots;    //Get All Buy  Opened Lots
double sellCycleLots;    //Get All Sell Opened Lots
string ObjName=IntegerToString(ChartID(),0,' ');   //The global object name 
double netProfitSince;
double netProfitSinceStart;
//+------------------------------------------------------------------+
//|//Pip Display Variables                                           |
//+------------------------------------------------------------------+
double pipValue;
double buyPipValue;
double sellPipValue;
double pipValue1Lot;

double profitSinceLastCycle;
//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
int init()
  {

//News Settings

//--- check for DLL
   if(UseNews)
     {
      if(!TerminalInfoInteger(TERMINAL_DLLS_ALLOWED))
        {
         Alert(INAME+": Please Allow DLL Imports!");
         return(INIT_FAILED);
        }

      TimeOfDay=(int)TimeLocal()%86400;
      Midnight=TimeLocal()-TimeOfDay;

      xmlFileName=INAME+"-ffcal_week_this.xml";
      //--- checks the existence of the file.
      if(!FileIsExist(xmlFileName))
        {
         xmlDownload();
         xmlRead();
        }
      else
        {
         xmlRead();
        }

      xmlModifed=(datetime)FileGetInteger(xmlFileName,FILE_MODIFY_DATE,false);

      if(xmlModifed<TimeLocal()-(UpdateHour*3600))
        {
         Print(INAME+": xml file is out of date");
         xmlUpdate();
        }
      //--- set timer to update old xml file every x hours  
      else EventSetTimer(UpdateHour*3600);

      //RedefineEventArray();
      ArrayResize(Event,3,0);
      
      Event[0][TITLE]="Test1";
      Event[1][TITLE]="Test2";
      Event[2][TITLE]="Test3";


      Event[0][USED]=true;
      Event[1][USED]=true;
      Event[2][USED]=true;

      Event[0][DATE]=D'2020.01.01 02:00';
      Event[1][DATE]=D'2020.01.01 03:00';
      Event[2][DATE]=D'2020.01.01 04:00';


      for(int i=2; i>=0; i--)
        {
         if(Event[i][USED]==true)
           {
            Print(i+" : "+Event[i][TITLE]+" , "+Event[i][DATE]);
           }
        }
      Print(" *** News List *** ");

     }
//News Settings

   Comment("Copyright 2020 HelmiFX.");

   DrawChartInfo();            //Drawing Chart Info
   P=GetP(); // To account for 5 digit brokers. Used to convert pips to decimal place
   YenPairAdjustFactor=GetYenAdjustFactor(); // Adjust for YenPair
   ChartSettings();

   if(UseTrailingStops) ArrayResize(TrailingStopList,1000,0);
   if(UseBreakeven) ArrayResize(BreakevenList,1000,0);

   now=Time[0];
   today=Time[0];

   currentLotBuy=Lot;
   currentOrdersBuy=1;

   currentLotSell=Lot;
   currentOrdersSell=1;

   smallTarget=SmallProfitTarget;
   bigTarget=BigProfitTarget;
   lossTarget=LossTarget;

   closestNews=GetClosestNews();

   return(INIT_SUCCEEDED);


  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+

void OnTimer()
  {
//--- 
   Print(INAME+": xml file is out of date");
   xmlUpdate();
//---
  }
//+------------------------------------------------------------------+
//| Download XML                                                         |
//+------------------------------------------------------------------+

void xmlDownload()
  {
//---
   ResetLastError();
   string sUrl="https://cdn-nfs.faireconomy.media/ff_calendar_thisweek.xml";
   string FilePath;
   if(IsTesting())
     {
      FilePath=StringConcatenate(TerminalInfoString(TERMINAL_DATA_PATH),"\\tester\\files\\",xmlFileName);
     }
   else
     {
      FilePath=StringConcatenate(TerminalInfoString(TERMINAL_DATA_PATH),"\\MQL4\\files\\",xmlFileName);
     }

   int FileGet=URLDownloadToFileW(NULL,sUrl,FilePath,0,NULL);
   if(FileGet==0) PrintFormat(INAME+": %s file downloaded successfully!",xmlFileName);
//--- check for errors   
   else PrintFormat(INAME+": failed to download %s file, Error code = %d",xmlFileName,GetLastError());
//---
  }
//+------------------------------------------------------------------+
//| Read the XML file                                                |
//+------------------------------------------------------------------+
void xmlRead()
  {
//---
   ResetLastError();
   int FileHandle=FileOpen(xmlFileName,FILE_BIN|FILE_READ);
   if(FileHandle!=INVALID_HANDLE)
     {
      //--- receive the file size 
      ulong size=FileSize(FileHandle);
      //--- read data from the file
      while(!FileIsEnding(FileHandle))
         sData=FileReadString(FileHandle,(int)size);
      //--- close
      FileClose(FileHandle);
     }
//--- check for errors   
   else PrintFormat(INAME+": failed to open %s file, Error code = %d",xmlFileName,GetLastError());
//---
  }
//+------------------------------------------------------------------+
//| Check for update XML                                             |
//+------------------------------------------------------------------+
void xmlUpdate()
  {
//--- do not download on saturday
   if(TimeDayOfWeek(Midnight)==6) return;
   else
     {
      Print(INAME+": check for updates...");
      Print(INAME+": delete old file");
      FileDelete(xmlFileName);
      xmlDownload();
      xmlRead();
      xmlModifed=(datetime)FileGetInteger(xmlFileName,FILE_MODIFY_DATE,true);
      PrintFormat(INAME+": updated successfully! last modified: %s",(string)xmlModifed);

      //RedefineEventArray();
     }
//---
  }
//+------------------------------------------------------------------+
//|  Check for update XML                                                                 |
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+

void OnDeinit(const int reason)
  {
//---
   if(reason!=5 && reason!=3)
     {
      ObjectsDeleteAll(0,0,OBJ_LABEL);
      ObjectsDeleteAll(0,0,OBJ_RECTANGLE_LABEL);
      ObjectsDeleteAll(0,0,OBJ_ARROW);
      ObjectsDeleteAll(ChartID(),0);
     }

   EventKillTimer();

  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
int start()
  {
   cycleProfit=GetCycleProfit(0)+GetCycleProfit(1);               //Set Cycle Profits onto the chart.
   buyCycleProfit=GetCycleProfit(0);
   sellCycleProfit=GetCycleProfit(1);
   buyCycleLots=GetCycleLots(0);                //Set Cycle Lots onto the chart.
   sellCycleLots=GetCycleLots(1);
   cycleLots=GetCycleLots(0)+GetCycleLots(1);
   netProfitSince=GetNetProfitSince(now,OP_BUY)+GetNetProfitSince(now,OP_SELL);
   netProfitSinceStart=GetNetProfitSince(today,OP_BUY)+GetNetProfitSince(today,OP_SELL);
   pipValue=(((MarketInfo(Symbol(),MODE_TICKVALUE)*Point*P)/MarketInfo(Symbol(),MODE_TICKSIZE))*cycleLots);// pip value of 1 lot
   buyPipValue=(((MarketInfo(Symbol(),MODE_TICKVALUE)*Point*P)/MarketInfo(Symbol(),MODE_TICKSIZE))*buyCycleLots);// pip value of 1 lot
   sellPipValue=(((MarketInfo(Symbol(),MODE_TICKVALUE)*Point*P)/MarketInfo(Symbol(),MODE_TICKSIZE))*sellCycleLots);// pip value of 1 lot
   pipValue1Lot=(((MarketInfo(Symbol(),MODE_TICKVALUE)*Point*P)/MarketInfo(Symbol(),MODE_TICKSIZE))*1);// pip value of 1 lot
   buyLastClosedProfit = GetClosedProfitSince(today,OP_BUY);
   sellLastClosedProfit= GetClosedProfitSince(today,OP_SELL);
   UpdateChartInfo();

   if((IsBigProfitLimitBreached(IsBigProfitLimitActivated,bigTarget,OnJournaling)==true || !canTrade) || (IsProfitLimitBreached(IsSmallProfitLimitActivated,smallTarget,OnJournaling)==true || !canTrade))
     {
      return 0;
     }

//----------Entry & Exit Variables-----------

   MA1_0=iMA(Symbol(),MATimeFrame,MA,0,MAMethod,MAAppliedPrice,0);

//----------TP & SL Variables-----------

   if(UseTrailingStops)
     {
      UpdateTrailingList(OnJournaling,RetryInterval,MagicNumber);
      ReviewTrailingStop(OnJournaling,TrailingStopOffset,RetryInterval,MagicNumber,P);
     }

   if(UseBreakeven)
     {
      UpdateBreakevenList(OnJournaling,RetryInterval,MagicNumber);
      ReviewBreakeven(OnJournaling,BreakevenOffset,RetryInterval,MagicNumber,P);
     }

//---------- Timer and News Timer-----------

   if(useWorkingTimer1 || useWorkingTimer2)
     {
      if(((useWorkingTimer1 && !isItTime1()) || !useWorkingTimer1) && ((useWorkingTimer2 && !isItTime2()) || !useWorkingTimer2))
        {
         CheckSLTPExit(true);

         return(0);
        }
     }

   if(UseNews)
     {
      closestNews=GetClosestNews();
      if((closestNews!=0 && (TimeCurrent()<=closestNews -(BeforeTimer*60) || TimeCurrent()>=closestNews+(AfterTimer*60)) || closestNews==0))
        {
         CheckSLTPExit(true);

         return(0);
        }
     }

//----------Exit Rules (All Opened Positions)-----------

   CheckSLTPExit(false);
////---------- Is New Bar -----------
//
//   if(!isNewBar())
//     {
//      return(0);
//     }

//----------Entry Rules (Market) -----------

   if(CountPosOrders(MagicNumber,OP_BUY)+CountPosOrders(MagicNumber,OP_SELL)==0)
     {
      if(OrderTypes!=0)
        {
         if(EntrySignal()==1)
           { // Open Long Positions

            for(int i=0;i<currentOrdersBuy;i++)
              {
               OrderNumber=OpenPositionMarket(OP_BUY,currentLotBuy,StopLoss,TakeProfit,MagicNumber,Slippage,OnJournaling,P,MaxRetriesPerTick);
               if(UseTrailingStops) SetTrailingStop(OnJournaling,RetryInterval,MagicNumber,P,OrderNumber);
               if(UseBreakeven) SetBreakeven(OnJournaling,RetryInterval,MagicNumber,P,OrderNumber);
               Orders.Add(OrderNumber);
              }

            if(currentOrdersBuy*OrdersMultiplier>MaxOrdersBuy)
              {
               currentOrdersBuy=1;
              }
            else
              {
               currentOrdersBuy*=OrdersMultiplier;
              }

            currentOrdersSell=1;
           }
        }

      if(OrderTypes!=1)
        {
         if(EntrySignal()==2)
           { // Open Short Positions

            for(int i=0;i<currentOrdersSell;i++)
              {
               OrderNumber=OpenPositionMarket(OP_SELL,currentLotSell,StopLoss,TakeProfit,MagicNumber,Slippage,OnJournaling,P,MaxRetriesPerTick);
               if(UseTrailingStops) SetTrailingStop(OnJournaling,RetryInterval,MagicNumber,P,OrderNumber);
               if(UseBreakeven) SetBreakeven(OnJournaling,RetryInterval,MagicNumber,P,OrderNumber);
               Orders.Add(OrderNumber);
              }

            if(currentOrdersSell*OrdersMultiplier>MaxOrdersSell)
              {
               currentOrdersSell=1;
              }
            else
              {
               currentOrdersSell*=OrdersMultiplier;
              }

            currentOrdersBuy=1;

           }
        }
     }

   return (0);
  }
//+------------------------------------------------------------------+   
//|End of Start()                                                        |
//+------------------------------------------------------------------+


//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//|                     FUNCTIONS LIBRARY                                   
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

/*

Content:
1) EntrySignal
2) ExitSignal
3) GetLot
4) CheckLot
5) CountPosOrders
6) IsMaxPositionsReached
7) SARAboveOrBelowPrice
8)	CloseAboveOrBelowEMA
9) OpenPositionMarket
10) CloseOrderPosition
11) GetP
12) GetYenAdjustFactor
13) UpdateTrailingList
14) ReviewTrailingStop
15) SetTrailingStop
16) GetSellStopLoss
17) GetBuyStopLoss
18) GetSellTakeProfit
19) GetBuyTakeProfit
20) SARCross
21) IsLossTargetBreached
22) HandleTradingEnvironment
23) GetErrorDescription
24) CheckAccount
*/

//+------------------------------------------------------------------+
// Start of EntrySignal()                                    			|
//+------------------------------------------------------------------+
int EntrySignal()
  {
// Type: Customisable 
// Modify this function to suit your trading robot

// This function checks for entry signals

   int output=0;

   if(Bid>MA1_0)
     {
      output=1;
     }

   if(Bid<MA1_0)
     {
      output=2;
     }

   return (output);
  }
//+------------------------------------------------------------------+
// End of EntrySignal()                                              |
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
// Start of GetLot()                                       				|
//+------------------------------------------------------------------+
double GetLot(double FixedLots)
  {
// Type: Customisable 
// Modify this function to suit your trading robot

// This is our sizing algorithm

   double output;

   output=FixedLots;

   output=NormalizeDouble(output,2); // Round to 2 decimal place
   return(output);
  }
//+------------------------------------------------------------------+
// End of GetLot()                                                   |
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
// Start of CheckLot()                                       				|
//+------------------------------------------------------------------+

double CheckLot(double lot,bool Journaling)
  {
// Type: Fixed Template 
// Do not edit unless you know what you're doing

// This function checks if our Lots to be trade satisfies any broker limitations

   double LotToOpen=0;
   LotToOpen=NormalizeDouble(lot,2);
   LotToOpen=MathFloor(LotToOpen/MarketInfo(Symbol(),MODE_LOTSTEP))*MarketInfo(Symbol(),MODE_LOTSTEP);

   if(LotToOpen<MarketInfo(Symbol(),MODE_MINLOT))LotToOpen=MarketInfo(Symbol(),MODE_MINLOT);
   if(LotToOpen>MarketInfo(Symbol(),MODE_MAXLOT))LotToOpen=MarketInfo(Symbol(),MODE_MAXLOT);
   LotToOpen=NormalizeDouble(LotToOpen,2);

   if(Journaling && LotToOpen!=lot)Print("EA Journaling: Trading Lot has been changed by CheckLot function. Requested lot: "+DoubleToString(lot)+". Lot to open: "+DoubleToString(LotToOpen));

   return(LotToOpen);
  }
//+------------------------------------------------------------------+
//| End of CheckLot()
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//| Start of GetCycleLots()                                                             |
//+------------------------------------------------------------------+
double GetCycleLots(int type)
  {

// Type: Customisable 
// Modify this function to suit your trading robot

// this function returns the net profit of the opened orders 

   double Lots=0;

   for(int i=0; i<OrdersTotal(); i++)
     {
      if(OrderSelect(i,SELECT_BY_POS,MODE_TRADES)==true && OrderSymbol()==Symbol() && (OrderMagicNumber()==MagicNumber) && OrderType()==type)
         Lots+=OrderLots();
     }
   return(NormalizeDouble(Lots,2));

  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
// End of GetCycleLots()                                    		|
//+------------------------------------------------------------------+
double GetCycleProfit(int type)
  {

// Type: Customisable 
// Modify this function to suit your trading robot

// this function returns the net profit of the opened orders 

   double Profit=0;

   for(int i=0; i<OrdersTotal(); i++)
     {
      if(OrderSelect(i,SELECT_BY_POS,MODE_TRADES)==true && OrderSymbol()==Symbol() && (OrderMagicNumber()==MagicNumber) && OrderType()==type)
         Profit+=OrderProfit();
     }
   return(NormalizeDouble(Profit,2));
  }
//+------------------------------------------------------------------+
// End of GetCycleLots()                                    		|
//+------------------------------------------------------------------+
double GetNetProfitSince(datetime time,int ordertype)
  {

// Type: Customisable 
// Modify this function to suit your trading robot

// this function returns the net profit of the opened orders 

   double Profit=0;

   for(int i=OrdersTotal()-1; i>=0; i--)
     {
      if(OrderSelect(i,SELECT_BY_POS,MODE_TRADES)==true && OrderSymbol()==Symbol() && (OrderMagicNumber()==MagicNumber) && OrderOpenTime()>time && OrderType()==ordertype)
         Profit=Profit+OrderProfit();
     }

   for(int i=OrdersHistoryTotal()-1; i>=0; i--)
     {
      if(OrderSelect(i,SELECT_BY_POS,MODE_HISTORY)==true && OrderSymbol()==Symbol() && (OrderMagicNumber()==MagicNumber) && OrderOpenTime()>time && OrderType()==ordertype)
         Profit=Profit+OrderProfit()+OrderSwap()+OrderCommission();
     }
   return(Profit);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
// End of GetCycleLots()                                    		|
//+------------------------------------------------------------------+
double GetClosedProfitSince(datetime time,int ordertype)
  {

// Type: Customisable 
// Modify this function to suit your trading robot

// this function returns the net profit of the opened orders 

   double Profit=0;

   for(int i=OrdersHistoryTotal()-1; i>=0; i--)
     {
      if(OrderSelect(i,SELECT_BY_POS,MODE_HISTORY)==true && OrderSymbol()==Symbol() && (OrderMagicNumber()==MagicNumber) && OrderOpenTime()>time && OrderType()==ordertype)
         Profit=Profit+OrderProfit()+OrderSwap()+OrderCommission();
     }
   return(Profit);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
// End of GetCycleProfit()                                    			|
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|Start of CountPosOrders()	" Count Positions"
//+------------------------------------------------------------------+
int CountPosOrders(int Magic,int TYPE)
  {
// Type: Fixed Template 
// Do not edit unless you know what you're doing

// This function counts number of positions/orders of OrderType TYPE

   int orders=0;
   for(int i=0; i<OrdersTotal(); i++)
     {
      if(OrderSelect(i,SELECT_BY_POS,MODE_TRADES)==true && OrderSymbol()==Symbol() && OrderMagicNumber()==Magic && OrderType()==TYPE)
         orders++;
     }
   return(orders);

  }
//+------------------------------------------------------------------+
//| End of CountPosOrders() " Count Positions"
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//| Start of IsMaxPositionsReached()                                             
//+------------------------------------------------------------------+
bool IsMaxPositionsReached(int MaxPositions,int Magic,bool Journaling)
  {
// Type: Fixed Template 
// Do not edit unless you know what you're doing 

// This function checks the number of positions we are holding against the maximum allowed 

   bool result=False;
   if(CountPosOrders(Magic,OP_BUY)+CountPosOrders(Magic,OP_SELL)>MaxPositions)
     {
      result=True;
      if(Journaling)Print("Max Orders Exceeded");
        } else if(CountPosOrders(Magic,OP_BUY)+CountPosOrders(Magic,OP_SELL)==MaxPositions) {
      result=True;
     }

   return(result);

  }
//+------------------------------------------------------------------+
//| End of IsMaxPositionsReached()                                                
//+------------------------------------------------------------------+
void CloseOnProfit(int orderType)
  {
   int total=OrdersTotal();
   for(int i=total-1; i>=0; i--)
     {
      if(OrderSelect(i,SELECT_BY_POS,MODE_TRADES)==true && OrderSymbol()==Symbol() && OrderMagicNumber()==MagicNumber && OrderType()==orderType)
        {
         if(OrderProfit()>0)
           {
            bool Closing=false;
            double Price=0;
            color arrow_color=0;if(OrderType()==OP_BUY)arrow_color=MediumSeaGreen;if(OrderType()==OP_SELL)arrow_color=DarkOrange;
            if(OnJournaling)Print("EA Journaling: Trying to close position "+OrderTicket()+" ...");
            HandleTradingEnvironment(OnJournaling,RetryInterval);
            if(OrderType()==OP_BUY)Price=Bid; if(OrderType()==OP_SELL)Price=Ask;
            Closing=OrderClose(OrderTicket(),OrderLots(),Price,Slippage*P,arrow_color);
            if(OnJournaling && !Closing)Print("EA Journaling: Unexpected Error has happened. Error Description: "+GetErrorDescription(GetLastError()));
            if(OnJournaling && Closing)Print("EA Journaling: Position successfully closed. Profit Order");
           }
        }
     }
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void CloseOnLoss(int orderType)
  {
   int total=OrdersTotal();
   for(int i=total-1; i>=0; i--)
     {
      if(OrderSelect(i,SELECT_BY_POS,MODE_TRADES)==true && OrderSymbol()==Symbol() && OrderMagicNumber()==MagicNumber && OrderType()==orderType)
        {
         if(OrderProfit()<=0)
           {
            bool Closing=false;
            double Price=0;
            color arrow_color=0;if(OrderType()==OP_BUY)arrow_color=MediumSeaGreen;if(OrderType()==OP_SELL)arrow_color=DarkOrange;
            if(OnJournaling)Print("EA Journaling: Trying to close position "+OrderTicket()+" ...");
            HandleTradingEnvironment(OnJournaling,RetryInterval);
            if(OrderType()==OP_BUY)Price=Bid; if(OrderType()==OP_SELL)Price=Ask;
            Closing=OrderClose(OrderTicket(),OrderLots(),Price,Slippage*P,arrow_color);
            if(OnJournaling && !Closing)Print("EA Journaling: Unexpected Error has happened. Error Description: "+GetErrorDescription(GetLastError()));
            if(OnJournaling && Closing)Print("EA Journaling: Position successfully closed. Loss Order");
           }
        }
     }
  }
//+------------------------------------------------------------------+
//|Start of OpenPositionMarket()
//+------------------------------------------------------------------+
int OpenPositionMarket(int TYPE,double LOT,double SL,double TP,int Magic,int Slip,bool Journaling,int K,int Max_Retries_Per_Tick)
  {
// Type: Fixed Template 
// Do not edit unless you know what you're doing 

// This function submits new orders

   int tries=0;
   string symbol=Symbol();
   int cmd=TYPE;
   double volume=CheckLot(LOT,Journaling);
   if(MarketInfo(symbol,MODE_MARGINREQUIRED)*volume>AccountFreeMargin())
     {
      Print("Can not open a trade. Not enough free margin to open "+volume+" on "+symbol);
      return(-1);
     }
   int slippage=Slip*K; // Slippage is in points. 1 point = 0.0001 on 4 digit broker and 0.00001 on a 5 digit broker
   string comment=" "+TYPE+"(#"+Magic+")";
   int magic=Magic;
   datetime expiration=0;
   color arrow_color=0;if(TYPE==OP_BUY)arrow_color=DodgerBlue;if(TYPE==OP_SELL)arrow_color=DeepPink;
   double stoploss=0;
   double takeprofit=0;
   double initTP = TP;
   double initSL = SL;
   int Ticket=-1;
   double price=0;

   while(tries<Max_Retries_Per_Tick) // Edits stops and take profits before the market order is placed
     {
      RefreshRates();
      if(TYPE==OP_BUY)price=Ask;if(TYPE==OP_SELL)price=Bid;

      // Sets Take Profits and Stop Loss. Check against Stop Level Limitations.
      if(TYPE==OP_BUY && SL!=0)
        {
         stoploss=NormalizeDouble(Ask-SL*K*Point,Digits);
         if(Bid-stoploss<=MarketInfo(Symbol(),MODE_STOPLEVEL)*Point)
           {
            stoploss=NormalizeDouble(Bid-MarketInfo(Symbol(),MODE_STOPLEVEL)*Point,Digits);
            if(Journaling)Print("EA Journaling: Stop Loss changed from "+initSL+" to "+MarketInfo(Symbol(),MODE_STOPLEVEL)/K+" pips");
           }
        }
      if(TYPE==OP_SELL && SL!=0)
        {
         stoploss=NormalizeDouble(Bid+SL*K*Point,Digits);
         if(stoploss-Ask<=MarketInfo(Symbol(),MODE_STOPLEVEL)*Point)
           {
            stoploss=NormalizeDouble(Ask+MarketInfo(Symbol(),MODE_STOPLEVEL)*Point,Digits);
            if(Journaling)Print("EA Journaling: Stop Loss changed from "+initSL+" to "+MarketInfo(Symbol(),MODE_STOPLEVEL)/K+" pips");
           }
        }
      if(TYPE==OP_BUY && TP!=0)
        {
         takeprofit=NormalizeDouble(Ask+TP*K*Point,Digits);
         if(takeprofit-Bid<=MarketInfo(Symbol(),MODE_STOPLEVEL)*Point)
           {
            takeprofit=NormalizeDouble(Ask+MarketInfo(Symbol(),MODE_STOPLEVEL)*Point,Digits);
            if(Journaling)Print("EA Journaling: Take Profit changed from "+initTP+" to "+MarketInfo(Symbol(),MODE_STOPLEVEL)/K+" pips");
           }
        }
      if(TYPE==OP_SELL && TP!=0)
        {
         takeprofit=NormalizeDouble(Bid-TP*K*Point,Digits);
         if(Ask-takeprofit<=MarketInfo(Symbol(),MODE_STOPLEVEL)*Point)
           {
            takeprofit=NormalizeDouble(Bid-MarketInfo(Symbol(),MODE_STOPLEVEL)*Point,Digits);
            if(Journaling)Print("EA Journaling: Take Profit changed from "+initTP+" to "+MarketInfo(Symbol(),MODE_STOPLEVEL)/K+" pips");
           }
        }
      if(Journaling)Print("EA Journaling: Trying to place a market order...");
      HandleTradingEnvironment(Journaling,RetryInterval);
      Ticket=OrderSend(symbol,cmd,volume,price,slippage,stoploss,takeprofit,comment,magic,expiration,arrow_color);
      if(Ticket>0)break;
      tries++;
     }

   if(Journaling && Ticket<0)Print("EA Journaling: Unexpected Error has happened. Error Description: "+GetErrorDescription(GetLastError()));
   if(Journaling && Ticket>0)
     {
      Print("EA Journaling: Order successfully placed. Ticket: "+Ticket);
     }
   return(Ticket);
  }
//+------------------------------------------------------------------+
//|End of OpenPositionMarket()
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|Start of CloseOrderPosition()
//+------------------------------------------------------------------+
bool CloseOrderPosition(int TYPE,bool Journaling,int Magic,int Slip,int K)
  {
// Type: Fixed Template 
// Do not edit unless you know what you're doing
   int ordersPos=OrdersTotal();

   for(int i=ordersPos-1; i>=0; i--)
     {
      // Note: Once pending orders become positions, OP_BUYLIMIT AND OP_BUYSTOP becomes OP_BUY, OP_SELLLIMIT and OP_SELLSTOP becomes OP_SELL
      if(TYPE==OP_BUY || TYPE==OP_SELL)
        {
         if(OrderSelect(i,SELECT_BY_POS,MODE_TRADES)==true && OrderSymbol()==Symbol() && OrderMagicNumber()==Magic && OrderType()==TYPE)
           {
            bool Closing=false;
            double Price=0;
            color arrow_color=0;if(TYPE==OP_BUY)arrow_color=MediumSeaGreen;if(TYPE==OP_SELL)arrow_color=DarkOrange;
            if(Journaling)Print("EA Journaling: Trying to close position "+OrderTicket()+" ...");
            HandleTradingEnvironment(Journaling,RetryInterval);
            if(TYPE==OP_BUY)Price=Bid; if(TYPE==OP_SELL)Price=Ask;
            Closing=OrderClose(OrderTicket(),OrderLots(),Price,Slip*K,arrow_color);
            if(Journaling && !Closing)Print("EA Journaling: Unexpected Error has happened. Error Description: "+GetErrorDescription(GetLastError()));
            if(Journaling && Closing)Print("EA Journaling: Position successfully closed.");
           }
        }
     }
   if(CountPosOrders(Magic, TYPE)==0)return(true); else return(false);
  }
//+------------------------------------------------------------------+
//| End of CloseOrderPosition()
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
// Start of getP()                                                   |
//+------------------------------------------------------------------+

int GetP()
  {

   int output;

   if(Digits==5 || Digits==3) output=10;else output=1;

   return(output);
  }
//+------------------------------------------------------------------+
// End of GetP()                                                    	|
//+------------------------------------------------------------------+


//+------------------------------------------------------------------+
// Start of GetYenAdjustFactor()                                     |
//+------------------------------------------------------------------+

int GetYenAdjustFactor()
  {

   int output=1;

   if(Digits==3 || Digits==2) output=100;

   return(output);

  }
//+------------------------------------------------------------------+
// End of GetYenAdjustFactor()                                       |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//| Start of UpdateTrailingList()                              
//+------------------------------------------------------------------+

void UpdateTrailingList(bool Journaling,int Retry_Interval,int Magic)
  {
// Type: Fixed Template 
// Do not edit unless you know what you're doing

// This function clears the elements of your VolTrailingList if the corresponding positions has been closed

   int ordersPos=OrdersTotal();
   int orderTicketNumber;
   bool doesPosExist;

// Check the VolTrailingList, match with current list of positions. Make sure the all the positions exists. 
// If it doesn't, it means there are positions that have been closed

   for(int x=0; x<ArrayRange(TrailingStopList,0); x++)
     { // Looping through all order number in list

      doesPosExist=False;
      orderTicketNumber=TrailingStopList[x,0];

      if(orderTicketNumber!=0)
        { // Order exists
         for(int y=ordersPos-1; y>=0; y--)
           { // Looping through all current open positions
            if(OrderSelect(y,SELECT_BY_POS,MODE_TRADES)==true && OrderSymbol()==Symbol() && OrderMagicNumber()==Magic)
              {
               if(orderTicketNumber==OrderTicket())
                 { // Checks order number in list against order number of current positions
                  doesPosExist=True;
                  break;
                 }
              }
           }

         if(doesPosExist==False)
           { // Deletes elements if the order number does not match any current positions
            TrailingStopList[x,0] = 0;
            TrailingStopList[x,1] = 0;
           }
        }
     }
  }
//+------------------------------------------------------------------+
//| End of UpdateTrailingList                                        
//+------------------------------------------------------------------+



void ReviewTrailingStop(bool Journaling,double TrailingStop_Offset,int Retry_Interval,int Magic,int K)
  {
// Type: Fixed Template 
// Do not edit unless you know what you're doing 

// This function updates volatility trailing stops levels for all positions (using OrderModify) if appropriate conditions are met

   bool doesTrailingRecordExist;
   int posTicketNumber;
   for(int i=OrdersTotal()-1; i>=0; i--)
     { // Looping through all orders

      if(OrderSelect(i,SELECT_BY_POS,MODE_TRADES)==true && OrderSymbol()==Symbol() && OrderMagicNumber()==Magic)
        {
         doesTrailingRecordExist=False;
         posTicketNumber=OrderTicket();
         for(int x=0; x<ArrayRange(TrailingStopList,0); x++)
           { // Looping through all order number in list 

            if(posTicketNumber==TrailingStopList[x,0])
              { // If condition holds, it means the position have a volatility trailing stop level attached to it

               doesTrailingRecordExist=True;
               bool Modify=false;
               RefreshRates();

               // We update the volatility trailing stop record using OrderModify.
               if(OrderType()==OP_BUY && (Bid-TrailingStopList[x,1]>(TrailingStop_Offset*K*Point)))
                 {
                  if(TrailingStopList[x,1]!=Bid-(TrailingStop_Offset*K*Point))
                    {
                     // if(Journaling)Print("EA Journaling: Trying to modify order "+OrderTicket()+" ...");
                     HandleTradingEnvironment(Journaling,Retry_Interval);
                     Modify=OrderModify(OrderTicket(),OrderOpenPrice(),TrailingStopList[x,1],OrderTakeProfit(),0,CLR_NONE);
                     if(Journaling && !Modify)Print("EA Journaling: Unexpected Error has happened. Error Description: "+GetErrorDescription(GetLastError()));
                     if(Journaling && Modify) Print("EA Journaling: Order successfully modified, trailing stop changed.");

                     TrailingStopList[x,1]=Bid-(TrailingStop_Offset*K*Point);
                    }
                 }
               if(OrderType()==OP_SELL && ((TrailingStopList[x,1]-Ask>(TrailingStop_Offset*K*Point))))
                 {
                  if(TrailingStopList[x,1]!=Ask+(TrailingStop_Offset*K*Point))
                    {
                     //if(Journaling)Print("EA Journaling: Trying to modify order "+OrderTicket()+" ...");
                     HandleTradingEnvironment(Journaling,Retry_Interval);
                     Modify=OrderModify(OrderTicket(),OrderOpenPrice(),TrailingStopList[x,1],OrderTakeProfit(),0,CLR_NONE);
                     if(Journaling && !Modify)Print("EA Journaling: Unexpected Error has happened. Error Description: "+GetErrorDescription(GetLastError()));
                     if(Journaling && Modify) Print("EA Journaling: Order successfully modified, trailing stop changed.");
                     TrailingStopList[x,1]=Ask+(TrailingStop_Offset*K*Point);
                    }
                 }
               break;
              }
           }
         // If order does not have a record attached to it. Alert the trader.
         //if(!doesTrailingRecordExist && Journaling) Print("EA Journaling: Error. Order "+posTicketNumber+" has no volatility trailing stop attached to it.");
        }
     }
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//| End of Review Volatility Trailing Stop
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//| Start of SetTrailingStop
//+------------------------------------------------------------------+

void SetTrailingStop(bool Journaling,int Retry_Interval,int Magic,int K,int OrderNum)
  {
// Type: Fixed Template 
// Do not edit unless you know what you're doing 

// This function adds new volatility trailing stop level using OrderModify()
   double trailingStopLossLimit=0;
   bool Modify=False;
   bool IsTrailingStopAdded=False;
   if(OrderSelect(OrderNum,SELECT_BY_TICKET)==true && OrderSymbol()==Symbol() && OrderMagicNumber()==Magic)
     {
      RefreshRates();
      if(OrderType()==OP_BUY || OrderType()==OP_BUYSTOP)
        {
         trailingStopLossLimit=OrderOpenPrice();// virtual stop loss.
         IsTrailingStopAdded=True;
        }
      if(OrderType()==OP_SELL || OrderType()==OP_SELLSTOP)
        {
         trailingStopLossLimit=OrderOpenPrice();// virtual stop loss.
         IsTrailingStopAdded=True;
        }
      // Records trailingStopLossLimit for future use
      if(IsTrailingStopAdded==True)
        {
         for(int x=0; x<ArrayRange(TrailingStopList,0); x++) // Loop through elements in VolTrailingList
           {
            if(TrailingStopList[x,0]==0) // Checks if the element is empty
              {
               TrailingStopList[x,0]=OrderNum; // Add order number
               TrailingStopList[x,1]=trailingStopLossLimit; // Add Trailing Stop into the List
               Modify=true;
               if(Journaling && Modify) Print("Trailing Stop For "+OrderNum+" Has been Set successfully to "+TrailingStopOffset);
               break;
              }
           }
        }
     }

   if(Journaling && !Modify) Print("Couldnt set Trailing Stop For "+OrderNum+" !");
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//| End of SetTrailingStop
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//| Start of UpdateTrailingList()                              
//+------------------------------------------------------------------+

void UpdateBreakevenList(bool Journaling,int Retry_Interval,int Magic)
  {
// Type: Fixed Template 
// Do not edit unless you know what you're doing

// This function clears the elements of your BreakevenList if the corresponding positions has been closed

   int ordersPos=OrdersTotal();
   int orderTicketNumber;
   bool doesPosExist;

// Check the VolTrailingList, match with current list of positions. Make sure the all the positions exists. 
// If it doesn't, it means there are positions that have been closed

   for(int x=0; x<ArrayRange(BreakevenList,0); x++)
     { // Looping through all order number in list

      doesPosExist=False;
      orderTicketNumber=BreakevenList[x,0];

      if(orderTicketNumber!=0)
        { // Order exists
         for(int y=ordersPos-1; y>=0; y--)
           { // Looping through all current open positions
            if(OrderSelect(y,SELECT_BY_POS,MODE_TRADES)==true && OrderSymbol()==Symbol() && OrderMagicNumber()==Magic)
              {
               if(orderTicketNumber==OrderTicket())
                 { // Checks order number in list against order number of current positions
                  doesPosExist=True;
                  break;
                 }
              }
           }

         if(doesPosExist==False)
           { // Deletes elements if the order number does not match any current positions
            BreakevenList[x,0] = 0;
            BreakevenList[x,1] = 0;
           }
        }
     }
  }
//+------------------------------------------------------------------+
//| End of UpdateTrailingList                                        
//+------------------------------------------------------------------+



void ReviewBreakeven(bool Journaling,double Breakeven_Offset,int Retry_Interval,int Magic,int K)
  {
// Type: Fixed Template 
// Do not edit unless you know what you're doing 

// This function updates volatility trailing stops levels for all positions (using OrderModify) if appropriate conditions are met

   bool doesBreakevenRecordExist;
   int posTicketNumber;
   for(int i=OrdersTotal()-1; i>=0; i--)
     { // Looping through all orders

      if(OrderSelect(i,SELECT_BY_POS,MODE_TRADES)==true && OrderSymbol()==Symbol() && OrderMagicNumber()==Magic)
        {
         doesBreakevenRecordExist=False;
         posTicketNumber=OrderTicket();
         for(int x=0; x<ArrayRange(BreakevenList,0); x++)
           { // Looping through all order number in list 

            if(posTicketNumber==BreakevenList[x,0])
              { // If condition holds, it means the position have a volatility trailing stop level attached to it

               doesBreakevenRecordExist=True;
               bool Modify=false;
               RefreshRates();

               // We update the volatility trailing stop record using OrderModify.
               if(OrderType()==OP_BUY && (Bid-BreakevenList[x,1]>(Breakeven_Offset*K*Point)) && BreakevenList[x,1]!=-1)
                 {
                  // if(Journaling)Print("EA Journaling: Trying to modify order "+OrderTicket()+" ...");
                  HandleTradingEnvironment(Journaling,Retry_Interval);
                  Modify=OrderModify(OrderTicket(),OrderOpenPrice(),OrderOpenPrice(),OrderTakeProfit(),0,CLR_NONE);
                  if(Journaling && !Modify)Print("EA Journaling: Unexpected Error has happened. Error Description: "+GetErrorDescription(GetLastError()));
                  if(Journaling && Modify) Print("EA Journaling: Order successfully modified, Breakeven changed.");

                  BreakevenList[x,1]=-1;

                 }
               if(OrderType()==OP_SELL && ((BreakevenList[x,1]-Ask>(Breakeven_Offset*K*Point))) && BreakevenList[x,1]!=-1)
                 {

                  //if(Journaling)Print("EA Journaling: Trying to modify order "+OrderTicket()+" ...");
                  HandleTradingEnvironment(Journaling,Retry_Interval);
                  Modify=OrderModify(OrderTicket(),OrderOpenPrice(),OrderOpenPrice(),OrderTakeProfit(),0,CLR_NONE);
                  if(Journaling && !Modify)Print("EA Journaling: Unexpected Error has happened. Error Description: "+GetErrorDescription(GetLastError()));
                  if(Journaling && Modify) Print("EA Journaling: Order successfully modified, Breakeven changed.");
                  BreakevenList[x,1]=-1;

                 }
               break;
              }
           }

        }
     }
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//| End of Review Volatility Trailing Stop
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//| Start of SetBreakeven
//+------------------------------------------------------------------+

void SetBreakeven(bool Journaling,int Retry_Interval,int Magic,int K,int OrderNum)
  {
// Type: Fixed Template 
// Do not edit unless you know what you're doing 

// This function adds new volatility trailing stop level using OrderModify()
   double BreakevenLimit=0;
   bool Modify=False;
   bool IsBreakevenAdded=False;
   if(OrderSelect(OrderNum,SELECT_BY_TICKET)==true && OrderSymbol()==Symbol() && OrderMagicNumber()==Magic)
     {
      RefreshRates();
      if(OrderType()==OP_BUY || OrderType()==OP_BUYSTOP)
        {
         BreakevenLimit=OrderOpenPrice();// virtual stop loss.
         IsBreakevenAdded=True;
        }
      if(OrderType()==OP_SELL || OrderType()==OP_SELLSTOP)
        {
         BreakevenLimit=OrderOpenPrice();// virtual stop loss.
         IsBreakevenAdded=True;
        }
      // Records trailingStopLossLimit for future use
      if(IsBreakevenAdded==True)
        {
         for(int x=0; x<ArrayRange(BreakevenList,0); x++) // Loop through elements in VolTrailingList
           {
            if(BreakevenList[x,0]==0) // Checks if the element is empty
              {
               BreakevenList[x,0]=OrderNum; // Add order number
               BreakevenList[x,1]=BreakevenLimit; // Add Trailing Stop into the List
               Modify=true;
               if(Journaling && Modify) Print("Breakeven For "+OrderNum+" Has been Set successfully to "+BreakevenOffset);
               break;
              }
           }
        }
     }

   if(Journaling && !Modify) Print("Couldnt set Breakeven For "+OrderNum+" !");
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//| End of SetBreakeven
//+------------------------------------------------------------------+




//+------------------------------------------------------------------+
//| Start of HandleTradingEnvironment()                                         
//+------------------------------------------------------------------+
void HandleTradingEnvironment(bool Journaling,int Retry_Interval)
  {
// Type: Fixed Template 
// Do not edit unless you know what you're doing 

// This function checks for errors

   if(IsTradeAllowed()==true)return;
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
   if(!IsConnected())
     {
      if(Journaling)Print("EA Journaling: Terminal is not connected to server...");
      return;
     }
   if(!IsTradeAllowed() && Journaling)Print("EA Journaling: Trade is not alowed for some reason...");
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
   if(IsConnected() && !IsTradeAllowed())
     {
      while(IsTradeContextBusy()==true)
        {
         if(Journaling)Print("EA Journaling: Trading context is busy... Will wait a bit...");
         Sleep(Retry_Interval);
        }
     }
   RefreshRates();
  }
//+------------------------------------------------------------------+
//| End of HandleTradingEnvironment()                              
//+------------------------------------------------------------------+  
//+------------------------------------------------------------------+
//| Start of GetErrorDescription()                                               
//+------------------------------------------------------------------+
string GetErrorDescription(int error)
  {
// Type: Fixed Template 
// Do not edit unless you know what you're doing

// This function returns the exact error

   string ErrorDescription="";
//---
   switch(error)
     {
      case 0:     ErrorDescription = "NO Error. Everything should be good.";                                    break;
      case 1:     ErrorDescription = "No error returned, but the result is unknown";                            break;
      case 2:     ErrorDescription = "Common error";                                                            break;
      case 3:     ErrorDescription = "Invalid trade parameters";                                                break;
      case 4:     ErrorDescription = "Trade server is busy";                                                    break;
      case 5:     ErrorDescription = "Old version of the client terminal";                                      break;
      case 6:     ErrorDescription = "No connection with trade server";                                         break;
      case 7:     ErrorDescription = "Not enough rights";                                                       break;
      case 8:     ErrorDescription = "Too frequent requests";                                                   break;
      case 9:     ErrorDescription = "Malfunctional trade operation";                                           break;
      case 64:    ErrorDescription = "Account disabled";                                                        break;
      case 65:    ErrorDescription = "Invalid account";                                                         break;
      case 128:   ErrorDescription = "Trade timeout";                                                           break;
      case 129:   ErrorDescription = "Invalid price";                                                           break;
      case 130:   ErrorDescription = "Invalid stops";                                                           break;
      case 131:   ErrorDescription = "Invalid trade volume";                                                    break;
      case 132:   ErrorDescription = "Market is closed";                                                        break;
      case 133:   ErrorDescription = "Trade is disabled";                                                       break;
      case 134:   ErrorDescription = "Not enough money";                                                        break;
      case 135:   ErrorDescription = "Price changed";                                                           break;
      case 136:   ErrorDescription = "Off quotes";                                                              break;
      case 137:   ErrorDescription = "Broker is busy";                                                          break;
      case 138:   ErrorDescription = "Requote";                                                                 break;
      case 139:   ErrorDescription = "Order is locked";                                                         break;
      case 140:   ErrorDescription = "Long positions only allowed";                                             break;
      case 141:   ErrorDescription = "Too many requests";                                                       break;
      case 145:   ErrorDescription = "Modification denied because order too close to market";                   break;
      case 146:   ErrorDescription = "Trade context is busy";                                                   break;
      case 147:   ErrorDescription = "Expirations are denied by broker";                                        break;
      case 148:   ErrorDescription = "Too many open and pending orders (more than allowed)";                    break;
      case 4000:  ErrorDescription = "No error";                                                                break;
      case 4001:  ErrorDescription = "Wrong function pointer";                                                  break;
      case 4002:  ErrorDescription = "Array index is out of range";                                             break;
      case 4003:  ErrorDescription = "No memory for function call stack";                                       break;
      case 4004:  ErrorDescription = "Recursive stack overflow";                                                break;
      case 4005:  ErrorDescription = "Not enough stack for parameter";                                          break;
      case 4006:  ErrorDescription = "No memory for parameter string";                                          break;
      case 4007:  ErrorDescription = "No memory for temp string";                                               break;
      case 4008:  ErrorDescription = "Not initialized string";                                                  break;
      case 4009:  ErrorDescription = "Not initialized string in array";                                         break;
      case 4010:  ErrorDescription = "No memory for array string";                                              break;
      case 4011:  ErrorDescription = "Too long string";                                                         break;
      case 4012:  ErrorDescription = "Remainder from zero divide";                                              break;
      case 4013:  ErrorDescription = "Zero divide";                                                             break;
      case 4014:  ErrorDescription = "Unknown command";                                                         break;
      case 4015:  ErrorDescription = "Wrong jump (never generated error)";                                      break;
      case 4016:  ErrorDescription = "Not initialized array";                                                   break;
      case 4017:  ErrorDescription = "DLL calls are not allowed";                                               break;
      case 4018:  ErrorDescription = "Cannot load library";                                                     break;
      case 4019:  ErrorDescription = "Cannot call function";                                                    break;
      case 4020:  ErrorDescription = "Expert function calls are not allowed";                                   break;
      case 4021:  ErrorDescription = "Not enough memory for temp string returned from function";                break;
      case 4022:  ErrorDescription = "System is busy (never generated error)";                                  break;
      case 4050:  ErrorDescription = "Invalid function parameters count";                                       break;
      case 4051:  ErrorDescription = "Invalid function parameter value";                                        break;
      case 4052:  ErrorDescription = "String function internal error";                                          break;
      case 4053:  ErrorDescription = "Some array error";                                                        break;
      case 4054:  ErrorDescription = "Incorrect series array using";                                            break;
      case 4055:  ErrorDescription = "Custom indicator error";                                                  break;
      case 4056:  ErrorDescription = "Arrays are incompatible";                                                 break;
      case 4057:  ErrorDescription = "Global variables processing error";                                       break;
      case 4058:  ErrorDescription = "Global variable not found";                                               break;
      case 4059:  ErrorDescription = "Function is not allowed in testing mode";                                 break;
      case 4060:  ErrorDescription = "Function is not confirmed";                                               break;
      case 4061:  ErrorDescription = "Send mail error";                                                         break;
      case 4062:  ErrorDescription = "String parameter expected";                                               break;
      case 4063:  ErrorDescription = "Integer parameter expected";                                              break;
      case 4064:  ErrorDescription = "Double parameter expected";                                               break;
      case 4065:  ErrorDescription = "Array as parameter expected";                                             break;
      case 4066:  ErrorDescription = "Requested history data in updating state";                                break;
      case 4067:  ErrorDescription = "Some error in trading function";                                          break;
      case 4099:  ErrorDescription = "End of file";                                                             break;
      case 4100:  ErrorDescription = "Some file error";                                                         break;
      case 4101:  ErrorDescription = "Wrong file name";                                                         break;
      case 4102:  ErrorDescription = "Too many opened files";                                                   break;
      case 4103:  ErrorDescription = "Cannot open file";                                                        break;
      case 4104:  ErrorDescription = "Incompatible access to a file";                                           break;
      case 4105:  ErrorDescription = "No order selected";                                                       break;
      case 4106:  ErrorDescription = "Unknown symbol";                                                          break;
      case 4107:  ErrorDescription = "Invalid price";                                                           break;
      case 4108:  ErrorDescription = "Invalid ticket";                                                          break;
      case 4109:  ErrorDescription = "EA is not allowed to trade is not allowed. ";                             break;
      case 4110:  ErrorDescription = "Longs are not allowed. Check the expert properties";                      break;
      case 4111:  ErrorDescription = "Shorts are not allowed. Check the expert properties";                     break;
      case 4200:  ErrorDescription = "Object exists already";                                                   break;
      case 4201:  ErrorDescription = "Unknown object property";                                                 break;
      case 4202:  ErrorDescription = "Object does not exist";                                                   break;
      case 4203:  ErrorDescription = "Unknown object type";                                                     break;
      case 4204:  ErrorDescription = "No object name";                                                          break;
      case 4205:  ErrorDescription = "Object coordinates error";                                                break;
      case 4206:  ErrorDescription = "No specified subwindow";                                                  break;
      case 4207:  ErrorDescription = "Some error in object function";                                           break;
      default:    ErrorDescription = "No error or error is unknown";
     }
   return(ErrorDescription);
  }
//+------------------------------------------------------------------+
//| End of GetErrorDescription()                                         
//+------------------------------------------------------------------+


//+------------------------------------------------------------------+
//| Start of ChartSettings()                                         
//+------------------------------------------------------------------+
void ChartSettings()
  {
   ChartSetInteger(0,CHART_SHOW_GRID,0);
   ChartSetInteger(0,CHART_MODE,CHART_CANDLES);
   ChartSetInteger(0,CHART_AUTOSCROLL,0,True);
   WindowRedraw();
  }
//+------------------------------------------------------------------+
//| End of ChartSettings()                                         
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//| Start of CloseAllPositions()
//+------------------------------------------------------------------+ 
void CloseAllPositions()
  {
// Type: Fixed Template 
// Do not edit unless you know what you're doing

// This function closes all positions 

   CloseOrderPosition(OP_BUY,OnJournaling,MagicNumber,Slippage,P);

   CloseOrderPosition(OP_SELL,OnJournaling,MagicNumber,Slippage,P);

   return;

  }
//+------------------------------------------------------------------+
//| End of CloseAllPositions()
//+------------------------------------------------------------------+ 

//+------------------------------------------------------------------+
//| Start of IsProfitLimitBreached()                                    
//+------------------------------------------------------------------+
bool IsProfitLimitBreached(bool ProfitLimitActivated,double ProfitLimitValue,bool Journaling)
  {
   bool output=False;
   if(ProfitLimitActivated==False) return (output);

   if(netProfitSince>=ProfitLimitValue)
     {
      output=True;

      for(int i=Orders.Total()-1; i>=0; i--)
        {
         if(OrderSelect(Orders.At(i),SELECT_BY_TICKET,MODE_TRADES)==true && OrderSymbol()==Symbol() && (OrderMagicNumber()==MagicNumber))
           {
            Orders.Delete(i);
           }
        }

      CloseAllPositions();
      CheckSLTPExit(true);
      UpdateChartInfo();

      currentLotBuy=Lot;
      currentLotSell=Lot;
      currentOrdersBuy=1;
      currentOrdersSell=1;

      smallTarget=SmallProfitTarget;
      bigTarget=BigProfitTarget;
      lossTarget=LossTarget;

      if(Journaling) Print("Small Profit threshold ("+SmallProfitTarget+") REACHED ,from "+TimeToString(now,TIME_DATE|TIME_MINUTES)+" untill "+TimeToString(TimeCurrent(),TIME_DATE|TIME_MINUTES));
      now=TimeCurrent();

     }

   return (output);
  }
//+------------------------------------------------------------------+
//|End of IsProfitLimitBreached                                                                   |
//+------------------------------------------------------------------+


//+------------------------------------------------------------------+
//| Start of IsBigProfitLimitBreached()                                    
//+------------------------------------------------------------------+
bool IsBigProfitLimitBreached(bool BigProfitLimitActivated,double BigProfitLimitValue,bool Journaling)
  {

   bool output=False;

   if(BigProfitLimitActivated==False || canTrade==false ) return (output);

   if(netProfitSinceStart>=BigProfitLimitValue)
     {
      canTrade=false;
      output=True;
      CloseAllPositions();
      now=TimeCurrent();
      CheckSLTPExit(true);
      UpdateChartInfo();

      currentLotBuy=Lot;
      currentLotSell=Lot;
      currentOrdersBuy=1;
      currentOrdersSell=1;

      smallTarget=SmallProfitTarget;
      bigTarget=BigProfitTarget;
      lossTarget=LossTarget;

      if(Journaling) Print("Big Profit threshold ("+BigProfitTarget+") REACHED ,from "+TimeToString(today,TIME_DATE|TIME_MINUTES)+" untill "+TimeToString(Time[0],TIME_DATE|TIME_MINUTES));

     }

   return (output);
  }
//+------------------------------------------------------------------+
//|End of IsBigProfitLimitBreached                                                                   |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//|Start of DrawChartInfo                                                                  |
//+------------------------------------------------------------------+

void DrawChartInfo()
  {

   ObjectCreate(ChartID(),ObjName+"InfoBackground",OBJ_RECTANGLE_LABEL,0,0,0);
   ObjectSetInteger(ChartID(),ObjName+"InfoBackground",OBJPROP_XSIZE,-210);
   ObjectSetInteger(ChartID(),ObjName+"InfoBackground",OBJPROP_YSIZE,470);
   ObjectSetInteger(ChartID(),ObjName+"InfoBackground",OBJPROP_XDISTANCE,xDistance);
   ObjectSetInteger(ChartID(),ObjName+"InfoBackground",OBJPROP_YDISTANCE,20);
   ObjectSetInteger(ChartID(),ObjName+"InfoBackground",OBJPROP_CORNER,CORNER_RIGHT_UPPER);
   ObjectSetInteger(ChartID(),ObjName+"InfoBackground",OBJPROP_BGCOLOR,Black);
   ObjectSetInteger(ChartID(),ObjName+"InfoBackground",OBJPROP_BORDER_COLOR,Black);
   ObjectSetInteger(ChartID(),ObjName+"InfoBackground",OBJPROP_BACK,false);
   ObjectSetInteger(ChartID(),ObjName+"InfoBackground",OBJPROP_SELECTABLE,false);
   ObjectSetInteger(ChartID(),ObjName+"InfoBackground",OBJPROP_SELECTED,false);
   ObjectSetInteger(ChartID(),ObjName+"InfoBackground",OBJPROP_HIDDEN,false);
   ObjectSetInteger(ChartID(),ObjName+"InfoBackground",OBJPROP_BORDER_TYPE,BORDER_FLAT);

   ObjectCreate(ChartID(),ObjName+"Findus1",OBJ_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"Findus1",OBJPROP_TEXT,"Find us on  :");
   ObjectSetInteger(ChartID(),ObjName+"Findus1",OBJPROP_CORNER,CORNER_LEFT_LOWER);
   ObjectSetInteger(ChartID(),ObjName+"Findus1",OBJPROP_XDISTANCE,1);
   ObjectSetInteger(ChartID(),ObjName+"Findus1",OBJPROP_YDISTANCE,48);
   ObjectSetString(ChartID(),ObjName+"Findus1",OBJPROP_FONT,"Verdana");
   ObjectSetInteger(ChartID(),ObjName+"Findus1",OBJPROP_FONTSIZE,8);
   ObjectSetInteger(ChartID(),ObjName+"Findus1",OBJPROP_ANCHOR,ANCHOR_LEFT);
   ObjectSetInteger(ChartID(),ObjName+"Findus1",OBJPROP_COLOR,White);

   ObjectCreate(ChartID(),ObjName+"Findus2",OBJ_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"Findus2",OBJPROP_TEXT,"Facbook : HelmiFx");
   ObjectSetInteger(ChartID(),ObjName+"Findus2",OBJPROP_CORNER,CORNER_LEFT_LOWER);
   ObjectSetInteger(ChartID(),ObjName+"Findus2",OBJPROP_XDISTANCE,1);
   ObjectSetInteger(ChartID(),ObjName+"Findus2",OBJPROP_YDISTANCE,36);
   ObjectSetString(ChartID(),ObjName+"Findus2",OBJPROP_FONT,"Verdana");
   ObjectSetInteger(ChartID(),ObjName+"Findus2",OBJPROP_FONTSIZE,8);
   ObjectSetInteger(ChartID(),ObjName+"Findus2",OBJPROP_ANCHOR,ANCHOR_LEFT);
   ObjectSetInteger(ChartID(),ObjName+"Findus2",OBJPROP_COLOR,White);

   ObjectCreate(ChartID(),ObjName+"Findus3",OBJ_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"Findus3",OBJPROP_TEXT,"Twitter : @HelmiForex");
   ObjectSetInteger(ChartID(),ObjName+"Findus3",OBJPROP_CORNER,CORNER_LEFT_LOWER);
   ObjectSetInteger(ChartID(),ObjName+"Findus3",OBJPROP_XDISTANCE,1);
   ObjectSetInteger(ChartID(),ObjName+"Findus3",OBJPROP_YDISTANCE,24);
   ObjectSetString(ChartID(),ObjName+"Findus3",OBJPROP_FONT,"Verdana");
   ObjectSetInteger(ChartID(),ObjName+"Findus3",OBJPROP_FONTSIZE,8);
   ObjectSetInteger(ChartID(),ObjName+"Findus3",OBJPROP_ANCHOR,ANCHOR_LEFT);
   ObjectSetInteger(ChartID(),ObjName+"Findus3",OBJPROP_COLOR,White);

   ObjectCreate(ChartID(),ObjName+"Findus4",OBJ_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"Findus4",OBJPROP_TEXT,"Youtube : HelmiForex");
   ObjectSetInteger(ChartID(),ObjName+"Findus4",OBJPROP_CORNER,CORNER_LEFT_LOWER);
   ObjectSetInteger(ChartID(),ObjName+"Findus4",OBJPROP_XDISTANCE,1);
   ObjectSetInteger(ChartID(),ObjName+"Findus4",OBJPROP_YDISTANCE,12);
   ObjectSetString(ChartID(),ObjName+"Findus4",OBJPROP_FONT,"Verdana");
   ObjectSetInteger(ChartID(),ObjName+"Findus4",OBJPROP_FONTSIZE,8);
   ObjectSetInteger(ChartID(),ObjName+"Findus4",OBJPROP_ANCHOR,ANCHOR_LEFT);
   ObjectSetInteger(ChartID(),ObjName+"Findus4",OBJPROP_COLOR,White);

   ObjectCreate(ChartID(),ObjName+"HelmiFX2",OBJ_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"HelmiFX2",OBJPROP_TEXT,"www.helmifx.com");
   ObjectSetInteger(ChartID(),ObjName+"HelmiFX2",OBJPROP_CORNER,CORNER_RIGHT_UPPER);
   ObjectSetInteger(ChartID(),ObjName+"HelmiFX2",OBJPROP_XDISTANCE,xDistance);
   ObjectSetInteger(ChartID(),ObjName+"HelmiFX2",OBJPROP_YDISTANCE,37);
   ObjectSetString(ChartID(),ObjName+"HelmiFX2",OBJPROP_FONT,"Verdana");
   ObjectSetInteger(ChartID(),ObjName+"HelmiFX2",OBJPROP_FONTSIZE,8);
   ObjectSetInteger(ChartID(),ObjName+"HelmiFX2",OBJPROP_ANCHOR,ANCHOR_RIGHT);
   ObjectSetInteger(ChartID(),ObjName+"HelmiFX2",OBJPROP_COLOR,White);

   ObjectCreate(ChartID(),ObjName+"Symbol",OBJ_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"Symbol",OBJPROP_TEXT,Symbol());
   ObjectSetInteger(ChartID(),ObjName+"Symbol",OBJPROP_CORNER,CORNER_RIGHT_UPPER);
   ObjectSetInteger(ChartID(),ObjName+"Symbol",OBJPROP_XDISTANCE,xDistance);
   ObjectSetInteger(ChartID(),ObjName+"Symbol",OBJPROP_YDISTANCE,62);
   ObjectSetString(ChartID(),ObjName+"Symbol",OBJPROP_FONT,"Verdana");
   ObjectSetInteger(ChartID(),ObjName+"Symbol",OBJPROP_FONTSIZE,12);
   ObjectSetInteger(ChartID(),ObjName+"Symbol",OBJPROP_ANCHOR,ANCHOR_RIGHT);
   ObjectSetInteger(ChartID(),ObjName+"Symbol",OBJPROP_COLOR,Yellow);

   ObjectCreate(ChartID(),ObjName+"BuyPipValue",OBJ_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"BuyPipValue",OBJPROP_TEXT,"Buy "+CountPosOrders(MagicNumber,OP_BUY)+" = "+DoubleToStr(buyCycleLots,2)+" L = "+DoubleToStr(buyPipValue,2)+"$");
   ObjectSetInteger(ChartID(),ObjName+"BuyPipValue",OBJPROP_CORNER,CORNER_RIGHT_UPPER);
   ObjectSetInteger(ChartID(),ObjName+"BuyPipValue",OBJPROP_XDISTANCE,xDistance);
   ObjectSetInteger(ChartID(),ObjName+"BuyPipValue",OBJPROP_YDISTANCE,87);
   ObjectSetString(ChartID(),ObjName+"BuyPipValue",OBJPROP_FONT,"Verdana");
   ObjectSetInteger(ChartID(),ObjName+"BuyPipValue",OBJPROP_FONTSIZE,10);
   ObjectSetInteger(ChartID(),ObjName+"BuyPipValue",OBJPROP_ANCHOR,ANCHOR_RIGHT);
   ObjectSetInteger(ChartID(),ObjName+"BuyPipValue",OBJPROP_COLOR,DeepPink);

   ObjectCreate(ChartID(),ObjName+"SellPipValue",OBJ_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"SellPipValue",OBJPROP_TEXT,"Sell "+CountPosOrders(MagicNumber,OP_SELL)+" = "+DoubleToStr(sellCycleLots,2)+" L = "+DoubleToStr(sellPipValue,2)+"$");
   ObjectSetInteger(ChartID(),ObjName+"SellPipValue",OBJPROP_CORNER,CORNER_RIGHT_UPPER);
   ObjectSetInteger(ChartID(),ObjName+"SellPipValue",OBJPROP_XDISTANCE,xDistance);
   ObjectSetInteger(ChartID(),ObjName+"SellPipValue",OBJPROP_YDISTANCE,112);
   ObjectSetString(ChartID(),ObjName+"SellPipValue",OBJPROP_FONT,"Verdana");
   ObjectSetInteger(ChartID(),ObjName+"SellPipValue",OBJPROP_FONTSIZE,10);
   ObjectSetInteger(ChartID(),ObjName+"SellPipValue",OBJPROP_ANCHOR,ANCHOR_RIGHT);
   ObjectSetInteger(ChartID(),ObjName+"SellPipValue",OBJPROP_COLOR,MediumVioletRed);

   ObjectCreate(ChartID(),ObjName+"Equity",OBJ_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"Equity",OBJPROP_TEXT,"Equity : "+DoubleToStr(AccountEquity(),2));
   ObjectSetInteger(ChartID(),ObjName+"Equity",OBJPROP_CORNER,CORNER_RIGHT_UPPER);
   ObjectSetInteger(ChartID(),ObjName+"Equity",OBJPROP_XDISTANCE,xDistance);
   ObjectSetInteger(ChartID(),ObjName+"Equity",OBJPROP_YDISTANCE,137);
   ObjectSetString(ChartID(),ObjName+"Equity",OBJPROP_FONT,"Verdana");
   ObjectSetInteger(ChartID(),ObjName+"Equity",OBJPROP_FONTSIZE,10);
   ObjectSetInteger(ChartID(),ObjName+"Equity",OBJPROP_ANCHOR,ANCHOR_RIGHT);
   ObjectSetInteger(ChartID(),ObjName+"Equity",OBJPROP_COLOR,BurlyWood);

   ObjectCreate(ChartID(),ObjName+"Balance",OBJ_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"Balance",OBJPROP_TEXT,"Balance : "+DoubleToStr(AccountBalance(),2));
   ObjectSetInteger(ChartID(),ObjName+"Balance",OBJPROP_CORNER,CORNER_RIGHT_UPPER);
   ObjectSetInteger(ChartID(),ObjName+"Balance",OBJPROP_XDISTANCE,xDistance);
   ObjectSetInteger(ChartID(),ObjName+"Balance",OBJPROP_YDISTANCE,162);
   ObjectSetString(ChartID(),ObjName+"Balance",OBJPROP_FONT,"Verdana");
   ObjectSetInteger(ChartID(),ObjName+"Balance",OBJPROP_FONTSIZE,10);
   ObjectSetInteger(ChartID(),ObjName+"Balance",OBJPROP_ANCHOR,ANCHOR_RIGHT);
   ObjectSetInteger(ChartID(),ObjName+"Balance",OBJPROP_COLOR,SkyBlue);

   ObjectCreate(ChartID(),ObjName+"ClosedBuyPositionsProfit",OBJ_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"ClosedBuyPositionsProfit",OBJPROP_TEXT,"Closed Buy "+DoubleToStr(buyLastClosedProfit,2));
   ObjectSetInteger(ChartID(),ObjName+"ClosedBuyPositionsProfit",OBJPROP_CORNER,CORNER_RIGHT_UPPER);
   ObjectSetInteger(ChartID(),ObjName+"ClosedBuyPositionsProfit",OBJPROP_XDISTANCE,xDistance);
   ObjectSetInteger(ChartID(),ObjName+"ClosedBuyPositionsProfit",OBJPROP_YDISTANCE,187);
   ObjectSetString(ChartID(),ObjName+"ClosedBuyPositionsProfit",OBJPROP_FONT,"Verdana");
   ObjectSetInteger(ChartID(),ObjName+"ClosedBuyPositionsProfit",OBJPROP_FONTSIZE,11);
   ObjectSetInteger(ChartID(),ObjName+"ClosedBuyPositionsProfit",OBJPROP_ANCHOR,ANCHOR_RIGHT);
   ObjectSetInteger(ChartID(),ObjName+"ClosedBuyPositionsProfit",OBJPROP_COLOR,Khaki);

   ObjectCreate(ChartID(),ObjName+"ClosedSellPositionsProfit",OBJ_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"ClosedSellPositionsProfit",OBJPROP_TEXT,"Closed Sell "+DoubleToStr(sellLastClosedProfit,2));
   ObjectSetInteger(ChartID(),ObjName+"ClosedSellPositionsProfit",OBJPROP_CORNER,CORNER_RIGHT_UPPER);
   ObjectSetInteger(ChartID(),ObjName+"ClosedSellPositionsProfit",OBJPROP_XDISTANCE,xDistance);
   ObjectSetInteger(ChartID(),ObjName+"ClosedSellPositionsProfit",OBJPROP_YDISTANCE,212);
   ObjectSetString(ChartID(),ObjName+"ClosedSellPositionsProfit",OBJPROP_FONT,"Verdana");
   ObjectSetInteger(ChartID(),ObjName+"ClosedSellPositionsProfit",OBJPROP_FONTSIZE,11);
   ObjectSetInteger(ChartID(),ObjName+"ClosedSellPositionsProfit",OBJPROP_ANCHOR,ANCHOR_RIGHT);
   ObjectSetInteger(ChartID(),ObjName+"ClosedSellPositionsProfit",OBJPROP_COLOR,Chartreuse);

   ObjectCreate(ChartID(),ObjName+"CurrentBuyPositionsProfit",OBJ_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"CurrentBuyPositionsProfit",OBJPROP_TEXT,"Current Buy "+DoubleToStr(buyCycleProfit,2));
   ObjectSetInteger(ChartID(),ObjName+"CurrentBuyPositionsProfit",OBJPROP_CORNER,CORNER_RIGHT_UPPER);
   ObjectSetInteger(ChartID(),ObjName+"CurrentBuyPositionsProfit",OBJPROP_XDISTANCE,xDistance);
   ObjectSetInteger(ChartID(),ObjName+"CurrentBuyPositionsProfit",OBJPROP_YDISTANCE,237);
   ObjectSetString(ChartID(),ObjName+"CurrentBuyPositionsProfit",OBJPROP_FONT,"Verdana");
   ObjectSetInteger(ChartID(),ObjName+"CurrentBuyPositionsProfit",OBJPROP_FONTSIZE,11);
   ObjectSetInteger(ChartID(),ObjName+"CurrentBuyPositionsProfit",OBJPROP_ANCHOR,ANCHOR_RIGHT);
   ObjectSetInteger(ChartID(),ObjName+"CurrentBuyPositionsProfit",OBJPROP_COLOR,GreenYellow);

   ObjectCreate(ChartID(),ObjName+"CurrentSellPositionsProfit",OBJ_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"CurrentSellPositionsProfit",OBJPROP_TEXT,"Current Sell "+DoubleToStr(sellCycleProfit,2));
   ObjectSetInteger(ChartID(),ObjName+"CurrentSellPositionsProfit",OBJPROP_CORNER,CORNER_RIGHT_UPPER);
   ObjectSetInteger(ChartID(),ObjName+"CurrentSellPositionsProfit",OBJPROP_XDISTANCE,xDistance);
   ObjectSetInteger(ChartID(),ObjName+"CurrentSellPositionsProfit",OBJPROP_YDISTANCE,262);
   ObjectSetString(ChartID(),ObjName+"CurrentSellPositionsProfit",OBJPROP_FONT,"Verdana");
   ObjectSetInteger(ChartID(),ObjName+"CurrentSellPositionsProfit",OBJPROP_FONTSIZE,11);
   ObjectSetInteger(ChartID(),ObjName+"CurrentSellPositionsProfit",OBJPROP_ANCHOR,ANCHOR_RIGHT);
   ObjectSetInteger(ChartID(),ObjName+"CurrentSellPositionsProfit",OBJPROP_COLOR,IndianRed);

   ObjectCreate(ChartID(),ObjName+"NetProfit",OBJ_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"NetProfit",OBJPROP_TEXT,"Net Profit Since Start "+DoubleToStr(netProfitSinceStart,2));
   ObjectSetInteger(ChartID(),ObjName+"NetProfit",OBJPROP_CORNER,CORNER_RIGHT_UPPER);
   ObjectSetInteger(ChartID(),ObjName+"NetProfit",OBJPROP_XDISTANCE,xDistance);
   ObjectSetInteger(ChartID(),ObjName+"NetProfit",OBJPROP_YDISTANCE,287);
   ObjectSetString(ChartID(),ObjName+"NetProfit",OBJPROP_FONT,"Verdana");
   ObjectSetInteger(ChartID(),ObjName+"NetProfit",OBJPROP_FONTSIZE,11);
   ObjectSetInteger(ChartID(),ObjName+"NetProfit",OBJPROP_ANCHOR,ANCHOR_RIGHT);
   ObjectSetInteger(ChartID(),ObjName+"NetProfit",OBJPROP_COLOR,DarkTurquoise);

   ObjectCreate(ChartID(),ObjName+"NetProfitSince",OBJ_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"NetProfitSince",OBJPROP_TEXT,"Net Profit This Target "+DoubleToStr(netProfitSince,2));
   ObjectSetInteger(ChartID(),ObjName+"NetProfitSince",OBJPROP_CORNER,CORNER_RIGHT_UPPER);
   ObjectSetInteger(ChartID(),ObjName+"NetProfitSince",OBJPROP_XDISTANCE,xDistance);
   ObjectSetInteger(ChartID(),ObjName+"NetProfitSince",OBJPROP_YDISTANCE,312);
   ObjectSetString(ChartID(),ObjName+"NetProfitSince",OBJPROP_FONT,"Verdana");
   ObjectSetInteger(ChartID(),ObjName+"NetProfitSince",OBJPROP_FONTSIZE,11);
   ObjectSetInteger(ChartID(),ObjName+"NetProfitSince",OBJPROP_ANCHOR,ANCHOR_RIGHT);
   ObjectSetInteger(ChartID(),ObjName+"NetProfitSince",OBJPROP_COLOR,Plum);

   ObjectCreate(ChartID(),ObjName+"CycleProfit",OBJ_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"CycleProfit",OBJPROP_TEXT,"Cycle Profit "+DoubleToStr(cycleProfit,2));
   ObjectSetInteger(ChartID(),ObjName+"CycleProfit",OBJPROP_CORNER,CORNER_RIGHT_UPPER);
   ObjectSetInteger(ChartID(),ObjName+"CycleProfit",OBJPROP_XDISTANCE,xDistance);
   ObjectSetInteger(ChartID(),ObjName+"CycleProfit",OBJPROP_YDISTANCE,337);
   ObjectSetString(ChartID(),ObjName+"CycleProfit",OBJPROP_FONT,"Verdana");
   ObjectSetInteger(ChartID(),ObjName+"CycleProfit",OBJPROP_FONTSIZE,11);
   ObjectSetInteger(ChartID(),ObjName+"CycleProfit",OBJPROP_ANCHOR,ANCHOR_RIGHT);
   ObjectSetInteger(ChartID(),ObjName+"CycleProfit",OBJPROP_COLOR,Pink);

   ObjectCreate(ChartID(),ObjName+"SmallTarget",OBJ_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"SmallTarget",OBJPROP_TEXT,"Small Target "+DoubleToStr(smallTarget,2));
   ObjectSetInteger(ChartID(),ObjName+"SmallTarget",OBJPROP_CORNER,CORNER_RIGHT_UPPER);
   ObjectSetInteger(ChartID(),ObjName+"SmallTarget",OBJPROP_XDISTANCE,xDistance);
   ObjectSetInteger(ChartID(),ObjName+"SmallTarget",OBJPROP_YDISTANCE,362);
   ObjectSetString(ChartID(),ObjName+"SmallTarget",OBJPROP_FONT,"Verdana");
   ObjectSetInteger(ChartID(),ObjName+"SmallTarget",OBJPROP_FONTSIZE,11);
   ObjectSetInteger(ChartID(),ObjName+"SmallTarget",OBJPROP_ANCHOR,ANCHOR_RIGHT);
   ObjectSetInteger(ChartID(),ObjName+"SmallTarget",OBJPROP_COLOR,LightSteelBlue);

   ObjectCreate(ChartID(),ObjName+"BigTarget",OBJ_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"BigTarget",OBJPROP_TEXT,"Big Target "+DoubleToStr(bigTarget,2));
   ObjectSetInteger(ChartID(),ObjName+"BigTarget",OBJPROP_CORNER,CORNER_RIGHT_UPPER);
   ObjectSetInteger(ChartID(),ObjName+"BigTarget",OBJPROP_XDISTANCE,xDistance);
   ObjectSetInteger(ChartID(),ObjName+"BigTarget",OBJPROP_YDISTANCE,387);
   ObjectSetString(ChartID(),ObjName+"BigTarget",OBJPROP_FONT,"Verdana");
   ObjectSetInteger(ChartID(),ObjName+"BigTarget",OBJPROP_FONTSIZE,11);
   ObjectSetInteger(ChartID(),ObjName+"BigTarget",OBJPROP_ANCHOR,ANCHOR_RIGHT);
   ObjectSetInteger(ChartID(),ObjName+"BigTarget",OBJPROP_COLOR,Cornsilk);

   ObjectCreate(ChartID(),ObjName+"LossTarget",OBJ_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"LossTarget",OBJPROP_TEXT,"Loss Target -"+DoubleToStr(lossTarget,2));
   ObjectSetInteger(ChartID(),ObjName+"LossTarget",OBJPROP_CORNER,CORNER_RIGHT_UPPER);
   ObjectSetInteger(ChartID(),ObjName+"LossTarget",OBJPROP_XDISTANCE,xDistance);
   ObjectSetInteger(ChartID(),ObjName+"LossTarget",OBJPROP_YDISTANCE,412);
   ObjectSetString(ChartID(),ObjName+"LossTarget",OBJPROP_FONT,"Verdana");
   ObjectSetInteger(ChartID(),ObjName+"LossTarget",OBJPROP_FONTSIZE,11);
   ObjectSetInteger(ChartID(),ObjName+"LossTarget",OBJPROP_ANCHOR,ANCHOR_RIGHT);
   ObjectSetInteger(ChartID(),ObjName+"LossTarget",OBJPROP_COLOR,Thistle);

   ObjectCreate(ChartID(),ObjName+"News",OBJ_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"News",OBJPROP_TEXT,"Closest News "+GetClosestNews());
   ObjectSetInteger(ChartID(),ObjName+"News",OBJPROP_CORNER,CORNER_RIGHT_UPPER);
   ObjectSetInteger(ChartID(),ObjName+"News",OBJPROP_XDISTANCE,xDistance);
   ObjectSetInteger(ChartID(),ObjName+"News",OBJPROP_YDISTANCE,462);
   ObjectSetString(ChartID(),ObjName+"News",OBJPROP_FONT,"Verdana");
   ObjectSetInteger(ChartID(),ObjName+"News",OBJPROP_FONTSIZE,11);
   ObjectSetInteger(ChartID(),ObjName+"News",OBJPROP_ANCHOR,ANCHOR_RIGHT);
   ObjectSetInteger(ChartID(),ObjName+"News",OBJPROP_COLOR,LightGreen);

  }
//+------------------------------------------------------------------+
// End of DrawChartInfo()                                          |
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
// Start of UpdateChartInfo()                                          |
//+------------------------------------------------------------------+
void UpdateChartInfo()
  {

   ObjectSetString(ChartID(),ObjName+"ClosedBuyPositionsProfit",OBJPROP_TEXT,"Closed Buy "+DoubleToStr(buyLastClosedProfit,2));
   ObjectSetString(ChartID(),ObjName+"ClosedSellPositionsProfit",OBJPROP_TEXT,"Closed Sell "+DoubleToStr(sellLastClosedProfit,2));
   ObjectSetString(ChartID(),ObjName+"CurrentBuyPositionsProfit",OBJPROP_TEXT,"Current Buy "+DoubleToStr(buyCycleProfit,2));
   ObjectSetString(ChartID(),ObjName+"CurrentSellPositionsProfit",OBJPROP_TEXT,"Current Sell "+DoubleToStr(sellCycleProfit,2));
   ObjectSetString(ChartID(),ObjName+"BuyPipValue",OBJPROP_TEXT,"Buy "+CountPosOrders(MagicNumber,OP_BUY)+" = "+DoubleToStr(buyCycleLots,2)+" L = "+DoubleToStr(buyPipValue,2)+"$");
   ObjectSetString(ChartID(),ObjName+"SellPipValue",OBJPROP_TEXT,"Sell "+CountPosOrders(MagicNumber,OP_SELL)+" = "+DoubleToStr(sellCycleLots,2)+" L = "+DoubleToStr(sellPipValue,2)+"$");
   ObjectSetString(ChartID(),ObjName+"NetProfit",OBJPROP_TEXT,"Net Profit Since Start "+DoubleToStr(netProfitSinceStart,2));
   ObjectSetString(ChartID(),ObjName+"Equity",OBJPROP_TEXT,"Equity : "+DoubleToStr(AccountEquity(),2));
   ObjectSetString(ChartID(),ObjName+"Balance",OBJPROP_TEXT,"Balance : "+DoubleToStr(AccountBalance(),2));
   ObjectSetString(ChartID(),ObjName+"CycleProfit",OBJPROP_TEXT,"Cycle Profit "+DoubleToString(cycleProfit,2));
   ObjectSetString(ChartID(),ObjName+"SmallTarget",OBJPROP_TEXT,"Small Target "+DoubleToStr(smallTarget,2));
   ObjectSetString(ChartID(),ObjName+"BigTarget",OBJPROP_TEXT,"Big Target "+DoubleToStr(bigTarget,2));
   ObjectSetString(ChartID(),ObjName+"LossTarget",OBJPROP_TEXT,"Loss Target -"+DoubleToStr(lossTarget,2));
   ObjectSetString(ChartID(),ObjName+"NetProfitSince",OBJPROP_TEXT,"Net Profit This Target "+DoubleToStr(netProfitSince,2));

   ObjectSetString(ChartID(),ObjName+"News",OBJPROP_TEXT,"News "+(closestNews==0? "OFF": TimeToStr(closestNews)));

  }
//+------------------------------------------------------------------+
// End of UpdateChartInfo()                                          |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//| Start of isNewBar()                                         
//+------------------------------------------------------------------+
bool isNewBar()
  {
//--- memorize the time of opening of the last bar in the static variable
   static datetime last_time=0;
//--- current time
   datetime lastbar_time=SeriesInfoInteger(Symbol(),Period(),SERIES_LASTBAR_DATE);

//--- if it is the first call of the function
   if(last_time==0)
     {
      //--- set the time and exit
      last_time=lastbar_time;
      return(false);
     }

//--- if the time differs
   if(last_time!=lastbar_time)
     {
      //--- memorize the time and return true
      last_time=lastbar_time;
      return(true);
     }
//--- if we passed to this line, then the bar is not new; return false
   return(false);
  }
//+------------------------------------------------------------------+
//| End of isNewBar()                                         
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
// Start of UpdateTakeProfit()                        						|
//+------------------------------------------------------------------+

void UpdateTakeProfit(int ordersType,bool Journaling,double takeProfit,int Magic)
  {

// Type: Customisable 
// Modify this function to suit your trading robot

// this function updates the take profit for all opened position after it gets recalculated by GetAverageTakeProfit
   if(takeProfit==0) // 
     {
      return;
     }

   int ordersPos=OrdersTotal();
   bool Modify=false;
   for(int i=ordersPos-1; i>=0; i--)
     {
      // Note: Once pending orders become positions, OP_BUYLIMIT AND OP_BUYSTOP becomes OP_BUY, OP_SELLLIMIT and OP_SELLSTOP becomes OP_SELL
      if(ordersType==OP_BUY || ordersType==OP_SELL)
        {
         if(OrderSelect(i,SELECT_BY_POS,MODE_TRADES)==true && OrderSymbol()==Symbol() && OrderMagicNumber()==Magic && OrderType()==ordersType)
           {
            if(OrderTakeProfit()!=takeProfit)
              {
               HandleTradingEnvironment(Journaling,RetryInterval);
               Modify=OrderModify(OrderTicket(),OrderOpenPrice(),OrderStopLoss(),takeProfit,0,CLR_NONE);
              }
           }
        }
     }

   if(Journaling && !Modify)Print("EA Journaling: Unexpected Error has happened. Error Description: "+GetErrorDescription(GetLastError()));
   if(Journaling && Modify)Print("EA Journaling: Opened Positions successfully modified, TakeProfit Changed.");

  }
//+------------------------------------------------------------------+
// End of UpdateTakeProfit()                        						|
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
// Start of UpdateStopLoss()                        						|
//+------------------------------------------------------------------+

void UpdateStopLoss(int ordersType,bool Journaling,double stopLoss,int Magic)
  {

// Type: Customisable 
// Modify this function to suit your trading robot

// this function updates the take profit for all opened position after it gets recalculated by GetAverageTakeProfit
   if(stopLoss==0) // 
     {
      return;
     }

   int ordersPos=OrdersTotal();
   bool Modify=false;
   for(int i=ordersPos-1; i>=0; i--)
     {
      // Note: Once pending orders become positions, OP_BUYLIMIT AND OP_BUYSTOP becomes OP_BUY, OP_SELLLIMIT and OP_SELLSTOP becomes OP_SELL
      if(ordersType==OP_BUY || ordersType==OP_SELL)
        {
         if(OrderSelect(i,SELECT_BY_POS,MODE_TRADES)==true && OrderSymbol()==Symbol() && OrderMagicNumber()==Magic && OrderType()==ordersType)
           {
            if(OrderStopLoss()!=stopLoss)
              {
               HandleTradingEnvironment(Journaling,RetryInterval);
               Modify=OrderModify(OrderTicket(),OrderOpenPrice(),stopLoss,OrderTakeProfit(),0,CLR_NONE);
              }
           }
        }
     }

   if(Journaling && !Modify)Print("EA Journaling: Unexpected Error has happened. Error Description: "+GetErrorDescription(GetLastError()));
   if(Journaling && Modify)Print("EA Journaling: Opened Positions successfully modified, StopLoss Changed.");

  }
//+------------------------------------------------------------------+
// End of UpdateStopLoss()                        						|
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//| Start of CheckSLTPExit()                                         |
//+------------------------------------------------------------------+

void CheckSLTPExit(bool timer)
  {

//1 Profit, 2 Loss
   int orderStatusBuy=0;
   int orderStatusSell=0;

   bool orderClosedBuy = false;
   bool orderClosedSell= false;

   for(int i=Orders.Total()-1; i>=0; i--)
     {
      if(OrderSelect(Orders.At(i),SELECT_BY_TICKET,MODE_TRADES)==true && OrderSymbol()==Symbol() && (OrderMagicNumber()==MagicNumber))
        {
         if(OrderCloseTime()!=0)
           {
            if(OrderType()==OP_BUY)
              {
               orderStatusBuy= OrderProfit()>=0?1:2;
               orderClosedBuy=true;
              }
            else if(OrderType()==OP_SELL)
              {
               orderStatusSell=OrderProfit()>=0?1:2;
               orderClosedSell=true;
              }

            Orders.Delete(i);
           }
        }
     }

   if(orderClosedBuy)
     {
      if(orderStatusBuy==1) // Profit
        {
         if(!timer)
           {
            for(int i=0;i<currentOrdersBuy;i++)
              {
               OrderNumber=OpenPositionMarket(OP_BUY,currentLotBuy,0,0,MagicNumber,Slippage,OnJournaling,P,MaxRetriesPerTick);
               if(UseTrailingStops) SetTrailingStop(OnJournaling,RetryInterval,MagicNumber,P,OrderNumber);
               if(UseBreakeven) SetBreakeven(OnJournaling,RetryInterval,MagicNumber,P,OrderNumber);
               Orders.Add(OrderNumber);
               lastSignal=1;
              }
            if(StopLoss!=0) UpdateStopLoss(OP_BUY,OnJournaling,Ask-(StopLoss*Point*P),MagicNumber);
            if(TakeProfit!=0) UpdateTakeProfit(OP_BUY,OnJournaling,Ask+(TakeProfit*Point*P),MagicNumber);

            if(currentOrdersBuy*OrdersMultiplier>MaxOrdersBuy)
              {
               currentOrdersBuy=1;
              }
            else
              {
               currentOrdersBuy*=OrdersMultiplier;
              }
           }

         currentOrdersSell=1;

        }
      else // Loss
        {
         if(netProfitSince<=(-1*lossTarget)) // we need to multiply the lot and the target;
           {
            if(currentLotSell*LotMultiplier>LotMax)
              {
               currentLotSell=currentLotSell=LotMax;
              }
            else
              {
               if(IsMultiplyTarget)
                 {
                  smallTarget*=LotMultiplier;
                 }

               currentLotSell*=LotMultiplier;
               currentLotBuy*=LotMultiplier;
               lossTarget*=LotMultiplier;
              }
           }
         if(!timer)
           {
            OrderNumber=OpenPositionMarket(OP_SELL,currentLotSell,StopLoss,TakeProfit,MagicNumber,Slippage,OnJournaling,P,MaxRetriesPerTick);
            if(UseTrailingStops) SetTrailingStop(OnJournaling,RetryInterval,MagicNumber,P,OrderNumber);
            if(UseBreakeven) SetBreakeven(OnJournaling,RetryInterval,MagicNumber,P,OrderNumber);
            Orders.Add(OrderNumber);
            lastSignal=2;

            if(currentOrdersSell*OrdersMultiplier>MaxOrdersSell)
              {
               currentOrdersSell=1;
              }
            else
              {
               currentOrdersSell*=OrdersMultiplier;
              }
           }

         currentOrdersBuy=1;

        }
     }

   if(orderClosedSell)
     {
      if(orderStatusSell==1) // Profit
        {
         if(!timer)
           {
            for(int i=0;i<currentOrdersSell;i++)
              {
               OrderNumber=OpenPositionMarket(OP_SELL,currentLotSell,0,0,MagicNumber,Slippage,OnJournaling,P,MaxRetriesPerTick);
               if(UseTrailingStops) SetTrailingStop(OnJournaling,RetryInterval,MagicNumber,P,OrderNumber);
               if(UseBreakeven) SetBreakeven(OnJournaling,RetryInterval,MagicNumber,P,OrderNumber);
               Orders.Add(OrderNumber);
               lastSignal=2;
              }
            if(StopLoss!=0) UpdateStopLoss(OP_SELL,OnJournaling,Bid+(StopLoss*Point*P),MagicNumber);
            if(TakeProfit!=0) UpdateTakeProfit(OP_SELL,OnJournaling,Bid-(TakeProfit*Point*P),MagicNumber);

            if(currentOrdersSell*OrdersMultiplier>MaxOrdersSell)
              {
               currentOrdersSell=1;
              }
            else
              {
               currentOrdersSell*=OrdersMultiplier;
              }
           }

         currentOrdersBuy=1;
        }
      else // Loss
        {
         if(netProfitSince<=(-1*lossTarget)) // we need to multiply the lot and the target;
           {
            if(currentLotBuy*LotMultiplier>LotMax)
              {
               currentLotBuy=currentLotSell=LotMax;
              }
            else
              {
               if(IsMultiplyTarget)
                 {
                  smallTarget*=LotMultiplier;
                 }
               currentLotBuy*=LotMultiplier;
               currentLotSell*=LotMultiplier;
               lossTarget*=LotMultiplier;
              }
           }
         if(!timer)
           {
            OrderNumber=OpenPositionMarket(OP_BUY,currentLotBuy,StopLoss,TakeProfit,MagicNumber,Slippage,OnJournaling,P,MaxRetriesPerTick);
            if(UseTrailingStops) SetTrailingStop(OnJournaling,RetryInterval,MagicNumber,P,OrderNumber);
            if(UseBreakeven) SetBreakeven(OnJournaling,RetryInterval,MagicNumber,P,OrderNumber);
            Orders.Add(OrderNumber);
            lastSignal=1;

            if(currentOrdersBuy*OrdersMultiplier>MaxOrdersBuy)
              {
               currentOrdersBuy=1;
              }
            else
              {
               currentOrdersBuy*=OrdersMultiplier;
              }
           }

         currentOrdersSell=1;
        }
     }
  }
//+------------------------------------------------------------------+
//| End of CheckSLTPExit()                                         |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//| End of isItTime()                                         
//+------------------------------------------------------------------+

bool isItTime1()
  {
   Current_Time1=TimeHour(TimeCurrent());
   if(Start1<0)Start1=0;
   if(End1<0) End1=0;
   if(Start1==0 || Start1>24) Start1=24; if(End1==0 || End1>24) End1=24; if(Current_Time1==0) Current_Time1=24;

   if(Start1<End1)
      if( (Current_Time1 < Start1) || (Current_Time1 >= End1) ) return(false);

   if(Start1>End1)
      if( (Current_Time1 < Start1) && (Current_Time1 >= End1) ) return(false);

   if(Start1==End1)
     {
      return false;
     }

   return(true);
  }
//+------------------------------------------------------------------+
//| End of isItTime()                                         
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//| End of isItTime()                                         
//+------------------------------------------------------------------+

bool isItTime2()
  {
   Current_Time2=TimeHour(TimeCurrent());
   if(Start2<0)Start2=0;
   if(End2<0) End2=0;
   if(Start2==0 || Start2>24) Start2=24; if(End2==0 || End2>24) End2=24; if(Current_Time2==0) Current_Time2=24;

   if(Start2<End2)
      if( (Current_Time2 < Start2) || (Current_Time2 >= End2) ) return(false);

   if(Start2>End2)
      if( (Current_Time2 < Start2) && (Current_Time2 >= End2) ) return(false);

   if(Start2==End2)
     {
      return false;
     }

   return(true);
  }
//+------------------------------------------------------------------+
//| End of isItTime()                                         
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//| Converts ff time & date into yyyy.mm.dd hh:mm - by deVries       |
//+------------------------------------------------------------------+
string MakeDateTime(string strDate,string strTime)
  {
//---
   int n1stDash=StringFind(strDate, "-");
   int n2ndDash=StringFind(strDate, "-", n1stDash+1);

   string strMonth=StringSubstr(strDate,0,2);
   string strDay=StringSubstr(strDate,3,2);
   string strYear=StringSubstr(strDate,6,4);

   int nTimeColonPos=StringFind(strTime,":");
   string strHour=StringSubstr(strTime,0,nTimeColonPos);
   string strMinute=StringSubstr(strTime,nTimeColonPos+1,2);
   string strAM_PM=StringSubstr(strTime,StringLen(strTime)-2);

   int nHour24=StrToInteger(strHour);
   if((strAM_PM=="pm" || strAM_PM=="PM") && nHour24!=12) nHour24+=12;
   if((strAM_PM=="am" || strAM_PM=="AM") && nHour24==12) nHour24=0;
   string strHourPad="";
   if(nHour24<10) strHourPad="0";
   return(StringConcatenate(strYear, ".", strMonth, ".", strDay, " ", strHourPad, nHour24, ":", strMinute));
//---
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//| Impact to number - by authors                                    |
//+------------------------------------------------------------------+
int ImpactToNumber(string impact)
  {
//---
   if(impact == "High") return(3);
   else if(impact == "Medium") return(2);
   else if(impact == "Low") return(1);
   else return(0);
//---
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
datetime GetWinLocalDateTime()
  {
   double hundrednSecPerSec=10.0*1000000.0;
   double bit32to64=65536.0*65536.0;
   double secondsBetween1601And1970=11644473600.0;
   int    TimeArray[4];
   int    FileTimeArray[2];   // 100nSec since 1601/01/01 UTC
   int    LocalFileTimeArray[2];   // 100nSec since 1601/01/01 Local

   GetSystemTime(TimeArray);
   SystemTimeToFileTime(TimeArray,FileTimeArray);
   FileTimeToLocalFileTime(FileTimeArray,LocalFileTimeArray);

   double lfLo32=LocalFileTimeArray[0];
   if(lfLo32<0)
      lfLo32=bit32to64+lfLo32;
   double ticksSince1601=LocalFileTimeArray[1]*bit32to64+lfLo32;
   double secondsSince1601 = ticksSince1601 / hundrednSecPerSec;
   double secondsSince1970 = secondsSince1601 - secondsBetween1601And1970;
   return (secondsSince1970);
  }
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
datetime GetWinUtcDateTime()
  {
   double hundrednSecPerSec=10.0*1000000.0;
   double bit32to64=65536.0*65536.0;
   double secondsBetween1601And1970=11644473600.0;
   int    TimeArray[4];
   int    FileTimeArray[2];   // 100nSec since 1601/01/01 UTC
   GetSystemTime(TimeArray);
   SystemTimeToFileTime(TimeArray,FileTimeArray);

   double lfLo32=FileTimeArray[0];
   if(lfLo32<0)
      lfLo32=bit32to64+lfLo32;
   double ticksSince1601=FileTimeArray[1]*bit32to64+lfLo32;
   double secondsSince1601 = ticksSince1601 / hundrednSecPerSec;
   double secondsSince1970 = secondsSince1601 - secondsBetween1601And1970;
   return (secondsSince1970);
  }
//+------------------------------------------------------------------+

datetime GetClosestNews()
  {
   datetime temp=0;
   for(int i=0;i<3;i++)
     {
      if(Event[i][USED]==true)
        {
         if((TimeSeconds(TimeCurrent())<=Event[i][DATE]))) // (datetime(MakeDateTime(Event[i][DATE],Event[i][TIME]))-(GetWinUtcDateTime()-GetWinLocalDateTime()))
           {
            return Event[i][DATE];
           }
        }
     }

   return temp;
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+

void RedefineEventArray()
  {
   ArrayResize(Event,0,0);
   ArrayResize(Event,200,0);

//--- define the XML Tags, Vars
   string sTags[7]={"<title>","<country>","<date><![CDATA[","<time><![CDATA[","<impact><![CDATA[","<forecast><![CDATA[","<previous><![CDATA["};
   string eTags[7]={"</title>","</country>","]]></date>","]]></time>","]]></impact>","]]></forecast>","]]></previous>"};
   int next=-1;
   int BoEvent=0,begin=0,end=0;
   string myEvent="";
//--- Minutes calculation
   datetime EventTime=0;
   int EventMinute=0;
//--- split the currencies into the two parts 
   string MainSymbol=StringSubstr(Symbol(),0,3);
   string SecondSymbol=StringSubstr(Symbol(),3,3);
//--- loop to get the data from xml tags
   while(true)
     {
      BoEvent=StringFind(sData,"<event>",BoEvent);
      if(BoEvent==-1) break;
      BoEvent+=7;
      next=StringFind(sData,"</event>",BoEvent);
      if(next == -1) break;
      myEvent = StringSubstr(sData,BoEvent,next-BoEvent);
      BoEvent = next;
      begin=0;
      for(int i=0; i<7; i++)
        {
         Event[EventIndex][i]="";
         next=StringFind(myEvent,sTags[i],begin);
         //--- Within this event, if tag not found, then it must be missing; skip it
         if(next==-1) continue;
         else
           {
            //--- We must have found the sTag okay...
            //--- Advance past the start tag
            begin=next+StringLen(sTags[i]);
            end=StringFind(myEvent,eTags[i],begin);
            //---Find start of end tag and Get data between start and end tag
            if(end>begin && end!=-1)
               Event[EventIndex][i]=StringSubstr(myEvent,begin,end-begin);
           }
        }
      //--- sometimes they forget to remove the tags :)
      if(StringFind(Event[EventIndex][TITLE],"<![CDATA[")!=-1)
         StringReplace(Event[EventIndex][TITLE],"<![CDATA[","");
      if(StringFind(Event[EventIndex][TITLE],"]]>")!=-1)
         StringReplace(Event[EventIndex][TITLE],"]]>","");
      if(StringFind(Event[EventIndex][TITLE],"]]>")!=-1)
         StringReplace(Event[EventIndex][TITLE],"]]>","");
      //---
      if(StringFind(Event[EventIndex][FORECAST],"&lt;")!=-1)
         StringReplace(Event[EventIndex][FORECAST],"&lt;","");
      if(StringFind(Event[EventIndex][PREVIOUS],"&lt;")!=-1)
         StringReplace(Event[EventIndex][PREVIOUS],"&lt;","");

      //--- filters that define whether we want to skip this particular currencies or events
      if(MainSymbol!=Event[EventIndex][COUNTRY] && SecondSymbol!=Event[EventIndex][COUNTRY])
        {
         Event[EventIndex][USED]=false; continue;
        }
      if(!IncludeHigh && Event[EventIndex][IMPACT]=="High")
        {
         Event[EventIndex][USED]=false;continue;
        }
      if(!IncludeMedium && Event[EventIndex][IMPACT]=="Medium")
        {
         Event[EventIndex][USED]=false;continue;
        }
      if(!IncludeLow && Event[EventIndex][IMPACT]=="Low")
        {
         Event[EventIndex][USED]=false;continue;
        }
      if(!IncludeSpeaks && StringFind(Event[EventIndex][TITLE],"Speaks")!=-1)
        {
         continue;
        }
      if(!IncludeHolidays && Event[EventIndex][IMPACT]=="Holiday")
        {
         Event[EventIndex][USED]=false;continue;
        }
      if(Event[EventIndex][TIME]=="All Day" ||
         Event[EventIndex][TIME]=="Tentative" ||
         Event[EventIndex][TIME]=="")
        {
         Event[EventIndex][USED]=false;continue;
        }
      if(FindKeyword!="")
        {
         if(StringFind(Event[EventIndex][TITLE],FindKeyword)==-1)
           {
            Event[EventIndex][USED]=false;continue;
           }
        }
      if(IgnoreKeyword!="")
        {
         if(StringFind(Event[EventIndex][TITLE],IgnoreKeyword)!=-1)
           {
            Event[EventIndex][USED]=false;continue;
           }
        }

      //--- set some values (dashes) if empty
      if(Event[EventIndex][FORECAST]=="") Event[EventIndex][FORECAST]="---";
      if(Event[EventIndex][PREVIOUS]=="") Event[EventIndex][PREVIOUS]="---";
      //--- Convert Event time to MT4 time
      EventTime=datetime(MakeDateTime(Event[EventIndex][DATE],Event[EventIndex][TIME]));
      //--- calculate how many minutes before the event (may be negative)
      EventMinute=int(EventTime-GetWinUtcDateTime())/60;

      if(EventTime<GetWinLocalDateTime() || EventMinute<0)
        {
         Event[EventIndex][USED]=false;continue;
        }
      if(Event[EventIndex][TITLE]=="")
        {
         Event[EventIndex][USED]=false;continue;
        }

      Event[EventIndex][USED]=true;
      EventIndex++;
     }

   for(int i=EventIndex; i>=0; i--)
     {
      if(Event[i][USED]==true)
        {
         Print(i+" : "+Event[i][TITLE]+" , "+Event[i][COUNTRY]+" , "+Event[i][IMPACT]+" , "+Event[i][FORECAST]+" , "+Event[i][PREVIOUS]+" , "+(datetime(MakeDateTime(Event[i][DATE],Event[i][TIME]))-(GetWinUtcDateTime()-GetWinLocalDateTime())));
        }
     }
   Print(" *** News List *** ");
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
