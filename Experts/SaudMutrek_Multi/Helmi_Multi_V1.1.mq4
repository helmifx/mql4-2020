//+------------------------------------------------------------------+
//|           								 Project3_V1.0.mq4 				   |
//|                        Copyright 2017, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//|
//+------------------------------------------------------------------+

#property copyright "Copyright 2017 HelmiFX."
#property link      "www.helmifx.com"
#property strict
#property description "Saud Mutrek's Expert(Scalping)"
#property icon "photo.ico"

#include <Arrays\ArrayInt.mqh>

#import "user32.dll"
int GetAncestor(int hWnd,int gaFlags);
int GetDlgItem(int hDlg,int nIDDlgItem);
int     MessageBoxA(int hWnd,string szText,string szCaption,int nType);
int RegisterWindowMessageA(string MessageName);
int RegisterWindowMessageW(string MessageName);
int PostMessageW(int hwnd,int msg,int wparam,uchar &Name[]);
int PostMessageA(int hwnd,int msg,int wparam,string Name);

void keybd_event(int VirtualKey,int ScanCode,int Flags,int ExtraInfo);

#import




#define WM_COMMAND   0x0111
#define WM_KEYDOWN   0x0100
#define VK_HOME      0x0024
#define VK_DOWN      0x0028
#define INDICATOR_NAME "WARDEH"
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
enum Periods
  {
   M1=1,M5=5,M15=15,M30=30,H1=60,H4=240,D1=1440,W1=10080,MN=43200
  };
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
enum CLOSING_REASON
  {
   SAR=0,//SAR
   MA=1,//MA
   HA=2,//HeikenAshi
   Fractals=3,//Fractals
  };
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//| Setup                                               
//+------------------------------------------------------------------+
extern string  Header1="**************************************************************************************"; // ----------Entry Rules-----------
extern string PS="Please enter the currencies names seperated by ','"; //  ---
extern string CurrenciesList="";
extern ENUM_TIMEFRAMES CurrenciesTimeFrame=PERIOD_M5;
extern string  Note1="*******************************************"; // MA Settings
extern bool   UseMA=true;
extern ENUM_MA_METHOD MATypes=MODE_SMA;
extern int     FastMA=5;
extern int     SlowMA=15;
extern ENUM_TIMEFRAMES MATimeFrame=PERIOD_M5; // Signal Period for MAs
extern string  Note30="*******************************************"; //  HeikenAshi Settings
extern bool    UseHA=true;
extern ENUM_TIMEFRAMES HATimeFrame=PERIOD_M5; // Signal Period for HeikenAshi Indicator
extern string  Note20="*******************************************"; //  Fractals Settings
extern bool    UseFractals=true;
extern ENUM_TIMEFRAMES FractalsTimeFrame=PERIOD_M5; // Signal Period for Fractals Indicator
extern string  Note31="*******************************************"; //  SAR Settings
extern bool    UseSAR = true;
extern double  SARStep=0.02;            //SAR Step	
extern double  SARMaximum=0.2;          //SAR Maximum
extern ENUM_TIMEFRAMES SARTimeFrame=PERIOD_M5;
extern string  Note4="*******************************************"; // Profit to be reached 
extern double  ProfitLimit=1;
extern string  Note500="*******************************************"; // Reopen order if profited
extern bool    ReopenIfProfit=true;
extern string  Header100="**************************************************************************************"; // ----------Closing Rules-----------
extern string Sep="**********************************************";// Close order if Opposite Closing Reason Appered
extern bool  OppositeCloseingOn=false;
extern CLOSING_REASON Closing_Reason=0;
extern string Sep1="**********************************************";// Open Oposite order if an Order was closed by a Closing_Reason
extern bool  OppositeOpeningOnAfterClose=false;
extern string  Header3="**************************************************************************************"; //----------Lots -----------
extern double  Lot=0.01;
extern string  Header7="**************************************************************************************"; // ----------Not Working Times Settings-----------
extern bool    useNotWorkingTimer=false;
extern string  Note200="Please use 24Hour format, between 0 and 24"; // --- 
extern int     Start=0;
extern int     End=6;
extern string  Header700="**************************************************************************************"; // ----------Profit Increment Settings-----------
extern bool    ProfitIncrement=true;
extern double  ProfitIncrementValue=0.05;
extern string  Header8="**************************************************************************************"; // ----------Chart Info -----------
extern int     xDistance=1; //Chart info distance from top right corner
extern bool    DrawInfoBackground=true;
extern string  Header9="**************************************************************************************"; // ----------EA General -----------
extern int     MagicNumber=123456;
extern int     Slippage=3;
extern bool    OnJournaling=true; // Add EA updates in the Journal Tab

string  InternalHeader1="----------Errors Handling Settings-----------";

int     RetryInterval=100; // Pause Time before next retry (in milliseconds)
int     MaxRetriesPerTick=10;

string  InternalHeader2="----------Service Variables-----------";

//+------------------------------------------------------------------+
//|General Variables                                                                 |
//+------------------------------------------------------------------+

int P[15],YenPairAdjustFactor[15];

CArrayInt Orders;

int OrderNumber;
double profitFinalLimit;
int Current_Time;

string Currencies[];
int CurrenciesOrders[15];
bool CurrenciesInMarket[15];

bool indicatorExist;

int EntryValid[15];
int ExitValid[15];

//+------------------------------------------------------------------+
//|trend5 Indicator Variables                                        |
//+------------------------------------------------------------------+

double indicatorValue1_1_HAOpen[15];
double indicatorValue2_1_HAClose[15];

double indicatorValue1_1_FractalUp[15];
double indicatorValue2_1_FractalDown[15];

int indicatorFlag_MA[15];
int indicatorFlag_HA[15];
int indicatorFlag_Fractals[15];

//+------------------------------------------------------------------+
//|SAR Variables                                                                  |
//+------------------------------------------------------------------+
double MySAR_1[15],MySAR_2[15];
double Close_1[15];
double High_Last[15],Low_Last[15];
double MyFastMA[15],MySlowMA[15];

int SARCrossTriggered[15],SARCurrentDirection[15],SARLastDirection[15];
bool SARFirstTime[15]={true};
int SARDirection[15];

int MACrossTriggered[15],MACurrentDirection[15],MALastDirection[15];
bool MAFirstTime[15]={true};
int MADirection[15];

int HADirection[15];
int FractalsDirection[15];

long currentchartID=0;
long previouschartID=ChartFirst();
int limit=0;
double lotSum;
//+------------------------------------------------------------------+
//|Chart Display Variables                                                                  |
//+------------------------------------------------------------------+

double buyLastClosedProfit=0;
double sellLastClosedProfit=0;
double cycleProfit; //Get All Opened Profit
double buyCycleProfit;
double sellCycleProfit;
double cycleLots;
double buyCycleLots;    //Get All Buy  Opened Lots
double sellCycleLots;    //Get All Sell Opened Lots
double netProfit;    // All the profit of all opened positions by the same MagicNumber
string ObjName=IntegerToString(ChartID(),0,' ');   //The global object name 
string signalValue;
datetime signalTime_0;
datetime signalTime_1;
string signalReason;
string SARValue;
int indicator_handle;
//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
int init()
  {

   if(!CheckAccount()) //Check if the user account is live or not. 
      return (INIT_FAILED);

   if(IsDllsAllowed()==false)
     {
      Print("DLL call is not allowed. Experts cannot run.");
      MessageBox("DLL call is not allowed. Experts cannot run.",NULL,0);
      return(INIT_FAILED);
     }
// expert body that calls external DLL functions

   CurrenciesSplit();      //Splits MultiplySequence

   for(int i=0;i<ArraySize(Currencies);i++)
     {
      P[i]=GetP(Currencies[i]); // To account for 5 digit brokers. Used to convert pips to decimal place
      YenPairAdjustFactor[i]=GetYenAdjustFactor(Currencies[i]); // Adjust for YenPair

     }

// ChartApplyTemplate(0,"SaudMutrek_Multi.tpl");

   if(UninitializeReason()!=3 && UninitializeReason()!=5)
     {
      for(int i=0;i<ArraySize(Currencies);i++)
        {
         if(Currencies[i]!=Symbol())
           {
            currentchartID=ChartOpen(Currencies[i],CurrenciesTimeFrame);
            bool applySet=ChartApplyTemplate(currentchartID,"Helmi_Multi.tpl");//TDL
            if(!applySet)
              {
               Print("Unable to set template (Helmi_Multi.tpl)- Last Error "+GetLastError());
               break;
              }
           }
         ChartSettings(currentchartID);
        }
     }
   ChartSetInteger(0,CHART_BRING_TO_TOP,0,True);
   DrawChartInfo();            //Drawing Chart Info
   profitFinalLimit=ProfitLimit;
   UpdateChartInfo();

   return(INIT_SUCCEEDED);


  }
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+

void OnDeinit(const int reason)
  {
//---
   if(reason!=5 && reason!=3)
     {
      ObjectsDeleteAll(0,0,OBJ_LABEL);
      ObjectsDeleteAll(0,0,OBJ_RECTANGLE_LABEL);
      ObjectsDeleteAll(0,0,OBJ_ARROW);
      ObjectsDeleteAll(ChartID(),0);
     }

   for(int i=ObjectsTotal(); i>=0; i--)
     {
      string name=ObjectName(i);
      if(StringFind(name,ObjName)==0) ObjectDelete(name);
     }

   Print("The Lot Sum is : "+lotSum);
  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
int start()
  {

   cycleProfit=GetCycleProfit(0)+GetCycleProfit(1);               //Set Cycle Profits onto the chart.
   buyCycleProfit=GetCycleProfit(0);
   sellCycleProfit=GetCycleProfit(1);
   buyCycleLots=GetCycleLots(0);                //Set Cycle Lots onto the chart.
   sellCycleLots=GetCycleLots(1);
   cycleLots=GetCycleLots(0)+GetCycleLots(1);
   netProfit=cycleProfit+buyLastClosedProfit+sellLastClosedProfit;
   UpdateChartInfo();

   IsProfitLimitBreached(profitFinalLimit,OnJournaling);

   CheckSLTPExit();

//----------Entry & Exit Variables-----------

   for(int i=0;i<ArraySize(Currencies);i++)
     {

      High_Last[i]=iHigh(Currencies[i],SARTimeFrame,2);
      Low_Last[i]=iLow(Currencies[i],SARTimeFrame,2);
      MySAR_1[i]=iSAR(Currencies[i],SARTimeFrame,SARStep,SARMaximum,1);
      MySAR_2[i]=iSAR(Currencies[i],SARTimeFrame,SARStep,SARMaximum,2);
      SARCrossTriggered[i]=SARCross(MySAR_1[i],MySAR_2[i],High_Last[i],Low_Last[i],i);   //Checking for a cross P SAR values.

                                                                                         //if(SARCrossTriggered[i]!=0)
      // {
      // }
      SARDirection[i]=SARCrossTriggered[i];   // when a cross happens SARDirection hold the value of it.

      if(UseMA)
        {
         Close_1[i] =iClose(Currencies[i],MATimeFrame,1);
         MyFastMA[i]=iMA(Currencies[i],MATimeFrame,FastMA,0,MATypes,PRICE_CLOSE,1);
         MySlowMA[i]=iMA(Currencies[i],MATimeFrame,SlowMA,0,MATypes,PRICE_CLOSE,1);


         MACrossTriggered[i]=MACross(MyFastMA[i],MySlowMA[i],i);    //Checking for a cross on the E tunnel

         if(MACrossTriggered[i]!=0)
           {
            MADirection[i]=MACrossTriggered[i];
           }
        }

      else
        {
         MACrossTriggered[i]=0;
         MADirection[i]=0;
        }

      if(UseHA)
        {
         indicatorValue1_1_HAOpen[i]=iCustom(Currencies[i],HATimeFrame,"Heiken Ashi",Red,White,Red,White,2,1);
         indicatorValue2_1_HAClose[i]=iCustom(Currencies[i],HATimeFrame,"Heiken Ashi",Red,White,Red,White,3,1);

         HADirection[i]=CheckHADirection(indicatorValue1_1_HAOpen[i],indicatorValue2_1_HAClose[i]);
        }
      else
        {
         HADirection[i]=0;
        }

      if(UseFractals)
        {
         indicatorValue1_1_FractalUp[i]=iFractals(Currencies[i],FractalsTimeFrame,MODE_UPPER,3);
         indicatorValue2_1_FractalDown[i]=iFractals(Currencies[i],FractalsTimeFrame,MODE_LOWER,3);
         if(CheckFractals(indicatorValue1_1_FractalUp[i],indicatorValue2_1_FractalDown[i])!=0)
           {
            FractalsDirection[i]=CheckFractals(indicatorValue1_1_FractalUp[i],indicatorValue2_1_FractalDown[i]);
           }
        }
      else
        {
         FractalsDirection[i]=0;
        }
     }

//----------New Bar Check-----------

   if(!isNewBar())
     {
      return(0);
     }

   if(useNotWorkingTimer)
     {
      if(isItTime()) return(0);
     }

//----------Indicator's Direction Check-----------
   for(int i=0;i<ArraySize(Currencies);i++)
     {
      if(CurrenciesInMarket[i]==true)
        {
         if((Closing_Reason==0 && SARDirection[i]==2) || (Closing_Reason==1 && MADirection[i]==1) || (Closing_Reason==2 && HADirection[i]==1) || (Closing_Reason==3 && FractalsDirection[i]==1)) // Green Signal, and a cross bottom happend and stayed bottom
           {
            ExitValid[i]=2;
           }

         if((Closing_Reason==0 && SARDirection[i]==1) || (Closing_Reason==1 && MADirection[i]==2) || (Closing_Reason==2 && HADirection[i]==2) || (Closing_Reason==3 && FractalsDirection[i]==2)) // Red Signal, and a cross bottom happend and stayed bottom
           {
            ExitValid[i]=1;
           }
        }
     }

//----------Exit Rules (All Opened Positions)-----------
   if(OppositeCloseingOn)
     {
      for(int i=0;i<ArraySize(Currencies);i++)
        {
         if(ExitValid[i]==1 && CountPosOrders(Currencies[i],OP_BUY)>0)
           { // Close Long Positions
            CloseOrderPosition(Currencies[i],OP_BUY,OnJournaling,MagicNumber,Slippage,P[i]);
            CurrenciesOrders[i]=0;
            CurrenciesInMarket[i]=false;

            if(OppositeOpeningOnAfterClose)
              {
               OrderNumber=OpenPositionMarket(Currencies[i],OP_SELL,Lot,0,0,MagicNumber,Slippage,OnJournaling,P[i],MaxRetriesPerTick);
               Orders.Add(OrderNumber);
               CurrenciesOrders[i]=OrderNumber;
               CurrenciesInMarket[i]=true;
               if(ProfitIncrement)
                 {
                  profitFinalLimit+=ProfitIncrementValue;
                 }
               lotSum+=Lot;
              }
           }
         if(ExitValid[i]==2 && CountPosOrders(Currencies[i],OP_SELL)>0)
           { // Close Short Positions
            CloseOrderPosition(Currencies[i],OP_SELL,OnJournaling,MagicNumber,Slippage,P[i]);
            CurrenciesOrders[i]=0;
            CurrenciesInMarket[i]=false;
            if(OppositeOpeningOnAfterClose)
              {
               OrderNumber=OpenPositionMarket(Currencies[i],OP_BUY,Lot,0,0,MagicNumber,Slippage,OnJournaling,P[i],MaxRetriesPerTick);
               Orders.Add(OrderNumber);
               CurrenciesOrders[i]=OrderNumber;
               CurrenciesInMarket[i]=true;
               if(ProfitIncrement)
                 {
                  profitFinalLimit+=ProfitIncrementValue;
                 }
               lotSum+=Lot;

              }
           }
        }
     }

//---------- Entry Rules (Market) -----------
   for(int i=0;i<ArraySize(Currencies);i++)
     {
      if(CurrenciesInMarket[i]==false)
        {
         if(UseSAR)
           {

            if(((UseMA && MADirection[i]==1) || (UseHA && HADirection[i]==1) || (UseFractals && FractalsDirection[i]==1)) && SARDirection[i]==2) // Green Signal, and a cross bottom happend and stayed bottom
              {
               EntryValid[i]=1;
              }

            else  if(((UseMA && MADirection[i]==2) || (UseHA && HADirection[i]==2) || (UseFractals && FractalsDirection[i]==2)) && SARDirection[i]==1) // Red Signal, and a cross bottom happend and stayed bottom
              {
               EntryValid[i]=2;
              }
           }
         else
           {
            if((UseMA && MADirection[i]==1) || (UseHA && HADirection[i]==1) || (UseFractals && FractalsDirection[i]==1)) // Green Signal, and a cross bottom happend and stayed bottom
              {
               EntryValid[i]=1;
              }

            else  if((UseMA && MADirection[i]==2) || (UseHA && HADirection[i]==2) || (UseFractals && FractalsDirection[i]==2)) // Red Signal, and a cross bottom happend and stayed bottom
              {
               EntryValid[i]=2;
              }

           }
        }
     }

   for(int i=0;i<ArraySize(Currencies);i++)
     {
      if(EntrySignal(i)==1 && CountPosOrders(MagicNumber,Currencies[i])==0)
        { // Open Long Positions
         OrderNumber=OpenPositionMarket(Currencies[i],OP_BUY,Lot,0,0,MagicNumber,Slippage,OnJournaling,P[i],MaxRetriesPerTick);
         Orders.Add(OrderNumber);
         CurrenciesOrders[i]=OrderNumber;
         CurrenciesInMarket[i]=true;
         if(ProfitIncrement)
           {
            profitFinalLimit+=ProfitIncrementValue;
           }
         lotSum+=Lot;

        }

      if(EntrySignal(i)==2 && CountPosOrders(MagicNumber,Currencies[i])==0)
        { // Open Short Positions
         OrderNumber=OpenPositionMarket(Currencies[i],OP_SELL,Lot,0,0,MagicNumber,Slippage,OnJournaling,P[i],MaxRetriesPerTick);
         Orders.Add(OrderNumber);
         CurrenciesOrders[i]=OrderNumber;
         CurrenciesInMarket[i]=true;
         if(ProfitIncrement)
           {
            profitFinalLimit+=ProfitIncrementValue;
           }
         lotSum+=Lot;

        }
     }
   return (0);
  }
//+------------------------------------------------------------------+   
//|End of Start()                                                        |
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
// Start of EntrySignal()                                    			|
//+------------------------------------------------------------------+
int EntrySignal(int index)
  {
// Type: Customisable 
// Modify this function to suit your trading robot

// This function checks for entry signals

   int output=0;

   if(EntryValid[index]==1) // Green Signal, and a cross bottom happend and stayed bottom
     {
      output=1;
     }

   else if(EntryValid[index]==2) // Red Signal, and a cross up happend and stayed up
     {
      output=2;
     }

   return (output);
  }
//+------------------------------------------------------------------+
// End of EntrySignal()                                              |
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
// Start of ExitSignal()                                  			   |
//+------------------------------------------------------------------+
int ExitSignal(int index)
  {
// Type: Customisable 
// Modify this function to suit your trading robot

// This function checks for exit signals

   int output=0;

   if(ExitValid[index]==1) //Buy Signal
     {
      output=1;
     }
   else if(ExitValid[index]==2) //Sell Signal
     {
      output=2;
     }

   return (output);
  }
//+------------------------------------------------------------------+
// End of ExitSignal()                                               |
//+------------------------------------------------------------------+


//+------------------------------------------------------------------+
// Start of CheckLot()                                       				|
//+------------------------------------------------------------------+

double CheckLot(double lot,bool Journaling)
  {
// Type: Fixed Template 
// Do not edit unless you know what you're doing

// This function checks if our Lots to be trade satisfies any broker limitations

   double LotToOpen=0;
   LotToOpen=NormalizeDouble(lot,2);
   LotToOpen=MathFloor(LotToOpen/MarketInfo(Symbol(),MODE_LOTSTEP))*MarketInfo(Symbol(),MODE_LOTSTEP);

   if(LotToOpen<MarketInfo(Symbol(),MODE_MINLOT))LotToOpen=MarketInfo(Symbol(),MODE_MINLOT);
   if(LotToOpen>MarketInfo(Symbol(),MODE_MAXLOT))LotToOpen=MarketInfo(Symbol(),MODE_MAXLOT);
   LotToOpen=NormalizeDouble(LotToOpen,2);

   if(Journaling && LotToOpen!=lot)Print("EA Journaling: Trading Lot has been changed by CheckLot function. Requested lot: "+DoubleToString(lot)+". Lot to open: "+DoubleToString(LotToOpen));

   return(LotToOpen);
  }
//+------------------------------------------------------------------+
//| End of CheckLot()
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//| Start of GetCycleLots()                                                             |
//+------------------------------------------------------------------+
double GetCycleLots(int type)
  {

// Type: Customisable 
// Modify this function to suit your trading robot

// this function returns the net profit of the opened orders 

   double Lots=0;

   for(int i=0; i<OrdersTotal(); i++)
     {
      if(OrderSelect(i,SELECT_BY_POS,MODE_TRADES)==true && (OrderMagicNumber()==MagicNumber) && OrderType()==type)
         Lots+=OrderLots();
     }
   return(NormalizeDouble(Lots,2));

  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
// End of GetCycleLots()                                    		|
//+------------------------------------------------------------------+
double GetCycleProfit(int type)
  {

// Type: Customisable 
// Modify this function to suit your trading robot

// this function returns the net profit of the opened orders 

   double Profit=0;

   for(int i=0; i<OrdersTotal(); i++)
     {
      if(OrderSelect(i,SELECT_BY_POS,MODE_TRADES)==true && (OrderMagicNumber()==MagicNumber) && OrderType()==type)
         Profit+=OrderProfit();
     }

   return(NormalizeDouble(Profit,1));
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
// End of GetCycleProfit()                                    			|
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|Start of CountPosOrders()	" Count Positions"
//+------------------------------------------------------------------+
int CountPosOrders(string symbolName,int TYPE)
  {
// Type: Fixed Template 
// Do not edit unless you know what you're doing

// This function counts number of positions/orders of OrderType TYPE

   int orders=0;
   for(int i=0; i<OrdersTotal(); i++)
     {
      if(OrderSelect(i,SELECT_BY_POS,MODE_TRADES)==true && OrderSymbol()==symbolName && OrderType()==TYPE)
         orders++;
     }
   return(orders);

  }
//+------------------------------------------------------------------+
//| End of CountPosOrders() " Count Positions"
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|Start of OpenPositionMarket()
//+------------------------------------------------------------------+
int OpenPositionMarket(string symbolName,int TYPE,double LOT,double SL,double TP,int Magic,int Slip,bool Journaling,int K,int Max_Retries_Per_Tick)
  {
// Type: Fixed Template 
// Do not edit unless you know what you're doing 

// This function submits new orders

   int tries=0;
   string symbol=symbolName;
   int cmd=TYPE;
   double volume=CheckLot(LOT,Journaling);
   if(MarketInfo(symbol,MODE_MARGINREQUIRED)*volume>AccountFreeMargin())
     {
      Print("Can not open a trade. Not enough free margin to open "+volume+" on "+symbol);
      return(-1);
     }
   int slippage=Slip*K; // Slippage is in points. 1 point = 0.0001 on 4 digit broker and 0.00001 on a 5 digit broker
   string comment=" "+TYPE+"(#"+Magic+")";
   int magic=Magic;
   datetime expiration=0;
   color arrow_color=0;if(TYPE==OP_BUY)arrow_color=DodgerBlue;if(TYPE==OP_SELL)arrow_color=DeepPink;
   double stoploss=0;
   double takeprofit=0;
   double initTP = TP;
   double initSL = SL;
   int Ticket=-1;
   double price=0;

   while(tries<Max_Retries_Per_Tick) // Edits stops and take profits before the market order is placed
     {
      RefreshRates();
      if(TYPE==OP_BUY)price=Ask;if(TYPE==OP_SELL)price=Bid;

      // Sets Take Profits and Stop Loss. Check against Stop Level Limitations.
      if(TYPE==OP_BUY && SL!=0)
        {
         stoploss=NormalizeDouble(Ask-SL*K*MarketInfo(symbolName,MODE_POINT),MarketInfo(symbolName,MODE_DIGITS));
         if(Bid-stoploss<=MarketInfo(Symbol(),MODE_STOPLEVEL)*MarketInfo(symbolName,MODE_POINT))
           {
            stoploss=NormalizeDouble(Bid-MarketInfo(Symbol(),MODE_STOPLEVEL)*MarketInfo(symbolName,MODE_POINT),MarketInfo(symbolName,MODE_DIGITS));
            if(Journaling)Print("EA Journaling: Stop Loss changed from "+initSL+" to "+MarketInfo(Symbol(),MODE_STOPLEVEL)/K+" pips");
           }
        }
      if(TYPE==OP_SELL && SL!=0)
        {
         stoploss=NormalizeDouble(Bid+SL*K*MarketInfo(symbolName,MODE_POINT),MarketInfo(symbolName,MODE_DIGITS));
         if(stoploss-Ask<=MarketInfo(Symbol(),MODE_STOPLEVEL)*MarketInfo(symbolName,MODE_POINT))
           {
            stoploss=NormalizeDouble(Ask+MarketInfo(Symbol(),MODE_STOPLEVEL)*MarketInfo(symbolName,MODE_POINT),MarketInfo(symbolName,MODE_DIGITS));
            if(Journaling)Print("EA Journaling: Stop Loss changed from "+initSL+" to "+MarketInfo(Symbol(),MODE_STOPLEVEL)/K+" pips");
           }
        }
      if(TYPE==OP_BUY && TP!=0)
        {
         takeprofit=NormalizeDouble(Ask+TP*K*MarketInfo(symbolName,MODE_POINT),MarketInfo(symbolName,MODE_DIGITS));
         if(takeprofit-Bid<=MarketInfo(Symbol(),MODE_STOPLEVEL)*MarketInfo(symbolName,MODE_POINT))
           {
            takeprofit=NormalizeDouble(Ask+MarketInfo(Symbol(),MODE_STOPLEVEL)*MarketInfo(symbolName,MODE_POINT),MarketInfo(symbolName,MODE_DIGITS));
            if(Journaling)Print("EA Journaling: Take Profit changed from "+initTP+" to "+MarketInfo(Symbol(),MODE_STOPLEVEL)/K+" pips");
           }
        }
      if(TYPE==OP_SELL && TP!=0)
        {
         takeprofit=NormalizeDouble(Bid-TP*K*MarketInfo(symbolName,MODE_POINT),MarketInfo(symbolName,MODE_DIGITS));
         if(Ask-takeprofit<=MarketInfo(Symbol(),MODE_STOPLEVEL)*MarketInfo(symbolName,MODE_POINT))
           {
            takeprofit=NormalizeDouble(Bid-MarketInfo(Symbol(),MODE_STOPLEVEL)*MarketInfo(symbolName,MODE_POINT),MarketInfo(symbolName,MODE_DIGITS));
            if(Journaling)Print("EA Journaling: Take Profit changed from "+initTP+" to "+MarketInfo(Symbol(),MODE_STOPLEVEL)/K+" pips");
           }
        }
      if(Journaling)Print("EA Journaling: Trying to place a market order...");
      HandleTradingEnvironment(Journaling,RetryInterval);
      Ticket=OrderSend(symbol,cmd,volume,price,slippage,stoploss,takeprofit,comment,magic,expiration,arrow_color);
      if(Ticket>0)break;
      tries++;
     }

   if(Journaling && Ticket<0)Print("EA Journaling: Unexpected Error has happened. Error Description: "+GetErrorDescription(GetLastError()));
   if(Journaling && Ticket>0)
     {
      Print("EA Journaling: Order successfully placed. Ticket: "+Ticket);
     }
   return(Ticket);
  }
//+------------------------------------------------------------------+
//|End of OpenPositionMarket()
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|Start of CloseOrderPosition()
//+------------------------------------------------------------------+
void CloseOrderPosition(string symbolName,int TYPE,bool Journaling,int Magic,int Slip,int K)
  {
// Type: Fixed Template 
// Do not edit unless you know what you're doing
   int ordersPos=OrdersTotal();

   for(int i=ordersPos-1; i>=0; i--)
     {
      // Note: Once pending orders become positions, OP_BUYLIMIT AND OP_BUYSTOP becomes OP_BUY, OP_SELLLIMIT and OP_SELLSTOP becomes OP_SELL
      if(TYPE==OP_BUY || TYPE==OP_SELL)
        {
         if(OrderSelect(i,SELECT_BY_POS,MODE_TRADES)==true && OrderSymbol()==symbolName && OrderMagicNumber()==Magic && OrderType()==TYPE)
           {
            bool Closing=false;
            double Price=0;
            color arrow_color=0;if(TYPE==OP_BUY)arrow_color=MediumSeaGreen;if(TYPE==OP_SELL)arrow_color=DarkOrange;
            if(Journaling)Print("EA Journaling: Trying to close position "+OrderTicket()+" ...");
            HandleTradingEnvironment(Journaling,RetryInterval);
            if(TYPE==OP_BUY)Price=Bid; if(TYPE==OP_SELL)Price=Ask;
            Closing=OrderClose(OrderTicket(),OrderLots(),Price,Slip*K,arrow_color);
            if(Journaling && !Closing)Print("EA Journaling: Unexpected Error has happened. Error Description: "+GetErrorDescription(GetLastError()));
            if(Journaling && Closing)Print("EA Journaling: Position successfully closed.");
           }
        }
     }

  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void CloseAnOrder(string symbolName,int orderNumber,int TYPE,bool Journaling,int Magic,int Slip,int K)
  {
// Type: Fixed Template 
// Do not edit unless you know what you're doing
   int ordersPos=OrdersTotal();

   if(OrderSelect(orderNumber,SELECT_BY_TICKET,MODE_TRADES)==true && (OrderMagicNumber()==MagicNumber))
     {
      bool Closing=false;
      double Price=0;
      color arrow_color=0;if(TYPE==OP_BUY)arrow_color=MediumSeaGreen;if(TYPE==OP_SELL)arrow_color=DarkOrange;
      if(Journaling)Print("EA Journaling: Trying to close position "+OrderTicket()+" ...");
      HandleTradingEnvironment(Journaling,RetryInterval);
      if(TYPE==OP_BUY)Price=Bid; if(TYPE==OP_SELL)Price=Ask;
      Closing=OrderClose(OrderTicket(),OrderLots(),Price,Slip*K,arrow_color);
      if(Journaling && !Closing)Print("EA Journaling: Unexpected Error has happened. Error Description: "+GetErrorDescription(GetLastError()));
      if(Journaling && Closing)Print("EA Journaling: Position successfully closed.");
     }
  }
//+------------------------------------------------------------------+
//| End of CloseOrderPosition()
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
// Start of getP()                                                   |
//+------------------------------------------------------------------+

int GetP(string symbolName)
  {

   int output;

   if(MarketInfo(symbolName,MODE_DIGITS)==5 || MarketInfo(symbolName,MODE_DIGITS)==3) output=10;else output=1;

   return(output);
  }
//+------------------------------------------------------------------+
// End of GetP()                                                    	|
//+------------------------------------------------------------------+


//+------------------------------------------------------------------+
// Start of GetYenAdjustFactor()                                     |
//+------------------------------------------------------------------+

int GetYenAdjustFactor(string symbolName)
  {

   int output=1;

   if(MarketInfo(symbolName,MODE_DIGITS)==3 || MarketInfo(symbolName,MODE_DIGITS)==2) output=100;

   return(output);

  }
//+------------------------------------------------------------------+
// End of GetYenAdjustFactor()                                       |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//| Start of UpdateTrailingList()                              
//+------------------------------------------------------------------+


//+------------------------------------------------------------------+
//| Start of HandleTradingEnvironment()                                         
//+------------------------------------------------------------------+
void HandleTradingEnvironment(bool Journaling,int Retry_Interval)
  {
// Type: Fixed Template 
// Do not edit unless you know what you're doing 

// This function checks for errors

   if(IsTradeAllowed()==true)return;
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
   if(!IsConnected())
     {
      if(Journaling)Print("EA Journaling: Terminal is not connected to server...");
      return;
     }
   if(!IsTradeAllowed() && Journaling)Print("EA Journaling: Trade is not alowed for some reason...");
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
   if(IsConnected() && !IsTradeAllowed())
     {
      while(IsTradeContextBusy()==true)
        {
         if(Journaling)Print("EA Journaling: Trading context is busy... Will wait a bit...");
         Sleep(Retry_Interval);
        }
     }
   RefreshRates();
  }
//+------------------------------------------------------------------+
//| End of HandleTradingEnvironment()                              
//+------------------------------------------------------------------+  
//+------------------------------------------------------------------+
//| Start of GetErrorDescription()                                               
//+------------------------------------------------------------------+
string GetErrorDescription(int error)
  {
// Type: Fixed Template 
// Do not edit unless you know what you're doing

// This function returns the exact error

   string ErrorDescription="";
//---
   switch(error)
     {
      case 0:     ErrorDescription = "NO Error. Everything should be good.";                                    break;
      case 1:     ErrorDescription = "No error returned, but the result is unknown";                            break;
      case 2:     ErrorDescription = "Common error";                                                            break;
      case 3:     ErrorDescription = "Invalid trade parameters";                                                break;
      case 4:     ErrorDescription = "Trade server is busy";                                                    break;
      case 5:     ErrorDescription = "Old version of the client terminal";                                      break;
      case 6:     ErrorDescription = "No connection with trade server";                                         break;
      case 7:     ErrorDescription = "Not enough rights";                                                       break;
      case 8:     ErrorDescription = "Too frequent requests";                                                   break;
      case 9:     ErrorDescription = "Malfunctional trade operation";                                           break;
      case 64:    ErrorDescription = "Account disabled";                                                        break;
      case 65:    ErrorDescription = "Invalid account";                                                         break;
      case 128:   ErrorDescription = "Trade timeout";                                                           break;
      case 129:   ErrorDescription = "Invalid price";                                                           break;
      case 130:   ErrorDescription = "Invalid stops";                                                           break;
      case 131:   ErrorDescription = "Invalid trade volume";                                                    break;
      case 132:   ErrorDescription = "Market is closed";                                                        break;
      case 133:   ErrorDescription = "Trade is disabled";                                                       break;
      case 134:   ErrorDescription = "Not enough money";                                                        break;
      case 135:   ErrorDescription = "Price changed";                                                           break;
      case 136:   ErrorDescription = "Off quotes";                                                              break;
      case 137:   ErrorDescription = "Broker is busy";                                                          break;
      case 138:   ErrorDescription = "Requote";                                                                 break;
      case 139:   ErrorDescription = "Order is locked";                                                         break;
      case 140:   ErrorDescription = "Long positions only allowed";                                             break;
      case 141:   ErrorDescription = "Too many requests";                                                       break;
      case 145:   ErrorDescription = "Modification denied because order too close to market";                   break;
      case 146:   ErrorDescription = "Trade context is busy";                                                   break;
      case 147:   ErrorDescription = "Expirations are denied by broker";                                        break;
      case 148:   ErrorDescription = "Too many open and pending orders (more than allowed)";                    break;
      case 4000:  ErrorDescription = "No error";                                                                break;
      case 4001:  ErrorDescription = "Wrong function pointer";                                                  break;
      case 4002:  ErrorDescription = "Array index is out of range";                                             break;
      case 4003:  ErrorDescription = "No memory for function call stack";                                       break;
      case 4004:  ErrorDescription = "Recursive stack overflow";                                                break;
      case 4005:  ErrorDescription = "Not enough stack for parameter";                                          break;
      case 4006:  ErrorDescription = "No memory for parameter string";                                          break;
      case 4007:  ErrorDescription = "No memory for temp string";                                               break;
      case 4008:  ErrorDescription = "Not initialized string";                                                  break;
      case 4009:  ErrorDescription = "Not initialized string in array";                                         break;
      case 4010:  ErrorDescription = "No memory for array string";                                              break;
      case 4011:  ErrorDescription = "Too long string";                                                         break;
      case 4012:  ErrorDescription = "Remainder from zero divide";                                              break;
      case 4013:  ErrorDescription = "Zero divide";                                                             break;
      case 4014:  ErrorDescription = "Unknown command";                                                         break;
      case 4015:  ErrorDescription = "Wrong jump (never generated error)";                                      break;
      case 4016:  ErrorDescription = "Not initialized array";                                                   break;
      case 4017:  ErrorDescription = "DLL calls are not allowed";                                               break;
      case 4018:  ErrorDescription = "Cannot load library";                                                     break;
      case 4019:  ErrorDescription = "Cannot call function";                                                    break;
      case 4020:  ErrorDescription = "Expert function calls are not allowed";                                   break;
      case 4021:  ErrorDescription = "Not enough memory for temp string returned from function";                break;
      case 4022:  ErrorDescription = "System is busy (never generated error)";                                  break;
      case 4050:  ErrorDescription = "Invalid function parameters count";                                       break;
      case 4051:  ErrorDescription = "Invalid function parameter value";                                        break;
      case 4052:  ErrorDescription = "String function internal error";                                          break;
      case 4053:  ErrorDescription = "Some array error";                                                        break;
      case 4054:  ErrorDescription = "Incorrect series array using";                                            break;
      case 4055:  ErrorDescription = "Custom indicator error";                                                  break;
      case 4056:  ErrorDescription = "Arrays are incompatible";                                                 break;
      case 4057:  ErrorDescription = "Global variables processing error";                                       break;
      case 4058:  ErrorDescription = "Global variable not found";                                               break;
      case 4059:  ErrorDescription = "Function is not allowed in testing mode";                                 break;
      case 4060:  ErrorDescription = "Function is not confirmed";                                               break;
      case 4061:  ErrorDescription = "Send mail error";                                                         break;
      case 4062:  ErrorDescription = "String parameter expected";                                               break;
      case 4063:  ErrorDescription = "Integer parameter expected";                                              break;
      case 4064:  ErrorDescription = "Double parameter expected";                                               break;
      case 4065:  ErrorDescription = "Array as parameter expected";                                             break;
      case 4066:  ErrorDescription = "Requested history data in updating state";                                break;
      case 4067:  ErrorDescription = "Some error in trading function";                                          break;
      case 4099:  ErrorDescription = "End of file";                                                             break;
      case 4100:  ErrorDescription = "Some file error";                                                         break;
      case 4101:  ErrorDescription = "Wrong file name";                                                         break;
      case 4102:  ErrorDescription = "Too many opened files";                                                   break;
      case 4103:  ErrorDescription = "Cannot open file";                                                        break;
      case 4104:  ErrorDescription = "Incompatible access to a file";                                           break;
      case 4105:  ErrorDescription = "No order selected";                                                       break;
      case 4106:  ErrorDescription = "Unknown symbol";                                                          break;
      case 4107:  ErrorDescription = "Invalid price";                                                           break;
      case 4108:  ErrorDescription = "Invalid ticket";                                                          break;
      case 4109:  ErrorDescription = "EA is not allowed to trade is not allowed. ";                             break;
      case 4110:  ErrorDescription = "Longs are not allowed. Check the expert properties";                      break;
      case 4111:  ErrorDescription = "Shorts are not allowed. Check the expert properties";                     break;
      case 4200:  ErrorDescription = "Object exists already";                                                   break;
      case 4201:  ErrorDescription = "Unknown object property";                                                 break;
      case 4202:  ErrorDescription = "Object does not exist";                                                   break;
      case 4203:  ErrorDescription = "Unknown object type";                                                     break;
      case 4204:  ErrorDescription = "No object name";                                                          break;
      case 4205:  ErrorDescription = "Object coordinates error";                                                break;
      case 4206:  ErrorDescription = "No specified subwindow";                                                  break;
      case 4207:  ErrorDescription = "Some error in object function";                                           break;
      default:    ErrorDescription = "No error or error is unknown";
     }
   return(ErrorDescription);
  }
//+------------------------------------------------------------------+
//| End of GetErrorDescription()                                         
//+------------------------------------------------------------------+


//+------------------------------------------------------------------+
//| Start of ChartSettings()                                         
//+------------------------------------------------------------------+
void ChartSettings(long chartId)
  {
   ChartSetInteger(chartId,CHART_SHOW_GRID,0);
   ChartSetInteger(chartId,CHART_MODE,CHART_CANDLES);
   ChartSetInteger(chartId,CHART_AUTOSCROLL,0,True);
   ChartSetInteger(chartId,CHART_SHIFT,0,True);

   WindowRedraw();
  }
//+------------------------------------------------------------------+
//| End of ChartSettings()                                         
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//|End of IsLossLimitBreached()                                      
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//| Start of IsProfitLimitBreached()                                    
//+------------------------------------------------------------------+
void IsProfitLimitBreached(double ProfitLimitValue,bool Journaling)
  {

// Type: Fixed Template 
// Do not edit unless you know what you're doing

// This function determines if our maximum Profit threshold is breached
   double profitPrint;
   double orderProfit;
   double orderTicket;
   int orderType;

   if(cycleProfit>ProfitLimitValue)
     {
      for(int i=0;i<ArraySize(Currencies);i++)
        {
         if(OrderSelect(CurrenciesOrders[i],SELECT_BY_TICKET,MODE_TRADES)==true && (OrderMagicNumber()==MagicNumber))
           {
            orderProfit=OrderProfit();
            orderTicket= OrderTicket();
            orderType=OrderType();

            CloseAnOrder(Currencies[i],orderTicket,orderType,OnJournaling,MagicNumber,Slippage,P[i]);

            if(orderProfit>0 && ReopenIfProfit)
              {
               OrderNumber=OpenPositionMarket(Currencies[i],orderType,Lot,0,0,MagicNumber,Slippage,OnJournaling,P[i],MaxRetriesPerTick);
               Orders.Add(OrderNumber);
               CurrenciesOrders[i]=OrderNumber;
               CurrenciesInMarket[i]=true;
               if(ProfitIncrement)
                 {
                  profitFinalLimit+=ProfitIncrementValue;
                 }
               lotSum+=Lot;

              }
            else
              {
               CurrenciesOrders[i]=0;
               CurrenciesInMarket[i]=false;
              }
           }
        }
      profitFinalLimit=ProfitLimit;
      profitPrint=NormalizeDouble(cycleProfit,4);
      if(Journaling) Print("Profit threshold breached. Current Profit: "+profitPrint);
     }
  }
//+------------------------------------------------------------------+
//|End of IsProfitLimitBreached                                                                   |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//|Start of DrawChartInfo                                                                  |
//+------------------------------------------------------------------+

void DrawChartInfo()
  {

   if(DrawInfoBackground)
     {
      ObjectCreate(ChartID(),ObjName+"InfoBackground",OBJ_RECTANGLE_LABEL,0,0,0);
      ObjectSetInteger(ChartID(),ObjName+"InfoBackground",OBJPROP_XSIZE,-200);
      ObjectSetInteger(ChartID(),ObjName+"InfoBackground",OBJPROP_YSIZE,240);
      ObjectSetInteger(ChartID(),ObjName+"InfoBackground",OBJPROP_XDISTANCE,xDistance);
      ObjectSetInteger(ChartID(),ObjName+"InfoBackground",OBJPROP_YDISTANCE,30);
      ObjectSetInteger(ChartID(),ObjName+"InfoBackground",OBJPROP_CORNER,CORNER_RIGHT_UPPER);
      ObjectSetInteger(ChartID(),ObjName+"InfoBackground",OBJPROP_BGCOLOR,Black);
      ObjectSetInteger(ChartID(),ObjName+"InfoBackground",OBJPROP_BORDER_COLOR,Black);
      ObjectSetInteger(ChartID(),ObjName+"InfoBackground",OBJPROP_BACK,false);
      ObjectSetInteger(ChartID(),ObjName+"InfoBackground",OBJPROP_SELECTABLE,false);
      ObjectSetInteger(ChartID(),ObjName+"InfoBackground",OBJPROP_SELECTED,false);
      ObjectSetInteger(ChartID(),ObjName+"InfoBackground",OBJPROP_HIDDEN,false);
      ObjectSetInteger(ChartID(),ObjName+"InfoBackground",OBJPROP_BORDER_TYPE,BORDER_FLAT);
     }

   ObjectCreate(ChartID(),ObjName+"HelmiFX2",OBJ_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"HelmiFX2",OBJPROP_TEXT,"www.helmifx.com");
   ObjectSetInteger(ChartID(),ObjName+"HelmiFX2",OBJPROP_CORNER,CORNER_RIGHT_UPPER);
   ObjectSetInteger(ChartID(),ObjName+"HelmiFX2",OBJPROP_XDISTANCE,xDistance);
   ObjectSetInteger(ChartID(),ObjName+"HelmiFX2",OBJPROP_YDISTANCE,37);
   ObjectSetString(ChartID(),ObjName+"HelmiFX2",OBJPROP_FONT,"Verdana");
   ObjectSetInteger(ChartID(),ObjName+"HelmiFX2",OBJPROP_FONTSIZE,8);
   ObjectSetInteger(ChartID(),ObjName+"HelmiFX2",OBJPROP_ANCHOR,ANCHOR_RIGHT);
   ObjectSetInteger(ChartID(),ObjName+"HelmiFX2",OBJPROP_COLOR,White);

   ObjectCreate(ChartID(),ObjName+"Equity",OBJ_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"Equity",OBJPROP_TEXT,"Equity : "+DoubleToStr(AccountEquity(),2));
   ObjectSetInteger(ChartID(),ObjName+"Equity",OBJPROP_CORNER,CORNER_RIGHT_UPPER);
   ObjectSetInteger(ChartID(),ObjName+"Equity",OBJPROP_XDISTANCE,xDistance);
   ObjectSetInteger(ChartID(),ObjName+"Equity",OBJPROP_YDISTANCE,62);
   ObjectSetString(ChartID(),ObjName+"Equity",OBJPROP_FONT,"Verdana");
   ObjectSetInteger(ChartID(),ObjName+"Equity",OBJPROP_FONTSIZE,10);
   ObjectSetInteger(ChartID(),ObjName+"Equity",OBJPROP_ANCHOR,ANCHOR_RIGHT);
   ObjectSetInteger(ChartID(),ObjName+"Equity",OBJPROP_COLOR,BurlyWood);

   ObjectCreate(ChartID(),ObjName+"Balance",OBJ_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"Balance",OBJPROP_TEXT,"Balance : "+DoubleToStr(AccountBalance(),2));
   ObjectSetInteger(ChartID(),ObjName+"Balance",OBJPROP_CORNER,CORNER_RIGHT_UPPER);
   ObjectSetInteger(ChartID(),ObjName+"Balance",OBJPROP_XDISTANCE,xDistance);
   ObjectSetInteger(ChartID(),ObjName+"Balance",OBJPROP_YDISTANCE,87);
   ObjectSetString(ChartID(),ObjName+"Balance",OBJPROP_FONT,"Verdana");
   ObjectSetInteger(ChartID(),ObjName+"Balance",OBJPROP_FONTSIZE,10);
   ObjectSetInteger(ChartID(),ObjName+"Balance",OBJPROP_ANCHOR,ANCHOR_RIGHT);
   ObjectSetInteger(ChartID(),ObjName+"Balance",OBJPROP_COLOR,SkyBlue);

   ObjectCreate(ChartID(),ObjName+"ClosedBuyPositionsProfit",OBJ_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"ClosedBuyPositionsProfit",OBJPROP_TEXT,"Closed Buy "+DoubleToString(buyLastClosedProfit,2));
   ObjectSetInteger(ChartID(),ObjName+"ClosedBuyPositionsProfit",OBJPROP_CORNER,CORNER_RIGHT_UPPER);
   ObjectSetInteger(ChartID(),ObjName+"ClosedBuyPositionsProfit",OBJPROP_XDISTANCE,xDistance);
   ObjectSetInteger(ChartID(),ObjName+"ClosedBuyPositionsProfit",OBJPROP_YDISTANCE,112);
   ObjectSetString(ChartID(),ObjName+"ClosedBuyPositionsProfit",OBJPROP_FONT,"Verdana");
   ObjectSetInteger(ChartID(),ObjName+"ClosedBuyPositionsProfit",OBJPROP_FONTSIZE,11);
   ObjectSetInteger(ChartID(),ObjName+"ClosedBuyPositionsProfit",OBJPROP_ANCHOR,ANCHOR_RIGHT);
   ObjectSetInteger(ChartID(),ObjName+"ClosedBuyPositionsProfit",OBJPROP_COLOR,Blue);

   ObjectCreate(ChartID(),ObjName+"ClosedSellPositionsProfit",OBJ_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"ClosedSellPositionsProfit",OBJPROP_TEXT,"Closed Sell "+DoubleToString(sellLastClosedProfit,2));
   ObjectSetInteger(ChartID(),ObjName+"ClosedSellPositionsProfit",OBJPROP_CORNER,CORNER_RIGHT_UPPER);
   ObjectSetInteger(ChartID(),ObjName+"ClosedSellPositionsProfit",OBJPROP_XDISTANCE,xDistance);
   ObjectSetInteger(ChartID(),ObjName+"ClosedSellPositionsProfit",OBJPROP_YDISTANCE,137);
   ObjectSetString(ChartID(),ObjName+"ClosedSellPositionsProfit",OBJPROP_FONT,"Verdana");
   ObjectSetInteger(ChartID(),ObjName+"ClosedSellPositionsProfit",OBJPROP_FONTSIZE,11);
   ObjectSetInteger(ChartID(),ObjName+"ClosedSellPositionsProfit",OBJPROP_ANCHOR,ANCHOR_RIGHT);
   ObjectSetInteger(ChartID(),ObjName+"ClosedSellPositionsProfit",OBJPROP_COLOR,Chartreuse);

   ObjectCreate(ChartID(),ObjName+"CurrentBuyPositionsProfit",OBJ_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"CurrentBuyPositionsProfit",OBJPROP_TEXT,"Current Buy "+DoubleToString(buyCycleProfit,2));
   ObjectSetInteger(ChartID(),ObjName+"CurrentBuyPositionsProfit",OBJPROP_CORNER,CORNER_RIGHT_UPPER);
   ObjectSetInteger(ChartID(),ObjName+"CurrentBuyPositionsProfit",OBJPROP_XDISTANCE,xDistance);
   ObjectSetInteger(ChartID(),ObjName+"CurrentBuyPositionsProfit",OBJPROP_YDISTANCE,162);
   ObjectSetString(ChartID(),ObjName+"CurrentBuyPositionsProfit",OBJPROP_FONT,"Verdana");
   ObjectSetInteger(ChartID(),ObjName+"CurrentBuyPositionsProfit",OBJPROP_FONTSIZE,11);
   ObjectSetInteger(ChartID(),ObjName+"CurrentBuyPositionsProfit",OBJPROP_ANCHOR,ANCHOR_RIGHT);
   ObjectSetInteger(ChartID(),ObjName+"CurrentBuyPositionsProfit",OBJPROP_COLOR,GreenYellow);

   ObjectCreate(ChartID(),ObjName+"CurrentSellPositionsProfit",OBJ_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"CurrentSellPositionsProfit",OBJPROP_TEXT,"Current Sell "+DoubleToString(sellCycleProfit,2));
   ObjectSetInteger(ChartID(),ObjName+"CurrentSellPositionsProfit",OBJPROP_CORNER,CORNER_RIGHT_UPPER);
   ObjectSetInteger(ChartID(),ObjName+"CurrentSellPositionsProfit",OBJPROP_XDISTANCE,xDistance);
   ObjectSetInteger(ChartID(),ObjName+"CurrentSellPositionsProfit",OBJPROP_YDISTANCE,187);
   ObjectSetString(ChartID(),ObjName+"CurrentSellPositionsProfit",OBJPROP_FONT,"Verdana");
   ObjectSetInteger(ChartID(),ObjName+"CurrentSellPositionsProfit",OBJPROP_FONTSIZE,11);
   ObjectSetInteger(ChartID(),ObjName+"CurrentSellPositionsProfit",OBJPROP_ANCHOR,ANCHOR_RIGHT);
   ObjectSetInteger(ChartID(),ObjName+"CurrentSellPositionsProfit",OBJPROP_COLOR,IndianRed);

   ObjectCreate(ChartID(),ObjName+"NetProfit",OBJ_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"NetProfit",OBJPROP_TEXT,"Net Profit "+DoubleToString(netProfit,2));
   ObjectSetInteger(ChartID(),ObjName+"NetProfit",OBJPROP_CORNER,CORNER_RIGHT_UPPER);
   ObjectSetInteger(ChartID(),ObjName+"NetProfit",OBJPROP_XDISTANCE,xDistance);
   ObjectSetInteger(ChartID(),ObjName+"NetProfit",OBJPROP_YDISTANCE,212);
   ObjectSetString(ChartID(),ObjName+"NetProfit",OBJPROP_FONT,"Verdana");
   ObjectSetInteger(ChartID(),ObjName+"NetProfit",OBJPROP_FONTSIZE,11);
   ObjectSetInteger(ChartID(),ObjName+"NetProfit",OBJPROP_ANCHOR,ANCHOR_RIGHT);
   ObjectSetInteger(ChartID(),ObjName+"NetProfit",OBJPROP_COLOR,DarkTurquoise);

   ObjectCreate(ChartID(),ObjName+"CycleProfit",OBJ_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"CycleProfit",OBJPROP_TEXT,"Cycle Profit "+DoubleToString(cycleProfit,2));
   ObjectSetInteger(ChartID(),ObjName+"CycleProfit",OBJPROP_CORNER,CORNER_RIGHT_UPPER);
   ObjectSetInteger(ChartID(),ObjName+"CycleProfit",OBJPROP_XDISTANCE,xDistance);
   ObjectSetInteger(ChartID(),ObjName+"CycleProfit",OBJPROP_YDISTANCE,237);
   ObjectSetString(ChartID(),ObjName+"CycleProfit",OBJPROP_FONT,"Verdana");
   ObjectSetInteger(ChartID(),ObjName+"CycleProfit",OBJPROP_FONTSIZE,11);
   ObjectSetInteger(ChartID(),ObjName+"CycleProfit",OBJPROP_ANCHOR,ANCHOR_RIGHT);
   ObjectSetInteger(ChartID(),ObjName+"CycleProfit",OBJPROP_COLOR,Pink);

   ObjectCreate(ChartID(),ObjName+"TargetProfit",OBJ_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"TargetProfit",OBJPROP_TEXT,"Target Profit: "+profitFinalLimit);
   ObjectSetInteger(ChartID(),ObjName+"TargetProfit",OBJPROP_CORNER,CORNER_LEFT_UPPER);
   ObjectSetInteger(ChartID(),ObjName+"TargetProfit",OBJPROP_XDISTANCE,xDistance);
   ObjectSetInteger(ChartID(),ObjName+"TargetProfit",OBJPROP_YDISTANCE,30);
   ObjectSetString(ChartID(),ObjName+"TargetProfit",OBJPROP_FONT,"Verdana");
   ObjectSetInteger(ChartID(),ObjName+"TargetProfit",OBJPROP_FONTSIZE,10);
   ObjectSetInteger(ChartID(),ObjName+"TargetProfit",OBJPROP_ANCHOR,ANCHOR_LEFT);
   ObjectSetInteger(ChartID(),ObjName+"TargetProfit",OBJPROP_COLOR,Moccasin);

  }
//+------------------------------------------------------------------+
// End of DrawChartInfo()                                          |
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
// Start of UpdateChartInfo()                                          |
//+------------------------------------------------------------------+
void UpdateChartInfo()
  {
   ObjectSetString(ChartID(),ObjName+"ClosedBuyPositionsProfit",OBJPROP_TEXT,"Closed Buy "+DoubleToString(buyLastClosedProfit,2));
   ObjectSetString(ChartID(),ObjName+"ClosedSellPositionsProfit",OBJPROP_TEXT,"Closed Sell "+DoubleToString(sellLastClosedProfit,2));
   ObjectSetString(ChartID(),ObjName+"CurrentBuyPositionsProfit",OBJPROP_TEXT,"Current Buy "+DoubleToString(buyCycleProfit,2));
   ObjectSetString(ChartID(),ObjName+"CurrentSellPositionsProfit",OBJPROP_TEXT,"Current Sell "+DoubleToString(sellCycleProfit,2));
   ObjectSetString(ChartID(),ObjName+"NetProfit",OBJPROP_TEXT,"Net Profit "+DoubleToString(netProfit,2));
   ObjectSetString(ChartID(),ObjName+"Equity",OBJPROP_TEXT,"Equity : "+DoubleToStr(AccountEquity(),2));
   ObjectSetString(ChartID(),ObjName+"Balance",OBJPROP_TEXT,"Balance : "+DoubleToStr(AccountBalance(),2));
   ObjectSetString(ChartID(),ObjName+"CycleProfit",OBJPROP_TEXT,"Cycle Profit "+DoubleToString(cycleProfit,2));
   ObjectSetString(ChartID(),ObjName+"TargetProfit",OBJPROP_TEXT,"Target Profit: "+DoubleToStr(profitFinalLimit,2));
  }
//+------------------------------------------------------------------+
// End of UpdateChartInfo()                                          |
//+------------------------------------------------------------------+


//+------------------------------------------------------------------+
//| Start of CheckSLTPExit()                                         |
//+------------------------------------------------------------------+

void CheckSLTPExit()
  {

   for(int i=Orders.Total()-1; i>=0; i--)
      //+------------------------------------------------------------------+
      //|                                                                  |
      //+------------------------------------------------------------------+
     {
      if(OrderSelect(Orders.At(i),SELECT_BY_TICKET,MODE_TRADES)==true && (OrderMagicNumber()==MagicNumber))
        {
         if(OrderCloseTime()!=0)
           {
            if(OrderType()==OP_BUY)
              {
               buyLastClosedProfit+=OrderProfit();
               Orders.Delete(i);
              }
            else if(OrderType()==OP_SELL)
              {
               sellLastClosedProfit+=OrderProfit();
               Orders.Delete(i);
              }
           }
        }
     }

  }
//+------------------------------------------------------------------+
//| End of CheckSLTPExit()                                         |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//| Start of isNewBar()                                         
//+------------------------------------------------------------------+
bool isNewBar()
  {
//--- memorize the time of opening of the last bar in the static variable
   static datetime last_time=0;
//--- current time
   datetime lastbar_time=SeriesInfoInteger(Symbol(),Period(),SERIES_LASTBAR_DATE);
//--- if it is the first call of the function
   if(last_time==0)
     {
      //--- set the time and exit
      last_time=lastbar_time;
      return(false);
     }
//--- if the time differs
   if(last_time!=lastbar_time)
     {
      //--- memorize the time and return true
      last_time=lastbar_time;
      return(true);
     }
//--- if we passed to this line, then the bar is not new; return false
   return(false);
  }
//+------------------------------------------------------------------+
//| End of isNewBar()                                         
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//| End of isItTime()                                         
//+------------------------------------------------------------------+

bool isItTime()
  {
   Current_Time=TimeHour(TimeCurrent());
   if(Start<0)Start=0;
   if(End<0) End=0;
   if(Start==0 || Start>24) Start=24; if(End==0 || End>24) End=24; if(Current_Time==0) Current_Time=24;

   if(Start<End)
      if( (Current_Time < Start) || (Current_Time >= End) ) return(false);

   if(Start>End)
      if( (Current_Time < Start) && (Current_Time >= End) ) return(false);
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
   if(Start==End)
     {
      return false;
     }

   return(true);
  }
//+------------------------------------------------------------------+
//| End of isItTime()                                         
//+------------------------------------------------------------------+



//+------------------------------------------------------------------+
// Start of CheckHAHighPDirection()                                    			|
//+------------------------------------------------------------------+
int CheckHAHighPDirection(double openHighPeriodShift1,double closeHighPeriodShift1)
  {
// Type: Customisable 
// Modify this function to suit your trading robot

// This function checks for the direction of the HA Indicator on the High Period

   int output=0;
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
   if(openHighPeriodShift1<closeHighPeriodShift1)
     {
      output=1;         //White
        }else{
      //+------------------------------------------------------------------+
      //|                                                                  |
      //+------------------------------------------------------------------+
      output=2;         //Red
     }

   return (output);
  }
//+------------------------------------------------------------------+
// End of CheckHAHighPDirection()                                              |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//| End of SARCross()                                         
//+------------------------------------------------------------------+
int SARCross(double SARShift1,double SARShift2,double HighLast,double LowLast,int index)
  {
// Type: Customizable
// Do not edit unless you know what you're doing

// This function determines if a cross happened between Parabolic SAR Values : SARShift1,SARShift2
//----
   if(SARShift1>SARShift2 && SARShift1>=HighLast && SARShift2<=LowLast)
     {
      SARCurrentDirection[index]=1;  // line1 above line2
     }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
   else if(SARShift1<SARShift2 && SARShift2>=HighLast && SARShift1<=LowLast)
     {
      SARCurrentDirection[index]=2;  // line1 below line2 
     }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
   else
     {
      SARCurrentDirection[index]=SARLastDirection[index];
     }
//----
   if(SARFirstTime[index]==true) // Need to check if this is the first time the function is run
     {
      SARFirstTime[index]=false; // Change variable to false
      SARLastDirection[index]=SARCurrentDirection[index]; // Set new direction
      return (0);
     }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
   if(SARCurrentDirection[index]!=SARLastDirection[index] && SARFirstTime[index]==false) // If not the first time and there is a direction change
     {
      SARLastDirection[index]=SARCurrentDirection[index]; // Set new direction
      return(SARCurrentDirection[index]); // 1 for up, 2 for down
     }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
   else
     {
      return(0);  // No direction change
     }
  }
//+------------------------------------------------------------------+
// End of  SARCross()                            |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//| Start of CheckAccount()                                         
//+------------------------------------------------------------------+


bool CheckAccount()
  {
   if(AccountInfoInteger(ACCOUNT_TRADE_MODE)==ACCOUNT_TRADE_MODE_DEMO)
     {
      return true;
     }

   else if(AccountInfoInteger(ACCOUNT_TRADE_MODE)==ACCOUNT_TRADE_MODE_REAL)
     {
      if(MQLInfoInteger(MQL_TESTER))
        {
         return true;
        }
      Print("Your account type is Live,You cant Proceed.\n HelmiFX");
      return (false);
     }

   else if(AccountInfoInteger(ACCOUNT_TRADE_MODE)==ACCOUNT_TRADE_MODE_CONTEST)
     {
      if(MQLInfoInteger(MQL_TESTER))
        {
         return true;
        }

      Print("Your account type is Contest,You cant Proceed.\n HelmiFX");
      return (false);
     }

   return false;
  }
//+------------------------------------------------------------------+
//| End of CheckAccount()                                         
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|Start of CurrenciesSplit()                                                                |
//+------------------------------------------------------------------+

void CurrenciesSplit()
  {

// Type: Customisable 
// Modify this function to suit your trading robot

// this function splits the multiplySequence into multiplySequenceArray

   static bool firstTime=true;

   if(firstTime)
     {
      StringSplit(CurrenciesList,',',Currencies); // Splitting the multiplication sequene of the lot
      firstTime=false;
     }

   for(int i=0;i<ArraySize(Currencies);i++)
     {
      StringToUpper(Currencies[i]);
     }

  }
//+------------------------------------------------------------------+
//|End of CurrenciesSplit()                                                                |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//|Start of ChartWindow()                                                                |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
int ChartWindow(string SymbolName)
  {
   int hFile,SymbolsTotal,hTerminal,hWnd;

   hFile=FileOpenHistory("symbols.sel",FILE_BIN|FILE_READ);
   if(hFile < 0) return(-1);

   SymbolsTotal=(FileSize(hFile)-4)/128;
   FileSeek(hFile,4,SEEK_SET);

   hTerminal=GetAncestor(WindowHandle(Symbol(),Period()),2);

   hWnd = GetDlgItem(hTerminal, 0xE81C);
   hWnd = GetDlgItem(hWnd, 0x50);
   hWnd = GetDlgItem(hWnd, 0x8A71);

   PostMessageA(hWnd,WM_KEYDOWN,VK_HOME,0);

   for(int i=0; i<SymbolsTotal; i++)
     {
      if(FileReadString(hFile,12)==SymbolName)
        {
         PostMessageA(hTerminal,WM_COMMAND,33160,0);
         return(0);
        }
      PostMessageA(hWnd,WM_KEYDOWN,VK_DOWN,0);
      FileSeek(hFile,116,SEEK_CUR);
     }

   FileClose(hFile);

   return(-1);
  }
//+------------------------------------------------------------------+
void StartCustomIndicator(int hWnd,string IndicatorName,bool AutomaticallyAcceptDefaults=true)
  {
   uchar name2[];
   StringToCharArray(IndicatorName,name2,0,StringLen(IndicatorName));

   int MessageNumber=RegisterWindowMessageW("MetaTrader4_Internal_Message");
   int r=PostMessageW(hWnd,MessageNumber,15,name2);
   Sleep(10);
  }
//+------------------------------------------------------------------+
//| Start of MACross()                                                                 |
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
int MACross(double fastMA,double slowMA,int index)
  {
// Type: Customizable
// Do not edit unless you know what you're doing

// This function determines if a cross happened between 2 lines/data set here : FastEMA,SlowEMA
//----
   if(fastMA>slowMA)
      MACurrentDirection[index]=1;  // line1 above line2
   if(fastMA<slowMA)
      MACurrentDirection[index]=2;  // line1 below line2
//----
   if(MAFirstTime[index]==true) // Need to check if this is the first time the function is run
     {
      MAFirstTime[index]=false; // Change variable to false
      MALastDirection[index]=MACurrentDirection[index]; // Set new direction
      return (0);
     }

   if(MACurrentDirection[index]!=MALastDirection[index] && MAFirstTime[index]==false) // If not the first time and there is a direction change
     {
      MALastDirection[index]=MACurrentDirection[index]; // Set new direction
      return(MACurrentDirection[index]); // 1 for up, 2 for down
     }
   else
     {
      return(0);  // No direction change
     }
  }
//+------------------------------------------------------------------+
//|End of MACross()                                                                  |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//|Start of CheckHADirection()                                                                  |
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
int CheckHADirection(double openShift,double closeShift)
  {
// Type: Customisable 
// Modify this function to suit your trading robot

// This function checks for the direction of the HA Indicator on the High Period

   int output=0;
   if(openShift<closeShift)
     {
      output=1;         //White
        }else{
      output=2;         //Red
     }

   return (output);
  }
//+------------------------------------------------------------------+
//|End of CheckHADirection()                                                                  |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//|Start of CheckFractals()                                                                  |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
int CheckFractals(double FractalUp,double FractalDown)
  {
// Type: Customisable 
// Modify this function to suit your trading robot

// This function checks for entry signals

   int output=0;

   if(FractalUp>0)
     {
      output=1;
     }

   else if(FractalDown>0)
     {
      output=2;
     }

   return (output);
  }
//+------------------------------------------------------------------+
int CountPosOrders(int Magic,string symbol)
  {
// Type: Fixed Template 
// Do not edit unless you know what you're doing

// This function counts number of positions/orders of OrderType TYPE

   int orders=0;
   for(int i=0; i<OrdersTotal(); i++)
     {
      if(OrderSelect(i,SELECT_BY_POS,MODE_TRADES)==true && OrderSymbol()==symbol && OrderMagicNumber()==Magic)
         orders++;
     }
   return(orders);

  }
//+------------------------------------------------------------------+
