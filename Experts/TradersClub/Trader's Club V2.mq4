//+------------------------------------------------------------------+
//|                                             Trader's Club V1.mq4 |
//|                                    Copyright 2018, Ahmed Darwish |
//|                                                  premium License |
//+------------------------------------------------------------------+
#property copyright "Copyright 2018"
#property version   "1.00"
#property strict
//---
enum mod
  {
   m0,//Buy
   m1,//Sell
   m2//Buy & Sell
  };
//---
extern string pn0                = "Trader's Club V1";      //Product Name
extern string pn1                = "premium License";       //Product License
extern string pn2                = "Ahmed Darwish";         //Product Author

extern string Setting            ="=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=";//Initial setting
input mod mode                   =2;                        //Trading mode
extern double Lot                =0.05;                     //Initial Lot Size
input double multi               =1.5;                      //Initial Factor
extern int distance              =60;                       //Grid distance (pips)
extern int TP                    =20;                       //Take Profit (pips)
input double basket              =0;                        //Trend basket profit $$
input int pips                   =10;                       //Auto Basket addition points

extern string Setting3           ="=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=";//Safety 2
input bool  Cool_Down            =false;                    //Cool Down System
extern int startup_orders        =5;                        //start up
extern int every_orders          =2;                        //Every
extern int distance2             =25;                       //Grid distance (pips)
input double freezing_multi      =0.3;                      //addition Factor

extern string Setting4           ="=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=";//Setting 2
input string comment             ="Trader's Club V1";       //Orders Comment
input int Magic                  =23246;                    //Magic Number
input bool comm                  =false;                    //Account Status

extern string Setting5           ="=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=";//Setting 4
input double  max_DD             =50;                       //Maximum draw-down %, 0=disable
input bool use_lines             =false;                    //Use Lines close
input string H_name              ="P buy";                  //Target For Buy (H line name)
input string H_name2             ="P sell";                 //Target For Sell (H line name)

//--- Globla Variable

int op,ch_b,ch_s;
double tp=0,Sell_zero_price,Buy_zero_price,buy_sl,sell_sl,initial;
bool stop_buy=false,stop_sell=false;
//---

int day = 1;
int month = 8;
int year = 2018;
//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
int OnInit()
  {
//---
   initial=AccountBalance();
   if(Digits==3 || Digits==5){
     distance*=10;
     distance2*=10;
     TP*=10;}
//---
   return(INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
  {
//---
ObjectDelete("Trader's Club V1");
ObjectDelete("part_2 0");
//---
  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OnTick()
  {
//---
//////////////////////////////////////////////////////////////////////////
//++------------------------------------------------------------------++//
//++------------------------------------------------------------------++//
//||                             Step one                             ||//
//++------------------------------------------------------------------++//
//++------------------------------------------------------------------++//
//////////////////////////////////////////////////////////////////////////
if(mode!=1 && stop_buy==false){
ch_b = check(OP_BUY);
double lastpos_price=last_pos_price(OP_BUY);

if(ch_b==0 || (lastpos_price-distance*_Point>=Bid && (ch_b<startup_orders || Cool_Down==false)) || (lastpos_price-distance2*_Point>=Bid && ch_b>=startup_orders)){
   if(AccountFreeMarginCheck(_Symbol,OP_BUY,Lot)<=0){Alert("No enough Money!");ExpertRemove();}
   tp=Ask+TP*Point;
   if(ch_b>0)tp=0;
   op=OrderSend(Symbol(),OP_BUY,lot(ch_b,OP_BUY),Ask,3,0,tp,comment,Magic,0,clrDodgerBlue);
   Buy_zero_price=0;
   if(basket==0)if(ch_b>=1 && op!=-1)tp_mod(OP_BUY);}

if(ch_b>1)
if(basket>0)
if(Profit(OP_BUY)>=basket)
   while(check(OP_BUY)!=0)
      CloseBuyOrders();
}
//---
if(mode!=0 && stop_sell==false){
ch_s = check(OP_SELL);
double lastpos_price=last_pos_price(OP_SELL);

if(ch_s==0 || (lastpos_price+distance*_Point<=Bid && (ch_s<startup_orders || Cool_Down==false)) || (lastpos_price+distance2*_Point<=Bid && ch_s>=startup_orders)){
   if(AccountFreeMarginCheck(_Symbol,OP_SELL,Lot)<=0){Alert("No enough Money!");ExpertRemove();}
   tp=Bid-TP*Point;
   if(ch_s>0)tp=0;
   op=OrderSend(Symbol(),OP_SELL,lot(ch_s,OP_SELL),Bid,3,0,tp,comment,Magic,0,clrViolet);
   Sell_zero_price=0;
   if(basket==0)if(ch_s>=1 && op!=-1)tp_mod(OP_SELL);}

if(ch_s>1)
if(basket>0)
if(Profit(OP_SELL)>=basket)
   while(check(OP_SELL)!=0)
      CloseSellOrders();
}

//////////////////////////////////////////////////////////////////////////
//++------------------------------------------------------------------++//
//++------------------------------------------------------------------++//
//||                             Step Two                             ||//
//++------------------------------------------------------------------++//
//++------------------------------------------------------------------++//
//////////////////////////////////////////////////////////////////////////
if(ch_b!=0){
if(use_lines)
if(Open[1]<ObjectGetDouble(0,H_name,OBJPROP_PRICE1,0) && Close[1]>ObjectGetDouble(0,H_name,OBJPROP_PRICE1,0)){
   stop_buy=true;
   while(check(OP_BUY)!=0)
      CloseBuyOrders();
   SendMail("Trader's Club EA",StringConcatenate("The price Closed above your buy H-Line\nSymbol: ",_Symbol));}

if(max_DD>0)
if(Profit(OP_BUY)*-1>=AccountBalance()*(max_DD/100)){
   while(check(OP_BUY)!=0)
      CloseBuyOrders();
   SendMail("Trader's Club EA",StringConcatenate("Closed all buy orders by Draw-Down system\nSymbol: ",_Symbol));
   stop_buy=true;}
//---

}
//---
//---
if(ch_s!=0){
if(use_lines)
if(Open[1]>ObjectGetDouble(0,H_name2,OBJPROP_PRICE1,0) && Close[1]<ObjectGetDouble(0,H_name2,OBJPROP_PRICE1,0)){
   stop_sell=true;
   while(check(OP_SELL)!=0)
      CloseSellOrders();
   SendMail("Trader's Club EA",StringConcatenate("The price Closed below your sell H-Line\nSymbol: ",_Symbol));}

if(max_DD>0)
if(Profit(OP_SELL)*-1>=AccountBalance()*(max_DD/100)){
   while(check(OP_SELL)!=0)
      CloseSellOrders();
   SendMail("Trader's Club EA",StringConcatenate("Closed all sell orders by Draw-Down system\nSymbol: ",_Symbol));
   stop_sell=true;}
//---


//////////////////////////////////////////////////////////////////////////
//++------------------------------------------------------------------++//
//++------------------------------------------------------------------++//
//||                            Additions                             ||//
//++------------------------------------------------------------------++//
//++------------------------------------------------------------------++//
//////////////////////////////////////////////////////////////////////////

if((IsTesting() && Volume[0]<2) || IsTesting()==false){
if(comm)
Comment("Account Balance: ",AccountBalance(),"\nAccount Equity: ",AccountEquity(),"\nTotal opened lots: ",
   total_lots(),"\nCurrent profit: ",NormalizeDouble(Profit(OP_BUY)+Profit(OP_SELL),1),"\nTotal monthly lots: ",tml());
}

if(IsTesting()){
string name="part_2 0";
ObjectCreate(0,name,OBJ_LABEL,0,0,0);
ObjectSetInteger(0,name,OBJPROP_COLOR,clrLime);
ObjectSetInteger(0,name,OBJPROP_FONTSIZE,14);
ObjectSetString(0,name,OBJPROP_TEXT,StringConcatenate("Profit:  +"+DoubleToStr(AccountBalance()-initial,2)+" ",AccountCurrency(),"  (",DoubleToStr((AccountBalance()-initial)*100/initial,2),"%)"));
ObjectSetInteger(0,name,OBJPROP_CORNER,CORNER_RIGHT_LOWER);
ObjectSetInteger(0,name,OBJPROP_SELECTABLE,false);
ObjectSetInteger(0,name,OBJPROP_YDISTANCE,65);
ObjectSetInteger(0,name,OBJPROP_XDISTANCE,300);
ObjectSetInteger(0,name,OBJPROP_BACK,false);
ObjectSet(name,OBJPROP_SELECTABLE,false);
ObjectSet(name,OBJPROP_HIDDEN,true);
}
//---
ObjectCreate(0,"Trader's Club V1",OBJ_LABEL,0,0,0);
ObjectSetString(0,"Trader's Club V1",OBJPROP_TEXT,"Trader's Club V1");
ObjectSetInteger(0,"Th3Eng",OBJPROP_FONTSIZE,7);
ObjectSet("Trader's Club V1",OBJPROP_CORNER,2);
ObjectSet("Trader's Club V1",OBJPROP_YDISTANCE,10);
ObjectSet("Trader's Club V1",OBJPROP_XDISTANCE,10);
ObjectSet("Trader's Club V1",OBJPROP_COLOR,clrGold);
ObjectSet("Trader's Club V1",OBJPROP_SELECTABLE,false);
ObjectSet("Trader's Club V1",OBJPROP_HIDDEN,true);}
//---
  }
//////////////////////////////////////////////////////////////////////////
//++------------------------------------------------------------------++//
//++------------------------------------------------------------------++//
//||                            Functions                             ||//
//++------------------------------------------------------------------++//
//++------------------------------------------------------------------++//
//////////////////////////////////////////////////////////////////////////
void CloseBuyOrders(){
  for (int cnt = 0 ; cnt < OrdersTotal() ; cnt++){
    if(OrderSelect(cnt,SELECT_BY_POS,MODE_TRADES))
    if (OrderMagicNumber() == Magic&& OrderSymbol() == Symbol() && OrderType()==OP_BUY){
        op=OrderClose(OrderTicket(),OrderLots(),Bid,3,clrNONE);}}}
// ----------------------------- //
void CloseSellOrders(){
for (int cnt = 0 ; cnt < OrdersTotal() ; cnt++){
    if(OrderSelect(cnt,SELECT_BY_POS,MODE_TRADES))
    if (OrderMagicNumber() == Magic&& OrderSymbol() == Symbol() && OrderType()==OP_SELL){
       op=OrderClose(OrderTicket(),OrderLots(),Ask,3,clrNONE);}}}
// ----------------------------- //
int check(int OP){
int x = 0;
for (int pos_0 = 0; pos_0 < OrdersTotal(); pos_0++){
   if(OrderSelect(pos_0, SELECT_BY_POS, MODE_TRADES))
   if(OrderType() == OP && OrderMagicNumber() == Magic && OrderSymbol() == Symbol())
      x++;}
//++++
return(x);}
// ----------------------------- //
double Profit(int OP){
double x = 0;
for (int pos_0 = 0; pos_0 < OrdersTotal(); pos_0++){
   if(OrderSelect(pos_0, SELECT_BY_POS, MODE_TRADES))
   if(OrderMagicNumber() == Magic && OrderSymbol() == Symbol() && OrderType()==OP)
      x+=OrderProfit();}
//++++
return(x);}
// ----------------------------- //
double total_lots(){
double x = 0;
for (int pos_0 = 0; pos_0 < OrdersTotal(); pos_0++){
   if(OrderSelect(pos_0, SELECT_BY_POS, MODE_TRADES))
      x+=OrderLots();}
//++++
return(x);}
// ----------------------------- //
double tml(){
double x = 0;
for (int pos_0 = 0; pos_0 < OrdersHistoryTotal(); pos_0++){
   if(OrderSelect(pos_0, SELECT_BY_POS, MODE_HISTORY))
   if(OrderOpenTime()>=iTime(_Symbol,PERIOD_MN1,0))
      x+=OrderLots();}
//++++
return(x);}
// ----------------------------- //
void modify_buy(){
for (int cnt = 0 ; cnt < OrdersTotal() ; cnt++){
    if(OrderSelect(cnt,SELECT_BY_POS,MODE_TRADES))
    if (OrderMagicNumber() == Magic&& OrderSymbol() == Symbol() && (OrderStopLoss()==0 || OrderStopLoss()<Buy_zero_price))
       op=OrderModify(OrderTicket(),OrderOpenPrice(),NormalizeDouble(Buy_zero_price,Digits),OrderTakeProfit(),0,clrNONE);}}
// ----------------------------- //
void modify_sell(){
for (int cnt = 0 ; cnt < OrdersTotal() ; cnt++){
    if(OrderSelect(cnt,SELECT_BY_POS,MODE_TRADES))
    if (OrderMagicNumber() == Magic&& OrderSymbol() == Symbol() && (OrderStopLoss()==0 || OrderStopLoss()>Sell_zero_price))
       op=OrderModify(OrderTicket(),OrderOpenPrice(),NormalizeDouble(Sell_zero_price,Digits),OrderTakeProfit(),0,clrNONE);}}
// ----------------------------- //
void tp_mod(int OP){
double x=0,l=0;
for (int pos_0 = 0; pos_0 < OrdersTotal(); pos_0++){
   if(OrderSelect(pos_0, SELECT_BY_POS, MODE_TRADES))
   if(OrderMagicNumber() == Magic && OrderSymbol() == Symbol() && OrderType()==OP){
      x+=(OrderLots()*OrderOpenPrice());
      l+=OrderLots();}}

double new_tp=x/l;

for (int pos_0 = OrdersTotal()-1; pos_0>=0 ; pos_0--){
   if(OrderSelect(pos_0, SELECT_BY_POS, MODE_TRADES))
   if(OrderMagicNumber() == Magic && OrderSymbol() == Symbol() && OrderType()==OP){
      if(OP==OP_BUY)op=OrderModify(OrderTicket(),OrderOpenPrice(),OrderStopLoss(),NormalizeDouble(new_tp,Digits)+pips*_Point,0,clrNONE);
      if(OP==OP_SELL)op=OrderModify(OrderTicket(),OrderOpenPrice(),OrderStopLoss(),NormalizeDouble(new_tp,Digits)-pips*_Point,0,clrNONE);}}
}
// ----------------------------- //
double last_pos_price(int OP){
for (int pos_0 = OrdersTotal()-1; pos_0 >=0; pos_0--){
   if(OrderSelect(pos_0, SELECT_BY_POS, MODE_TRADES))
   if(OrderMagicNumber() == Magic && OrderSymbol() == Symbol() && OrderType()==OP)
      return(OrderOpenPrice());}
//++++
return(0);}
// ----------------------------- //
double last_pos_lot(int OP){
for (int pos_0 = OrdersTotal()-1; pos_0 >=0; pos_0--){
   if(OrderSelect(pos_0, SELECT_BY_POS, MODE_TRADES))
   if(OrderMagicNumber() == Magic && OrderSymbol() == Symbol() && OrderType()==OP)
      return(OrderLots());}
//++++
return(0);}
// ----------------------------- //
double lot(int x,int OP){

if(x==0)return Lot;

if(x!=0 && (x<startup_orders || Cool_Down==false))return (NormalizeDouble(last_pos_lot(OP)*multi,2));
if(x>=startup_orders && x<startup_orders+every_orders && Cool_Down)return (NormalizeDouble(  last_pos_lot(OP)*(multi+freezing_multi)  ,2));
if(x>startup_orders && x<startup_orders+every_orders*2 && Cool_Down)return (NormalizeDouble(last_pos_lot(OP)*(multi+(freezing_multi*2)),2));
if(x>startup_orders && x<startup_orders+every_orders*3 && Cool_Down)return (NormalizeDouble(last_pos_lot(OP)*(multi+(freezing_multi*3)),2));
if(x>startup_orders && x<startup_orders+every_orders*4 && Cool_Down)return (NormalizeDouble(last_pos_lot(OP)*(multi+(freezing_multi*4)),2));
if(x>startup_orders && x<startup_orders+every_orders*5 && Cool_Down)return (NormalizeDouble(last_pos_lot(OP)*(multi+(freezing_multi*5)),2));
if(x>startup_orders && x<startup_orders+every_orders*6 && Cool_Down)return (NormalizeDouble(last_pos_lot(OP)*(multi+(freezing_multi*6)),2));
if(x>startup_orders && x<startup_orders+every_orders*7 && Cool_Down)return (NormalizeDouble(last_pos_lot(OP)*(multi+(freezing_multi*7)),2));
if(x>startup_orders && x<startup_orders+every_orders*8 && Cool_Down)return (NormalizeDouble(last_pos_lot(OP)*(multi+(freezing_multi*8)),2));

return(Lot);}


