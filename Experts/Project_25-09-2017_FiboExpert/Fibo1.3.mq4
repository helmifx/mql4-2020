//+------------------------------------------------------------------+
//|            Project1_V1.0_Forex Extremus Sensitive Indicator.mq4  |
//|                        Copyright 2017, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//|
//+------------------------------------------------------------------+

#property copyright "Copyright 2017 HelmiFX."
#property link      "www.helmifx.com"
#property strict
#property description "Fibo"
#property icon "photo.ico"

#include <Arrays\ArrayInt.mqh> //TDL stop making it open new limits on multiple cross of the entryReason
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
enum ENTRYWAY
  {
   CrossAndClose=0,// Wait for the close?
   CrossOnly=1,// Enter after cross 
  };
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
enum ENTRYVALUE
  {
   Level38 =0,// 38.2
   Level50 =1,// 50.0
   Level61 =2,// 61.8 
   AllLevels=3 // All Levels. 
  };
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
enum STOPLOSSVALUE
  {
   Level78=0,// 78.6
   Level100=1,// 100
  };
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
enum TAKEPROFITVALUE
  {

   Level23=0,// 23.6
   Level0=1,// 0
   LevelN38=2,// -38.2
  };                                //extern int MaxPositionsAllowed=2;  // Max Positions Allowed
//+------------------------------------------------------------------+
//| Setup                                               
//+------------------------------------------------------------------+
extern string  Header1="**************************************************************************************"; // ----------Entry Values -----------
extern ENTRYWAY EntryWay=0;
extern ENTRYVALUE EntryValue=3; // enter on what level?
extern double EntryRange=50; // Range in Pips
extern string  Header10="**************************************************************************************"; // ----------ZigZag Indicator Values -----------
extern int       Depth=15;     // Depth
extern int       Deviation=5;  // Deviation
extern int       Backstep=14;   // Backstep
extern string  Header2="**************************************************************************************"; //----------Lots -----------
extern double FirstLot=0.1;
extern double SecondLot= 0.1;
extern double ThirdLot = 0.1;
extern string  Note4="*******************************************"; // ---
extern bool    isPositionSizing=false;
extern string  Note5="*******************************************"; // PositionSizing Settings if used
extern double Risk=0.5;         //Risk in Percentage
extern double incrementFactor=0.0;  // increment factor on risk after each position.

extern string  Header3="**************************************************************************************"; //----------TP & SL Settings-----------
extern STOPLOSSVALUE StopLossValue=0;
extern string  Note6="*******************************************"; // --- 
extern TAKEPROFITVALUE TakeProfitValue=0;
extern double  ClosingOffset=3; // number of pips added to the stoploss.
extern string  Header4="**************************************************************************************"; // ----------Trailing Stop Settings-----------
extern bool    UseTrailingStops=false;
extern double  TrailingStopOffset=10;
extern string  Header5="**************************************************************************************"; // ----------Breakeven Settings-----------
extern bool    UseBreakeven=false;
extern double  BreakevenOffset=5;
extern string  Header6="**************************************************************************************"; // ----------Not Working Times Settings-----------
extern bool    useNotWorkingTimer=false;
extern string  Note7="Please use 24Hour format, between 0 and 24"; // --- 
extern int     Start=0;
extern int     End=6;
extern string  Header7="**************************************************************************************";; // ----------Set Max Loss Limit-----------
extern bool    IsLossLimitActivated=false;
extern double  LossLimit=0;
extern bool    IsProfitLimitActivated=false;
extern double  ProfitLimit=0;
extern string  Header8="**************************************************************************************"; // ----------Chart Info -----------
extern int     xDistance=1; //Chart info distance from top right corner
extern bool DrawInfoBackground=true;
extern string  Header9="**************************************************************************************"; // ----------EA General -----------
extern int     MagicNumber=123456;
extern int     Slippage=3;
extern bool    OnJournaling=true; // Add EA updates in the Journal Tab

string  InternalHeader1="----------Errors Handling Settings-----------";

int     RetryInterval=100; // Pause Time before next retry (in milliseconds)
int     MaxRetriesPerTick=10;

string  InternalHeader2="----------Service Variables-----------";

//+------------------------------------------------------------------+
//|General Variables                                                                 |
//+------------------------------------------------------------------+

bool takeProfitOpposit;
bool stopLossOpposit;

int P,YenPairAdjustFactor;

CArrayInt Orders;

int OrderNumber;

int Current_Time;

double TrailingStopList[][2];

double BreakevenList[][2];

bool entryLock=false;
//+------------------------------------------------------------------+
//|Fibo Variables                                               |
//+------------------------------------------------------------------+

int  lastTrend=0; // 1 for UpTrend, 2 for DownTrend

double lastLowestLow;
double lastHighestHigh;

double entryReason;
double exitReason;

double entryLevel38;
double entryLevel50;
double entryLevel61;

double stopLossLevel78;
double stopLossLevel100;

double takeProfitLevelN38;
double takeProfitLevel23;
double takeProfitLevel0;

double stopLossValue;
double takeProfitValue;

//+------------------------------------------------------------------+
//|ZigZag Indicator Variables                                               |
//+------------------------------------------------------------------+
double zigzag_low;
double zigzag_high;
double zigzag_flag;

//+------------------------------------------------------------------+
//|StopLoss and TakeProfit Variables                                               |
//+------------------------------------------------------------------+
double BuyStopLoss;
double SellStopLoss;
double BuyTakeProfit;
double SellTakeProfit;

//+------------------------------------------------------------------+
//|Cross Variables                                                                  |
//+------------------------------------------------------------------+
double Close_1;
double Close_2;

int BuyCrossTriggered,BuyCurrentDirection,BuyLastDirection;
bool BuyFirstTime=true;
int BuyDirection;

int SellCrossTriggered,SellCurrentDirection,SellLastDirection;
bool SellFirstTime=true;
int SellDirection;

int ExitCrossTriggered,ExitCurrentDirection,ExitLastDirection;
bool ExitFirstTime=true;
int ExitDirection;
//+------------------------------------------------------------------+
//|Chart Display Variables                                           |
//+------------------------------------------------------------------+

double lastClosedProfit=0;   //GetLastManually Closed profit
double cycleProfit;          //Get All Opened Profit
double cycleLots;          //Get All Opened Lots
double netProfit;             // All the profit of all opened positions by the same MagicNumber
string ObjName=IntegerToString(ChartID(),0,' ');   //The global object name 

//+------------------------------------------------------------------+
//|//Pip Display Variables                                           |
//+------------------------------------------------------------------+
double pipValue;
double pipValue1Lot;
//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
int init()
  {

   DrawChartInfo();            //Drawing Chart Info
   ChartSetInteger(0,CHART_SHOW_OBJECT_DESCR,true);
   P=GetP(); // To account for 5 digit brokers. Used to convert pips to decimal place
   YenPairAdjustFactor=GetYenAdjustFactor(); // Adjust for YenPair
   ChartSettings();

   if(UseTrailingStops) ArrayResize(TrailingStopList,100,0);
   if(UseBreakeven) ArrayResize(BreakevenList,100,0);

   return(INIT_SUCCEEDED);


  }
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+

void OnDeinit(const int reason)
  {
//---
   if(reason!=5 && reason!=3)
     {
      ObjectsDeleteAll(0,0,OBJ_LABEL);
      ObjectsDeleteAll(0,0,OBJ_RECTANGLE_LABEL);
      ObjectsDeleteAll(0,0,OBJ_ARROW);
      ObjectsDeleteAll(0,0,OBJ_HLINE);

      ObjectsDeleteAll(ChartID(),0);
     }
  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
int start()
  {

   cycleProfit=GetCycleProfit(0)+GetCycleProfit(1);               //Set Cycle Profits onto the chart.
   cycleLots=GetCycleLots(0)+GetCycleLots(1);                  //Set Cycle Lots onto the chart.
   pipValue=(((MarketInfo(Symbol(),MODE_TICKVALUE)*Point*P)/MarketInfo(Symbol(),MODE_TICKSIZE))*cycleLots);// pip value of 1 lot
   pipValue1Lot=(((MarketInfo(Symbol(),MODE_TICKVALUE)*Point*P)/MarketInfo(Symbol(),MODE_TICKSIZE))*1);// pip value of 1 lot
   netProfit=cycleProfit+lastClosedProfit;
   UpdateChartInfo();

   if(IsLossLimitBreached(IsLossLimitActivated,LossLimit,OnJournaling)==true || IsProfitLimitBreached(IsProfitLimitActivated,ProfitLimit,OnJournaling)==true)
     {
      return 0;
     }

   CheckSLTPExit();

//----------Entry & Exit Variables-----------

   zigzag_flag=iCustom(Symbol(),Period(),"ZigZag",Depth,Deviation,Backstep,0,1);
   zigzag_high=iCustom(Symbol(),Period(),"ZigZag",Depth,Deviation,Backstep,1,1);
   zigzag_low=iCustom(Symbol(),Period(),"ZigZag",Depth,Deviation,Backstep,2,1);

   CheckLastTrend(zigzag_low,zigzag_high,zigzag_flag);

   if(EntryWay==1 && lastTrend!=0) //when cross enter
     {
      BuyCrossTriggered=BuyCross(Ask,entryReason);
      SellCrossTriggered=SellCross(Bid,entryReason);
     }

   ExitCrossTriggered=ExitCross(Bid,exitReason);

   Close_1=iClose(Symbol(),Period(),1); // TDL SHOULD I KEEP IT LIKE THIS OR GET IT BACK AFTER THE CANDLE CHECK
   Close_2=iClose(Symbol(),Period(),2);

   if(EntryWay==0 && lastTrend!=0) // wait till the close
     {
      BuyCrossTriggered=BuyCross(Close_1,entryReason);
      SellCrossTriggered=SellCross(Close_1,entryReason);
     }

//----------Exit Rules (Market) -----------

   if(ExitCrossTriggered==1)
     {
      lastTrend=0;
      ObjectsDeleteAll(0,0,OBJ_HLINE);
      CloseOrderPosition(OP_BUYLIMIT,OnJournaling,MagicNumber,Slippage,P,RetryInterval);
      entryLock=false;
     }

   if(ExitCrossTriggered==2)
     {
      lastTrend=0;
      ObjectsDeleteAll(0,0,OBJ_HLINE);
      CloseOrderPosition(OP_SELLLIMIT,OnJournaling,MagicNumber,Slippage,P,RetryInterval);
      entryLock=false;
     }

//----------Trailing Stop-----------

   if(UseTrailingStops)
     {
      UpdateTrailingList(OnJournaling,RetryInterval,MagicNumber);
      ReviewTrailingStop(OnJournaling,TrailingStopOffset,RetryInterval,MagicNumber,P);
     }

   if(UseBreakeven)
     {
      UpdateBreakevenList(OnJournaling,RetryInterval,MagicNumber);
      ReviewBreakeven(OnJournaling,BreakevenOffset,RetryInterval,MagicNumber,P);
     }

//----------New Bar Check-----------

   if(!isNewBar() && EntryWay==0)
     {
      return(0);
     }

   if(useNotWorkingTimer)
     {
      if(isItTime()) return(0);
     }

//----------Entry Rules (Market) -----------

   if(EntrySignal(BuyCrossTriggered,SellCrossTriggered)==1 && entryLock==false && lastTrend==1 && GetRange())
     { // Open Short Positions
      OrderNumber=OpenPositionPending(OP_BUYLIMIT,entryLevel38,0,GetLot(isPositionSizing,FirstLot,GetBuyStopLossPips(stopLossValue,entryLevel38,P)),GetBuyStopLossPips(stopLossValue,entryLevel38,P),GetBuyTakeProfitPips(takeProfitValue,entryLevel38,P),MagicNumber,Slippage,OnJournaling,P,MaxRetriesPerTick,RetryInterval);
      if(UseTrailingStops) SetTrailingStop(OnJournaling,RetryInterval,MagicNumber,P,OrderNumber);
      if(UseBreakeven) SetBreakeven(OnJournaling,RetryInterval,MagicNumber,P,OrderNumber);
      Orders.Add(OrderNumber);
      Risk+=incrementFactor;

      OrderNumber=OpenPositionPending(OP_BUYLIMIT,entryLevel50,0,GetLot(isPositionSizing,SecondLot,GetBuyStopLossPips(stopLossValue,entryLevel50,P)),GetBuyStopLossPips(stopLossValue,entryLevel50,P),GetBuyTakeProfitPips(takeProfitValue,entryLevel50,P),MagicNumber,Slippage,OnJournaling,P,MaxRetriesPerTick,RetryInterval);
      if(UseTrailingStops) SetTrailingStop(OnJournaling,RetryInterval,MagicNumber,P,OrderNumber);
      if(UseBreakeven) SetBreakeven(OnJournaling,RetryInterval,MagicNumber,P,OrderNumber);
      Orders.Add(OrderNumber);
      Risk+=incrementFactor;

      OrderNumber=OpenPositionPending(OP_BUYLIMIT,entryLevel61,0,GetLot(isPositionSizing,ThirdLot,GetBuyStopLossPips(stopLossValue,entryLevel61,P)),GetBuyStopLossPips(stopLossValue,entryLevel61,P),GetBuyTakeProfitPips(takeProfitValue,entryLevel61,P),MagicNumber,Slippage,OnJournaling,P,MaxRetriesPerTick,RetryInterval);
      if(UseTrailingStops) SetTrailingStop(OnJournaling,RetryInterval,MagicNumber,P,OrderNumber);
      if(UseBreakeven) SetBreakeven(OnJournaling,RetryInterval,MagicNumber,P,OrderNumber);
      Orders.Add(OrderNumber);
      Risk+=incrementFactor;

      entryLock=true;
     }

   if(EntrySignal(BuyCrossTriggered,SellCrossTriggered)==2 && entryLock==false && lastTrend==2 && GetRange())
     { // Open Long Positions

      OrderNumber=OpenPositionPending(OP_SELLLIMIT,entryLevel38,0,GetLot(isPositionSizing,FirstLot,GetSellStopLossPips(stopLossValue,entryLevel38,P)),GetSellStopLossPips(stopLossValue,entryLevel38,P),GetSellTakeProfitPips(takeProfitValue,entryLevel38,P),MagicNumber,Slippage,OnJournaling,P,MaxRetriesPerTick,RetryInterval);
      if(UseTrailingStops) SetTrailingStop(OnJournaling,RetryInterval,MagicNumber,P,OrderNumber);
      if(UseBreakeven) SetBreakeven(OnJournaling,RetryInterval,MagicNumber,P,OrderNumber);
      Orders.Add(OrderNumber);
      Risk+=incrementFactor;

      OrderNumber=OpenPositionPending(OP_SELLLIMIT,entryLevel50,0,GetLot(isPositionSizing,SecondLot,GetSellStopLossPips(stopLossValue,entryLevel50,P)),GetSellStopLossPips(stopLossValue,entryLevel50,P),GetSellTakeProfitPips(takeProfitValue,entryLevel50,P),MagicNumber,Slippage,OnJournaling,P,MaxRetriesPerTick,RetryInterval);
      if(UseTrailingStops) SetTrailingStop(OnJournaling,RetryInterval,MagicNumber,P,OrderNumber);
      if(UseBreakeven) SetBreakeven(OnJournaling,RetryInterval,MagicNumber,P,OrderNumber);
      Orders.Add(OrderNumber);
      Risk+=incrementFactor;

      //Print("Third SellLimit: "+"entryLevel61 : "+entryLevel61+" StopLoss : "+GetSellStopLossPips(stopLossValue,entryLevel61,P)+" TakeProfit : "+GetSellTakeProfitPips(takeProfitValue,entryLevel61,P)+" stopLossValue"+stopLossValue+" takeProfitValue"+takeProfitValue);
      OrderNumber=OpenPositionPending(OP_SELLLIMIT,entryLevel61,0,GetLot(isPositionSizing,ThirdLot,GetSellStopLossPips(stopLossValue,entryLevel61,P)),GetSellStopLossPips(stopLossValue,entryLevel61,P),GetSellTakeProfitPips(takeProfitValue,entryLevel61,P),MagicNumber,Slippage,OnJournaling,P,MaxRetriesPerTick,RetryInterval);
      if(UseTrailingStops) SetTrailingStop(OnJournaling,RetryInterval,MagicNumber,P,OrderNumber);
      if(UseBreakeven) SetBreakeven(OnJournaling,RetryInterval,MagicNumber,P,OrderNumber);
      Orders.Add(OrderNumber);
      Risk+=incrementFactor;

      entryLock=true;
     }

   return (0);
  }
//+------------------------------------------------------------------+   
//|End of Start()                                                        |
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
// Start of EntrySignal()                                    			|
//+------------------------------------------------------------------+
int EntrySignal(int BuyCrossOccured,int SellCrossOccured)
  {
// Type: Customisable 
// Modify this function to suit your trading robot

// This function checks for entry signals

   int output=0;
   if(BuyCrossOccured==2) //Buy Signal
     {
      output=1;
     }

   if(SellCrossOccured==1) //Sell Signal 
     {
      output=2;
     }

   return (output);
  }
//+------------------------------------------------------------------+
// End of EntrySignal()                                              |
//+------------------------------------------------------------------+


//+------------------------------------------------------------------+
// Start of GetLot()                                       				|
//+------------------------------------------------------------------+
double GetLot(bool isPositionSizingOn,double FixedLots,double stopLoss)
  {
// Type: Customisable 
// Modify this function to suit your trading robot

// This is our sizing algorithm

   double output;

   if(isPositionSizing)
     {
      if(stopLoss==0)
        {
         output=FixedLots;
        }
      else
        {
         double Spread=MarketInfo(Symbol(),MODE_SPREAD)/P;
         output=((Risk*AccountEquity()/100)/(pipValue1Lot *(stopLoss+Spread)));
        }
     }
   else
     {
      output=FixedLots;
     }

   output=NormalizeDouble(output,2); // Round to 2 decimal place
   return(output);
  }
//+------------------------------------------------------------------+
// End of GetLot()                                                   |
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
// Start of CheckLot()                                       				|
//+------------------------------------------------------------------+

double CheckLot(double lot,bool Journaling)
  {
// Type: Fixed Template 
// Do not edit unless you know what you're doing

// This function checks if our Lots to be trade satisfies any broker limitations

   double LotToOpen=0;
   LotToOpen=NormalizeDouble(lot,2);
   LotToOpen=MathFloor(LotToOpen/MarketInfo(Symbol(),MODE_LOTSTEP))*MarketInfo(Symbol(),MODE_LOTSTEP);

   if(LotToOpen<MarketInfo(Symbol(),MODE_MINLOT))LotToOpen=MarketInfo(Symbol(),MODE_MINLOT);
   if(LotToOpen>MarketInfo(Symbol(),MODE_MAXLOT))LotToOpen=MarketInfo(Symbol(),MODE_MAXLOT);
   LotToOpen=NormalizeDouble(LotToOpen,2);

   if(Journaling && LotToOpen!=lot)Print("EA Journaling: Trading Lot has been changed by CheckLot function. Requested lot: "+DoubleToString(lot)+". Lot to open: "+DoubleToString(LotToOpen));

   return(LotToOpen);
  }
//+------------------------------------------------------------------+
//| End of CheckLot()
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//| Start of GetCycleLots()                                                             |
//+------------------------------------------------------------------+
double GetCycleLots(int type)
  {

// Type: Customisable 
// Modify this function to suit your trading robot

// this function returns the net profit of the opened orders 

   double Lots=0;

   for(int i=0; i<OrdersTotal(); i++)
     {
      if(OrderSelect(i,SELECT_BY_POS,MODE_TRADES)==true && OrderSymbol()==Symbol() && (OrderMagicNumber()==MagicNumber) && OrderType()==type)
         Lots+=OrderLots();
     }
   return(NormalizeDouble(Lots,2));

  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
// End of GetCycleLots()                                    		|
//+------------------------------------------------------------------+
double GetCycleProfit(int type)
  {

// Type: Customisable 
// Modify this function to suit your trading robot

// this function returns the net profit of the opened orders 

   double Profit=0;

   for(int i=0; i<OrdersTotal(); i++)
     {
      if(OrderSelect(i,SELECT_BY_POS,MODE_TRADES)==true && OrderSymbol()==Symbol() && (OrderMagicNumber()==MagicNumber) && OrderType()==type)
         Profit+=OrderProfit();
     }
   return(NormalizeDouble(Profit,1));
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
// End of GetCycleProfit()                                    			|
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|Start of CountPosOrders()	" Count Positions"
//+------------------------------------------------------------------+
int CountPosOrders(int Magic,int TYPE)
  {
// Type: Fixed Template 
// Do not edit unless you know what you're doing

// This function counts number of positions/orders of OrderType TYPE

   int orders=0;
   for(int i=0; i<OrdersTotal(); i++)
     {
      if(OrderSelect(i,SELECT_BY_POS,MODE_TRADES)==true && OrderSymbol()==Symbol() && OrderMagicNumber()==Magic && OrderType()==TYPE)
         orders++;
     }
   return(orders);

  }
//+------------------------------------------------------------------+
//| End of CountPosOrders() " Count Positions"
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|Start of OpenPositionMarket()
//+------------------------------------------------------------------+
int OpenPositionMarket(int TYPE,double LOT,double SL,double TP,int Magic,int Slip,bool Journaling,int K,int Max_Retries_Per_Tick)
  {
// Type: Fixed Template 
// Do not edit unless you know what you're doing 

// This function submits new orders

   int tries=0;
   string symbol=Symbol();
   int cmd=TYPE;
   double volume=CheckLot(LOT,Journaling);
   if(MarketInfo(symbol,MODE_MARGINREQUIRED)*volume>AccountFreeMargin())
     {
      Print("Can not open a trade. Not enough free margin to open "+volume+" on "+symbol);
      return(-1);
     }
   int slippage=Slip*K; // Slippage is in points. 1 point = 0.0001 on 4 digit broker and 0.00001 on a 5 digit broker
   string comment=" "+TYPE+"(#"+Magic+")";
   int magic=Magic;
   datetime expiration=0;
   color arrow_color=0;if(TYPE==OP_BUY)arrow_color=DodgerBlue;if(TYPE==OP_SELL)arrow_color=DeepPink;
   double stoploss=0;
   double takeprofit=0;
   double initTP = TP;
   double initSL = SL;
   int Ticket=-1;
   double price=0;

   while(tries<Max_Retries_Per_Tick) // Edits stops and take profits before the market order is placed
     {
      RefreshRates();
      if(TYPE==OP_BUY)price=Ask;if(TYPE==OP_SELL)price=Bid;

      // Sets Take Profits and Stop Loss. Check against Stop Level Limitations.
      if(TYPE==OP_BUY && SL!=0)
        {
         stoploss=NormalizeDouble(Ask-SL*K*Point,Digits);
         if(Bid-stoploss<=MarketInfo(Symbol(),MODE_STOPLEVEL)*Point)
           {
            stoploss=NormalizeDouble(Bid-MarketInfo(Symbol(),MODE_STOPLEVEL)*Point,Digits);
            if(Journaling)Print("EA Journaling: Stop Loss changed from "+initSL+" to "+MarketInfo(Symbol(),MODE_STOPLEVEL)/K+" pips");
           }
        }
      if(TYPE==OP_SELL && SL!=0)
        {
         stoploss=NormalizeDouble(Bid+SL*K*Point,Digits);
         if(stoploss-Ask<=MarketInfo(Symbol(),MODE_STOPLEVEL)*Point)
           {
            stoploss=NormalizeDouble(Ask+MarketInfo(Symbol(),MODE_STOPLEVEL)*Point,Digits);
            if(Journaling)Print("EA Journaling: Stop Loss changed from "+initSL+" to "+MarketInfo(Symbol(),MODE_STOPLEVEL)/K+" pips");
           }
        }
      if(TYPE==OP_BUY && TP!=0)
        {
         takeprofit=NormalizeDouble(Ask+TP*K*Point,Digits);
         if(takeprofit-Bid<=MarketInfo(Symbol(),MODE_STOPLEVEL)*Point)
           {
            takeprofit=NormalizeDouble(Ask+MarketInfo(Symbol(),MODE_STOPLEVEL)*Point,Digits);
            if(Journaling)Print("EA Journaling: Take Profit changed from "+initTP+" to "+MarketInfo(Symbol(),MODE_STOPLEVEL)/K+" pips");
           }
        }
      if(TYPE==OP_SELL && TP!=0)
        {
         takeprofit=NormalizeDouble(Bid-TP*K*Point,Digits);
         if(Ask-takeprofit<=MarketInfo(Symbol(),MODE_STOPLEVEL)*Point)
           {
            takeprofit=NormalizeDouble(Bid-MarketInfo(Symbol(),MODE_STOPLEVEL)*Point,Digits);
            if(Journaling)Print("EA Journaling: Take Profit changed from "+initTP+" to "+MarketInfo(Symbol(),MODE_STOPLEVEL)/K+" pips");
           }
        }
      if(Journaling)Print("EA Journaling: Trying to place a market order...");
      HandleTradingEnvironment(Journaling,RetryInterval);
      Ticket=OrderSend(symbol,cmd,volume,price,slippage,stoploss,takeprofit,comment,magic,expiration,arrow_color);
      if(Ticket>0)break;
      tries++;
     }

   if(Journaling && Ticket<0)Print("EA Journaling: Unexpected Error has happened. Error Description: "+GetErrorDescription(GetLastError()));
   if(Journaling && Ticket>0)
     {
      Print("EA Journaling: Order successfully placed. Ticket: "+Ticket);
     }
   return(Ticket);
  }
//+------------------------------------------------------------------+
//|End of OpenPositionMarket()
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//| Start of OpenPositionPending()
//+------------------------------------------------------------------+
int OpenPositionPending(int TYPE,double OpenPrice,double LOT,double SL,double TP,int Magic,int Slip,bool Journaling,int K,int Max_Retries_Per_Tick,int Retry_Interval)
  {
// Type: Fixed Template 
// Do not edit unless you know what you're doing 

// This function submits new pending orders
   OpenPrice= NormalizeDouble(OpenPrice,Digits);
   int tries=0;
   string symbol=Symbol();
   int cmd=TYPE;
   double volume=CheckLot(LOT,Journaling);
   if(MarketInfo(symbol,MODE_MARGINREQUIRED)*volume>AccountFreeMargin())
     {
      Print("Can not open a trade. Not enough free margin to open "+volume+" on "+symbol);
      return(-1);
     }
   int slippage=Slip*K; // Slippage is in points. 1 point = 0.0001 on 4 digit broker and 0.00001 on a 5 digit broker
   string comment=" "+TYPE+"(#"+Magic+")";
   int magic=Magic;
   color arrow_color=0;if(TYPE==OP_BUYLIMIT || TYPE==OP_BUYSTOP)arrow_color=Blue;if(TYPE==OP_SELLLIMIT || TYPE==OP_SELLSTOP)arrow_color=Red;
   double stoploss=0;
   double takeprofit=0;
   double initTP = TP;
   double initSL = SL;
   int Ticket=-1;
   double price=0;

   while(tries<Max_Retries_Per_Tick) // Edits stops and take profits before the market order is placed
     {
      RefreshRates();

      // We are able to send in TP and SL when we open our orders even if we are using ECN brokers

      // Sets Take Profits and Stop Loss. Check against Stop Level Limitations.
      stoploss=SL;
      takeprofit=TP;

      // We are able to send in TP and SL when we open our orders even if we are using ECN brokers

      // Sets Take Profits and Stop Loss. Check against Stop Level Limitations.

      if(Journaling)Print("EA Journaling: Trying to place a pending order...");
      HandleTradingEnvironment(Journaling,Retry_Interval);

      //Note: We did not modify Open Price if it breaches the Stop Level Limitations as Open Prices are sensitive and important. It is unsafe to change it automatically.
      Ticket=OrderSend(symbol,cmd,volume,OpenPrice,slippage,stoploss,takeprofit,comment,magic,0,arrow_color);
      if(Ticket>0)break;
      tries++;
     }

   if(Journaling && Ticket<0)Print("EA Journaling: Unexpected Error has happened. Error Description: "+GetErrorDescription(GetLastError()));
   if(Journaling && Ticket>0)
     {
      Print("EA Journaling: Order successfully placed. Ticket: "+Ticket);
     }
   return(Ticket);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//| End of OpenPositionPending()
//+------------------------------------------------------------------+ 

//+------------------------------------------------------------------+
//| Start of OpenPositionPending()
//+------------------------------------------------------------------+
int OpenPositionPending(int TYPE,double OpenPrice,datetime expiration,double LOT,double SL,double TP,int Magic,int Slip,bool Journaling,int K,int Max_Retries_Per_Tick,int Retry_Interval)
  {
// Type: Fixed Template 
// Do not edit unless you know what you're doing 

// This function submits new pending orders
   OpenPrice= NormalizeDouble(OpenPrice,Digits);
   int tries=0;
   string symbol=Symbol();
   int cmd=TYPE;
   double volume=CheckLot(LOT,Journaling);
   if(MarketInfo(symbol,MODE_MARGINREQUIRED)*volume>AccountFreeMargin())
     {
      Print("Can not open a trade. Not enough free margin to open "+volume+" on "+symbol);
      return(-1);
     }
   int slippage=Slip*K; // Slippage is in points. 1 point = 0.0001 on 4 digit broker and 0.00001 on a 5 digit broker
   string comment=" "+TYPE+"(#"+Magic+")";
   int magic=Magic;
   color arrow_color=0;if(TYPE==OP_BUYLIMIT || TYPE==OP_BUYSTOP)arrow_color=Blue;if(TYPE==OP_SELLLIMIT || TYPE==OP_SELLSTOP)arrow_color=Red;
   double stoploss=0;
   double takeprofit=0;
   double initTP = TP;
   double initSL = SL;
   int Ticket=-1;
   double price=0;

   while(tries<Max_Retries_Per_Tick) // Edits stops and take profits before the market order is placed
     {
      RefreshRates();

      // We are able to send in TP and SL when we open our orders even if we are using ECN brokers

      // Sets Take Profits and Stop Loss. Check against Stop Level Limitations.
      if((TYPE==OP_BUYLIMIT || TYPE==OP_BUYSTOP) && SL!=0)
        {
         stoploss=NormalizeDouble(OpenPrice-SL*K*Point,Digits);
         if(OpenPrice-stoploss<=MarketInfo(Symbol(),MODE_STOPLEVEL)*Point)
           {
            stoploss=NormalizeDouble(OpenPrice-MarketInfo(Symbol(),MODE_STOPLEVEL)*Point,Digits);
            if(Journaling)Print("EA Journaling: Stop Loss changed from "+initSL+" to "+(OpenPrice-stoploss)/(K*Point)+" pips");
           }
        }
      if((TYPE==OP_BUYLIMIT || TYPE==OP_BUYSTOP) && TP!=0)
        {
         takeprofit=NormalizeDouble(OpenPrice+TP*K*Point,Digits);
         if(takeprofit-OpenPrice<=MarketInfo(Symbol(),MODE_STOPLEVEL)*Point)
           {
            takeprofit=NormalizeDouble(OpenPrice+MarketInfo(Symbol(),MODE_STOPLEVEL)*Point,Digits);
            if(Journaling)Print("EA Journaling: Take Profit changed from "+initTP+" to "+(takeprofit-OpenPrice)/(K*Point)+" pips");
           }
        }
      if((TYPE==OP_SELLLIMIT || TYPE==OP_SELLSTOP) && SL!=0)
        {
         stoploss=NormalizeDouble(OpenPrice+SL*K*Point,Digits);
         if(stoploss-OpenPrice<=MarketInfo(Symbol(),MODE_STOPLEVEL)*Point)
           {
            stoploss=NormalizeDouble(OpenPrice+MarketInfo(Symbol(),MODE_STOPLEVEL)*Point,Digits);
            if(Journaling)Print("EA Journaling: Stop Loss changed from "+initSL+" to "+(OpenPrice-stoploss)/(K*Point)+" pips");
           }
        }
      if((TYPE==OP_SELLLIMIT || TYPE==OP_SELLSTOP) && TP!=0)
        {
         takeprofit=NormalizeDouble(OpenPrice-TP*K*Point,Digits);
         if(OpenPrice-takeprofit<=MarketInfo(Symbol(),MODE_STOPLEVEL)*Point)
           {
            takeprofit=NormalizeDouble(OpenPrice-MarketInfo(Symbol(),MODE_STOPLEVEL)*Point,Digits);
            if(Journaling)Print("EA Journaling: Take Profit changed from "+initTP+" to "+(OrderOpenPrice()-stoploss)/(K*Point)+" pips");
           }
        }
      if(Journaling)Print("EA Journaling: Trying to place a pending order...");
      HandleTradingEnvironment(Journaling,Retry_Interval);

      //Note: We did not modify Open Price if it breaches the Stop Level Limitations as Open Prices are sensitive and important. It is unsafe to change it automatically.
      Ticket=OrderSend(symbol,cmd,volume,OpenPrice,slippage,stoploss,takeprofit,comment,magic,expiration,arrow_color);
      if(Ticket>0)break;
      tries++;
     }

   if(Journaling && Ticket<0)Print("EA Journaling: Unexpected Error has happened. Error Description: "+GetErrorDescription(GetLastError()));
   if(Journaling && Ticket>0)
     {
      Print("EA Journaling: Order successfully placed. Ticket: "+Ticket);
     }
   return(Ticket);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//| End of OpenPositionPending()
//+------------------------------------------------------------------+ 
//+------------------------------------------------------------------+
//|Start of CloseOrderPosition()
//+------------------------------------------------------------------+
bool CloseOrderPosition(int TYPE,bool Journaling,int Magic,int Slip,int K,int Retry_Interval)
  {
// Type: Fixed Template 
// Do not edit unless you know what you're doing

// This function closes all positions of type TYPE or Deletes pending orders of type TYPE
   int ordersPos=OrdersTotal();

   for(int i=ordersPos-1; i>=0; i--)
     {
      // Note: Once pending orders become positions, OP_BUYLIMIT AND OP_BUYSTOP becomes OP_BUY, OP_SELLLIMIT and OP_SELLSTOP becomes OP_SELL
      if(TYPE==OP_BUY || TYPE==OP_SELL)
        {
         if(OrderSelect(i,SELECT_BY_POS,MODE_TRADES)==true && OrderSymbol()==Symbol() && OrderMagicNumber()==Magic && OrderType()==TYPE)
           {
            bool Closing=false;
            double Price=0;
            color arrow_color=0;if(TYPE==OP_BUY)arrow_color=Magenta;if(TYPE==OP_SELL)arrow_color=Green;
            if(Journaling)Print("EA Journaling: Trying to close position "+OrderTicket()+" ...");
            HandleTradingEnvironment(Journaling,RetryInterval);
            if(TYPE==OP_BUY)Price=Bid; if(TYPE==OP_SELL)Price=Ask;
            Closing=OrderClose(OrderTicket(),OrderLots(),Price,Slip*K,arrow_color);
            if(Journaling && !Closing)Print("EA Journaling: Unexpected Error has happened. Error Description: "+GetErrorDescription(GetLastError()));
            if(Journaling && Closing)Print("EA Journaling: Position successfully closed.");
           }
        }
      else
        {
         bool Delete=false;
         if(OrderSelect(i,SELECT_BY_POS,MODE_TRADES)==true && OrderSymbol()==Symbol() && OrderMagicNumber()==Magic && OrderType()==TYPE)
           {
            if(Journaling)Print("EA Journaling: Trying to delete order "+OrderTicket()+" ...");
            HandleTradingEnvironment(Journaling,RetryInterval);
            Delete=OrderDelete(OrderTicket(),White);
            if(Journaling && !Delete)Print("EA Journaling: Unexpected Error has happened Deleting the Pending Order. Error Description: "+GetErrorDescription(GetLastError()));
            if(Journaling && Delete)Print("EA Journaling: Order successfully deleted.");
           }
        }
     }
   if(CountPosOrders(Magic, TYPE)==0)return(true); else return(false);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//| End of CloseOrderPosition()
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
// Start of getP()                                                   |
//+------------------------------------------------------------------+

int GetP()
  {

   int output;

   if(Digits==5 || Digits==3) output=10;else output=1;

   return(output);
  }
//+------------------------------------------------------------------+
// End of GetP()                                                    	|
//+------------------------------------------------------------------+


//+------------------------------------------------------------------+
// Start of GetYenAdjustFactor()                                     |
//+------------------------------------------------------------------+

int GetYenAdjustFactor()
  {

   int output=1;

   if(Digits==3 || Digits==2) output=100;

   return(output);

  }
//+------------------------------------------------------------------+
// End of GetYenAdjustFactor()                                       |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//| Start of UpdateTrailingList()                              
//+------------------------------------------------------------------+

void UpdateTrailingList(bool Journaling,int Retry_Interval,int Magic)
  {
// Type: Fixed Template 
// Do not edit unless you know what you're doing

// This function clears the elements of your VolTrailingList if the corresponding positions has been closed

   int ordersPos=OrdersTotal();
   int orderTicketNumber;
   bool doesPosExist;

// Check the VolTrailingList, match with current list of positions. Make sure the all the positions exists. 
// If it doesn't, it means there are positions that have been closed

   for(int x=0; x<ArrayRange(TrailingStopList,0); x++)
     { // Looping through all order number in list

      doesPosExist=False;
      orderTicketNumber=TrailingStopList[x,0];

      if(orderTicketNumber!=0)
        { // Order exists
         for(int y=ordersPos-1; y>=0; y--)
           { // Looping through all current open positions
            if(OrderSelect(y,SELECT_BY_POS,MODE_TRADES)==true && OrderSymbol()==Symbol() && OrderMagicNumber()==Magic)
              {
               if(orderTicketNumber==OrderTicket())
                 { // Checks order number in list against order number of current positions
                  doesPosExist=True;
                  break;
                 }
              }
           }

         if(doesPosExist==False)
           { // Deletes elements if the order number does not match any current positions
            TrailingStopList[x,0] = 0;
            TrailingStopList[x,1] = 0;
           }
        }
     }
  }
//+------------------------------------------------------------------+
//| End of UpdateTrailingList                                        
//+------------------------------------------------------------------+



void ReviewTrailingStop(bool Journaling,double TrailingStop_Offset,int Retry_Interval,int Magic,int K)
  {
// Type: Fixed Template 
// Do not edit unless you know what you're doing 

// This function updates volatility trailing stops levels for all positions (using OrderModify) if appropriate conditions are met

   bool doesTrailingRecordExist;
   int posTicketNumber;
   for(int i=OrdersTotal()-1; i>=0; i--)
     { // Looping through all orders

      if(OrderSelect(i,SELECT_BY_POS,MODE_TRADES)==true && OrderSymbol()==Symbol() && OrderMagicNumber()==Magic)
        {
         doesTrailingRecordExist=False;
         posTicketNumber=OrderTicket();
         for(int x=0; x<ArrayRange(TrailingStopList,0); x++)
           { // Looping through all order number in list 

            if(posTicketNumber==TrailingStopList[x,0])
              { // If condition holds, it means the position have a volatility trailing stop level attached to it

               doesTrailingRecordExist=True;
               bool Modify=false;
               RefreshRates();

               // We update the volatility trailing stop record using OrderModify.
               if(OrderType()==OP_BUY && (Bid-TrailingStopList[x,1]>(TrailingStop_Offset*K*Point)))
                 {
                  if(TrailingStopList[x,1]!=Bid-(TrailingStop_Offset*K*Point))
                    {
                     // if(Journaling)Print("EA Journaling: Trying to modify order "+OrderTicket()+" ...");
                     HandleTradingEnvironment(Journaling,Retry_Interval);
                     Modify=OrderModify(OrderTicket(),OrderOpenPrice(),TrailingStopList[x,1],OrderTakeProfit(),0,CLR_NONE);
                     if(Journaling && !Modify)Print("EA Journaling: Unexpected Error has happened. Error Description: "+GetErrorDescription(GetLastError()));
                     if(Journaling && Modify) Print("EA Journaling: Order successfully modified, trailing stop changed.");

                     TrailingStopList[x,1]=Bid-(TrailingStop_Offset*K*Point);
                    }
                 }
               if(OrderType()==OP_SELL && ((TrailingStopList[x,1]-Ask>(TrailingStop_Offset*K*Point))))
                 {
                  if(TrailingStopList[x,1]!=Ask+(TrailingStop_Offset*K*Point))
                    {
                     //if(Journaling)Print("EA Journaling: Trying to modify order "+OrderTicket()+" ...");
                     HandleTradingEnvironment(Journaling,Retry_Interval);
                     Modify=OrderModify(OrderTicket(),OrderOpenPrice(),TrailingStopList[x,1],OrderTakeProfit(),0,CLR_NONE);
                     if(Journaling && !Modify)Print("EA Journaling: Unexpected Error has happened. Error Description: "+GetErrorDescription(GetLastError()));
                     if(Journaling && Modify) Print("EA Journaling: Order successfully modified, trailing stop changed.");
                     TrailingStopList[x,1]=Ask+(TrailingStop_Offset*K*Point);
                    }
                 }
               break;
              }
           }
         // If order does not have a record attached to it. Alert the trader.
         //if(!doesTrailingRecordExist && Journaling) Print("EA Journaling: Error. Order "+posTicketNumber+" has no volatility trailing stop attached to it.");
        }
     }
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//| End of Review Volatility Trailing Stop
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//| Start of SetTrailingStop
//+------------------------------------------------------------------+

void SetTrailingStop(bool Journaling,int Retry_Interval,int Magic,int K,int OrderNum)
  {
// Type: Fixed Template 
// Do not edit unless you know what you're doing 

// This function adds new volatility trailing stop level using OrderModify()
   double trailingStopLossLimit=0;
   bool Modify=False;
   bool IsTrailingStopAdded=False;
   if(OrderSelect(OrderNum,SELECT_BY_TICKET)==true && OrderSymbol()==Symbol() && OrderMagicNumber()==Magic)
     {
      RefreshRates();
      if(OrderType()==OP_BUY || OrderType()==OP_BUYSTOP)
        {
         trailingStopLossLimit=OrderOpenPrice();// virtual stop loss.
         IsTrailingStopAdded=True;
        }
      if(OrderType()==OP_SELL || OrderType()==OP_SELLSTOP)
        {
         trailingStopLossLimit=OrderOpenPrice();// virtual stop loss.
         IsTrailingStopAdded=True;
        }
      // Records trailingStopLossLimit for future use
      if(IsTrailingStopAdded==True)
        {
         for(int x=0; x<ArrayRange(TrailingStopList,0); x++) // Loop through elements in VolTrailingList
           {
            if(TrailingStopList[x,0]==0) // Checks if the element is empty
              {
               TrailingStopList[x,0]=OrderNum; // Add order number
               TrailingStopList[x,1]=trailingStopLossLimit; // Add Trailing Stop into the List
               Modify=true;
               if(Journaling && Modify) Print("Trailing Stop For "+OrderNum+" Has been Set successfully to "+TrailingStopOffset);
               break;
              }
           }
        }
     }

   if(Journaling && !Modify) Print("Couldnt set Trailing Stop For "+OrderNum+" !");
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//| End of SetTrailingStop
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//| Start of UpdateTrailingList()                              
//+------------------------------------------------------------------+

void UpdateBreakevenList(bool Journaling,int Retry_Interval,int Magic)
  {
// Type: Fixed Template 
// Do not edit unless you know what you're doing

// This function clears the elements of your BreakevenList if the corresponding positions has been closed

   int ordersPos=OrdersTotal();
   int orderTicketNumber;
   bool doesPosExist;

// Check the VolTrailingList, match with current list of positions. Make sure the all the positions exists. 
// If it doesn't, it means there are positions that have been closed

   for(int x=0; x<ArrayRange(BreakevenList,0); x++)
     { // Looping through all order number in list

      doesPosExist=False;
      orderTicketNumber=BreakevenList[x,0];

      if(orderTicketNumber!=0)
        { // Order exists
         for(int y=ordersPos-1; y>=0; y--)
           { // Looping through all current open positions
            if(OrderSelect(y,SELECT_BY_POS,MODE_TRADES)==true && OrderSymbol()==Symbol() && OrderMagicNumber()==Magic)
              {
               if(orderTicketNumber==OrderTicket())
                 { // Checks order number in list against order number of current positions
                  doesPosExist=True;
                  break;
                 }
              }
           }

         if(doesPosExist==False)
           { // Deletes elements if the order number does not match any current positions
            BreakevenList[x,0] = 0;
            BreakevenList[x,1] = 0;
           }
        }
     }
  }
//+------------------------------------------------------------------+
//| End of UpdateTrailingList                                        
//+------------------------------------------------------------------+



void ReviewBreakeven(bool Journaling,double Breakeven_Offset,int Retry_Interval,int Magic,int K)
  {
// Type: Fixed Template 
// Do not edit unless you know what you're doing 

// This function updates volatility trailing stops levels for all positions (using OrderModify) if appropriate conditions are met

   bool doesBreakevenRecordExist;
   int posTicketNumber;
   for(int i=OrdersTotal()-1; i>=0; i--)
     { // Looping through all orders

      if(OrderSelect(i,SELECT_BY_POS,MODE_TRADES)==true && OrderSymbol()==Symbol() && OrderMagicNumber()==Magic)
        {
         doesBreakevenRecordExist=False;
         posTicketNumber=OrderTicket();
         for(int x=0; x<ArrayRange(BreakevenList,0); x++)
           { // Looping through all order number in list 

            if(posTicketNumber==BreakevenList[x,0])
              { // If condition holds, it means the position have a volatility trailing stop level attached to it

               doesBreakevenRecordExist=True;
               bool Modify=false;
               RefreshRates();

               // We update the volatility trailing stop record using OrderModify.
               if(OrderType()==OP_BUY && (Bid-BreakevenList[x,1]>(Breakeven_Offset*K*Point)) && BreakevenList[x,1]!=-1)
                 {
                  // if(Journaling)Print("EA Journaling: Trying to modify order "+OrderTicket()+" ...");
                  HandleTradingEnvironment(Journaling,Retry_Interval);
                  Modify=OrderModify(OrderTicket(),OrderOpenPrice(),OrderOpenPrice(),OrderTakeProfit(),0,CLR_NONE);
                  if(Journaling && !Modify)Print("EA Journaling: Unexpected Error has happened. Error Description: "+GetErrorDescription(GetLastError()));
                  if(Journaling && Modify) Print("EA Journaling: Order successfully modified, Breakeven changed.");

                  BreakevenList[x,1]=-1;

                 }
               if(OrderType()==OP_SELL && ((BreakevenList[x,1]-Ask>(Breakeven_Offset*K*Point))) && BreakevenList[x,1]!=-1)
                 {

                  //if(Journaling)Print("EA Journaling: Trying to modify order "+OrderTicket()+" ...");
                  HandleTradingEnvironment(Journaling,Retry_Interval);
                  Modify=OrderModify(OrderTicket(),OrderOpenPrice(),OrderOpenPrice(),OrderTakeProfit(),0,CLR_NONE);
                  if(Journaling && !Modify)Print("EA Journaling: Unexpected Error has happened. Error Description: "+GetErrorDescription(GetLastError()));
                  if(Journaling && Modify) Print("EA Journaling: Order successfully modified, Breakeven changed.");
                  BreakevenList[x,1]=-1;

                 }
               break;
              }
           }

        }
     }
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//| End of Review Volatility Trailing Stop
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//| Start of SetBreakeven
//+------------------------------------------------------------------+

void SetBreakeven(bool Journaling,int Retry_Interval,int Magic,int K,int OrderNum)
  {
// Type: Fixed Template 
// Do not edit unless you know what you're doing 

// This function adds new volatility trailing stop level using OrderModify()
   double BreakevenLimit=0;
   bool Modify=False;
   bool IsBreakevenAdded=False;
   if(OrderSelect(OrderNum,SELECT_BY_TICKET)==true && OrderSymbol()==Symbol() && OrderMagicNumber()==Magic)
     {
      RefreshRates();
      if(OrderType()==OP_BUY || OrderType()==OP_BUYSTOP)
        {
         BreakevenLimit=OrderOpenPrice();// virtual stop loss.
         IsBreakevenAdded=True;
        }
      if(OrderType()==OP_SELL || OrderType()==OP_SELLSTOP)
        {
         BreakevenLimit=OrderOpenPrice();// virtual stop loss.
         IsBreakevenAdded=True;
        }
      // Records trailingStopLossLimit for future use
      if(IsBreakevenAdded==True)
        {
         for(int x=0; x<ArrayRange(BreakevenList,0); x++) // Loop through elements in VolTrailingList
           {
            if(BreakevenList[x,0]==0) // Checks if the element is empty
              {
               BreakevenList[x,0]=OrderNum; // Add order number
               BreakevenList[x,1]=BreakevenLimit; // Add Trailing Stop into the List
               Modify=true;
               if(Journaling && Modify) Print("Breakeven For "+OrderNum+" Has been Set successfully to "+BreakevenOffset);
               break;
              }
           }
        }
     }

   if(Journaling && !Modify) Print("Couldnt set Breakeven For "+OrderNum+" !");
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//| End of SetBreakeven
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//| Start of HandleTradingEnvironment()                                         
//+------------------------------------------------------------------+
void HandleTradingEnvironment(bool Journaling,int Retry_Interval)
  {
// Type: Fixed Template 
// Do not edit unless you know what you're doing 

// This function checks for errors

   if(IsTradeAllowed()==true)return;
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
   if(!IsConnected())
     {
      if(Journaling)Print("EA Journaling: Terminal is not connected to server...");
      return;
     }
   if(!IsTradeAllowed() && Journaling)Print("EA Journaling: Trade is not alowed for some reason...");
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
   if(IsConnected() && !IsTradeAllowed())
     {
      while(IsTradeContextBusy()==true)
        {
         if(Journaling)Print("EA Journaling: Trading context is busy... Will wait a bit...");
         Sleep(Retry_Interval);
        }
     }
   RefreshRates();
  }
//+------------------------------------------------------------------+
//| End of HandleTradingEnvironment()                              
//+------------------------------------------------------------------+  
//+------------------------------------------------------------------+
//| Start of GetErrorDescription()                                               
//+------------------------------------------------------------------+
string GetErrorDescription(int error)
  {
// Type: Fixed Template 
// Do not edit unless you know what you're doing

// This function returns the exact error

   string ErrorDescription="";
//---
   switch(error)
     {
      case 0:     ErrorDescription = "NO Error. Everything should be good.";                                    break;
      case 1:     ErrorDescription = "No error returned, but the result is unknown";                            break;
      case 2:     ErrorDescription = "Common error";                                                            break;
      case 3:     ErrorDescription = "Invalid trade parameters";                                                break;
      case 4:     ErrorDescription = "Trade server is busy";                                                    break;
      case 5:     ErrorDescription = "Old version of the client terminal";                                      break;
      case 6:     ErrorDescription = "No connection with trade server";                                         break;
      case 7:     ErrorDescription = "Not enough rights";                                                       break;
      case 8:     ErrorDescription = "Too frequent requests";                                                   break;
      case 9:     ErrorDescription = "Malfunctional trade operation";                                           break;
      case 64:    ErrorDescription = "Account disabled";                                                        break;
      case 65:    ErrorDescription = "Invalid account";                                                         break;
      case 128:   ErrorDescription = "Trade timeout";                                                           break;
      case 129:   ErrorDescription = "Invalid price";                                                           break;
      case 130:   ErrorDescription = "Invalid stops";                                                           break;
      case 131:   ErrorDescription = "Invalid trade volume";                                                    break;
      case 132:   ErrorDescription = "Market is closed";                                                        break;
      case 133:   ErrorDescription = "Trade is disabled";                                                       break;
      case 134:   ErrorDescription = "Not enough money";                                                        break;
      case 135:   ErrorDescription = "Price changed";                                                           break;
      case 136:   ErrorDescription = "Off quotes";                                                              break;
      case 137:   ErrorDescription = "Broker is busy";                                                          break;
      case 138:   ErrorDescription = "Requote";                                                                 break;
      case 139:   ErrorDescription = "Order is locked";                                                         break;
      case 140:   ErrorDescription = "Long positions only allowed";                                             break;
      case 141:   ErrorDescription = "Too many requests";                                                       break;
      case 145:   ErrorDescription = "Modification denied because order too close to market";                   break;
      case 146:   ErrorDescription = "Trade context is busy";                                                   break;
      case 147:   ErrorDescription = "Expirations are denied by broker";                                        break;
      case 148:   ErrorDescription = "Too many open and pending orders (more than allowed)";                    break;
      case 4000:  ErrorDescription = "No error";                                                                break;
      case 4001:  ErrorDescription = "Wrong function pointer";                                                  break;
      case 4002:  ErrorDescription = "Array index is out of range";                                             break;
      case 4003:  ErrorDescription = "No memory for function call stack";                                       break;
      case 4004:  ErrorDescription = "Recursive stack overflow";                                                break;
      case 4005:  ErrorDescription = "Not enough stack for parameter";                                          break;
      case 4006:  ErrorDescription = "No memory for parameter string";                                          break;
      case 4007:  ErrorDescription = "No memory for temp string";                                               break;
      case 4008:  ErrorDescription = "Not initialized string";                                                  break;
      case 4009:  ErrorDescription = "Not initialized string in array";                                         break;
      case 4010:  ErrorDescription = "No memory for array string";                                              break;
      case 4011:  ErrorDescription = "Too long string";                                                         break;
      case 4012:  ErrorDescription = "Remainder from zero divide";                                              break;
      case 4013:  ErrorDescription = "Zero divide";                                                             break;
      case 4014:  ErrorDescription = "Unknown command";                                                         break;
      case 4015:  ErrorDescription = "Wrong jump (never generated error)";                                      break;
      case 4016:  ErrorDescription = "Not initialized array";                                                   break;
      case 4017:  ErrorDescription = "DLL calls are not allowed";                                               break;
      case 4018:  ErrorDescription = "Cannot load library";                                                     break;
      case 4019:  ErrorDescription = "Cannot call function";                                                    break;
      case 4020:  ErrorDescription = "Expert function calls are not allowed";                                   break;
      case 4021:  ErrorDescription = "Not enough memory for temp string returned from function";                break;
      case 4022:  ErrorDescription = "System is busy (never generated error)";                                  break;
      case 4050:  ErrorDescription = "Invalid function parameters count";                                       break;
      case 4051:  ErrorDescription = "Invalid function parameter value";                                        break;
      case 4052:  ErrorDescription = "String function internal error";                                          break;
      case 4053:  ErrorDescription = "Some array error";                                                        break;
      case 4054:  ErrorDescription = "Incorrect series array using";                                            break;
      case 4055:  ErrorDescription = "Custom indicator error";                                                  break;
      case 4056:  ErrorDescription = "Arrays are incompatible";                                                 break;
      case 4057:  ErrorDescription = "Global variables processing error";                                       break;
      case 4058:  ErrorDescription = "Global variable not found";                                               break;
      case 4059:  ErrorDescription = "Function is not allowed in testing mode";                                 break;
      case 4060:  ErrorDescription = "Function is not confirmed";                                               break;
      case 4061:  ErrorDescription = "Send mail error";                                                         break;
      case 4062:  ErrorDescription = "String parameter expected";                                               break;
      case 4063:  ErrorDescription = "Integer parameter expected";                                              break;
      case 4064:  ErrorDescription = "Double parameter expected";                                               break;
      case 4065:  ErrorDescription = "Array as parameter expected";                                             break;
      case 4066:  ErrorDescription = "Requested history data in updating state";                                break;
      case 4067:  ErrorDescription = "Some error in trading function";                                          break;
      case 4099:  ErrorDescription = "End of file";                                                             break;
      case 4100:  ErrorDescription = "Some file error";                                                         break;
      case 4101:  ErrorDescription = "Wrong file name";                                                         break;
      case 4102:  ErrorDescription = "Too many opened files";                                                   break;
      case 4103:  ErrorDescription = "Cannot open file";                                                        break;
      case 4104:  ErrorDescription = "Incompatible access to a file";                                           break;
      case 4105:  ErrorDescription = "No order selected";                                                       break;
      case 4106:  ErrorDescription = "Unknown symbol";                                                          break;
      case 4107:  ErrorDescription = "Invalid price";                                                           break;
      case 4108:  ErrorDescription = "Invalid ticket";                                                          break;
      case 4109:  ErrorDescription = "EA is not allowed to trade is not allowed. ";                             break;
      case 4110:  ErrorDescription = "Longs are not allowed. Check the expert properties";                      break;
      case 4111:  ErrorDescription = "Shorts are not allowed. Check the expert properties";                     break;
      case 4200:  ErrorDescription = "Object exists already";                                                   break;
      case 4201:  ErrorDescription = "Unknown object property";                                                 break;
      case 4202:  ErrorDescription = "Object does not exist";                                                   break;
      case 4203:  ErrorDescription = "Unknown object type";                                                     break;
      case 4204:  ErrorDescription = "No object name";                                                          break;
      case 4205:  ErrorDescription = "Object coordinates error";                                                break;
      case 4206:  ErrorDescription = "No specified subwindow";                                                  break;
      case 4207:  ErrorDescription = "Some error in object function";                                           break;
      default:    ErrorDescription = "No error or error is unknown";
     }
   return(ErrorDescription);
  }
//+------------------------------------------------------------------+
//| End of GetErrorDescription()                                         
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//| Start of IsMaxPositionsReached()                                             
//+------------------------------------------------------------------+
bool IsMaxPositionsReached(int MaxPositions,int Magic,bool Journaling)
  {
// Type: Fixed Template 
// Do not edit unless you know what you're doing 

// This function checks the number of positions we are holding against the maximum allowed 

   int result=False;
   if(CountPosOrders(Magic,OP_BUY)+CountPosOrders(Magic,OP_SELL)>MaxPositions)
     {
      result=True;
      if(Journaling)Print("Max Orders Exceeded");
        } else if(CountPosOrders(Magic,OP_BUY)+CountPosOrders(Magic,OP_SELL)==MaxPositions) {
      result=True;
     }

   return(result);

/* Definitions: Position vs Orders
   
   Position describes an opened trade
   Order is a pending trade
   
   How to use in a sentence: Jim has 5 buy limit orders pending 10 minutes ago. The market just crashed. The orders were executed and he has 5 losing positions now lol.

*/
  }
//+------------------------------------------------------------------+
//| End of IsMaxPositionsReached()                                                
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//| Start of ChartSettings()                                         
//+------------------------------------------------------------------+
void ChartSettings()
  {
   ChartSetInteger(0,CHART_SHOW_GRID,0);
   ChartSetInteger(0,CHART_MODE,CHART_CANDLES);
   ChartSetInteger(0,CHART_AUTOSCROLL,0,True);
   WindowRedraw();
  }
//+------------------------------------------------------------------+
//| End of ChartSettings()                                         
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//| Start of CloseAllPositions()
//+------------------------------------------------------------------+ 
void CloseAllPositions()
  {
// Type: Fixed Template 
// Do not edit unless you know what you're doing

// This function closes all positions 

   CloseOrderPosition(OP_BUY,OnJournaling,MagicNumber,Slippage,P,RetryInterval);
   CloseOrderPosition(OP_SELL,OnJournaling,MagicNumber,Slippage,P,RetryInterval);

   return;

  }
//+------------------------------------------------------------------+
//| End of CloseAllPositions()
//+------------------------------------------------------------------+ 

//+------------------------------------------------------------------+
//| Start of IsLossLimitBreached()                                    
//+------------------------------------------------------------------+
bool IsLossLimitBreached(bool LossLimitActivated,double LossLimitValue,bool Journaling)
  {

// Type: Fixed Template 
// Do not edit unless you know what you're doing

// This function determines if our maximum Profit threshold is breached
//static bool firstTime=false;
   double lossPrint;
   bool output=False;

   if(LossLimitActivated==False) return (output);
//if(firstTime == true) return(true);

   if(cycleProfit<(-1 *LossLimitValue))
     {
      output=True;
      //firstTime=true;
      CloseAllPositions();
      lossPrint=NormalizeDouble(cycleProfit,4);
      if(Journaling) Print("Loss threshold breached. Current Loss: "+lossPrint);
     }

   return (output);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//|End of IsLossLimitBreached()                                      
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//| Start of IsProfitLimitBreached()                                    
//+------------------------------------------------------------------+
bool IsProfitLimitBreached(bool ProfitLimitActivated,double ProfitLimitValue,bool Journaling)
  {

// Type: Fixed Template 
// Do not edit unless you know what you're doing

// This function determines if our maximum Profit threshold is breached
   static bool firstTime=false;
   double profitPrint;
   bool output=False;

   if(ProfitLimitActivated==False) return (output);
//if(firstTime == true) return(true);

   if(cycleProfit>ProfitLimitValue)
     {
      output=True;
      //firstTime=true;
      CloseAllPositions();
      profitPrint=NormalizeDouble(cycleProfit,4);
      if(Journaling) Print("Profit threshold breached. Current Profit: "+profitPrint);
     }

   return (output);
  }
//+------------------------------------------------------------------+
//|End of IsProfitLimitBreached                                                                   |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//|Start of DrawChartInfo                                                                  |
//+------------------------------------------------------------------+

void DrawChartInfo()
  {

   if(DrawInfoBackground)
     {
      ObjectCreate(ChartID(),ObjName+"InfoBackground",OBJ_RECTANGLE_LABEL,0,0,0);
      ObjectSetInteger(ChartID(),ObjName+"InfoBackground",OBJPROP_XSIZE,-250);
      ObjectSetInteger(ChartID(),ObjName+"InfoBackground",OBJPROP_YSIZE,200);
      ObjectSetInteger(ChartID(),ObjName+"InfoBackground",OBJPROP_XDISTANCE,xDistance);
      ObjectSetInteger(ChartID(),ObjName+"InfoBackground",OBJPROP_YDISTANCE,10);
      ObjectSetInteger(ChartID(),ObjName+"InfoBackground",OBJPROP_CORNER,CORNER_RIGHT_UPPER);
      ObjectSetInteger(ChartID(),ObjName+"InfoBackground",OBJPROP_BGCOLOR,Black);
      ObjectSetInteger(ChartID(),ObjName+"InfoBackground",OBJPROP_BORDER_COLOR,Black);
      ObjectSetInteger(ChartID(),ObjName+"InfoBackground",OBJPROP_BACK,false);
      ObjectSetInteger(ChartID(),ObjName+"InfoBackground",OBJPROP_SELECTABLE,false);
      ObjectSetInteger(ChartID(),ObjName+"InfoBackground",OBJPROP_SELECTED,false);
      ObjectSetInteger(ChartID(),ObjName+"InfoBackground",OBJPROP_HIDDEN,false);
      ObjectSetInteger(ChartID(),ObjName+"InfoBackground",OBJPROP_BORDER_TYPE,BORDER_FLAT);
     }

   ObjectCreate(ChartID(),ObjName+"Symbol",OBJ_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"Symbol",OBJPROP_TEXT,Symbol());
   ObjectSetInteger(ChartID(),ObjName+"Symbol",OBJPROP_CORNER,CORNER_RIGHT_UPPER);
   ObjectSetInteger(ChartID(),ObjName+"Symbol",OBJPROP_XDISTANCE,xDistance);
   ObjectSetInteger(ChartID(),ObjName+"Symbol",OBJPROP_YDISTANCE,37);
   ObjectSetString(ChartID(),ObjName+"Symbol",OBJPROP_FONT,"Verdana");
   ObjectSetInteger(ChartID(),ObjName+"Symbol",OBJPROP_FONTSIZE,12);
   ObjectSetInteger(ChartID(),ObjName+"Symbol",OBJPROP_ANCHOR,ANCHOR_RIGHT);
   ObjectSetInteger(ChartID(),ObjName+"Symbol",OBJPROP_COLOR,Gold);

   ObjectCreate(ChartID(),ObjName+"Findus1",OBJ_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"Findus1",OBJPROP_TEXT,"Find us on  :");
   ObjectSetInteger(ChartID(),ObjName+"Findus1",OBJPROP_CORNER,CORNER_LEFT_LOWER);
   ObjectSetInteger(ChartID(),ObjName+"Findus1",OBJPROP_XDISTANCE,1);
   ObjectSetInteger(ChartID(),ObjName+"Findus1",OBJPROP_YDISTANCE,48);
   ObjectSetString(ChartID(),ObjName+"Findus1",OBJPROP_FONT,"Verdana");
   ObjectSetInteger(ChartID(),ObjName+"Findus1",OBJPROP_FONTSIZE,8);
   ObjectSetInteger(ChartID(),ObjName+"Findus1",OBJPROP_ANCHOR,ANCHOR_LEFT);
   ObjectSetInteger(ChartID(),ObjName+"Findus1",OBJPROP_COLOR,White);

   ObjectCreate(ChartID(),ObjName+"Findus2",OBJ_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"Findus2",OBJPROP_TEXT,"Facbook : HelmiFx");
   ObjectSetInteger(ChartID(),ObjName+"Findus2",OBJPROP_CORNER,CORNER_LEFT_LOWER);
   ObjectSetInteger(ChartID(),ObjName+"Findus2",OBJPROP_XDISTANCE,1);
   ObjectSetInteger(ChartID(),ObjName+"Findus2",OBJPROP_YDISTANCE,36);
   ObjectSetString(ChartID(),ObjName+"Findus2",OBJPROP_FONT,"Verdana");
   ObjectSetInteger(ChartID(),ObjName+"Findus2",OBJPROP_FONTSIZE,8);
   ObjectSetInteger(ChartID(),ObjName+"Findus2",OBJPROP_ANCHOR,ANCHOR_LEFT);
   ObjectSetInteger(ChartID(),ObjName+"Findus2",OBJPROP_COLOR,White);

   ObjectCreate(ChartID(),ObjName+"Findus3",OBJ_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"Findus3",OBJPROP_TEXT,"Twitter : @HelmiForex");
   ObjectSetInteger(ChartID(),ObjName+"Findus3",OBJPROP_CORNER,CORNER_LEFT_LOWER);
   ObjectSetInteger(ChartID(),ObjName+"Findus3",OBJPROP_XDISTANCE,1);
   ObjectSetInteger(ChartID(),ObjName+"Findus3",OBJPROP_YDISTANCE,24);
   ObjectSetString(ChartID(),ObjName+"Findus3",OBJPROP_FONT,"Verdana");
   ObjectSetInteger(ChartID(),ObjName+"Findus3",OBJPROP_FONTSIZE,8);
   ObjectSetInteger(ChartID(),ObjName+"Findus3",OBJPROP_ANCHOR,ANCHOR_LEFT);
   ObjectSetInteger(ChartID(),ObjName+"Findus3",OBJPROP_COLOR,White);

   ObjectCreate(ChartID(),ObjName+"Findus4",OBJ_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"Findus4",OBJPROP_TEXT,"Youtube : HelmiForex");
   ObjectSetInteger(ChartID(),ObjName+"Findus4",OBJPROP_CORNER,CORNER_LEFT_LOWER);
   ObjectSetInteger(ChartID(),ObjName+"Findus4",OBJPROP_XDISTANCE,1);
   ObjectSetInteger(ChartID(),ObjName+"Findus4",OBJPROP_YDISTANCE,12);
   ObjectSetString(ChartID(),ObjName+"Findus4",OBJPROP_FONT,"Verdana");
   ObjectSetInteger(ChartID(),ObjName+"Findus4",OBJPROP_FONTSIZE,8);
   ObjectSetInteger(ChartID(),ObjName+"Findus4",OBJPROP_ANCHOR,ANCHOR_LEFT);
   ObjectSetInteger(ChartID(),ObjName+"Findus4",OBJPROP_COLOR,White);

   ObjectCreate(ChartID(),ObjName+"PipValueOf1Lot",OBJ_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"PipValueOf1Lot",OBJPROP_TEXT,"Pip Value of 1 Lot is "+DoubleToStr(pipValue1Lot,2)+"$");
   ObjectSetInteger(ChartID(),ObjName+"PipValueOf1Lot",OBJPROP_CORNER,CORNER_RIGHT_UPPER);
   ObjectSetInteger(ChartID(),ObjName+"PipValueOf1Lot",OBJPROP_XDISTANCE,xDistance);
   ObjectSetInteger(ChartID(),ObjName+"PipValueOf1Lot",OBJPROP_YDISTANCE,62);
   ObjectSetString(ChartID(),ObjName+"PipValueOf1Lot",OBJPROP_FONT,"Verdana");
   ObjectSetInteger(ChartID(),ObjName+"PipValueOf1Lot",OBJPROP_FONTSIZE,12);
   ObjectSetInteger(ChartID(),ObjName+"PipValueOf1Lot",OBJPROP_ANCHOR,ANCHOR_RIGHT);
   ObjectSetInteger(ChartID(),ObjName+"PipValueOf1Lot",OBJPROP_COLOR,Pink);

   ObjectCreate(ChartID(),ObjName+"PipValue",OBJ_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"PipValue",OBJPROP_TEXT,"Total Lots = "+DoubleToStr(cycleLots,2)+" Lot = "+DoubleToStr(pipValue,2)+"$");
   ObjectSetInteger(ChartID(),ObjName+"PipValue",OBJPROP_CORNER,CORNER_RIGHT_UPPER);
   ObjectSetInteger(ChartID(),ObjName+"PipValue",OBJPROP_XDISTANCE,xDistance);
   ObjectSetInteger(ChartID(),ObjName+"PipValue",OBJPROP_YDISTANCE,87);
   ObjectSetString(ChartID(),ObjName+"PipValue",OBJPROP_FONT,"Verdana");
   ObjectSetInteger(ChartID(),ObjName+"PipValue",OBJPROP_FONTSIZE,10);
   ObjectSetInteger(ChartID(),ObjName+"PipValue",OBJPROP_ANCHOR,ANCHOR_RIGHT);
   ObjectSetInteger(ChartID(),ObjName+"PipValue",OBJPROP_COLOR,DeepPink);

   ObjectCreate(ChartID(),ObjName+"Equity",OBJ_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"Equity",OBJPROP_TEXT,"Account Equity : "+DoubleToStr(AccountEquity(),2));
   ObjectSetInteger(ChartID(),ObjName+"Equity",OBJPROP_CORNER,CORNER_RIGHT_UPPER);
   ObjectSetInteger(ChartID(),ObjName+"Equity",OBJPROP_XDISTANCE,xDistance);
   ObjectSetInteger(ChartID(),ObjName+"Equity",OBJPROP_YDISTANCE,112);
   ObjectSetString(ChartID(),ObjName+"Equity",OBJPROP_FONT,"Verdana");
   ObjectSetInteger(ChartID(),ObjName+"Equity",OBJPROP_FONTSIZE,10);
   ObjectSetInteger(ChartID(),ObjName+"Equity",OBJPROP_ANCHOR,ANCHOR_RIGHT);
   ObjectSetInteger(ChartID(),ObjName+"Equity",OBJPROP_COLOR,BurlyWood);

   ObjectCreate(ChartID(),ObjName+"NetProfit",OBJ_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"NetProfit",OBJPROP_TEXT,"Net Profit "+DoubleToString(netProfit,1));
   ObjectSetInteger(ChartID(),ObjName+"NetProfit",OBJPROP_CORNER,CORNER_RIGHT_UPPER);
   ObjectSetInteger(ChartID(),ObjName+"NetProfit",OBJPROP_XDISTANCE,xDistance);
   ObjectSetInteger(ChartID(),ObjName+"NetProfit",OBJPROP_YDISTANCE,137);
   ObjectSetString(ChartID(),ObjName+"NetProfit",OBJPROP_FONT,"Verdana");
   ObjectSetInteger(ChartID(),ObjName+"NetProfit",OBJPROP_FONTSIZE,11);
   ObjectSetInteger(ChartID(),ObjName+"NetProfit",OBJPROP_ANCHOR,ANCHOR_RIGHT);
   ObjectSetInteger(ChartID(),ObjName+"NetProfit",OBJPROP_COLOR,DarkTurquoise);

   ObjectCreate(ChartID(),ObjName+"ClosedPositionsProfit",OBJ_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"ClosedPositionsProfit",OBJPROP_TEXT,"Closed Positions Profit "+DoubleToString(lastClosedProfit,1));
   ObjectSetInteger(ChartID(),ObjName+"ClosedPositionsProfit",OBJPROP_CORNER,CORNER_RIGHT_UPPER);
   ObjectSetInteger(ChartID(),ObjName+"ClosedPositionsProfit",OBJPROP_XDISTANCE,xDistance);
   ObjectSetInteger(ChartID(),ObjName+"ClosedPositionsProfit",OBJPROP_YDISTANCE,162);
   ObjectSetString(ChartID(),ObjName+"ClosedPositionsProfit",OBJPROP_FONT,"Verdana");
   ObjectSetInteger(ChartID(),ObjName+"ClosedPositionsProfit",OBJPROP_FONTSIZE,11);
   ObjectSetInteger(ChartID(),ObjName+"ClosedPositionsProfit",OBJPROP_ANCHOR,ANCHOR_RIGHT);
   ObjectSetInteger(ChartID(),ObjName+"ClosedPositionsProfit",OBJPROP_COLOR,Blue);

   ObjectCreate(ChartID(),ObjName+"CurrentCycleProfit",OBJ_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"CurrentCycleProfit",OBJPROP_TEXT,"Current Cycle Profit "+DoubleToString(cycleProfit,1));
   ObjectSetInteger(ChartID(),ObjName+"CurrentCycleProfit",OBJPROP_CORNER,CORNER_RIGHT_UPPER);
   ObjectSetInteger(ChartID(),ObjName+"CurrentCycleProfit",OBJPROP_XDISTANCE,xDistance);
   ObjectSetInteger(ChartID(),ObjName+"CurrentCycleProfit",OBJPROP_YDISTANCE,187);
   ObjectSetString(ChartID(),ObjName+"CurrentCycleProfit",OBJPROP_FONT,"Verdana");
   ObjectSetInteger(ChartID(),ObjName+"CurrentCycleProfit",OBJPROP_FONTSIZE,11);
   ObjectSetInteger(ChartID(),ObjName+"CurrentCycleProfit",OBJPROP_ANCHOR,ANCHOR_RIGHT);
   ObjectSetInteger(ChartID(),ObjName+"CurrentCycleProfit",OBJPROP_COLOR,Teal);

  }
//+------------------------------------------------------------------+
// End of DrawChartInfo()                                          |
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
// Start of UpdateChartInfo()                                          |
//+------------------------------------------------------------------+
void UpdateChartInfo()
  {

   ObjectSetString(ChartID(),ObjName+"PipValueOf1Lot",OBJPROP_TEXT,"Pip Value of 1 Lot is "+DoubleToStr(pipValue1Lot,2)+"$");
   ObjectSetString(ChartID(),ObjName+"PipValue",OBJPROP_TEXT,"Total Lots = "+DoubleToStr(cycleLots,2)+" Lot = "+DoubleToStr(pipValue,2)+"$");
   ObjectSetString(ChartID(),ObjName+"ClosedPositionsProfit",OBJPROP_TEXT,"Closed Positions Profit "+DoubleToString(lastClosedProfit,1));
   ObjectSetString(ChartID(),ObjName+"CurrentCycleProfit",OBJPROP_TEXT,"Current Cycle Profit "+DoubleToString(cycleProfit,1));
   ObjectSetString(ChartID(),ObjName+"NetProfit",OBJPROP_TEXT,"Net Profit "+DoubleToString(netProfit,1));
   ObjectSetString(ChartID(),ObjName+"Equity",OBJPROP_TEXT,"Account Equity : "+DoubleToStr(AccountEquity(),2));
  }
//+------------------------------------------------------------------+
// End of UpdateChartInfo()                                          |
//+------------------------------------------------------------------+


//+------------------------------------------------------------------+
//| Start of CheckSLTPExit()                                         |
//+------------------------------------------------------------------+

void CheckSLTPExit()
  {

   for(int i=Orders.Total()-1; i>=0; i--)
     {
      if(OrderSelect(Orders.At(i),SELECT_BY_TICKET,MODE_TRADES)==true && OrderSymbol()==Symbol() && (OrderMagicNumber()==MagicNumber))
        {
         if(OrderCloseTime()!=0)
           {
            if(OrderType()==OP_BUY || OrderType()==OP_SELL) //
              {
               lastClosedProfit+=OrderProfit();
               Orders.Delete(i);
              }
           }
        }
     }

  }
//+------------------------------------------------------------------+
//| End of CheckSLTPExit()                                         |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//| Start of isNewBar()                                         
//+------------------------------------------------------------------+
bool isNewBar()
  {
//--- memorize the time of opening of the last bar in the static variable
   static datetime last_time=0;
//--- current time
   datetime lastbar_time=SeriesInfoInteger(Symbol(),Period(),SERIES_LASTBAR_DATE);

//--- if it is the first call of the function
   if(last_time==0)
     {
      //--- set the time and exit
      last_time=lastbar_time;
      return(false);
     }

//--- if the time differs
   if(last_time!=lastbar_time)
     {
      //--- memorize the time and return true
      last_time=lastbar_time;
      return(true);
     }
//--- if we passed to this line, then the bar is not new; return false
   return(false);
  }
//+------------------------------------------------------------------+
//| End of isNewBar()                                         
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//Start of GetBuyTakeProfitPips()                                    |
//+------------------------------------------------------------------+
double GetBuyTakeProfitPips(double takeprofitLevel,double entryLevel,int K)
  { // K represents our P multiplier to adjust for broker digits
// Type: Customisable 
// Modify this function to suit your trading robot

// This Function get the stop loss of the current buy order
   double TakeP;

   TakeP=((takeprofitLevel-entryLevel)/(K*Point)); // Stop loss calculated from the last Fractle Down
   TakeP=NormalizeDouble(TakeP,2);
   return(TakeP);
  }
//+------------------------------------------------------------------+
// End of GetBuyTakeProfitPips()                                  |
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//Start of GetSellTakeProfitPips()                                    |
//+------------------------------------------------------------------+
double GetSellTakeProfitPips(double takeprofitLevel,double entryLevel,int K)
  { // K represents our P multiplier to adjust for broker digits
// Type: Customisable 
// Modify this function to suit your trading robot

// This Function get the stop loss of the current buy order
   double TakeP;

   TakeP=((entryLevel-takeprofitLevel)/(K*Point)); // Stop loss calculated from the last Fractle Down
   TakeP=NormalizeDouble(TakeP,2);
   return(TakeP);
  }
//+------------------------------------------------------------------+
// End of GetSellTakeProfitPips()                                  |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//Start of GetBuyStopLossPips()                                    |
//+------------------------------------------------------------------+
double GetBuyStopLossPips(double stoplossLevel,double entryLevel,int K)
  { // K represents our P multiplier to adjust for broker digits
// Type: Customisable 
// Modify this function to suit your trading robot

// This Function get the stop loss of the current buy order
   double StopL;

   StopL=((entryLevel-stoplossLevel)/(K*Point)); // Stop loss calculated from the last Fractle Down
   StopL=NormalizeDouble(StopL,2);
   StopL+=ClosingOffset;
   return(StopL);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
// End of GetBuyStopLossPips()                                  |
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//Start of GetBuyStopLossPips()                                    |
//+------------------------------------------------------------------+
double GetSellStopLossPips(double stoplossLevel,double entryLevel,int K)
  { // K represents our P multiplier to adjust for broker digits
// Type: Customisable 
// Modify this function to suit your trading robot

// This Function get the stop loss of the current buy order
   double StopL;

   StopL=((stoplossLevel-entryLevel)/(K*Point)); // Stop loss calculated from the last Fractle Down
   StopL=NormalizeDouble(StopL,2);
   StopL+=ClosingOffset;
   return(StopL);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
// End of GetBuyStopLossPips()                                  |
//+------------------------------------------------------------------+


//+------------------------------------------------------------------+
//| End of isItTime()                                         
//+------------------------------------------------------------------+

bool isItTime()
  {
   Current_Time=TimeHour(TimeCurrent());
   if(Start<0)Start=0;
   if(End<0) End=0;
   if(Start==0 || Start>24) Start=24; if(End==0 || End>24) End=24; if(Current_Time==0) Current_Time=24;

   if(Start<End)
      if( (Current_Time < Start) || (Current_Time >= End) ) return(false);

   if(Start>End)
      if( (Current_Time < Start) && (Current_Time >= End) ) return(false);

   if(Start==End)
     {
      return false;
     }

   return(true);
  }
//+------------------------------------------------------------------+
//| End of isItTime()                                         
//+------------------------------------------------------------------+

int BuyCross(double BuyShift1,double Fibo)
  {
// Type: Customizable
// Do not edit unless you know what you're doing

// This function determines if a cross happened between Parabolic Buy Values : BuyShift1,BuyShift2
//----
   if(BuyShift1>Fibo)
     {
      BuyCurrentDirection=1;  // line1 above line2
     }

   else if(BuyShift1<=Fibo)
     {
      BuyCurrentDirection=2;  // line1 below line2 
     }

   else
     {
      BuyCurrentDirection=BuyLastDirection;
     }
//----
   if(BuyFirstTime==true) // Need to check if this is the first time the function is run
     {
      BuyFirstTime=false; // Change variable to false
      BuyLastDirection=BuyCurrentDirection; // Set new direction
      return (0);
     }

   if(BuyCurrentDirection!=BuyLastDirection && BuyFirstTime==false) // If not the first time and there is a direction change
     {
      BuyLastDirection=BuyCurrentDirection; // Set new direction
      return(BuyCurrentDirection); // 1 for up, 2 for down
     }

   else
     {
      return(0);  // No direction change
     }
  }
//+------------------------------------------------------------------+
//| End of isItTime()                                         
//+------------------------------------------------------------------+

int SellCross(double SellShift1,double Fibo)
  {
// Type: Customizable
// Do not edit unless you know what you're doing

// This function determines if a cross happened between Parabolic Sell Values : SellShift1,SellShift2
//----
   if(SellShift1>Fibo)
     {
      SellCurrentDirection=1;  // line1 above line2
     }

   else if(SellShift1<Fibo)
     {
      SellCurrentDirection=2;  // line1 below line2 
     }

   else
     {
      SellCurrentDirection=SellLastDirection;
     }
//----
   if(SellFirstTime==true) // Need to check if this is the first time the function is run
     {
      SellFirstTime=false; // Change variable to false
      SellLastDirection=SellCurrentDirection; // Set new direction
      return (0);
     }

   if(SellCurrentDirection!=SellLastDirection && SellFirstTime==false) // If not the first time and there is a direction change
     {
      SellLastDirection=SellCurrentDirection; // Set new direction
      return(SellCurrentDirection); // 1 for up, 2 for down
     }

   else
     {
      return(0);  // No direction change
     }
  }
//+------------------------------------------------------------------+
// Start of  ()                            |
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//| End of isItTime()                                         
//+------------------------------------------------------------------+

int ExitCross(double ExitShift1,double Fibo)
  {
// Type: Customizable
// Do not edit unless you know what you're doing

// This function determines if a cross happened between Parabolic Exit Values : ExitShift1,ExitShift2
//----
   if(ExitShift1>Fibo)
     {
      ExitCurrentDirection=1;  // line1 above line2
     }

   else if(ExitShift1<Fibo)
     {
      ExitCurrentDirection=2;  // line1 below line2 
     }

   else
     {
      ExitCurrentDirection=ExitLastDirection;
     }
//----
   if(ExitFirstTime==true) // Need to check if this is the first time the function is run
     {
      ExitFirstTime=false; // Change variable to false
      ExitLastDirection=ExitCurrentDirection; // Set new direction
      return (0);
     }

   if(ExitCurrentDirection!=ExitLastDirection && ExitFirstTime==false) // If not the first time and there is a direction change
     {
      ExitLastDirection=ExitCurrentDirection; // Set new direction
      return(ExitCurrentDirection); // 1 for up, 2 for down
     }

   else
     {
      return(0);  // No direction change
     }
  }
//+------------------------------------------------------------------+
// Start of  CheckLastTrend()                            |
//+------------------------------------------------------------------+
void CheckLastTrend(double ZigZagLow,double ZigZagHigh,double ZigZagFlag)
  {
   static bool firstTimeBuy=true,firstTimeSell=true;
   if(ZigZagFlag!=0) // new Peak or Trough
     {
      if(ZigZagFlag==ZigZagHigh) // new Peak
        {

         if(firstTimeBuy)
           {
            GetLastLowest();
            firstTimeBuy=false;
           }

         lastHighestHigh=ZigZagHigh;
         lastTrend=1;
         CalculateUpwardFibo();
         DrawEntryLines();

        }

      if(ZigZagFlag==ZigZagLow) // new Trough
        {
         if(firstTimeSell)
           {
            GetLastHighest();
            firstTimeSell=false;
           }

         lastLowestLow=ZigZagLow;
         lastTrend=2;
         CalculateDownwardFibo();
         DrawEntryLines();

        }
     }

  }
//+------------------------------------------------------------------+
// End of  CheckLastTrend()                            |
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
// Start of  DrawUpWardFibo()                            |
//+------------------------------------------------------------------+
void DrawEntryLines()
  {

   if(ObjectFind("EntryReason")!=0)
     {
      ObjectCreate("EntryReason",OBJ_HLINE,0,0,entryReason);
      ObjectSet("EntryReason",OBJPROP_HIDDEN,STYLE_DASH);
      ObjectSet("EntryReason",OBJPROP_STYLE,STYLE_DASH);
      ObjectSet("EntryReason",OBJPROP_COLOR,Magenta);
      ObjectSet("EntryReason",OBJPROP_SELECTABLE,false);
      ObjectSet("EntryReason",OBJPROP_SELECTED,false);
      ObjectSetText("EntryReason","                              23.6",8,"Arial",White);
     }
   else
     {
      ObjectMove("EntryReason",0,0,entryReason);
     }

   if(ObjectFind("EntryLine38")!=0 && (EntryValue==3 || EntryValue==0))
     {
      ObjectCreate("EntryLine38",OBJ_HLINE,0,0,entryLevel38);
      ObjectSet("EntryLine38",OBJPROP_STYLE,STYLE_DASH);
      ObjectSet("EntryLine38",OBJPROP_COLOR,Yellow);
      ObjectSet("EntryLine38",OBJPROP_SELECTABLE,false);
      ObjectSet("EntryLine38",OBJPROP_SELECTED,false);
      ObjectSetText("EntryLine38","                              38.2",8,"Arial",White);
     }
   else
     {
      ObjectMove("EntryLine38",0,0,entryLevel38);
     }

   if(ObjectFind("EntryLine50")!=0 && (EntryValue==3 || EntryValue==1))
     {
      ObjectCreate("EntryLine50",OBJ_HLINE,0,0,entryLevel50);
      ObjectSet("EntryLine50",OBJPROP_STYLE,STYLE_DASH);
      ObjectSet("EntryLine50",OBJPROP_COLOR,Yellow);
      ObjectSet("EntryLine50",OBJPROP_SELECTABLE,false);
      ObjectSet("EntryLine50",OBJPROP_SELECTED,false);
      ObjectSetText("EntryLine50","                              50",8,"Arial",White);
     }
   else
     {
      ObjectMove("EntryLine50",0,0,entryLevel50);
     }

   if(ObjectFind("EntryLine61")!=0 && (EntryValue==3 || EntryValue==2))
     {
      ObjectCreate("EntryLine61",OBJ_HLINE,0,0,entryLevel61);
      ObjectSet("EntryLine61",OBJPROP_STYLE,STYLE_DASH);
      ObjectSet("EntryLine61",OBJPROP_COLOR,Yellow);
      ObjectSet("EntryLine61",OBJPROP_SELECTABLE,false);
      ObjectSet("EntryLine61",OBJPROP_SELECTED,false);
      ObjectSetText("EntryLine61","                              61.8",8,"Arial",White);
     }
   else
     {
      ObjectMove("EntryLine61",0,0,entryLevel61);
     }

   if(ObjectFind("StopLossLine78")!=0 && StopLossValue==0)
     {
      ObjectCreate("StopLossLine78",OBJ_HLINE,0,0,stopLossLevel78);
      ObjectSet("StopLossLine78",OBJPROP_STYLE,STYLE_DASH);
      ObjectSet("StopLossLine78",OBJPROP_COLOR,Red);
      ObjectSet("StopLossLine78",OBJPROP_SELECTABLE,false);
      ObjectSet("StopLossLine78",OBJPROP_SELECTED,false);
      ObjectSetText("StopLossLine78","                              78.6",8,"Arial",White);
     }
   else
     {
      ObjectMove("StopLossLine78",0,0,stopLossLevel78);
     }

   if(ObjectFind("StopLossLine100")!=0 && StopLossValue==1)
     {
      ObjectCreate("StopLossLine100",OBJ_HLINE,0,0,stopLossLevel100);
      ObjectSet("StopLossLine100",OBJPROP_STYLE,STYLE_DASH);
      ObjectSet("StopLossLine100",OBJPROP_COLOR,Red);
      ObjectSet("StopLossLine100",OBJPROP_SELECTABLE,false);
      ObjectSet("StopLossLine100",OBJPROP_SELECTED,false);
      ObjectSetText("StopLossLine100","                              100",8,"Arial",White);
     }
   else
     {
      ObjectMove("StopLossLine100",0,0,stopLossLevel100);
     }

   if(ObjectFind("TakeProfitLineN38")!=0 && TakeProfitValue==2)
     {
      ObjectCreate("TakeProfitLineN38",OBJ_HLINE,0,0,takeProfitLevelN38);
      ObjectSet("TakeProfitLineN38",OBJPROP_STYLE,STYLE_DASH);
      ObjectSet("TakeProfitLineN38",OBJPROP_COLOR,Green);
      ObjectSet("TakeProfitLineN38",OBJPROP_SELECTABLE,false);
      ObjectSet("TakeProfitLineN38",OBJPROP_SELECTED,false);
      ObjectSetText("TakeProfitLineN38","                              -38.2",8,"Arial",White);
     }
   else
     {
      ObjectMove("TakeProfitLineN38",0,0,takeProfitLevelN38);
     }

   if(ObjectFind("TakeProfitLine23")!=0 && TakeProfitValue==1)
     {
      ObjectCreate("TakeProfitLine23",OBJ_HLINE,0,0,takeProfitLevel23);
      ObjectSet("TakeProfitLine23",OBJPROP_STYLE,STYLE_DASH);
      ObjectSet("TakeProfitLine23",OBJPROP_COLOR,Green);
      ObjectSet("TakeProfitLine23",OBJPROP_SELECTABLE,false);
      ObjectSet("TakeProfitLine23",OBJPROP_SELECTED,false);
      ObjectSetText("TakeProfitLine23","                              23.6",8,"Arial",White);
     }
   else
     {
      ObjectMove("TakeProfitLine23",0,0,takeProfitLevel23);
     }

   if(ObjectFind("TakeProfitLine0")!=0 && TakeProfitValue==0)
     {
      ObjectCreate("TakeProfitLine0",OBJ_HLINE,0,0,takeProfitLevel0);
      ObjectSet("TakeProfitLine0",OBJPROP_STYLE,STYLE_DASH);
      ObjectSet("TakeProfitLine0",OBJPROP_COLOR,Green);
      ObjectSet("TakeProfitLine0",OBJPROP_SELECTABLE,false);
      ObjectSet("TakeProfitLine0",OBJPROP_SELECTED,false);
      ObjectSetText("TakeProfitLine0","                              0",8,"Arial",White);
     }
   else
     {
      ObjectMove("TakeProfitLine0",0,0,takeProfitLevel0);
     }

   if(!GetRange())
     {
      ObjectSet("EntryReason",OBJPROP_COLOR,White);
      ObjectSet("EntryReason",OBJPROP_COLOR,White);
      ObjectSet("EntryReason",OBJPROP_COLOR,White);
      ObjectSet("EntryReason",OBJPROP_COLOR,White);
      ObjectSet("EntryReason",OBJPROP_COLOR,White);
      ObjectSet("EntryReason",OBJPROP_COLOR,White);
      ObjectSet("EntryReason",OBJPROP_COLOR,White);
     }

  }
//+------------------------------------------------------------------+
// End of  DrawUpWardFibo()                          					   |
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
// Start of  CalculateUpwardFibo()                            |
//+------------------------------------------------------------------+
void CalculateUpwardFibo()
  {

   entryLevel38=lastHighestHigh -((lastHighestHigh-lastLowestLow)*0.382);
   entryLevel38= NormalizeDouble(entryLevel38,Digits);
   entryLevel50=lastHighestHigh -((lastHighestHigh-lastLowestLow)*0.5);
   entryLevel50=NormalizeDouble(entryLevel50,Digits);
   entryLevel61=lastHighestHigh -((lastHighestHigh-lastLowestLow)*0.618);
   entryLevel61=NormalizeDouble(entryLevel61,Digits);

   stopLossLevel78=lastHighestHigh -((lastHighestHigh-lastLowestLow)*0.786);
   stopLossLevel78=NormalizeDouble(stopLossLevel78,Digits);
   stopLossLevel100=lastLowestLow;

   takeProfitLevel0=lastHighestHigh;

   takeProfitLevel23=lastHighestHigh -((lastHighestHigh-lastLowestLow)*0.236);
   takeProfitLevel23=NormalizeDouble(takeProfitLevel23,Digits);
   takeProfitLevelN38=lastHighestHigh+((lastHighestHigh-lastLowestLow)*0.382);
   takeProfitLevelN38= NormalizeDouble(takeProfitLevelN38,Digits);

   entryReason=lastHighestHigh -((lastHighestHigh-lastLowestLow)*0.236);
   entryReason= NormalizeDouble(entryReason,Digits);
   exitReason = lastHighestHigh;

   SetSLTPValues();
   DrawEntryLines();

  }
//+------------------------------------------------------------------+
// End of  CalculateUpwardFibo()                            |
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
// Start of  CalculateDownwardFibo()                            |
//+------------------------------------------------------------------+
void CalculateDownwardFibo()
  {

   entryLevel38=lastLowestLow+((lastHighestHigh-lastLowestLow)*0.382);
   entryLevel38= NormalizeDouble(entryLevel38,Digits);
   entryLevel50=lastLowestLow +((lastHighestHigh-lastLowestLow)*0.5);
   entryLevel50= NormalizeDouble(entryLevel50,Digits);
   entryLevel61=lastLowestLow +((lastHighestHigh-lastLowestLow)*0.618);
   entryLevel61= NormalizeDouble(entryLevel61,Digits);

   stopLossLevel78=lastLowestLow+((lastHighestHigh-lastLowestLow)*0.786);
   stopLossLevel78= NormalizeDouble(stopLossLevel78,Digits);
   stopLossLevel100=lastHighestHigh;

   takeProfitLevel0=lastLowestLow;
   takeProfitLevel23=lastLowestLow+((lastHighestHigh-lastLowestLow)*0.236);
   takeProfitLevel23= NormalizeDouble(takeProfitLevel23,Digits);

   takeProfitLevelN38=lastLowestLow-((lastHighestHigh-lastLowestLow)*0.382);
   takeProfitLevelN38=NormalizeDouble(takeProfitLevelN38,Digits);
   entryReason=lastLowestLow+((lastHighestHigh-lastLowestLow)*0.236);
   entryReason=NormalizeDouble(entryReason,Digits);
   exitReason=lastLowestLow;

   SetSLTPValues();
   DrawEntryLines();
  }
//+------------------------------------------------------------------+
// End of  CalculateDownwardFibo()                            |
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
// Start of  GetLastLowest()                            |
//+------------------------------------------------------------------+
void GetLastLowest()
  {
   double ZigZagLow,ZigZagFlag;
   for(int i=0;i<100;i++)
     {
      ZigZagFlag=iCustom(Symbol(),Period(),"ZigZag",Depth,Deviation,Backstep,0,i);
      ZigZagLow=iCustom(Symbol(),Period(),"ZigZag",Depth,Deviation,Backstep,2,i);

      if(ZigZagFlag!=0)
        {

         if(ZigZagFlag==ZigZagLow)
           {
            lastLowestLow=ZigZagFlag;
            break;
           }

        }
     }
  }
//+------------------------------------------------------------------+
// End of  GetLastLowest()                            |
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
// Start of  GetLastHighest()                            |
//+------------------------------------------------------------------+
void GetLastHighest()
  {
   double ZigZagHigh,ZigZagFlag;
   for(int i=0;i<100;i++)
     {
      ZigZagFlag=iCustom(Symbol(),Period(),"ZigZag",Depth,Deviation,Backstep,0,i);
      ZigZagHigh=iCustom(Symbol(),Period(),"ZigZag",Depth,Deviation,Backstep,1,i);

      if(ZigZagFlag!=0)
        {
         if(ZigZagFlag==ZigZagHigh)
           {
            lastHighestHigh=ZigZagFlag;
            break;
           }

        }
     }
  }
//+------------------------------------------------------------------+
// End of  GetLastHighest()                            |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
// End of  SetSLTPValues()                            |
//+------------------------------------------------------------------+
void SetSLTPValues()
  {
   if(StopLossValue==0) stopLossValue=stopLossLevel78;
   else if(StopLossValue==1) stopLossValue=stopLossLevel100;

   if(TakeProfitValue==0) takeProfitValue=takeProfitLevel23;
   else if(TakeProfitValue == 1) takeProfitValue = takeProfitLevel0;
   else if(TakeProfitValue == 2) takeProfitValue = takeProfitLevelN38;
  }
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
// End of  SetSLTPValues()                            |
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
// Start of  GetRange()                            |
//+------------------------------------------------------------------+
bool GetRange()
  {
   double range;
   range=(MathAbs(lastHighestHigh-lastLowestLow)/(Point*P));
   range= NormalizeDouble(range,Digits);
   if(range>EntryRange)
     {
      return true;
     }
   else
     {
      return false;
     }

  }
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
// End of  GetRange()                            |
//+------------------------------------------------------------------+
