//+------------------------------------------------------------------+
//|                                                      ProjectName |
//|                                      Copyright 2012, CompanyName |
//|                                       http://www.companyname.net |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020 HelmiFX."
#property link      "www.helmifx.com"
#property strict
#property description "Mottalyat Expert"
#property version "1.2"

#property description "Find us on  :\n"
#property description "Facbook : HelmiFx"
#property description "Twitter : @HelmiForex"  
#property description "Youtube : HelmiForex" 

#property icon "photo.ico"
#resource "picture.bmp"  


#include <Arrays\ArrayInt.mqh>
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
enum ENUM_ENTRYMODE
  {
   Custom=0,// Custom
   Stoch=1 // Stoch
  };
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
enum ENUM_ARRAYMODE
  {
   NextOnSL=0,// Next when StopLoss
   NextOnTP=1,// Next when TakePofit
  };
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
enum ENUM_ORDERTYPES
  {
   Buy =0 , // Buy
   Sell =1, // Sell
  };
//+------------------------------------------------------------------+
//| Setup                                               
//+------------------------------------------------------------------+ 
extern string  Header1="**************************************************************************************"; // ---------- Entry Mode -----------
extern ENUM_ENTRYMODE EntryMode=0; // Entry Mode
extern ENUM_ARRAYMODE ArrayMode=0; // Array Mode
extern string sep0="****************************"; // ------ If Entry Mode is 'Custom'
extern ENUM_ORDERTYPES OrderType_1=0; // Order Type 1
extern double OrderLot_1=0.01; // Order Lot 1
extern double OrderSL_1= 10; // Order SL 1
extern double OrderTP_1= 15; // Order TP 1
extern string sep2=""; // -- 
extern ENUM_ORDERTYPES OrderType_2=1; // Order Type 2
extern double OrderLot_2=0.01; // Order Lot 2
extern double OrderSL_2= 10; // Order SL 2
extern double OrderTP_2= 15; // Order TP 2
extern string sep3=""; // -- 
extern ENUM_ORDERTYPES OrderType_3=0; // Order Type 3
extern double OrderLot_3=0.01; // Order Lot 3
extern double OrderSL_3= 10; // Order SL 3
extern double OrderTP_3= 15; // Order TP 3
extern string sep4=""; // -- 
extern ENUM_ORDERTYPES OrderType_4=1; // Order Type 4
extern double OrderLot_4=0.01; // Order Lot 4
extern double OrderSL_4= 10; // Order SL 4
extern double OrderTP_4= 15; // Order TP 4
extern string sep5=""; // -- 
extern ENUM_ORDERTYPES OrderType_5=0; // Order Type 5
extern double OrderLot_5=0.01; // Order Lot 5
extern double OrderSL_5= 10; // Order SL 5
extern double OrderTP_5= 15; // Order TP 5
extern string sep6=""; // -- 
extern ENUM_ORDERTYPES OrderType_6=1; // Order Type 6
extern double OrderLot_6=0.01; // Order Lot 6
extern double OrderSL_6= 10; // Order SL 6
extern double OrderTP_6= 15; // Order TP 6
extern string sep7=""; // -- 
extern ENUM_ORDERTYPES OrderType_7=0; // Order Type 7
extern double OrderLot_7=0.01; // Order Lot 7
extern double OrderSL_7= 10; // Order SL 7
extern double OrderTP_7= 15; // Order TP 7
extern string sep8=""; // -- 
extern ENUM_ORDERTYPES OrderType_8=1; // Order Type 8
extern double OrderLot_8=0.01; // Order Lot 8
extern double OrderSL_8= 10; // Order SL 8
extern double OrderTP_8= 15; // Order TP 8
extern string sep9=""; // -- 
extern ENUM_ORDERTYPES OrderType_9=0; // Order Type 9
extern double OrderLot_9=0.01; // Order Lot 9
extern double OrderSL_9= 10; // Order SL 9
extern double OrderTP_9= 15; // Order TP 9
extern string sep10=""; // -- 
extern ENUM_ORDERTYPES OrderType_10=1; // Order Type 10
extern double OrderLot_10=0.01; // Order Lot 10
extern double OrderSL_10= 10; // Order SL 10
extern double OrderTP_10= 15; // Order TP 10
extern string sep11=""; // -- 
extern ENUM_ORDERTYPES OrderType_11=0; // Order Type 11
extern double OrderLot_11=0.01; // Order Lot 11
extern double OrderSL_11= 10; // Order SL 11
extern double OrderTP_11= 15; // Order TP 11
extern string sep12=""; // -- 
extern ENUM_ORDERTYPES OrderType_12=1; // Order Type 12
extern double OrderLot_12=0.01; // Order Lot 12
extern double OrderSL_12= 10; // Order SL 12
extern double OrderTP_12= 15; // Order TP 12
extern string sep13=""; // -- 
extern ENUM_ORDERTYPES OrderType_13=0; // Order Type 13
extern double OrderLot_13=0.01; // Order Lot 13
extern double OrderSL_13= 10; // Order SL 13
extern double OrderTP_13= 15; // Order TP 13
extern string sep14=""; // -- 
extern ENUM_ORDERTYPES OrderType_14=1; // Order Type 14
extern double OrderLot_14=0.01; // Order Lot 14
extern double OrderSL_14= 10; // Order SL 14
extern double OrderTP_14= 15; // Order TP 14
extern string sep15=""; // -- 
extern ENUM_ORDERTYPES OrderType_15=0; // Order Type 15
extern double OrderLot_15=0.01; // Order Lot 15
extern double OrderSL_15= 10; // Order SL 15
extern double OrderTP_15= 15; // Order TP 15
extern string sep16=""; // -- 
extern ENUM_ORDERTYPES OrderType_16=1; // Order Type 16
extern double OrderLot_16=0.01; // Order Lot 16
extern double OrderSL_16= 10; // Order SL 16
extern double OrderTP_16= 15; // Order TP 16
extern string sep17=""; // -- 
extern ENUM_ORDERTYPES OrderType_17=0; // Order Type 17
extern double OrderLot_17=0.01; // Order Lot 17
extern double OrderSL_17= 10; // Order SL 17
extern double OrderTP_17= 15; // Order TP 17
extern string sep18=""; // -- 
extern ENUM_ORDERTYPES OrderType_18=1; // Order Type 18
extern double OrderLot_18=0.01; // Order Lot 18
extern double OrderSL_18= 10; // Order SL 18
extern double OrderTP_18= 15; // Order TP 18
extern string sep19=""; // --
extern ENUM_ORDERTYPES OrderType_19=0; // Order Type 19
extern double OrderLot_19=0.01; // Order Lot 19
extern double OrderSL_19= 10; // Order SL 19
extern double OrderTP_19= 15; // Order TP 19
extern string sep20=""; // -- 
extern ENUM_ORDERTYPES OrderType_20=1; // Order Type 20
extern double OrderLot_20=0.01; // Order Lot 20
extern double OrderSL_20= 10; // Order SL 20
extern double OrderTP_20= 15; // Order TP 20
extern string sep200="****************************"; // ------ If Entry Mode is 'Stoch'
extern int KPeriod = 5;
extern int DPeriod = 3;
extern int Slowing = 3;
extern ENUM_MA_METHOD SOMAMethod=0;
extern int TopLevel=80;
extern int BottomLevel=20;
extern ENUM_TIMEFRAMES SOTimeFrame=0;
extern string sep2022="*****"; // ------ Orders Settings if 'Stoch'
extern double StochLot=0.01; // Stoch Buy Lot
extern double LotMultiplier=2; // Lot Multiplier
extern double StochSL= 10; // Stoch Buy SL
extern double StochTP= 15; // Stoch Buy TP
extern string sep300="****************************"; // ------ IsRestartLimit
extern bool IsRestartLimit=false; // Is RestartLimit activated?
extern int RestartLimit=8; // The position its going to restart on if it reached
extern string  Header11="**************************************************************************************"; // ----------EA General -----------
extern bool    UsePanel=true;
extern color   PanelLabelColor=clrWheat;
extern int     MagicNumber=656668;

string  InternalHeader1="----------Errors Handling Settings-----------";

int     RetryInterval=100; // Pause Time before next retry (in milliseconds)
int     MaxRetriesPerTick=10;
int     Slippage=3;
int     xDistance=1;
bool    OnJournaling=true; // Add EA updates in the Journal Tab
string  InternalHeader2="----------Service Variables-----------";

//+------------------------------------------------------------------+
//|General Variables                                                                 |
//+------------------------------------------------------------------+

double StopBuy,StopSell;
double TakeBuy,TakeSell;

int P;

CArrayInt Orders;

int OrdersArrayType[];
double OrdersArray[][3];//0 Lot,1 sl , 2 tp, 3 on/off, 4 
int OrdersArrayIndex=0;

int OrderNumber;

//double TrailingStopList[][2];
//double BreakevenList[][2];

bool canEnterStoch=true;

datetime today;
datetime StartofExpert;

double SOK_1;
double SOD_1;

double lastLot=StochLot;

int SOCrossTriggered,SOCurrentDirection,SOLastDirection;
bool SOFirstTime=true;
int SODirection;

//+-----------------------------------------------------------------+
//|Chart Display Variables                                                                  |
//+------------------------------------------------------------------+
double buyLastClosedProfit=0;
double sellLastClosedProfit=0;
double cycleProfit; //Get All Opened Profit
double buyCycleProfit;
double sellCycleProfit;
double cycleLots;
double buyCycleLots;    //Get All Buy  Opened Lots
double sellCycleLots;    //Get All Sell Opened Lots
double netProfit;    // All the profit of all opened positions by the same MagicNumber
string ObjName=IntegerToString(ChartID(),0,' ');   //The global object name 
double lotsTotal;

//+------------------------------------------------------------------+
//|//Pip Display Variables                                           |
//+------------------------------------------------------------------+
double pipValue;
double buyPipValue;
double sellPipValue;
double pipValue1Lot;
double spreadValue;

double profitSinceLastCycle;
//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
int init()
  {

   Comment("Copyright 2020 HelmiFX.");

//if(UseTrailingStops) ArrayResize(TrailingStopList,1000,0);
//if(UseBreakeven) ArrayResize(BreakevenList,1000,0);
   P=GetP(); // To account for 5 digit brokers. Used to convert pips to decimal place
   ChartSettings();

   ArrayResize(OrdersArray,20,0);
   ArrayInitialize(OrdersArray,0);

   ArrayResize(OrdersArrayType,20,0);
   ArrayInitialize(OrdersArrayType,0);

   today=Time[0];
   StartofExpert=Time[0];

   InitializeVariables();

   if(EntryMode==0) // Custom
     {
      EntryMarket();
     }

   if(UsePanel) DrawChartInfo();            //Drawing Chart Info

   return(INIT_SUCCEEDED);

  }
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
  {
//---
   if(reason!=5 && reason!=3)
     {
      ObjectsDeleteAll(0,0,OBJ_LABEL);
      ObjectsDeleteAll(0,0,OBJ_RECTANGLE_LABEL);
      ObjectsDeleteAll(0,0,OBJ_ARROW);
      ObjectsDeleteAll(0,0,OBJ_HLINE);

      ObjectsDeleteAll(ChartID(),0);
     }
  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
int start()
  {

   cycleProfit=GetCycleProfit(0)+GetCycleProfit(1); //Set Cycle Profits onto the chart.
   buyCycleProfit=GetCycleProfit(0);
   sellCycleProfit=GetCycleProfit(1);
   buyCycleLots=GetCycleLots(0); //Set Cycle Lots onto the chart.
   sellCycleLots=GetCycleLots(1);
   cycleLots=GetCycleLots(0)+GetCycleLots(1);
   lotsTotal=GetLotsSince(StartofExpert,OP_BUY)+GetLotsSince(StartofExpert,OP_SELL);
   pipValue=(((MarketInfo(Symbol(),MODE_TICKVALUE)*Point*P)/MarketInfo(Symbol(),MODE_TICKSIZE))*cycleLots);// pip value of 1 lot
   buyPipValue=(((MarketInfo(Symbol(),MODE_TICKVALUE)*Point*P)/MarketInfo(Symbol(),MODE_TICKSIZE))*buyCycleLots);// pip value of 1 lot
   sellPipValue=(((MarketInfo(Symbol(),MODE_TICKVALUE)*Point*P)/MarketInfo(Symbol(),MODE_TICKSIZE))*sellCycleLots);// pip value of 1 lot
   pipValue1Lot=(((MarketInfo(Symbol(),MODE_TICKVALUE)*Point*P)/MarketInfo(Symbol(),MODE_TICKSIZE))*1);// pip value of 1 lot
   netProfit=GetNetProfitSince(StartofExpert,OP_BUY)+GetNetProfitSince(StartofExpert,OP_SELL);
   buyLastClosedProfit = GetClosedProfitSince(StartofExpert,OP_BUY);
   sellLastClosedProfit= GetClosedProfitSince(StartofExpert,OP_SELL);
   spreadValue=MarketInfo(Symbol(),MODE_SPREAD)/P;

   if(UsePanel) UpdateChartInfo();

//----------TP & SL Variables-----------

//   if(UseTrailingStops)
//     {
//      UpdateTrailingList(OnJournaling,RetryInterval,MagicNumber);
//      ReviewTrailingStop(OnJournaling,TrailingStopOffset,RetryInterval,MagicNumber,P);
//     }
//
//   if(UseBreakeven)
//     {
//      UpdateBreakevenList(OnJournaling,RetryInterval,MagicNumber);
//      ReviewBreakeven(OnJournaling,BreakevenOffset,RetryInterval,MagicNumber,P);
//     }

   CheckSLTPExit();

//----------Entry & Exit Variables-----------

   SOK_1=iStochastic(Symbol(),SOTimeFrame,KPeriod,DPeriod,Slowing,SOMAMethod,0,MODE_MAIN,0);
   SOD_1=iStochastic(Symbol(),SOTimeFrame,KPeriod,DPeriod,Slowing,SOMAMethod,0,MODE_SIGNAL,0);
   SOCrossTriggered=SOCross(SOK_1,SOD_1,TopLevel,BottomLevel);
   SODirection=SOCurrentDirection;

   if(EntryMode==1) // Stoch
     {
      if(canEnterStoch)
        {
         if(SOCrossTriggered!=0 && NoOrdersOnline()) // Cross Happend
           {
            EntryMarket();
           }
        }
     }
   return (0);
  }
//+------------------------------------------------------------------+   
//|End of Start()                                                        |
//+------------------------------------------------------------------+

void EntryMarket()
  {
   if(EntryMode==0) // Custom
     {
      if(OrdersArrayType[OrdersArrayIndex]==0 && NoOrdersOnline())// Buy
        {

         OrderNumber=OpenPositionMarket("BUY",OP_BUY,OrdersArray[OrdersArrayIndex][0],OrdersArray[OrdersArrayIndex][1],OrdersArray[OrdersArrayIndex][2],MagicNumber,Slippage,OnJournaling,P,MaxRetriesPerTick);
         Orders.Add(OrderNumber);

        }
      else if(OrdersArrayType[OrdersArrayIndex]==1 && NoOrdersOnline())// Sell
        {
         OrderNumber=OpenPositionMarket("SELL",OP_SELL,OrdersArray[OrdersArrayIndex][0],OrdersArray[OrdersArrayIndex][1],OrdersArray[OrdersArrayIndex][2],MagicNumber,Slippage,OnJournaling,P,MaxRetriesPerTick);
         Orders.Add(OrderNumber);
        }
     }
   else // Stoch
     {
      if(SOCrossTriggered==1) // Buy 
        {
         OrderNumber=OpenPositionMarket("BUY",OP_BUY,lastLot,StochSL,StochTP,MagicNumber,Slippage,OnJournaling,P,MaxRetriesPerTick);
         Orders.Add(OrderNumber);
         canEnterStoch=false;

        }
      else if(SOCrossTriggered==2)// Sell
        {
         OrderNumber=OpenPositionMarket("SELL",OP_SELL,lastLot,StochSL,StochTP,MagicNumber,Slippage,OnJournaling,P,MaxRetriesPerTick);
         Orders.Add(OrderNumber);
         canEnterStoch=false;
        }
     }

  }
//+------------------------------------------------------------------+
// Start of CheckLot()                                       				|
//+------------------------------------------------------------------+

double CheckLot(double lot,bool Journaling)
  {
// Type: Fixed Template 
// Do not edit unless you know what you're doing

// This function checks if our Lots to be trade satisfies any broker limitations

   double LotToOpen=0;
   LotToOpen=NormalizeDouble(lot,2);
   LotToOpen=MathFloor(LotToOpen/MarketInfo(Symbol(),MODE_LOTSTEP))*MarketInfo(Symbol(),MODE_LOTSTEP);

   if(LotToOpen<MarketInfo(Symbol(),MODE_MINLOT))LotToOpen=MarketInfo(Symbol(),MODE_MINLOT);
   if(LotToOpen>MarketInfo(Symbol(),MODE_MAXLOT))LotToOpen=MarketInfo(Symbol(),MODE_MAXLOT);
   LotToOpen=NormalizeDouble(LotToOpen,2);

   if(Journaling && LotToOpen!=lot)Print("EA Journaling: Trading Lot has been changed by CheckLot function. Requested lot: "+DoubleToString(lot)+". Lot to open: "+DoubleToString(LotToOpen));

   return(LotToOpen);
  }
//+------------------------------------------------------------------+
//| End of CheckLot()
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//| Start of GetCycleLots()                                                             |
//+------------------------------------------------------------------+
double GetCycleLots(int type)
  {

// Type: Customisable 
// Modify this function to suit your trading robot

// this function returns the net profit of the opened orders 

   double Lots=0;

   for(int i=0; i<OrdersTotal(); i++)
     {
      if(OrderSelect(i,SELECT_BY_POS,MODE_TRADES)==true && OrderSymbol()==Symbol() && (OrderMagicNumber()==MagicNumber) && OrderType()==type)
         Lots+=OrderLots();
     }
   return(NormalizeDouble(Lots,2));

  }
//+------------------------------------------------------------------+
// End of GetCycleLots()                                    		|
//+------------------------------------------------------------------+
double GetCycleProfit(int type)
  {

// Type: Customisable 
// Modify this function to suit your trading robot

// this function returns the net profit of the opened orders 

   double Profit=0;

   for(int i=0; i<OrdersTotal(); i++)
     {
      if(OrderSelect(i,SELECT_BY_POS,MODE_TRADES)==true && OrderSymbol()==Symbol() && (OrderMagicNumber()==MagicNumber) && OrderType()==type)
         Profit=Profit+OrderProfit();
     }
   return(NormalizeDouble(Profit,2));
  }
//+------------------------------------------------------------------+
// End of GetCycleLots()                                    		|
//+------------------------------------------------------------------+
double GetNetProfitSince(datetime time,int ordertype)
  {

// Type: Customisable 
// Modify this function to suit your trading robot

// this function returns the net profit of the opened orders 

   double Profit=0;

   for(int i=OrdersTotal()-1; i>=0; i--)
     {
      if(OrderSelect(i,SELECT_BY_POS,MODE_TRADES)==true && OrderSymbol()==Symbol() && (OrderMagicNumber()==MagicNumber) && OrderOpenTime()>=time && OrderType()==ordertype)
         Profit=Profit+OrderProfit();
     }

   for(int i=OrdersHistoryTotal()-1; i>=0; i--)
     {
      if(OrderSelect(i,SELECT_BY_POS,MODE_HISTORY)==true && OrderSymbol()==Symbol() && (OrderMagicNumber()==MagicNumber) && OrderOpenTime()>=time && OrderType()==ordertype)
         Profit=Profit+OrderProfit()+OrderSwap()+OrderCommission();
     }
   return(Profit);
  }
//+------------------------------------------------------------------+
// End of GetCycleLots()                                    		|
//+------------------------------------------------------------------+
double GetLotsSince(datetime time,int ordertype)
  {

// Type: Customisable 
// Modify this function to suit your trading robot

// this function returns the net profit of the opened orders 

   double Lots=0;

   for(int i=OrdersTotal()-1; i>=0; i--)
     {
      if(OrderSelect(i,SELECT_BY_POS,MODE_TRADES)==true && OrderSymbol()==Symbol() && (OrderMagicNumber()==MagicNumber) && OrderOpenTime()>time && OrderType()==ordertype)
         Lots=Lots+OrderLots();
     }

   for(int i=OrdersHistoryTotal()-1; i>=0; i--)
     {
      if(OrderSelect(i,SELECT_BY_POS,MODE_HISTORY)==true && OrderSymbol()==Symbol() && (OrderMagicNumber()==MagicNumber) && OrderOpenTime()>time && OrderType()==ordertype)
         Lots=Lots+OrderLots();
     }
   return(Lots);
  }
//+------------------------------------------------------------------+
// End of GetCycleLots()                                    		|
//+------------------------------------------------------------------+
double GetClosedProfitSince(datetime time,int ordertype)
  {

// Type: Customisable 
// Modify this function to suit your trading robot

// this function returns the net profit of the opened orders 

   double Profit=0;

   for(int i=OrdersHistoryTotal()-1; i>=0; i--)
     {
      if(OrderSelect(i,SELECT_BY_POS,MODE_HISTORY)==true && OrderSymbol()==Symbol() && (OrderMagicNumber()==MagicNumber) && OrderOpenTime()>time && OrderType()==ordertype)
         Profit=Profit+OrderProfit()+OrderSwap()+OrderCommission();
     }
   return(Profit);
  }
//+------------------------------------------------------------------+
// End of GetCycleProfit()                                    			|
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|Start of CountPosOrders()	" Count Positions"
//+------------------------------------------------------------------+
int CountPosOrders(int Magic,int TYPE)
  {
// Type: Fixed Template 
// Do not edit unless you know what you're doing

// This function counts number of positions/orders of OrderType TYPE

   int orders=0;
   for(int i=0; i<OrdersTotal(); i++)
     {
      if(OrderSelect(i,SELECT_BY_POS,MODE_TRADES)==true && OrderSymbol()==Symbol() && OrderMagicNumber()==Magic && OrderType()==TYPE)
         orders++;
     }
   return(orders);

  }
//+------------------------------------------------------------------+
//|                                                                  |

//+------------------------------------------------------------------+
//| End of IsMaxPositionsReached()                                                
//+------------------------------------------------------------------+
void CloseOnLoss(int orderType)
  {
   int total=OrdersTotal();
   for(int i=total-1; i>=0; i--)
     {
      if(OrderSelect(i,SELECT_BY_POS,MODE_TRADES)==true && OrderSymbol()==Symbol() && OrderMagicNumber()==MagicNumber && OrderType()==orderType)
        {
         if(OrderProfit()<0)
           {
            bool Closing=false;
            double Price=0;
            color arrow_color=0;if(OrderType()==OP_BUY)arrow_color=MediumSeaGreen;if(OrderType()==OP_SELL)arrow_color=DarkOrange;
            if(OnJournaling)Print("EA Journaling: Trying to close position "+OrderTicket()+" ...");
            HandleTradingEnvironment(OnJournaling,RetryInterval);
            if(OrderType()==OP_BUY)Price=Bid; if(OrderType()==OP_SELL)Price=Ask;
            Closing=OrderClose(OrderTicket(),OrderLots(),Price,Slippage*P,arrow_color);
            if(OnJournaling && !Closing)Print("EA Journaling: Unexpected Error has happened. Error Description: "+GetErrorDescription(GetLastError()));
            if(OnJournaling && Closing)Print("EA Journaling: Position successfully closed. Profit Bar");
           }
        }
     }
  }
//+------------------------------------------------------------------+
//|Start of OpenPositionMarket()
//+------------------------------------------------------------------+
int OpenPositionMarket(string baseComment,int TYPE,double LOT,double SL,double TP,int Magic,int Slip,bool Journaling,int K,int Max_Retries_Per_Tick)
  {
// Type: Fixed Template 
// Do not edit unless you know what you're doing 

// This function submits new orders

   int tries=0;
   string symbol=Symbol();
   int cmd=TYPE;
   double volume=CheckLot(LOT,Journaling);
   if(MarketInfo(symbol,MODE_MARGINREQUIRED)*volume>AccountFreeMargin())
     {
      Print("Can not open a trade. Not enough free margin to open "+volume+" on "+symbol);
      return(-1);
     }
   int slippage=Slip*K; // Slippage is in points. 1 point = 0.0001 on 4 digit broker and 0.00001 on a 5 digit broker
   string comment=" "+baseComment+"(#"+Magic+")";;
   int magic=Magic;
   datetime expiration=0;
   color arrow_color=0;if(TYPE==OP_BUY)arrow_color=DodgerBlue;if(TYPE==OP_SELL)arrow_color=DeepPink;
   double stoploss=0;
   double takeprofit=0;
   double initTP = TP;
   double initSL = SL;
   int Ticket=-1;
   double price=0;

   while(tries<Max_Retries_Per_Tick) // Edits stops and take profits before the market order is placed
     {
      RefreshRates();
      if(TYPE==OP_BUY)price=Ask;if(TYPE==OP_SELL)price=Bid;

      // Sets Take Profits and Stop Loss. Check against Stop Level Limitations.
      if(TYPE==OP_BUY && SL!=0)
        {
         stoploss=NormalizeDouble(Ask-SL*K*Point,Digits);
         if(Bid-stoploss<=MarketInfo(Symbol(),MODE_STOPLEVEL)*Point)
           {
            stoploss=NormalizeDouble(Bid-MarketInfo(Symbol(),MODE_STOPLEVEL)*Point,Digits);
            if(Journaling)Print("EA Journaling: Stop Loss changed from "+initSL+" to "+MarketInfo(Symbol(),MODE_STOPLEVEL)/K+" pips");
           }
        }
      if(TYPE==OP_SELL && SL!=0)
        {
         stoploss=NormalizeDouble(Bid+SL*K*Point,Digits);
         if(stoploss-Ask<=MarketInfo(Symbol(),MODE_STOPLEVEL)*Point)
           {
            stoploss=NormalizeDouble(Ask+MarketInfo(Symbol(),MODE_STOPLEVEL)*Point,Digits);
            if(Journaling)Print("EA Journaling: Stop Loss changed from "+initSL+" to "+MarketInfo(Symbol(),MODE_STOPLEVEL)/K+" pips");
           }
        }
      if(TYPE==OP_BUY && TP!=0)
        {
         takeprofit=NormalizeDouble(Ask+TP*K*Point,Digits);
         if(takeprofit-Bid<=MarketInfo(Symbol(),MODE_STOPLEVEL)*Point)
           {
            takeprofit=NormalizeDouble(Ask+MarketInfo(Symbol(),MODE_STOPLEVEL)*Point,Digits);
            if(Journaling)Print("EA Journaling: Take Profit changed from "+initTP+" to "+MarketInfo(Symbol(),MODE_STOPLEVEL)/K+" pips");
           }
        }
      if(TYPE==OP_SELL && TP!=0)
        {
         takeprofit=NormalizeDouble(Bid-TP*K*Point,Digits);
         if(Ask-takeprofit<=MarketInfo(Symbol(),MODE_STOPLEVEL)*Point)
           {
            takeprofit=NormalizeDouble(Bid-MarketInfo(Symbol(),MODE_STOPLEVEL)*Point,Digits);
            if(Journaling)Print("EA Journaling: Take Profit changed from "+initTP+" to "+MarketInfo(Symbol(),MODE_STOPLEVEL)/K+" pips");
           }
        }
      if(Journaling)Print("EA Journaling: Trying to place a market order...");
      HandleTradingEnvironment(Journaling,RetryInterval);
      Ticket=OrderSend(symbol,cmd,volume,price,slippage,stoploss,takeprofit,comment,magic,expiration,arrow_color);
      if(Ticket>0)break;
      tries++;
     }

   if(Journaling && Ticket<0)Print("EA Journaling: Unexpected Error has happened. Error Description: "+GetErrorDescription(GetLastError()));
   if(Journaling && Ticket>0)
     {
      Print("EA Journaling: Order successfully placed. Ticket: "+Ticket);
     }
   return(Ticket);
  }
//+------------------------------------------------------------------+
//|End of OpenPositionMarket()
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|Start of CloseOrderPosition()
//+------------------------------------------------------------------+
bool CloseOrderPosition(int TYPE,bool Journaling,int Magic,int Slip,int K)
  {
// Type: Fixed Template 
// Do not edit unless you know what you're doing
   int ordersPos=OrdersTotal();

   for(int i=ordersPos-1; i>=0; i--)
     {
      // Note: Once pending orders become positions, OP_BUYLIMIT AND OP_BUYSTOP becomes OP_BUY, OP_SELLLIMIT and OP_SELLSTOP becomes OP_SELL
      if(TYPE==OP_BUY || TYPE==OP_SELL)
        {
         if(OrderSelect(i,SELECT_BY_POS,MODE_TRADES)==true && OrderSymbol()==Symbol() && OrderMagicNumber()==Magic && OrderType()==TYPE)
           {
            bool Closing=false;
            double Price=0;
            color arrow_color=0;if(TYPE==OP_BUY)arrow_color=MediumSeaGreen;if(TYPE==OP_SELL)arrow_color=DarkOrange;
            if(Journaling)Print("EA Journaling: Trying to close position "+OrderTicket()+" ...");
            HandleTradingEnvironment(Journaling,RetryInterval);
            if(TYPE==OP_BUY)Price=Bid; if(TYPE==OP_SELL)Price=Ask;
            Closing=OrderClose(OrderTicket(),OrderLots(),Price,Slip*K,arrow_color);
            if(Journaling && !Closing)Print("EA Journaling: Unexpected Error has happened. Error Description: "+GetErrorDescription(GetLastError()));
            if(Journaling && Closing)Print("EA Journaling: Position successfully closed.");
           }
        }
     }
   if(CountPosOrders(Magic, TYPE)==0)return(true); else return(false);
  }
//+------------------------------------------------------------------+
//| End of CloseOrderPosition()
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//| Start of CloseTicket()
//+------------------------------------------------------------------+ 
void CloseTicket(int ticket,bool Journaling,int Magic,int Slip,int K)
  {

// Type: Fixed Template 
// Do not edit unless you know what you're doing
   int ordersPos=OrdersTotal();
   if(OrderSelect(ticket,SELECT_BY_TICKET,MODE_TRADES)==true && OrderSymbol()==Symbol() && OrderMagicNumber()==Magic)
     {
      if(OrderCloseTime()==0) // Still Opened.
        {
         if(OrderType()==OP_BUY || OrderType()==OP_SELL)
           {
            bool Closing=false;
            double Price=0;
            color arrow_color=0;if(OrderType()==OP_BUY)arrow_color=MediumSeaGreen;if(OrderType()==OP_SELL)arrow_color=DarkOrange;
            if(Journaling)Print("EA Journaling: Trying to close position "+OrderTicket()+" ...");
            HandleTradingEnvironment(Journaling,RetryInterval);
            if(OrderType()==OP_BUY)Price=Bid; if(OrderType()==OP_SELL)Price=Ask;
            Closing=OrderClose(OrderTicket(),OrderLots(),Price,Slip*K,arrow_color);
            if(Journaling && !Closing)Print("EA Journaling: Unexpected Error has happened. Error Description: "+GetErrorDescription(GetLastError()));
            if(Journaling && Closing)Print("EA Journaling: Position successfully closed.");
           }
         else
           {
            bool Delete=false;
            if(Journaling)Print("EA Journaling: Trying to delete order "+OrderTicket()+" ...");
            HandleTradingEnvironment(Journaling,RetryInterval);
            Delete=OrderDelete(OrderTicket(),White);
            if(Journaling && !Delete)Print("EA Journaling: Unexpected Error has happened Deleting the Pending Order. Error Description: "+GetErrorDescription(GetLastError()));
            if(Journaling && Delete)Print("EA Journaling: Order successfully deleted.");

           }
        }
     }
  }
//+------------------------------------------------------------------+
//| End of CloseTicket()
//+------------------------------------------------------------------+ 
//+------------------------------------------------------------------+
// Start of getP()                                                   |
//+------------------------------------------------------------------+

int GetP()
  {

   int output;

   if(Digits==5 || Digits==3) output=10;
   else output=1;

   return(output);
  }
//+------------------------------------------------------------------+
// End of GetP()                                                    	|
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//| Start of HandleTradingEnvironment()                                         
//+------------------------------------------------------------------+
void HandleTradingEnvironment(bool Journaling,int Retry_Interval)
  {
// Type: Fixed Template 
// Do not edit unless you know what you're doing 

// This function checks for errors

   if(IsTradeAllowed()==true)return;
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
   if(!IsConnected())
     {
      if(Journaling)Print("EA Journaling: Terminal is not connected to server...");
      return;
     }
   if(!IsTradeAllowed() && Journaling)Print("EA Journaling: Trade is not alowed for some reason...");
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
   if(IsConnected() && !IsTradeAllowed())
     {
      while(IsTradeContextBusy()==true)
        {
         if(Journaling)Print("EA Journaling: Trading context is busy... Will wait a bit...");
         Sleep(Retry_Interval);
        }
     }
   RefreshRates();
  }
//+------------------------------------------------------------------+
//| End of HandleTradingEnvironment()                              
//+------------------------------------------------------------------+  
//+------------------------------------------------------------------+
//| Start of GetErrorDescription()                                               
//+------------------------------------------------------------------+
string GetErrorDescription(int error)
  {
// Type: Fixed Template 
// Do not edit unless you know what you're doing

// This function returns the exact error

   string ErrorDescription="";
//---
   switch(error)
     {
      case 0:     ErrorDescription = "NO Error. Everything should be good.";                                    break;
      case 1:     ErrorDescription = "No error returned, but the result is unknown";                            break;
      case 2:     ErrorDescription = "Common error";                                                            break;
      case 3:     ErrorDescription = "Invalid trade parameters";                                                break;
      case 4:     ErrorDescription = "Trade server is busy";                                                    break;
      case 5:     ErrorDescription = "Old version of the client terminal";                                      break;
      case 6:     ErrorDescription = "No connection with trade server";                                         break;
      case 7:     ErrorDescription = "Not enough rights";                                                       break;
      case 8:     ErrorDescription = "Too frequent requests";                                                   break;
      case 9:     ErrorDescription = "Malfunctional trade operation";                                           break;
      case 64:    ErrorDescription = "Account disabled";                                                        break;
      case 65:    ErrorDescription = "Invalid account";                                                         break;
      case 128:   ErrorDescription = "Trade timeout";                                                           break;
      case 129:   ErrorDescription = "Invalid price";                                                           break;
      case 130:   ErrorDescription = "Invalid stops";                                                           break;
      case 131:   ErrorDescription = "Invalid trade volume";                                                    break;
      case 132:   ErrorDescription = "Market is closed";                                                        break;
      case 133:   ErrorDescription = "Trade is disabled";                                                       break;
      case 134:   ErrorDescription = "Not enough money";                                                        break;
      case 135:   ErrorDescription = "Price changed";                                                           break;
      case 136:   ErrorDescription = "Off quotes";                                                              break;
      case 137:   ErrorDescription = "Broker is busy";                                                          break;
      case 138:   ErrorDescription = "Requote";                                                                 break;
      case 139:   ErrorDescription = "Order is locked";                                                         break;
      case 140:   ErrorDescription = "Long positions only allowed";                                             break;
      case 141:   ErrorDescription = "Too many requests";                                                       break;
      case 145:   ErrorDescription = "Modification denied because order too close to market";                   break;
      case 146:   ErrorDescription = "Trade context is busy";                                                   break;
      case 147:   ErrorDescription = "Expirations are denied by broker";                                        break;
      case 148:   ErrorDescription = "Too many open and pending orders (more than allowed)";                    break;
      case 4000:  ErrorDescription = "No error";                                                                break;
      case 4001:  ErrorDescription = "Wrong function pointer";                                                  break;
      case 4002:  ErrorDescription = "Array index is out of range";                                             break;
      case 4003:  ErrorDescription = "No memory for function call stack";                                       break;
      case 4004:  ErrorDescription = "Recursive stack overflow";                                                break;
      case 4005:  ErrorDescription = "Not enough stack for parameter";                                          break;
      case 4006:  ErrorDescription = "No memory for parameter string";                                          break;
      case 4007:  ErrorDescription = "No memory for temp string";                                               break;
      case 4008:  ErrorDescription = "Not initialized string";                                                  break;
      case 4009:  ErrorDescription = "Not initialized string in array";                                         break;
      case 4010:  ErrorDescription = "No memory for array string";                                              break;
      case 4011:  ErrorDescription = "Too long string";                                                         break;
      case 4012:  ErrorDescription = "Remainder from zero divide";                                              break;
      case 4013:  ErrorDescription = "Zero divide";                                                             break;
      case 4014:  ErrorDescription = "Unknown command";                                                         break;
      case 4015:  ErrorDescription = "Wrong jump (never generated error)";                                      break;
      case 4016:  ErrorDescription = "Not initialized array";                                                   break;
      case 4017:  ErrorDescription = "DLL calls are not allowed";                                               break;
      case 4018:  ErrorDescription = "Cannot load library";                                                     break;
      case 4019:  ErrorDescription = "Cannot call function";                                                    break;
      case 4020:  ErrorDescription = "Expert function calls are not allowed";                                   break;
      case 4021:  ErrorDescription = "Not enough memory for temp string returned from function";                break;
      case 4022:  ErrorDescription = "System is busy (never generated error)";                                  break;
      case 4050:  ErrorDescription = "Invalid function parameters count";                                       break;
      case 4051:  ErrorDescription = "Invalid function parameter value";                                        break;
      case 4052:  ErrorDescription = "String function internal error";                                          break;
      case 4053:  ErrorDescription = "Some array error";                                                        break;
      case 4054:  ErrorDescription = "Incorrect series array using";                                            break;
      case 4055:  ErrorDescription = "Custom indicator error";                                                  break;
      case 4056:  ErrorDescription = "Arrays are incompatible";                                                 break;
      case 4057:  ErrorDescription = "Global variables processing error";                                       break;
      case 4058:  ErrorDescription = "Global variable not found";                                               break;
      case 4059:  ErrorDescription = "Function is not allowed in testing mode";                                 break;
      case 4060:  ErrorDescription = "Function is not confirmed";                                               break;
      case 4061:  ErrorDescription = "Send mail error";                                                         break;
      case 4062:  ErrorDescription = "String parameter expected";                                               break;
      case 4063:  ErrorDescription = "Integer parameter expected";                                              break;
      case 4064:  ErrorDescription = "Double parameter expected";                                               break;
      case 4065:  ErrorDescription = "Array as parameter expected";                                             break;
      case 4066:  ErrorDescription = "Requested history data in updating state";                                break;
      case 4067:  ErrorDescription = "Some error in trading function";                                          break;
      case 4099:  ErrorDescription = "End of file";                                                             break;
      case 4100:  ErrorDescription = "Some file error";                                                         break;
      case 4101:  ErrorDescription = "Wrong file name";                                                         break;
      case 4102:  ErrorDescription = "Too many opened files";                                                   break;
      case 4103:  ErrorDescription = "Cannot open file";                                                        break;
      case 4104:  ErrorDescription = "Incompatible access to a file";                                           break;
      case 4105:  ErrorDescription = "No order selected";                                                       break;
      case 4106:  ErrorDescription = "Unknown symbol";                                                          break;
      case 4107:  ErrorDescription = "Invalid price";                                                           break;
      case 4108:  ErrorDescription = "Invalid ticket";                                                          break;
      case 4109:  ErrorDescription = "EA is not allowed to trade is not allowed. ";                             break;
      case 4110:  ErrorDescription = "Longs are not allowed. Check the expert properties";                      break;
      case 4111:  ErrorDescription = "Shorts are not allowed. Check the expert properties";                     break;
      case 4200:  ErrorDescription = "Object exists already";                                                   break;
      case 4201:  ErrorDescription = "Unknown object property";                                                 break;
      case 4202:  ErrorDescription = "Object does not exist";                                                   break;
      case 4203:  ErrorDescription = "Unknown object type";                                                     break;
      case 4204:  ErrorDescription = "No object name";                                                          break;
      case 4205:  ErrorDescription = "Object coordinates error";                                                break;
      case 4206:  ErrorDescription = "No specified subwindow";                                                  break;
      case 4207:  ErrorDescription = "Some error in object function";                                           break;
      default:    ErrorDescription = "No error or error is unknown";
     }
   return(ErrorDescription);
  }
//+------------------------------------------------------------------+
//| End of GetErrorDescription()                                         
//+------------------------------------------------------------------+


//+------------------------------------------------------------------+
//| Start of ChartSettings()                                         
//+------------------------------------------------------------------+
void ChartSettings()
  {
   ChartSetInteger(0,CHART_SHOW_GRID,0);
   ChartSetInteger(0,CHART_MODE,CHART_CANDLES);
   ChartSetInteger(0,CHART_AUTOSCROLL,0,True);
   WindowRedraw();
  }
//+------------------------------------------------------------------+
//| End of ChartSettings()                                         
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//| Start of CloseAllPositions()
//+------------------------------------------------------------------+ 
void CloseAllPositions()
  {
// Type: Fixed Template 
// Do not edit unless you know what you're doing

// This function closes all positions 

   CloseOrderPosition(OP_BUY,OnJournaling,MagicNumber,Slippage,P);

   CloseOrderPosition(OP_SELL,OnJournaling,MagicNumber,Slippage,P);

   return;

  }
//+------------------------------------------------------------------+
//| End of CloseAllPositions()
//+------------------------------------------------------------------+ 
//+------------------------------------------------------------------+
//|Start of DrawChartInfo                                                                  |
//+------------------------------------------------------------------+

void DrawChartInfo()
  {

   ObjectCreate(ChartID(),ObjName+"InfoBackground",OBJ_RECTANGLE_LABEL,0,0,0);
   ObjectSetInteger(ChartID(),ObjName+"InfoBackground",OBJPROP_XSIZE,-180);
   ObjectSetInteger(ChartID(),ObjName+"InfoBackground",OBJPROP_YSIZE,EntryMode==0?362:337);
   ObjectSetInteger(ChartID(),ObjName+"InfoBackground",OBJPROP_XDISTANCE,xDistance);
   ObjectSetInteger(ChartID(),ObjName+"InfoBackground",OBJPROP_YDISTANCE,20);
   ObjectSetInteger(ChartID(),ObjName+"InfoBackground",OBJPROP_CORNER,CORNER_RIGHT_UPPER);
   ObjectSetInteger(ChartID(),ObjName+"InfoBackground",OBJPROP_BGCOLOR,Black);
   ObjectSetInteger(ChartID(),ObjName+"InfoBackground",OBJPROP_BORDER_COLOR,Black);
   ObjectSetInteger(ChartID(),ObjName+"InfoBackground",OBJPROP_BACK,false);
   ObjectSetInteger(ChartID(),ObjName+"InfoBackground",OBJPROP_SELECTABLE,false);
   ObjectSetInteger(ChartID(),ObjName+"InfoBackground",OBJPROP_SELECTED,false);
   ObjectSetInteger(ChartID(),ObjName+"InfoBackground",OBJPROP_HIDDEN,false);
   ObjectSetInteger(ChartID(),ObjName+"InfoBackground",OBJPROP_BORDER_TYPE,BORDER_FLAT);

   ObjectCreate(ChartID(),ObjName+"HelmiFX2",OBJ_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"HelmiFX2",OBJPROP_TEXT,"HelmiFX");
   ObjectSetInteger(ChartID(),ObjName+"HelmiFX2",OBJPROP_CORNER,CORNER_RIGHT_UPPER);
   ObjectSetInteger(ChartID(),ObjName+"HelmiFX2",OBJPROP_XDISTANCE,xDistance);
   ObjectSetInteger(ChartID(),ObjName+"HelmiFX2",OBJPROP_YDISTANCE,37);
   ObjectSetString(ChartID(),ObjName+"HelmiFX2",OBJPROP_FONT,"Helvetica");
   ObjectSetInteger(ChartID(),ObjName+"HelmiFX2",OBJPROP_FONTSIZE,12);
   ObjectSetInteger(ChartID(),ObjName+"HelmiFX2",OBJPROP_ANCHOR,ANCHOR_RIGHT);
   ObjectSetInteger(ChartID(),ObjName+"HelmiFX2",OBJPROP_COLOR,White);

   ObjectCreate(ChartID(),ObjName+"Symbol",OBJ_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"Symbol",OBJPROP_TEXT,Symbol()+"  "+spreadValue);
   ObjectSetInteger(ChartID(),ObjName+"Symbol",OBJPROP_CORNER,CORNER_RIGHT_UPPER);
   ObjectSetInteger(ChartID(),ObjName+"Symbol",OBJPROP_XDISTANCE,xDistance);
   ObjectSetInteger(ChartID(),ObjName+"Symbol",OBJPROP_YDISTANCE,62);
   ObjectSetString(ChartID(),ObjName+"Symbol",OBJPROP_FONT,"Helvetica");
   ObjectSetInteger(ChartID(),ObjName+"Symbol",OBJPROP_FONTSIZE,12);
   ObjectSetInteger(ChartID(),ObjName+"Symbol",OBJPROP_ANCHOR,ANCHOR_RIGHT);
   ObjectSetInteger(ChartID(),ObjName+"Symbol",OBJPROP_COLOR,PanelLabelColor);

   ObjectCreate(ChartID(),ObjName+"BuyPipValue",OBJ_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"BuyPipValue",OBJPROP_TEXT,"Buy "+CountPosOrders(MagicNumber,OP_BUY)+" = "+DoubleToStr(buyCycleLots,2)+" L = "+DoubleToStr(buyPipValue,2)+"$");
   ObjectSetInteger(ChartID(),ObjName+"BuyPipValue",OBJPROP_CORNER,CORNER_RIGHT_UPPER);
   ObjectSetInteger(ChartID(),ObjName+"BuyPipValue",OBJPROP_XDISTANCE,xDistance);
   ObjectSetInteger(ChartID(),ObjName+"BuyPipValue",OBJPROP_YDISTANCE,87);
   ObjectSetString(ChartID(),ObjName+"BuyPipValue",OBJPROP_FONT,"Helvetica");
   ObjectSetInteger(ChartID(),ObjName+"BuyPipValue",OBJPROP_FONTSIZE,10);
   ObjectSetInteger(ChartID(),ObjName+"BuyPipValue",OBJPROP_ANCHOR,ANCHOR_RIGHT);
   ObjectSetInteger(ChartID(),ObjName+"BuyPipValue",OBJPROP_COLOR,PanelLabelColor);

   ObjectCreate(ChartID(),ObjName+"SellPipValue",OBJ_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"SellPipValue",OBJPROP_TEXT,"Sell "+CountPosOrders(MagicNumber,OP_SELL)+" = "+DoubleToStr(sellCycleLots,2)+" L = "+DoubleToStr(sellPipValue,2)+"$");
   ObjectSetInteger(ChartID(),ObjName+"SellPipValue",OBJPROP_CORNER,CORNER_RIGHT_UPPER);
   ObjectSetInteger(ChartID(),ObjName+"SellPipValue",OBJPROP_XDISTANCE,xDistance);
   ObjectSetInteger(ChartID(),ObjName+"SellPipValue",OBJPROP_YDISTANCE,112);
   ObjectSetString(ChartID(),ObjName+"SellPipValue",OBJPROP_FONT,"Helvetica");
   ObjectSetInteger(ChartID(),ObjName+"SellPipValue",OBJPROP_FONTSIZE,10);
   ObjectSetInteger(ChartID(),ObjName+"SellPipValue",OBJPROP_ANCHOR,ANCHOR_RIGHT);
   ObjectSetInteger(ChartID(),ObjName+"SellPipValue",OBJPROP_COLOR,PanelLabelColor);

   ObjectCreate(ChartID(),ObjName+"Equity",OBJ_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"Equity",OBJPROP_TEXT,"Equity : "+DoubleToStr(AccountEquity(),2));
   ObjectSetInteger(ChartID(),ObjName+"Equity",OBJPROP_CORNER,CORNER_RIGHT_UPPER);
   ObjectSetInteger(ChartID(),ObjName+"Equity",OBJPROP_XDISTANCE,xDistance);
   ObjectSetInteger(ChartID(),ObjName+"Equity",OBJPROP_YDISTANCE,137);
   ObjectSetString(ChartID(),ObjName+"Equity",OBJPROP_FONT,"Helvetica");
   ObjectSetInteger(ChartID(),ObjName+"Equity",OBJPROP_FONTSIZE,10);
   ObjectSetInteger(ChartID(),ObjName+"Equity",OBJPROP_ANCHOR,ANCHOR_RIGHT);
   ObjectSetInteger(ChartID(),ObjName+"Equity",OBJPROP_COLOR,PanelLabelColor);

   ObjectCreate(ChartID(),ObjName+"Balance",OBJ_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"Balance",OBJPROP_TEXT,"Balance : "+DoubleToStr(AccountBalance(),2));
   ObjectSetInteger(ChartID(),ObjName+"Balance",OBJPROP_CORNER,CORNER_RIGHT_UPPER);
   ObjectSetInteger(ChartID(),ObjName+"Balance",OBJPROP_XDISTANCE,xDistance);
   ObjectSetInteger(ChartID(),ObjName+"Balance",OBJPROP_YDISTANCE,162);
   ObjectSetString(ChartID(),ObjName+"Balance",OBJPROP_FONT,"Helvetica");
   ObjectSetInteger(ChartID(),ObjName+"Balance",OBJPROP_FONTSIZE,10);
   ObjectSetInteger(ChartID(),ObjName+"Balance",OBJPROP_ANCHOR,ANCHOR_RIGHT);
   ObjectSetInteger(ChartID(),ObjName+"Balance",OBJPROP_COLOR,PanelLabelColor);

   ObjectCreate(ChartID(),ObjName+"ClosedBuyPositionsProfit",OBJ_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"ClosedBuyPositionsProfit",OBJPROP_TEXT,"Closed Buy "+DoubleToStr(buyLastClosedProfit,2));
   ObjectSetInteger(ChartID(),ObjName+"ClosedBuyPositionsProfit",OBJPROP_CORNER,CORNER_RIGHT_UPPER);
   ObjectSetInteger(ChartID(),ObjName+"ClosedBuyPositionsProfit",OBJPROP_XDISTANCE,xDistance);
   ObjectSetInteger(ChartID(),ObjName+"ClosedBuyPositionsProfit",OBJPROP_YDISTANCE,187);
   ObjectSetString(ChartID(),ObjName+"ClosedBuyPositionsProfit",OBJPROP_FONT,"Helvetica");
   ObjectSetInteger(ChartID(),ObjName+"ClosedBuyPositionsProfit",OBJPROP_FONTSIZE,11);
   ObjectSetInteger(ChartID(),ObjName+"ClosedBuyPositionsProfit",OBJPROP_ANCHOR,ANCHOR_RIGHT);
   ObjectSetInteger(ChartID(),ObjName+"ClosedBuyPositionsProfit",OBJPROP_COLOR,PanelLabelColor);

   ObjectCreate(ChartID(),ObjName+"ClosedSellPositionsProfit",OBJ_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"ClosedSellPositionsProfit",OBJPROP_TEXT,"Closed Sell "+DoubleToStr(sellLastClosedProfit,2));
   ObjectSetInteger(ChartID(),ObjName+"ClosedSellPositionsProfit",OBJPROP_CORNER,CORNER_RIGHT_UPPER);
   ObjectSetInteger(ChartID(),ObjName+"ClosedSellPositionsProfit",OBJPROP_XDISTANCE,xDistance);
   ObjectSetInteger(ChartID(),ObjName+"ClosedSellPositionsProfit",OBJPROP_YDISTANCE,212);
   ObjectSetString(ChartID(),ObjName+"ClosedSellPositionsProfit",OBJPROP_FONT,"Helvetica");
   ObjectSetInteger(ChartID(),ObjName+"ClosedSellPositionsProfit",OBJPROP_FONTSIZE,11);
   ObjectSetInteger(ChartID(),ObjName+"ClosedSellPositionsProfit",OBJPROP_ANCHOR,ANCHOR_RIGHT);
   ObjectSetInteger(ChartID(),ObjName+"ClosedSellPositionsProfit",OBJPROP_COLOR,PanelLabelColor);

   ObjectCreate(ChartID(),ObjName+"CurrentBuyPositionsProfit",OBJ_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"CurrentBuyPositionsProfit",OBJPROP_TEXT,"Current Buy "+DoubleToStr(buyCycleProfit,2));
   ObjectSetInteger(ChartID(),ObjName+"CurrentBuyPositionsProfit",OBJPROP_CORNER,CORNER_RIGHT_UPPER);
   ObjectSetInteger(ChartID(),ObjName+"CurrentBuyPositionsProfit",OBJPROP_XDISTANCE,xDistance);
   ObjectSetInteger(ChartID(),ObjName+"CurrentBuyPositionsProfit",OBJPROP_YDISTANCE,237);
   ObjectSetString(ChartID(),ObjName+"CurrentBuyPositionsProfit",OBJPROP_FONT,"Helvetica");
   ObjectSetInteger(ChartID(),ObjName+"CurrentBuyPositionsProfit",OBJPROP_FONTSIZE,11);
   ObjectSetInteger(ChartID(),ObjName+"CurrentBuyPositionsProfit",OBJPROP_ANCHOR,ANCHOR_RIGHT);
   ObjectSetInteger(ChartID(),ObjName+"CurrentBuyPositionsProfit",OBJPROP_COLOR,PanelLabelColor);

   ObjectCreate(ChartID(),ObjName+"CurrentSellPositionsProfit",OBJ_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"CurrentSellPositionsProfit",OBJPROP_TEXT,"Current Sell "+DoubleToStr(sellCycleProfit,2));
   ObjectSetInteger(ChartID(),ObjName+"CurrentSellPositionsProfit",OBJPROP_CORNER,CORNER_RIGHT_UPPER);
   ObjectSetInteger(ChartID(),ObjName+"CurrentSellPositionsProfit",OBJPROP_XDISTANCE,xDistance);
   ObjectSetInteger(ChartID(),ObjName+"CurrentSellPositionsProfit",OBJPROP_YDISTANCE,262);
   ObjectSetString(ChartID(),ObjName+"CurrentSellPositionsProfit",OBJPROP_FONT,"Helvetica");
   ObjectSetInteger(ChartID(),ObjName+"CurrentSellPositionsProfit",OBJPROP_FONTSIZE,11);
   ObjectSetInteger(ChartID(),ObjName+"CurrentSellPositionsProfit",OBJPROP_ANCHOR,ANCHOR_RIGHT);
   ObjectSetInteger(ChartID(),ObjName+"CurrentSellPositionsProfit",OBJPROP_COLOR,PanelLabelColor);

   ObjectCreate(ChartID(),ObjName+"NetProfit",OBJ_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"NetProfit",OBJPROP_TEXT,"Net Profit "+DoubleToStr(netProfit,2));
   ObjectSetInteger(ChartID(),ObjName+"NetProfit",OBJPROP_CORNER,CORNER_RIGHT_UPPER);
   ObjectSetInteger(ChartID(),ObjName+"NetProfit",OBJPROP_XDISTANCE,xDistance);
   ObjectSetInteger(ChartID(),ObjName+"NetProfit",OBJPROP_YDISTANCE,287);
   ObjectSetString(ChartID(),ObjName+"NetProfit",OBJPROP_FONT,"Helvetica");
   ObjectSetInteger(ChartID(),ObjName+"NetProfit",OBJPROP_FONTSIZE,11);
   ObjectSetInteger(ChartID(),ObjName+"NetProfit",OBJPROP_ANCHOR,ANCHOR_RIGHT);
   ObjectSetInteger(ChartID(),ObjName+"NetProfit",OBJPROP_COLOR,PanelLabelColor);

   ObjectCreate(ChartID(),ObjName+"CycleProfit",OBJ_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"CycleProfit",OBJPROP_TEXT,"Cycle Profit "+DoubleToStr(cycleProfit,2));
   ObjectSetInteger(ChartID(),ObjName+"CycleProfit",OBJPROP_CORNER,CORNER_RIGHT_UPPER);
   ObjectSetInteger(ChartID(),ObjName+"CycleProfit",OBJPROP_XDISTANCE,xDistance);
   ObjectSetInteger(ChartID(),ObjName+"CycleProfit",OBJPROP_YDISTANCE,312);
   ObjectSetString(ChartID(),ObjName+"CycleProfit",OBJPROP_FONT,"Helvetica");
   ObjectSetInteger(ChartID(),ObjName+"CycleProfit",OBJPROP_FONTSIZE,11);
   ObjectSetInteger(ChartID(),ObjName+"CycleProfit",OBJPROP_ANCHOR,ANCHOR_RIGHT);
   ObjectSetInteger(ChartID(),ObjName+"CycleProfit",OBJPROP_COLOR,PanelLabelColor);

   ObjectCreate(ChartID(),ObjName+"ArrayMode",OBJ_LABEL,0,0,0);
   ObjectSetString(ChartID(),ObjName+"ArrayMode",OBJPROP_TEXT,"Next On: "+(ArrayMode==0?" StopLoss":" TakeProfit"));
   ObjectSetInteger(ChartID(),ObjName+"ArrayMode",OBJPROP_CORNER,CORNER_RIGHT_UPPER);
   ObjectSetInteger(ChartID(),ObjName+"ArrayMode",OBJPROP_XDISTANCE,xDistance);
   ObjectSetInteger(ChartID(),ObjName+"ArrayMode",OBJPROP_YDISTANCE,337);
   ObjectSetString(ChartID(),ObjName+"ArrayMode",OBJPROP_FONT,"Helvetica");
   ObjectSetInteger(ChartID(),ObjName+"ArrayMode",OBJPROP_FONTSIZE,11);
   ObjectSetInteger(ChartID(),ObjName+"ArrayMode",OBJPROP_ANCHOR,ANCHOR_RIGHT);
   ObjectSetInteger(ChartID(),ObjName+"ArrayMode",OBJPROP_COLOR,PanelLabelColor);

   if(EntryMode==0)
     {
      ObjectCreate(ChartID(),ObjName+"OrderNumber",OBJ_LABEL,0,0,0);
      ObjectSetString(ChartID(),ObjName+"OrderNumber",OBJPROP_TEXT,"OrderNumber : "+(OrdersArrayIndex+1));
      ObjectSetInteger(ChartID(),ObjName+"OrderNumber",OBJPROP_CORNER,CORNER_RIGHT_UPPER);
      ObjectSetInteger(ChartID(),ObjName+"OrderNumber",OBJPROP_XDISTANCE,xDistance);
      ObjectSetInteger(ChartID(),ObjName+"OrderNumber",OBJPROP_YDISTANCE,362);
      ObjectSetString(ChartID(),ObjName+"OrderNumber",OBJPROP_FONT,"Helvetica");
      ObjectSetInteger(ChartID(),ObjName+"OrderNumber",OBJPROP_FONTSIZE,11);
      ObjectSetInteger(ChartID(),ObjName+"OrderNumber",OBJPROP_ANCHOR,ANCHOR_RIGHT);
      ObjectSetInteger(ChartID(),ObjName+"OrderNumber",OBJPROP_COLOR,PanelLabelColor);
     }

  }
//+------------------------------------------------------------------+
// End of DrawChartInfo()                                          |
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
// Start of UpdateChartInfo()                                          |
//+------------------------------------------------------------------+
void UpdateChartInfo()
  {
   ObjectSetString(ChartID(),ObjName+"ClosedBuyPositionsProfit",OBJPROP_TEXT,"Closed Buy "+DoubleToStr(buyLastClosedProfit,2));
   ObjectSetString(ChartID(),ObjName+"ClosedSellPositionsProfit",OBJPROP_TEXT,"Closed Sell "+DoubleToStr(sellLastClosedProfit,2));
   ObjectSetString(ChartID(),ObjName+"CurrentBuyPositionsProfit",OBJPROP_TEXT,"Current Buy "+DoubleToStr(buyCycleProfit,2));
   ObjectSetString(ChartID(),ObjName+"CurrentSellPositionsProfit",OBJPROP_TEXT,"Current Sell "+DoubleToStr(sellCycleProfit,2));
   ObjectSetString(ChartID(),ObjName+"BuyPipValue",OBJPROP_TEXT,"Buy "+CountPosOrders(MagicNumber,OP_BUY)+" = "+DoubleToStr(buyCycleLots,2)+" L = "+DoubleToStr(buyPipValue,2)+"$");
   ObjectSetString(ChartID(),ObjName+"SellPipValue",OBJPROP_TEXT,"Sell "+CountPosOrders(MagicNumber,OP_SELL)+" = "+DoubleToStr(sellCycleLots,2)+" L = "+DoubleToStr(sellPipValue,2)+"$");
   ObjectSetString(ChartID(),ObjName+"NetProfit",OBJPROP_TEXT,"Net Profit "+DoubleToStr(netProfit,2));
   ObjectSetString(ChartID(),ObjName+"Equity",OBJPROP_TEXT,"Equity : "+DoubleToStr(AccountEquity(),2));
   ObjectSetString(ChartID(),ObjName+"Balance",OBJPROP_TEXT,"Balance : "+DoubleToStr(AccountBalance(),2));
   ObjectSetString(ChartID(),ObjName+"CycleProfit",OBJPROP_TEXT,"Cycle Profit "+DoubleToString(cycleProfit,2));
   ObjectSetString(ChartID(),ObjName+"Symbol",OBJPROP_TEXT,Symbol()+"  "+spreadValue);
   ObjectSetString(ChartID(),ObjName+"ArrayMode",OBJPROP_TEXT,"Next On: "+(ArrayMode==0?" StopLoss":" TakeProfit"));

   if(EntryMode==0)
      ObjectSetString(ChartID(),ObjName+"OrderNumber",OBJPROP_TEXT,"OrderNumber : "+(OrdersArrayIndex+1));

  }
//+------------------------------------------------------------------+
// End of UpdateChartInfo()                                          |
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//| Start of CheckSLTPExit()                                         |
//+------------------------------------------------------------------+

void CheckSLTPExit()
  {

   int orderType;
   double orderLot;
   double orderProfit;

   for(int i=Orders.Total()-1; i>=0; i--)
     {
      if(OrderSelect(Orders.At(i),SELECT_BY_TICKET,MODE_TRADES)==true && OrderSymbol()==Symbol() && (OrderMagicNumber()==MagicNumber))
        {
         if(OrderCloseTime()!=0)
           {

            orderType=OrderType();
            orderLot = OrderLots();
            orderProfit=OrderProfit();

            if(EntryMode==0) // Custom
              {
               if(orderProfit>=0) // Profit 
                 {
                  if(ArrayMode==0) // Next when StopLoss
                    {
                     OrdersArrayIndex=0;
                     EntryMarket();
                    }
                  else // Next when TakeProfit
                    {
                     if(IsRestartLimit && (OrdersArrayIndex+1%20)==RestartLimit)
                       {
                        OrdersArrayIndex=0;
                        EntryMarket();
                       }
                     else
                       {
                        OrdersArrayIndex=(OrdersArrayIndex+1)%20;
                        EntryMarket();
                       }
                    }
                 }
               else // Loss
                 {
                  if(ArrayMode==1) // Next when TakeProfit
                    {
                     OrdersArrayIndex=0;
                     EntryMarket();
                    }
                  else // Next when StopLoss
                    {
                     if(IsRestartLimit && (OrdersArrayIndex+1%20)==RestartLimit)
                       {
                        OrdersArrayIndex=0;
                        EntryMarket();
                       }
                     else
                       {
                        OrdersArrayIndex=(OrdersArrayIndex+1)%20;
                        EntryMarket();
                       }
                    }
                 }
              }
            else // Stoch
              {
               if(orderProfit>=0) // Profit 
                 {
                  if(ArrayMode==0) // Next when StopLoss
                    {
                     lastLot=StochLot;
                    }
                  else// Next when TakeProfit
                    {
                     lastLot*=LotMultiplier;
                    }
                 }
               else
                 {
                  if(ArrayMode==1) // Next when TakeProfit
                    {
                     lastLot=StochLot;
                    }
                  else// Next when StopLoss
                    {
                     lastLot*=LotMultiplier;
                    }
                 }

               canEnterStoch=true;
              }

            Orders.Delete(i);
           }
        }
     }

  }
//+------------------------------------------------------------------+
//| End of CheckSLTPExit()                                         |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//| Start of isNewBar()                                         
//+------------------------------------------------------------------+
bool isNewBar()
  {
//--- memorize the time of opening of the last bar in the static variable
   static datetime last_time=0;
//--- current time
   datetime lastbar_time=SeriesInfoInteger(Symbol(),Period(),SERIES_LASTBAR_DATE);
//--- if it is the first call of the function
   if(last_time==0)
     {
      //--- set the time and exit
      last_time=lastbar_time;
      return(false);
     }
//--- if the time differs
   if(last_time!=lastbar_time)
     {
      //--- memorize the time and return true
      last_time=lastbar_time;
      return(true);
     }
//--- if we passed to this line, then the bar is not new; return false
   return(false);
  }
//+------------------------------------------------------------------+
//| End of isNewBar()                                         
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
// Start of  SOCross()                            |
//+------------------------------------------------------------------+
int SOCross(double SOK,double SOD,int EntrySOTopLimit,int EntrySOBottomLimit)
  {
// Type: Customizable
// Do not edit unless you know what you're doing

// This function determines if a cross happened between 2 lines/data set here : FastEMA,SlowEMA
//----
   if(SOK>SOD && SOK<EntrySOBottomLimit)
      SOCurrentDirection=1;  // line1 above line2
   else if(SOK<SOD && SOK>EntrySOTopLimit)
      SOCurrentDirection=2;  // line1 below line2
   else
      SOCurrentDirection=0;

//----
   if(SOFirstTime==true) // Need to check if this is the first time the function is run
     {
      SOFirstTime=false; // Change variable to false
      SOLastDirection=SOCurrentDirection; // Set new direction
      return (0);
     }
   if(SOCurrentDirection!=SOLastDirection && SOFirstTime==false) // If not the first time and there is a direction change
     {
      SOLastDirection=SOCurrentDirection; // Set new direction
      return(SOCurrentDirection); // 1 for up, 2 for down
     }
   else
     {
      return(0);  // No direction change
     }
  }
//+------------------------------------------------------------------+
// End of  SOCross()                            |
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//| Start of InitializeVariables                                                                 |
//+------------------------------------------------------------------+

void InitializeVariables()
  {
   if(EntryMode==1) // Stoch
     {
      SOK_1=iStochastic(Symbol(),SOTimeFrame,KPeriod,DPeriod,Slowing,SOMAMethod,0,MODE_MAIN,0);
      SOD_1=iStochastic(Symbol(),SOTimeFrame,KPeriod,DPeriod,Slowing,SOMAMethod,0,MODE_SIGNAL,0);
      SOCrossTriggered=SOCross(SOK_1,SOD_1,TopLevel,BottomLevel);
      SODirection=SOCurrentDirection;
     }
   else if(EntryMode==0) // Custom
     {
      OrdersArrayType[0]=OrderType_1;
      OrdersArray[0][0] =OrderLot_1;
      OrdersArray[0][1] =OrderSL_1;
      OrdersArray[0][2] =OrderTP_1;

      OrdersArrayType[1]=OrderType_2;
      OrdersArray[1][0]=OrderLot_2;
      OrdersArray[1][1] =OrderSL_2;
      OrdersArray[1][2] =OrderTP_2;

      OrdersArrayType[2]=OrderType_3;
      OrdersArray[2][0] =OrderLot_3;
      OrdersArray[2][1] =OrderSL_3;
      OrdersArray[2][2] =OrderTP_3;

      OrdersArrayType[3]=OrderType_4;
      OrdersArray[3][0] =OrderLot_4;
      OrdersArray[3][1] =OrderSL_4;
      OrdersArray[3][2] =OrderTP_4;

      OrdersArrayType[4]=OrderType_5;
      OrdersArray[4][0] =OrderLot_5;
      OrdersArray[4][1] =OrderSL_5;
      OrdersArray[4][2] =OrderTP_5;

      OrdersArrayType[5]=OrderType_6;
      OrdersArray[5][0] =OrderLot_6;
      OrdersArray[5][1] =OrderSL_6;
      OrdersArray[5][2] =OrderTP_6;

      OrdersArrayType[6]=OrderType_7;
      OrdersArray[6][0] =OrderLot_7;
      OrdersArray[6][1] =OrderSL_7;
      OrdersArray[6][2] =OrderTP_7;

      OrdersArrayType[7]=OrderType_8;
      OrdersArray[7][0] =OrderLot_8;
      OrdersArray[7][1] =OrderSL_8;
      OrdersArray[7][2] =OrderTP_8;

      OrdersArrayType[8]=OrderType_9;
      OrdersArray[8][0] =OrderLot_9;
      OrdersArray[8][1] =OrderSL_9;
      OrdersArray[8][2] =OrderTP_9;

      OrdersArrayType[9]=OrderType_10;
      OrdersArray[9][0] =OrderLot_10;
      OrdersArray[9][1] =OrderSL_10;
      OrdersArray[9][2] =OrderTP_10;

      OrdersArrayType[10]=OrderType_11;
      OrdersArray[10][0] =OrderLot_11;
      OrdersArray[10][1] =OrderSL_11;
      OrdersArray[10][2] =OrderTP_11;

      OrdersArrayType[11]=OrderType_12;
      OrdersArray[11][0] =OrderLot_12;
      OrdersArray[11][1] =OrderSL_12;
      OrdersArray[11][2] =OrderTP_12;

      OrdersArrayType[12]=OrderType_13;
      OrdersArray[12][0] =OrderLot_13;
      OrdersArray[12][1] =OrderSL_13;
      OrdersArray[12][2] =OrderTP_13;

      OrdersArrayType[13]=OrderType_14;
      OrdersArray[13][0] =OrderLot_14;
      OrdersArray[13][1] =OrderSL_14;
      OrdersArray[13][2] =OrderTP_14;

      OrdersArrayType[14]=OrderType_15;
      OrdersArray[14][0] =OrderLot_15;
      OrdersArray[14][1] =OrderSL_15;
      OrdersArray[14][2] =OrderTP_15;

      OrdersArrayType[15]=OrderType_16;
      OrdersArray[15][0] =OrderLot_16;
      OrdersArray[15][1] =OrderSL_16;
      OrdersArray[15][2] =OrderTP_16;

      OrdersArrayType[16]=OrderType_17;
      OrdersArray[16][0] =OrderLot_17;
      OrdersArray[16][1] =OrderSL_17;
      OrdersArray[16][2] =OrderTP_17;

      OrdersArrayType[17]=OrderType_18;
      OrdersArray[17][0] =OrderLot_18;
      OrdersArray[17][1] =OrderSL_18;
      OrdersArray[17][2] =OrderTP_18;

      OrdersArrayType[18]=OrderType_19;
      OrdersArray[18][0] =OrderLot_19;
      OrdersArray[18][1] =OrderSL_19;
      OrdersArray[18][2] =OrderTP_19;

      OrdersArrayType[19]=OrderType_20;
      OrdersArray[19][0] =OrderLot_20;
      OrdersArray[19][1] =OrderSL_20;
      OrdersArray[19][2] =OrderTP_20;

     }
  }
//+------------------------------------------------------------------+
//| End of InitializeVariables                                                                 |
//+------------------------------------------------------------------+
//|                                                                 |

bool NoOrdersOnline()
  {
   return CountPosOrders(MagicNumber,OP_BUY)+CountPosOrders(MagicNumber,OP_SELL)==0;
  }
//+------------------------------------------------------------------+
// ************************* Commented
////+------------------------------------------------------------------+
////| Start of UpdateTrailingList()                              
////+------------------------------------------------------------------+
//
//void UpdateTrailingList(bool Journaling,int Retry_Interval,int Magic)
//  {
//// Type: Fixed Template 
//// Do not edit unless you know what you're doing
//
//// This function clears the elements of your VolTrailingList if the corresponding positions has been closed
//
//   int ordersPos=OrdersTotal();
//   int orderTicketNumber;
//   bool doesPosExist;
//
//// Check the VolTrailingList, match with current list of positions. Make sure the all the positions exists. 
//// If it doesn't, it means there are positions that have been closed
//
//   for(int x=0; x<ArrayRange(TrailingStopList,0); x++)
//     { // Looping through all order number in list
//
//      doesPosExist=False;
//      orderTicketNumber=TrailingStopList[x,0];
//
//      if(orderTicketNumber!=0)
//        { // Order exists
//         for(int y=ordersPos-1; y>=0; y--)
//           { // Looping through all current open positions
//            if(OrderSelect(y,SELECT_BY_POS,MODE_TRADES)==true && OrderSymbol()==Symbol() && OrderMagicNumber()==Magic)
//              {
//               if(orderTicketNumber==OrderTicket())
//                 { // Checks order number in list against order number of current positions
//                  doesPosExist=True;
//                  break;
//                 }
//              }
//           }
//
//         if(doesPosExist==False)
//           { // Deletes elements if the order number does not match any current positions
//            TrailingStopList[x,0] = 0;
//            TrailingStopList[x,1] = 0;
//           }
//        }
//     }
//  }
////+------------------------------------------------------------------+
////| End of UpdateTrailingList                                        
////+------------------------------------------------------------------+
//
//
//
//void ReviewTrailingStop(bool Journaling,double TrailingStop_Offset,int Retry_Interval,int Magic,int K)
//  {
//// Type: Fixed Template 
//// Do not edit unless you know what you're doing 
//
//// This function updates volatility trailing stops levels for all positions (using OrderModify) if appropriate conditions are met
//   double Spread=Ask-Bid;
//   bool doesTrailingRecordExist;
//   int posTicketNumber;
//   for(int i=OrdersTotal()-1; i>=0; i--)
//     { // Looping through all orders
//
//      if(OrderSelect(i,SELECT_BY_POS,MODE_TRADES)==true && OrderSymbol()==Symbol() && OrderMagicNumber()==Magic)
//        {
//         doesTrailingRecordExist=False;
//         posTicketNumber=OrderTicket();
//         for(int x=0; x<ArrayRange(TrailingStopList,0); x++)
//           { // Looping through all order number in list 
//
//            if(posTicketNumber==TrailingStopList[x,0])
//              { // If condition holds, it means the position have a volatility trailing stop level attached to it
//
//               doesTrailingRecordExist=True;
//               bool Modify=false;
//               RefreshRates();
//
//               // We update the volatility trailing stop record using OrderModify.
//               if(OrderType()==OP_BUY && (Bid-TrailingStopList[x,1]>(TrailingStop_Offset*K*Point)+Spread))
//                 {
//                  if(TrailingStopList[x,1]!=Bid-(TrailingStop_Offset*K*Point))
//                    {
//                     // if(Journaling)Print("EA Journaling: Trying to modify order "+OrderTicket()+" ...");
//                     HandleTradingEnvironment(Journaling,Retry_Interval);
//                     Modify=OrderModify(OrderTicket(),OrderOpenPrice(),TrailingStopList[x,1],OrderTakeProfit(),0,CLR_NONE);
//                     if(Journaling && !Modify)Print("EA Journaling: Unexpected Error has happened. Error Description: "+GetErrorDescription(GetLastError()));
//                     if(Journaling && Modify) Print("EA Journaling: Order successfully modified, trailing stop changed.");
//
//                     TrailingStopList[x,1]=Bid-(TrailingStop_Offset*K*Point);
//                    }
//                 }
//               if(OrderType()==OP_SELL && ((TrailingStopList[x,1]-Ask>(TrailingStop_Offset*K*Point)+Spread)))
//                 {
//                  if(TrailingStopList[x,1]!=Ask+(TrailingStop_Offset*K*Point))
//                    {
//                     //if(Journaling)Print("EA Journaling: Trying to modify order "+OrderTicket()+" ...");
//                     HandleTradingEnvironment(Journaling,Retry_Interval);
//                     Modify=OrderModify(OrderTicket(),OrderOpenPrice(),TrailingStopList[x,1],OrderTakeProfit(),0,CLR_NONE);
//                     if(Journaling && !Modify)Print("EA Journaling: Unexpected Error has happened. Error Description: "+GetErrorDescription(GetLastError()));
//                     if(Journaling && Modify) Print("EA Journaling: Order successfully modified, trailing stop changed.");
//                     TrailingStopList[x,1]=Ask+(TrailingStop_Offset*K*Point);
//                    }
//                 }
//               break;
//              }
//           }
//         // If order does not have a record attached to it. Alert the trader.
//         //if(!doesTrailingRecordExist && Journaling) Print("EA Journaling: Error. Order "+posTicketNumber+" has no volatility trailing stop attached to it.");
//        }
//     }
//  }
////+------------------------------------------------------------------+
////| End of Review Volatility Trailing Stop
////+------------------------------------------------------------------+
////+------------------------------------------------------------------+
////| Start of SetTrailingStop
////+------------------------------------------------------------------+
//
//void SetTrailingStop(bool Journaling,int Retry_Interval,int Magic,int K,int OrderNum)
//  {
//// Type: Fixed Template 
//// Do not edit unless you know what you're doing 
//
//// This function adds new volatility trailing stop level using OrderModify()
//   double trailingStopLossLimit=0;
//   bool Modify=False;
//   bool IsTrailingStopAdded=False;
//   if(OrderSelect(OrderNum,SELECT_BY_TICKET)==true && OrderSymbol()==Symbol() && OrderMagicNumber()==Magic)
//     {
//      RefreshRates();
//      if(OrderType()==OP_BUY || OrderType()==OP_BUYSTOP)
//        {
//         trailingStopLossLimit=OrderOpenPrice();// virtual stop loss.
//         IsTrailingStopAdded=True;
//        }
//      if(OrderType()==OP_SELL || OrderType()==OP_SELLSTOP)
//        {
//         trailingStopLossLimit=OrderOpenPrice();// virtual stop loss.
//         IsTrailingStopAdded=True;
//        }
//      // Records trailingStopLossLimit for future use
//      if(IsTrailingStopAdded==True)
//        {
//         for(int x=0; x<ArrayRange(TrailingStopList,0); x++) // Loop through elements in VolTrailingList
//           {
//            if(TrailingStopList[x,0]==0) // Checks if the element is empty
//              {
//               TrailingStopList[x,0]=OrderNum; // Add order number
//               TrailingStopList[x,1]=trailingStopLossLimit; // Add Trailing Stop into the List
//               Modify=true;
//               if(Journaling && Modify) Print("Trailing Stop For "+OrderNum+" Has been Set successfully to "+TrailingStopOffset);
//               break;
//              }
//           }
//        }
//     }
//
//   if(Journaling && !Modify) Print("Couldnt set Trailing Stop For "+OrderNum+" !");
//  }
////+------------------------------------------------------------------+
////| End of SetTrailingStop
////+------------------------------------------------------------------+
//
////+------------------------------------------------------------------+
////| Start of UpdateTrailingList()                              
////+------------------------------------------------------------------+
//
//void UpdateBreakevenList(bool Journaling,int Retry_Interval,int Magic)
//  {
//// Type: Fixed Template 
//// Do not edit unless you know what you're doing
//
//// This function clears the elements of your BreakevenList if the corresponding positions has been closed
//
//   int ordersPos=OrdersTotal();
//   int orderTicketNumber;
//   bool doesPosExist;
//
//// Check the VolTrailingList, match with current list of positions. Make sure the all the positions exists. 
//// If it doesn't, it means there are positions that have been closed
//
//   for(int x=0; x<ArrayRange(BreakevenList,0); x++)
//      //+------------------------------------------------------------------+
//      //|                                                                  |
//      //+------------------------------------------------------------------+
//     { // Looping through all order number in list
//
//      doesPosExist=False;
//      orderTicketNumber=BreakevenList[x,0];
//      //+------------------------------------------------------------------+
//      //|                                                                  |
//      //+------------------------------------------------------------------+
//      if(orderTicketNumber!=0)
//        { // Order exists
//         for(int y=ordersPos-1; y>=0; y--)
//           { // Looping through all current open positions
//            if(OrderSelect(y,SELECT_BY_POS,MODE_TRADES)==true && OrderSymbol()==Symbol() && OrderMagicNumber()==Magic)
//              {
//               if(orderTicketNumber==OrderTicket())
//                 { // Checks order number in list against order number of current positions
//                  doesPosExist=True;
//                  break;
//                 }
//              }
//           }
//
//         if(doesPosExist==False)
//           { // Deletes elements if the order number does not match any current positions
//            BreakevenList[x,0] = 0;
//            BreakevenList[x,1] = 0;
//           }
//        }
//     }
//  }
////+------------------------------------------------------------------+
////| End of UpdateTrailingList                                        
////+------------------------------------------------------------------+
//
//
//
//void ReviewBreakeven(bool Journaling,double Breakeven_Offset,int Retry_Interval,int Magic,int K)
//  {
//// Type: Fixed Template 
//// Do not edit unless you know what you're doing 
//
//// This function updates volatility trailing stops levels for all positions (using OrderModify) if appropriate conditions are met
//   double Spread=Ask-Bid;
//   bool doesBreakevenRecordExist;
//   int posTicketNumber;
//   for(int i=OrdersTotal()-1; i>=0; i--)
//      //+------------------------------------------------------------------+
//      //|                                                                  |
//      //+------------------------------------------------------------------+
//     { // Looping through all orders
//
//      if(OrderSelect(i,SELECT_BY_POS,MODE_TRADES)==true && OrderSymbol()==Symbol() && OrderMagicNumber()==Magic)
//        {
//         doesBreakevenRecordExist=False;
//         posTicketNumber=OrderTicket();
//         for(int x=0; x<ArrayRange(BreakevenList,0); x++)
//           { // Looping through all order number in list 
//
//            if(posTicketNumber==BreakevenList[x,0])
//              { // If condition holds, it means the position have a volatility trailing stop level attached to it
//
//               doesBreakevenRecordExist=True;
//               bool Modify=false;
//               RefreshRates();
//
//               // We update the volatility trailing stop record using OrderModify.
//               if(OrderType()==OP_BUY && (Bid-BreakevenList[x,1]>(Breakeven_Offset*K*Point)+Spread) && BreakevenList[x,1]!=-1)
//                 {
//                  // if(Journaling)Print("EA Journaling: Trying to modify order "+OrderTicket()+" ...");
//                  HandleTradingEnvironment(Journaling,Retry_Interval);
//                  Modify=OrderModify(OrderTicket(),OrderOpenPrice(),OrderOpenPrice()+Spread,OrderTakeProfit(),0,CLR_NONE);
//                  if(Journaling && !Modify)Print("EA Journaling: Unexpected Error has happened. Error Description: "+GetErrorDescription(GetLastError()));
//                  if(Journaling && Modify) Print("EA Journaling: Order successfully modified, Breakeven changed.");
//
//                  BreakevenList[x,1]=-1;
//
//                 }
//               if(OrderType()==OP_SELL && ((BreakevenList[x,1]-Ask>(Breakeven_Offset*K*Point)+Spread)) && BreakevenList[x,1]!=-1)
//                 {
//
//                  //if(Journaling)Print("EA Journaling: Trying to modify order "+OrderTicket()+" ...");
//                  HandleTradingEnvironment(Journaling,Retry_Interval);
//                  Modify=OrderModify(OrderTicket(),OrderOpenPrice(),OrderOpenPrice()-Spread,OrderTakeProfit(),0,CLR_NONE);
//                  if(Journaling && !Modify)Print("EA Journaling: Unexpected Error has happened. Error Description: "+GetErrorDescription(GetLastError()));
//                  if(Journaling && Modify) Print("EA Journaling: Order successfully modified, Breakeven changed.");
//                  BreakevenList[x,1]=-1;
//
//                 }
//               break;
//              }
//           }
//
//        }
//     }
//  }
////+------------------------------------------------------------------+
////|                                                                  |
////+------------------------------------------------------------------+
////+------------------------------------------------------------------+
////| End of Review Volatility Trailing Stop
////+------------------------------------------------------------------+
////+------------------------------------------------------------------+
////| Start of SetBreakeven
////+------------------------------------------------------------------+
//
//void SetBreakeven(bool Journaling,int Retry_Interval,int Magic,int K,int OrderNum)
//  {
//// Type: Fixed Template 
//// Do not edit unless you know what you're doing 
//
//// This function adds new volatility trailing stop level using OrderModify()
//   double BreakevenLimit=0;
//   bool Modify=False;
//   bool IsBreakevenAdded=False;
////+------------------------------------------------------------------+
////|                                                                  |
////+------------------------------------------------------------------+
//   if(OrderSelect(OrderNum,SELECT_BY_TICKET)==true && OrderSymbol()==Symbol() && OrderMagicNumber()==Magic)
//     {
//      RefreshRates();
//      //+------------------------------------------------------------------+
//      //|                                                                  |
//      //+------------------------------------------------------------------+
//      if(OrderType()==OP_BUY || OrderType()==OP_BUYSTOP)
//        {
//         BreakevenLimit=OrderOpenPrice();// virtual stop loss.
//         IsBreakevenAdded=True;
//        }
//      //+------------------------------------------------------------------+
//      //|                                                                  |
//      //+------------------------------------------------------------------+
//      if(OrderType()==OP_SELL || OrderType()==OP_SELLSTOP)
//        {
//         BreakevenLimit=OrderOpenPrice();// virtual stop loss.
//         IsBreakevenAdded=True;
//        }
//      //+------------------------------------------------------------------+
//      //|                                                                  |
//      //+------------------------------------------------------------------+
//      // Records trailingStopLossLimit for future use
//      if(IsBreakevenAdded==True)
//        {
//         for(int x=0; x<ArrayRange(BreakevenList,0); x++) // Loop through elements in VolTrailingList
//           {
//            if(BreakevenList[x,0]==0) // Checks if the element is empty
//              {
//               BreakevenList[x,0]=OrderNum; // Add order number
//               BreakevenList[x,1]=BreakevenLimit; // Add Trailing Stop into the List
//               Modify=true;
//               if(Journaling && Modify) Print("Breakeven For "+OrderNum+" Has been Set successfully to "+BreakevenOffset);
//               break;
//              }
//           }
//        }
//     }
//
//   if(Journaling && !Modify) Print("Couldnt set Breakeven For "+OrderNum+" !");
//  }
////+------------------------------------------------------------------+
////|                                                                  |
////+------------------------------------------------------------------+
